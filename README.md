# JAVA技术栈学习
## JAVA基础
> 一些基础但是比较重要的内容

|日期|DEMO|描述|
|:---|:---|:---|
|20240404|[00-base-reflex](00-base%2F00-base-reflex)|反射机制|
|20240404|[00-base-dynamic-compilation](00-base%2F00-base-dynamic-compilation)|动态编译|
|20240404|[00-base-script-engine](00-base%2F00-base-script-engine)|脚本引擎|
|20240404|[00-base-bytecode-operation-javaassist](00-base%2F00-base-bytecode-operation-javaassist)|javaassist操作字节码|
|20240404|[00-base-annotation](00-base%2F00-base-annotation)|注解及反射解析注解|
|20240405|[00-base-jvm-class-loading](00-base%2F00-base-jvm-class-loading)|JVM类加载机制及自定义类加载器|

## XML
> 被设计出来用于数据的记录和传递，经常被作用为配置文件

|日期|DEMO|描述|
|:---|:---|:---|
|20240331|[01-xml-01-know](01-xml%2F01-xml-01-know)|认识XML|
|20240331|[01-xml-02-verify-dtd](01-xml%2F01-xml-02-verify-dtd)|XML DTD定义文档结构|
|20240331|[01-xml-03-verify-xsd](01-xml%2F01-xml-03-verify-xsd)|XML Schema（未完成）|
|20240331|[01-xml-04-xml-handle](01-xml%2F01-xml-04-xml-handle)|XML 读写操作|

## JDK新特性
> 记录JDK的一些新特性

|日期|DEMO|描述|
|:---|:---|:---|
|20240401|[02-new-feature-generic-paradigm](02-new-feature%2F02-new-feature-generic-paradigm)|泛型|
|20240401|[02-new-feature-lambda](02-new-feature%2F02-new-feature-lambda)|Lambda表达式|
|20240401|[02-new-feature-interface](02-new-feature%2F02-new-feature-interface)|接口新特性|
|20240403|[02-new-feature-fun-interface](02-new-feature%2F02-new-feature-fun-interface)|函数式接口|
|20240403|[02-new-feature-stream](02-new-feature%2F02-new-feature-stream)|Stream流|
|20240405|[02-new-feature-try-with-resources](02-new-feature%2F02-new-feature-try-with-resources)|try-with-resources自动管理资源关闭|

## 设计模式
> JAVA常用设计模式

|日期|DEMO|描述|
|:---|:---|:---|
|20240405|[03-gof23-01-singleton-pattern](03-gof23%2F03-gof23-01-singleton-pattern)|单例模式|
|20240406|[03-gof23-02-factory-mode](03-gof23%2F03-gof23-02-factory-mode)|工厂模式|
|20240407|[03-gof23-03-builder-pattern](03-gof23%2F03-gof23-03-builder-pattern)|建造者模式|
|20240408|[03-gof23-04-prototype-pattern](03-gof23%2F03-gof23-04-prototype-pattern)|原型模式|
|20240408|[03-gof23-05-adapter-mode](03-gof23%2F03-gof23-05-adapter-mode)|适配器模式|
|20240409|[03-gof23-06-proxy-pattern](03-gof23%2F03-gof23-06-proxy-pattern)|代理模式|
|20240410|[03-gof23-07-bridge-mode](03-gof23%2F03-gof23-07-bridge-mode)|桥接模式|
|20240410|[03-gof23-08-composite-pattern](03-gof23%2F03-gof23-08-composite-pattern)|组合模式|
|20240410|[03-gof23-09-decorator-pattern](03-gof23%2F03-gof23-09-decorator-pattern)|装饰模式|
|20240411|[03-gof23-10-facade-pattern](03-gof23%2F03-gof23-10-facade-pattern)|外观模式|
|20240411|[03-gof23-11-flyweight-pattern](03-gof23%2F03-gof23-11-flyweight-pattern)|享元模式|
|20240413|[03-gof23-12-chain-of-responsibility-pattern](03-gof23%2F03-gof23-12-chain-of-responsibility-pattern)|责任链模式|
|20240416|[03-gof23-13-iterator-pattern](03-gof23%2F03-gof23-13-iterator-pattern)|迭代器模式|
|20240417|[03-gof23-14-mediator-pattern](03-gof23%2F03-gof23-14-mediator-pattern)|中介者模式|
|20240422|[03-gof23-15-command-pattern](03-gof23%2F03-gof23-15-command-pattern)|命令模式|
|20240424|[03-gof23-16-interpreter-pattern](03-gof23%2F03-gof23-16-interpreter-pattern)|解析器模式|
|20240424|[03-gof23-17-visitor-pattern](03-gof23%2F03-gof23-17-visitor-pattern)|访问者模式|
|20240425|[03-gof23-18-stratege-pattern](03-gof23%2F03-gof23-18-stratege-pattern)|策略模式|
|20240425|[03-gof23-19-template-method-pattern](03-gof23%2F03-gof23-19-template-method-pattern)|模板方法模式|
|20240425|[03-gof23-20-state-pattern](03-gof23%2F03-gof23-20-state-pattern)|状态模式|
|20240425|[03-gof23-21-observer-pattern](03-gof23%2F03-gof23-21-observer-pattern)|观察者模式|
|20240425|[03-gof23-22-memento-pattern](03-gof23%2F03-gof23-22-memento-pattern)|备忘录模式|

## Servlet

| 日期       | DEMO                                                        | 描述          |
|:---------|:------------------------------------------------------------|:------------|
| 20250226 | 05-Servlet/[01-servlet-base](05-Servlet%2F01-servlet-base)  | servlet基础   |
| 20250226 | 05-Servlet/[02-servlet-request-response](05-Servlet%2F02-servlet-request-response)  | servlet请求与响应 |
| 20250305 | 05-Servlet/[03-servlet-context](05-Servlet%2F03-servlet-context) | ServletContext介绍 |
| 20250310 | 05-Servlet/[04-servlet-forward-redirection](05-Servlet%2F04-servlet-forward-redirection) | Servlet转发与重定向 |
| 20250312 | 05-Servlet/[05-servlet-cookie-session](05-Servlet%2F05-servlet-cookie-session) | Servlet会话技术 |
| 20250317 | 05-Servlet/[06-servlet-jsp-el-jstl](05-Servlet%2F06-servlet-jsp-el-jstl) | JSP技术及EL表达式及JSTL表达式介绍 |


## Spring
### Spring Framework
> Spring 基础

|日期|DEMO|描述|
|:---|:---|:---|
|20240510|01-Spring-framework/[01-ioc-di](04-Spring%2F01-Spring-framework%2F01-ioc-di)|Spring IoC 详解|
|20250215|01-Spring-framework/[02-Spring-AOP](04-Spring%2F01-Spring-framework%2F02-Spring-AOP)|Spring AOP 详解|
|20250217|01-Spring-framework/[03-spring-tx](04-Spring%2F01-Spring-framework%2F03-spring-tx)|Spring事务详解|

### Spring Data JPA
> Spring Data JPA

|日期|DEMO|描述|
|:---|:---|:---|
|20250220|02-Spring-Data-JPA/[01-hibernate-jpa-demo](04-Spring%2F02-Spring-Data-JPA%2F01-hibernate-jpa-demo)| 基于hibernate实现的的JPA案例|
|20250220|02-Spring-Data-JPA/[02-spring-data-jpa-demo](04-Spring%2F02-Spring-Data-JPA%2F02-spring-data-jpa-demo)|基于spring整合Hibernate及Spring Data JPA的基础测试|
|20250220|02-Spring-Data-JPA/[03-springboot-hibernate-jpa-demo](04-Spring%2F02-Spring-Data-JPA%2F03-springboot-hibernate-jpa-demo)|基于springboot整合Hibernate及Spring Data JPA的基础测试|
|20250220|02-Spring-Data-JPA/[04-JpaRepository-method-demo](04-Spring%2F02-Spring-Data-JPA%2F04-JpaRepository-method-demo)|JpaRepository接口方法的使用|
|20250221|02-Spring-Data-JPA/[05-JpaSpecificationExecutor-method-demo](04-Spring%2F02-Spring-Data-JPA%2F05-JpaSpecificationExecutor-method-demo)|JpaSpecificationExecutor接口方法的使用|
|20250224|02-Spring-Data-JPA/[06-jpql-or-sql-demo](04-Spring%2F02-Spring-Data-JPA%2F06-jpql-or-sql-demo)|使用JPQL或者SQL的方式查询|

## 中间件

### Tomcat

| 日期       |DEMO| 描述       |
|:---------|:---|:---------|
| 20250225 |80-middleware/01-tomcat/[01-base](80-middleware%2F01-tomcat%2F01-base)| Tomcat基础 |


## 工具

### 自定义通用工具

| 日期       | DEMO                                    | 描述  |
|:---------|:------------------------------------------|:----|
| 20250313 | 01-common-utils/[common-utils](90-resources%2F01-common-utils%2Fcommon-utils)/captcha | 验证码 |
