import com.demo.dao.AccountRepository;
import com.demo.entity.AccountEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * 测试类
 *
 * @author Anna.
 * @date 2025/2/19 16:08
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-data-jpa.xml"})
public class SpringDataJpaTest {

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void test(){
        List<AccountEntity> all = accountRepository.findAll();
        for (AccountEntity account : all) {
            System.out.println(account);
        }
    }
}
