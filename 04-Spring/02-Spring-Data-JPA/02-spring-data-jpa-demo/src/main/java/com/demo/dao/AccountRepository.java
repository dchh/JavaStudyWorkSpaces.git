package com.demo.dao;

import com.demo.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * Account DAO 接口
 *   JpaRepository封装了基本查询方法，JpaSpecificationExecutor封装了复杂查询的方法
 * @author Anna.
 * @date 2025/2/19 9:39
 */
@Repository
public interface AccountRepository extends JpaRepository<AccountEntity,Long>, JpaSpecificationExecutor<AccountEntity> {
}
