# Spring Data JPA

## 什么是Spring Data JPA简介
Spring Data JPA是Spring框架下的一个模块，是<font color="red">基于JPA规范的上层封装，旨在简化JPA的使用</font>。
Spring Data JPA提供了一些常用的接口，如JpaRepository、JpaSpecificationExecutor等，这些接口包含了很多常用的CRUD操作方法，可直接继承使用。
同时，Spring Data JPA还提供了基于方法命名规范的查询方式，可以根据方法名自动生成相应的SQL语句，并执行查询操作。这种方式可以大大减少编写SQL语句的工作量。

**Spring Data JPA的优点:**
+ 简化了数据访问层代码的开发。 
+ 提供了丰富的CRUD操作方法和自定义查询方式。 
+ 支持排序、分页、事务管理等高级功能。 
+ 与Spring框架无缝集成，易于使用和维护。

**Spring Data JPA的缺点:**
+ 对于复杂的查询和关联关系处理可能不如原生SQL灵活。 
+ 在某些情况下，性能可能不如直接使用原生SQL优化后的查询。

**适合使用Spring Data JPA的项目类型:**
+ 需要快速开发数据访问层的应用。 
+ 对数据库操作有较高抽象需求的应用。 
+ 基于Spring框架开发的应用。

## 基于spring整合Hibernate及Spring Data JPA的基础测试

**MAVEN依赖：**
pom.xml
```xml
<dependencies>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-context</artifactId>
        <version>5.3.22</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-tx</artifactId>
        <version>5.3.22</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-aop</artifactId>
        <version>5.3.22</version>
    </dependency>
    <dependency>
        <groupId>org.aspectj</groupId>
        <artifactId>aspectjweaver</artifactId>
        <version>1.9.7</version>
    </dependency>
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>5.1.46</version>
    </dependency>
    <dependency>
        <groupId>org.hibernate</groupId>
        <artifactId>hibernate-core</artifactId>
        <version>5.6.8.Final</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.data</groupId>
        <artifactId>spring-data-jpa</artifactId>
        <version>2.6.4</version>
    </dependency>
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.13.2</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-test</artifactId>
        <version>5.3.22</version>
        <scope>test</scope>
    </dependency>
</dependencies>
```

**DB:**
account.sql
```sql
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `account`;
CREATE TABLE `account`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id主键',
  `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `money` float NULL DEFAULT NULL COMMENT '金额',
  `createDate` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`, `account`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

```
**DAO:**
AccountRepository.java
```java
package com.demo.dao;

import com.demo.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Account DAO 接口
 *   JpaRepository封装了基本查询方法，JpaSpecificationExecutor封装了复杂查询的方法
 * @author Anna.
 * @date 2025/2/19 9:39
 */
public interface AccountRepository extends JpaRepository<AccountEntity,Long>, JpaSpecificationExecutor<AccountEntity> {
}
```
**ENTITY:**
AccountEntity.java
```java
package com.demo.entity;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "account")
public class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "account")
    private String account;

    @Column(name = "name")
    private String name;

    @Column(name = "money")
    private Float money;

    @Column(name = "createDate")
    private Date createDate;

    // GET / SET
}

```

**配置文件：**
spring-data-jpa.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xmlns:jpa="http://www.springframework.org/schema/data/jpa"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd
                           http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop.xsd
                           http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd
                           http://www.springframework.org/schema/data/jpa  http://www.springframework.org/schema/data/jpa/spring-jpa.xsd
">

    <!-- 配置MySQL数据源 -->
    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql:///testdb"/>
        <property name="username" value="root"/>
        <property name="password" value="root"/>
    </bean>

    <!-- 创建entityManagerFactory对象交给spring容器管理 -->
    <bean id="entityManagerFactory" class="org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean">
        <!-- 连接数据源-->
        <property name="dataSource" ref="dataSource"></property>
        <!-- 配置JPA的实现厂家 hibernate-->
        <property name="persistenceProvider">
            <bean class="org.hibernate.jpa.HibernatePersistenceProvider"></bean>
        </property>
        <!-- 配置扫描的包（实体类所在包）-->
        <property name="packagesToScan" value="com.demo.entity"></property>
        <!-- 配置JPA的供应商适配器-->
        <property name="jpaVendorAdapter">
            <!-- 配置Hibernate适配器 -->
            <bean class="org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter">
                <!-- 配置是否自动创建数据库表 -->
                <property name="generateDdl" value="false"></property>
                <!-- 指定数据类型-->
                <property name="database" value="MYSQL"></property>
                <!-- 数据库方言：支持特有的语法-->
                <property name="databasePlatform" value="org.hibernate.dialect.MySQLDialect"></property>
                <!-- 配置是否显示执行SQL-->
                <property name="showSql" value="true"></property>
            </bean>
        </property>
        <!-- 配置jpa的方言高级特性-->
        <property name="jpaDialect">
            <bean class="org.springframework.orm.jpa.vendor.HibernateJpaDialect"></bean>
        </property>
    </bean>

    <!-- spring 整合 spring data jpa -->
    <jpa:repositories base-package="com.demo.dao" transaction-manager-ref="transactionManager"
                      entity-manager-factory-ref="entityManagerFactory"></jpa:repositories>

    <!-- 配置事务管理器-->
    <bean id="transactionManager" class="org.springframework.orm.jpa.JpaTransactionManager">
        <property name="entityManagerFactory" ref="entityManagerFactory"></property>
    </bean>

    <!-- 4.配置事务通知-->
    <tx:advice id="txAdvice" transaction-manager="transactionManager">
        <tx:attributes>
            <tx:method name="save*" propagation="REQUIRED"/>
            <tx:method name="insert*" propagation="REQUIRED"/>
            <tx:method name="update*" propagation="REQUIRED"/>
            <tx:method name="delete*" propagation="REQUIRED"/>
            <tx:method name="get*" read-only="true"/>
            <tx:method name="find*" read-only="true"/>
            <tx:method name="*" propagation="REQUIRED"/>
        </tx:attributes>
    </tx:advice>

    <!-- 5.配置AOP-->
    <aop:config>
        <aop:advisor advice-ref="txAdvice" pointcut="execution(* com.demo.service..*(..))" />
    </aop:config>
    
    <!-- 6. 配置包扫描-->
    <context:component-scan base-package="com.demo" ></context:component-scan>
</beans>
```

**测试代码：**
SpringDataJpaTest.java
```java
import com.demo.dao.AccountRepository;
import com.demo.entity.AccountEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-data-jpa.xml"})
public class SpringDataJpaTest {

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void test(){
        List<AccountEntity> all = accountRepository.findAll();
        for (AccountEntity account : all) {
            System.out.println(account);
        }
    }
}

```

**执行结果：**

![img_3.png](images/img_3.png)

## Spring Data JPA中常见的XML配置标签及其子标签

| 主标签 | 子标签 | 属性 | 默认值 | 可选值 | 作用 |
| :---: | :---: | :---: | :---: | :---: | :---: |
| `<jpa:repositories>` | - | `base-package` | 无 | String（包路径） | 指定Spring Data JPA仓库接口所在的包路径 |
| - | - | `entity-manager-factory-ref` | 无 | String（bean名称） | 指定实体管理器工厂的bean名称 |
| - | - | `transaction-manager-ref` | 无 | String（bean名称） | 指定事务管理器的bean名称 |
| - | - | `repository-impl-postfix` | 无 | String（后缀） | 指定仓库实现类的后缀 |
| `<jpa:entity-manager-factory>` | - | `persistence-unit-name` | 无 | String（持久化单元名称） | 指定持久化单元的名称 |
| - | - | `data-source` | 无 | String（bean名称） | 指定数据源bean的名称 |
| - | - | `packages-to-scan` | 无 | String（包路径，逗号分隔） | 指定要扫描的实体类所在的包路径 |
| - | - | `jpa-vendor-adapter` | 无 | bean名称或类名 | 指定JPA供应商的适配器 |
| - | - | `jpa-properties` | 无 | Map（键值对） | 指定JPA属性 |
| `<jpa:vendor-adapter>` | - | `database` | 无 | DB2, DERBY, H2, HSQL, INFORMIX, MYSQL, ORACLE, POSTGRESQL, SQL_SERVER, SYBASE | 指定数据库类型 |
| - | - | `database-platform` | 无 | String（方言类名） | 指定Hibernate方言 |
| - | - | `show-sql` | false | true, false | 是否在控制台显示SQL语句 |
| - | - | `generate-ddl` | false | true, false | 是否自动生成DDL语句 |



## gitee源码
> [git clone https://gitee.com/anna_spaces/JavaStudyWorkSpaces.git](https://gitee.com/anna_spaces/JavaStudyWorkSpaces.git)