# 基于hibernate实现的的JPA案例

## 什么是JPA
Java Persistence API（JPA）是Java规范，用于访问、管理和持久化Java类或对象与关系数据库之间的数据。
JPA是EJB 3.0的一部分，<font color="red">它不是一个实现或产品，而只是一个规范，包含需要实现的一组接口</font>。
**JPA为JPA实现提供了额外的抽象层，使得Java开发人员可以在没有具体SQL编程经验的情况下，通过简单的注解配置实现对数据的访问和操作。**
例如：Hibernate、TopLink等实现了JPA规范的框架。

![img.png](images/img.png)

## JPA的JPA介绍

###  核心接口与类

| **接口与类** | **用途** | **作用** |
| --- | --- | --- |
| **EntityManagerFactory** | 创建EntityManager实例的工厂 | 配置和管理JPA的持久化上下文，创建EntityManager实例，供整个应用程序共享 |
| **EntityManager** | 执行CRUD操作和查询 | 管理实体对象与数据库之间的映射关系，执行创建、读取、更新和删除操作，以及执行查询 |
| **EntityTransaction** | 管理事务 | 控制事务的开始、提交和回滚，确保数据的一致性和完整性 |

```
EntityManagerFactory factory = Persistence.createEntityManagerFactory("hibeinageJpa");
// 创建实体管理类
EntityManager entityManager = factory.createEntityManager();
// 获取事务对象
EntityTransaction transaction = entityManager.getTransaction();
```

### 常用注解

| 注解 | 作用 | 使用范围 | 默认值 | 可选值及其含义 | 简化案例 |
| --- | --- | --- | --- | --- | --- |
| @Entity | 标记一个类为JPA实体 | 类定义 | 无 | - | `@Entity public class User { ... }` |
| @Table | 指定实体类对应的数据库表信息 | 类定义 | 表名与实体类名一致 | name: 表名；catalog: Catalog名称；schema: Schema名称 | `@Table(name = "user_table") public class User { ... }` |
| @Id | 标记实体类的主键属性 | 属性或getter方法 | 无 | - | `@Id private Long id;` |
| @GeneratedValue | 指定主键生成策略 | 主键属性 | AUTO | IDENTITY(自增长，适用于Mysql), SEQUENCE(序列，适用于ORACLE), TABLE(中间表), AUTO（让ORM框架自动选择） | `@GeneratedValue(strategy = GenerationType.IDENTITY) private Long id;` |
| @Column | 描述实体类属性与数据库表列之间的映射关系 | 属性 | 列名与属性名一致，允许为NULL | name: 列名；nullable: 是否允许为NULL；unique: 是否唯一；length: 字段大小（仅对String有效）；columnDefinition: 数据库中的实际类型 | `@Column(name = "user_name", nullable = false) private String name;` |
| @Basic | 表示一个简单的属性到数据库表的字段的映射 | 属性 | fetch: EAGER；optional: true | fetch: EAGER（立即加载）或LAZY（延迟加载）；optional: 是否允许为null | `@Basic(fetch = FetchType.LAZY) private String description;` |
| @Transient | 表示该属性并非一个到数据库表的字段的映射 | 属性 | 无 | - | `@Transient private int age;` |
| @ManyToOne | 表示一个多对一的映射关系 | 属性 | fetch: EAGER；optional: true | fetch: EAGER或LAZY；optional: 是否允许为null；cascade: 级联操作策略 | `@ManyToOne private Group group;` |
| @OneToMany | 表示一个一对多的映射关系 | 属性 | fetch: LAZY；cascade: 无 | fetch: EAGER或LAZY；cascade: 级联操作策略 | `@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL) private List<Order> orders;` |
| @OneToOne | 表示一个一对一的映射关系 | 属性 | fetch: EAGER；optional: true | fetch: EAGER或LAZY；optional: 是否允许为null；cascade: 级联操作策略 | `@OneToOne private Address address;` |
| @NamedQuery | 定义命名查询 | 类定义 | 无 | name: 查询名称；query: JPQL查询语句 | `@NamedQuery(name = "User.findAll", query = "SELECT u FROM User u")` |
| @Transactional | 指定一个方法或类需要进行事务管理 | 方法或类定义 | 无 | propagation, isolation, timeout, readOnly, rollbackFor, noRollbackFor等 | `@Transactional public void updateUser(User user) { ... }` |
| @Access | 指定访问实体属性的方式 | 类定义 | FIELD | PROPERTY | `@Entity @Access(AccessType.PROPERTY) public class User { ... }` |
| @BatchSize | 指定批量操作的大小，优化性能 | 类定义 | 无 | size: 批量大小 | `@Entity @BatchSize(size = 20) public class User { ... }` |
| @Cache | 配置缓存策略 | 类或集合定义 | 无 | usage: 缓存策略（如READ_WRITE, NONSTRICT_READ_WRITE, READ_ONLY等） | `@Entity @Cache(usage = CacheConcurrencyStrategy.READ_WRITE) public class User { ... }` |
| @DynamicInsert | 仅插入变更的字段 | 类定义 | false | - | `@Entity @DynamicInsert public class User { ... }` |
| @DynamicUpdate | 仅更新变更的字段 | 类定义 | false | - | `@Entity @DynamicUpdate public class User { ... }` |

## 基于hibernate实现的的JPA案例

DB:
account.sql
```sql
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account`  (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id主键',
    `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
    `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
    `money` float NULL DEFAULT NULL COMMENT '金额',
    `createDate` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
```
**ENTITY:**
AccountEntity.java
```java
package com.demo.entity;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "account")
public class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "account")
    private String account;

    @Column(name = "name")
    private String name;

    @Column(name = "money")
    private Float money;

    @Column(name = "createDate")
    private Date createDate;

    // GET / SET
}
```

**配置文件：**
META-INF/persistence.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<persistence xmlns="http://java.sun.com/xml/ns/persistence"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://java.sun.com/xml/ns/persistence
    http://java.sun.com/xml/ns/persistence/persistence_2_0.xsd"
             version="2.0">
    <!--配置持久化单元
        name：持久化单元名称
        transaction-type：事务类型
             RESOURCE_LOCAL：本地事务管理
             JTA：分布式事务管理 -->
    <persistence-unit name="hibeinageJpa" transaction-type="RESOURCE_LOCAL">
        <!--配置JPA规范的服务提供商 -->
        <provider>org.hibernate.jpa.HibernatePersistenceProvider</provider>
        <properties>
            <!-- 数据库驱动 -->
            <property name="javax.persistence.jdbc.driver" value="com.mysql.jdbc.Driver" />
            <!-- 数据库地址 -->
            <property name="javax.persistence.jdbc.url" value="jdbc:mysql:///testdb" />
            <!-- 数据库用户名 -->
            <property name="javax.persistence.jdbc.user" value="root" />
            <!-- 数据库密码 -->
            <property name="javax.persistence.jdbc.password" value="root" />

            <!--jpa提供者的可选配置：我们的JPA规范的提供者为hibernate，所以jpa的核心配置中兼容hibernate的配置 -->
            <!-- 是否显示执行sql -->
            <property name="hibernate.show_sql" value="true" />
            <!-- 测试设置 每次启动Hibernate时创建表，如果表已存在则先删除 -->
            <property name="hibernate.hbm2ddl.auto" value="create" />
        </properties>
    </persistence-unit>
</persistence>
```

> 这里为什么将persistence.xml存放在META-INF目录下：测试使用`Persistence.createEntityManagerFactory("hibeinageJpa");`方法，默认读取该目录下文件。

![img_1.png](images/img_1.png)

> JPA持久化单元，相关标签及其子标签的作用
以下是一个关于XML配置Hibernate JPA持久化单元中相关标签及其子标签的详细表格，包括它们的作用、使用范围、默认值、可选值及其含义：

| 标签/子标签 | 作用 | 使用范围 | 默认值 | 可选值及其含义 |
| --- | --- | --- | --- | --- |
| `<persistence>` | 根元素，包含JPA配置信息 | 必须 | 无 | 无 |
| `<persistence-unit>` | 定义持久化单元，包含实体类与数据库之间的映射以及持久化操作的相关配置 | 必须 | 无 | 无 |
| `- name` | 指定持久化单元的名称 | 必须 | 无 | 用户自定义 |
| `- transaction-type` | 指定持久化单元的事务类型 | 可选 | 根据配置的数据源类型决定（jta-data-source时为JTA，non-jta-data-source时为RESOURCE_LOCAL） | JTA：全局事务<br>RESOURCE_LOCAL：本地事务 |
| `- provider` | 指定特定JPA实现的全限定类名 | 可选 | 使用找到的第一个JPA实现 | 例如：org.hibernate.ejb.HibernatePersistence（Hibernate实现） |
| `- jta-data-source` | 指定用于JTA事务的数据源的JNDI名称 | 可选 | 无 | 数据源的JNDI名称 |
| `- non-jta-data-source` | 指定用于RESOURCE_LOCAL事务的数据源的JNDI名称 | 可选 | 无 | 数据源的JNDI名称 |
| `- mapping-file` | 指定额外的ORM映射文件 | 可选 | 无 | 映射文件的路径 |
| `- jar-file` | 指定包含持久化类的JAR文件 | 可选 | 无 | JAR文件的路径 |
| `- class` | 指定持久化单元中的实体类 | 可选 | 无 | 实体类的全限定名 |
| `- exclude-unlisted-classes>` | 指定是否排除未列出的类 | 可选 | false | true：排除未列出的类<br>false：不排除（默认） |
| `- properties` | 包含配置特定JPA供应商的相关属性 | 可选 | 无 | 无 |
| `-- javax.persistence.jdbc.driver` | 指定数据库驱动类名 | 根据数据库类型配置 | 无 | 数据库驱动类名（例如：com.mysql.jdbc.Driver） |
| `-- javax.persistence.jdbc.url` | 指定数据库连接URL | 根据数据库类型配置 | 无 | 数据库连接URL（例如：jdbc:mysql://127.0.0.1:3306/test） |
| `-- javax.persistence.jdbc.user` | 指定数据库用户名 | 根据数据库配置 | 无 | 数据库用户名 |
| `-- javax.persistence.jdbc.password` | 指定数据库密码 | 根据数据库配置 | 无 | 数据库密码 |
| `-- 其他JPA实现特定属性` | 根据JPA实现提供的特定属性进行配置 | 可选 | 无 | 根据JPA实现文档进行配置 |

> Hibernate的各种属性详解,这使用了JPA因此不再配置Hibernate的数据库连接配置，而是引入Hibernate的一些其他属性配置。

| 属性名称 | 使用范围 | 默认值 | 可选值及其含义 |
| --- | --- | --- | --- |
| **hibernate.connection.driver_class** | 数据库连接配置 | 无 | 数据库驱动类名，例如`com.mysql.cj.jdbc.Driver`（MySQL） |
| **hibernate.connection.url** | 数据库连接配置 | 无 | 数据库连接URL，例如`jdbc:mysql://localhost:3306/mydatabase` |
| **hibernate.connection.username** | 数据库连接配置 | 无 | 数据库用户名 |
| **hibernate.connection.password** | 数据库连接配置 | 无 | 数据库密码 |
| **hibernate.dialect** | Hibernate方言配置 | 根据数据库自动选择 | 指定Hibernate使用的数据库方言，例如`org.hibernate.dialect.MySQL5Dialect`（MySQL 5） |
| **hibernate.hbm2ddl.auto** | 数据库表创建策略 | 无（通常需要明确配置） | <ul><li>`none`：不执行任何DDL语句</li><li>`create`：每次启动Hibernate时创建表，如果表已存在则先删除</li><li>`create-drop`：每次启动Hibernate时创建表，关闭时删除表</li><li>`update`：如果表不存在则创建，存在则更新表结构（推荐使用）</li><li>`validate`：验证数据库表结构是否与实体类映射一致，但不创建或更新表</li></ul> |
| **hibernate.connection.pool_size** | 连接池配置 | 通常由Hibernate或应用服务器管理 | 数据库连接池的大小，指定同时可以从连接池中获取的数据库连接数 |
| **hibernate.show_sql** | SQL输出配置 | `false` | `true`：在控制台输出Hibernate生成的SQL语句 |
| **hibernate.format_sql** | SQL格式化配置 | `false` | `true`：格式化Hibernate生成的SQL语句，使其更易读 |
| **hibernate.use_sql_comments** | SQL注释配置 | `false` | `true`：在生成的SQL语句中添加注释，有助于调试 |
| **hibernate.transaction.factory_class** | 事务策略配置 | 根据配置的事务管理器类型选择 | 指定Hibernate使用的事务工厂类，例如`org.springframework.orm.hibernate5.SpringTransactionFactory`（与Spring整合时） |
| **hibernate.current_session_context_class** | 当前会话上下文配置 | 根据应用程序类型选择 | 指定通过`getCurrentSession()`创建的会话的范围（上下文），例如`thread`（根据当前线程跟踪会话）或`jta`（根据JTA事务跟踪会话） |
| **hibernate.c3p0.min_size** | C3P0连接池配置（可选） | C3P0默认最小值 | C3P0连接池的最小连接数 |
| **hibernate.c3p0.max_size** | C3P0连接池配置（可选） | C3P0默认最大值 | C3P0连接池的最大连接数 |
| **hibernate.c3p0.timeout** | C3P0连接池配置（可选） | C3P0默认超时时间 | C3P0连接池的连接超时时间（毫秒） |

**测试代码：**
HibernateJpaTest.java
```java
import com.demo.entity.AccountEntity;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class HibernateJpaTest {

    @Test
    public void test(){
        // 创建实体管理类工厂，借助Persistence的静态方法获取（其中传递的参数为持久化单元名称，需要jpa配置文件中指定）
        // 该方法默认解析META-INF/persistence.xml文件
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("hibeinageJpa");
        // 创建实体管理类
        EntityManager entityManager = factory.createEntityManager();
        // 获取事务对象
        EntityTransaction transaction = entityManager.getTransaction();
        // 开启事务
        transaction.begin();

        AccountEntity account =new AccountEntity();
        account.setAccount("wangwu");

        // 保存操作
        entityManager.persist(account);
        // 提交事务
        transaction.commit();
        // 释放资源
        entityManager.close();
        factory.close();
    }
}

```
**执行结果：**

![img_2.png](images/img_2.png)

## gitee源码
> [git clone https://gitee.com/anna_spaces/JavaStudyWorkSpaces.git](https://gitee.com/anna_spaces/JavaStudyWorkSpaces.git)