import com.demo.entity.AccountEntity;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 * 测试类
 *
 * @author Anna.
 * @date 2025/2/19 10:22
 */
public class HibernateJpaTest {

    @Test
    public void test(){
        // 创建实体管理类工厂，借助Persistence的静态方法获取（其中传递的参数为持久化单元名称，需要jpa配置文件中指定）
        // 该方法默认解析META-INF/persistence.xml文件
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("hibeinageJpa");
        // 创建实体管理类
        EntityManager entityManager = factory.createEntityManager();
        // 获取事务对象
        EntityTransaction transaction = entityManager.getTransaction();
        // 开启事务
        transaction.begin();

        AccountEntity account =new AccountEntity();
        account.setAccount("wangwu");

        // 保存操作
        entityManager.persist(account);
        // 提交事务
        transaction.commit();
        // 释放资源
        entityManager.close();
        factory.close();
    }
}
