# 使用JPQL或者SQL的方式查询

使用Spring Data JPA提供的查询方法已经可以解决大部分的应用场景，但对于某些业务来说，通常我们还需要灵活的构建查询条件，
这时就可以使用`@Query`及`@Modifying`注解，结合JPQL或者SQL来完成相应的DML操作。

## 注解介绍

### @Param 注解

- **用途**：
    - 用于在`@Query`或`@Modifying`注解的查询字符串中绑定参数。它允许你将方法参数与查询中的命名参数对应起来。

- **属性**：
    - `value`：参数的名称，该名称应与查询字符串中的命名参数匹配。

### @Query 注解

- **用途**：
    - 用于在Spring Data JPA仓库接口中定义自定义的JPQL（Java Persistence Query Language）查询或原生SQL查询。

- **属性**：
    - `value`：查询字符串，可以是JPQL或原生SQL（当`nativeQuery=true`时）。
    - `nativeQuery`：布尔值，默认为`false`。当设置为`true`时，表示查询字符串是原生SQL。

### @Modifying 注解

- **用途**：
    - 用于标记执行更新或删除操作的JPQL或原生SQL查询。这些操作通常不会返回实体，而是返回受影响的行数。

- **注意事项**：
    - 与`@Query`注解一起使用。
    - 由于更新操作会修改数据库，因此通常需要在事务中执行。

## 使用SQL完成CRUD操作

**DAO:**
AccountRepository.java
```java
package com.demo.dao;

import com.demo.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;

public interface AccountRepository extends JpaRepository<AccountEntity,Long>, JpaSpecificationExecutor<AccountEntity> {

    /**
     * 新增
     * @param account
     * @param date
     * @param money
     * @param name
     */
    @Query(nativeQuery = true,value = "INSERT INTO `account`( `account`, `create_date`, `money`, `name`) VALUES (:account, :createDate, :money, :name)")
    @Modifying
    void insertBySql(@Param("account") String account, @Param("createDate") Date date, @Param("money") Float money, @Param("name") String name);

    /**
     * 修改
     * @param account
     * @param id
     */
    @Query(nativeQuery = true,value = "UPDATE `account` SET `account` = :account WHERE `id` = :id")
    @Modifying
    void updateBySql(@Param("account") String account, @Param("id") Long id);

    /**
     * 查询
     * @param id
     */
    @Query(nativeQuery = true, value = "SELECT * FROM account WHERE id = :id")
    AccountEntity enqrBySql(@Param("id") Long id);

    /**
     * 删除
     * @param id
     */
    @Query(nativeQuery = true, value = "DELETE FROM account WHERE id = :id")
    @Modifying
    void delBySql(@Param("id") Long id);
}

```

**测试代码：**
SqlDemoTest.java

1. 新增
```
@Test
@Transactional
public void testInsert(){
    accountRepository.insertBySql("zhangsan",new Date(System.currentTimeMillis()) , 1000F, "张三");
}
```

![img_1.png](images/img_1.png)

数据库：

![img_2.png](images/img_2.png)

> 对于新增/修改/删除操作，需要在事务（Transaction）环境中执行，因此这里添加了@Transactional注解，否则回报如下错误：

![img.png](images/img.png)

2. 修改
```
@Test
@Transactional
public void testUpdate(){
    accountRepository.updateBySql("zhangsan1",1L);
}
```

![img_3.png](images/img_3.png)

数据库：

![img_4.png](images/img_4.png)

3. 查询

```
@Test
public void testSelect(){
    AccountEntity account = accountRepository.enqrBySql(1L);
    System.out.println("执行结果：" + account);
}
```

![img_5.png](images/img_5.png)

4. 删除

```
@Test
@Transactional
public void testDel(){
    accountRepository.delBySql(1L);
}
```

![img_6.png](images/img_6.png)

数据库：

![img_7.png](images/img_7.png)

## 使用JPQL完成CRUD操作

### 什么是JPQL
JPQL，全称Java Persistence Query Language，即Java持久化查询语言。
它是基于首次在EJB 2.0中引入的EJB查询语言（EJB QL）发展而来的一种可移植的查询语言。
与Hibernate的HQL语法类似，都是旨在以面向对象表达式语言的表达式，将SQL语法和简单查询语义绑定在一起。

#### JPQL的基本语法

| JPQL组件 | 描述 | 示例 |
| --- | --- | --- |
| SELECT语句 | 用于从数据库中查询实体对象 | `SELECT u FROM User u` 表示查询所有User实体对象 |
| WHERE子句 | 用于指定查询条件 | `WHERE u.age > 20` 表示只查询年龄大于20的User实体对象 |
| ORDER BY子句 | 用于对查询结果进行排序 | `ORDER BY u.age DESC` 表示按照年龄降序排序 |
| GROUP BY子句 | 用于将查询结果按照指定属性进行分组 | `GROUP BY u.sex` 表示按照性别分组 |
| HAVING子句 | 通常与GROUP BY子句一起使用，用于对分组后的结果进行过滤 | `HAVING AVG(u.age) > 20` 表示只选择平均年龄大于20的分组 |
| UPDATE语句 | 用于更新数据库中的实体对象 | `UPDATE User u SET u.name='momor' WHERE u.name='bbb'` 表示将名字为“bbb”的User实体对象的名字更新为“momor” |
| DELETE语句 | 用于删除数据库中的实体对象<font color="red">（注意：DELETE在JPQL中不常用，因为通常通过EntityManager的remove方法删除实体，但理论上JPQL支持DELETE操作，只是实践中较少使用）</font> | `DELETE User u WHERE u.name='bbb'` |

> 在JPA（Java Persistence API）中，实际上并没有直接称为“JPQL INSERT”的语句。
> JPQL（Java Persistence Query Language）主要用于查询操作，它支持SELECT、UPDATE和DELETE语句，但不直接支持INSERT语句。
> INSERT操作在JPA中通常是通过EntityManager的persist方法来实现的.

> 对于查询JPQL通常期望的是以对象的形式返回，因此`SELECT u FROM User u`我们会简写为`FROM User u`，当然我们也可以自定义返回结果，详细见案例。

### 案例

**DAO:**
AccountRepository.java
```java
package com.demo.dao;

import com.demo.dto.RtnDto;
import com.demo.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;

public interface AccountRepository extends JpaRepository<AccountEntity,Long>, JpaSpecificationExecutor<AccountEntity> {

  /**
   * 修改
   * @param account
   * @param id
   */
  @Query(value = "UPDATE AccountEntity SET createDate = :createDate, account = :account WHERE id = :id")
  @Modifying
  void updateByJpql(@Param("account") String account, @Param("createDate") Date date, @Param("id") Long id);

  /**
   * 查询所有字段
   * @param id
   */
  @Query(value = "SELECT A.name,A.account FROM AccountEntity A WHERE A.id = :id")
  AccountEntity enqrAllColumnByJpql(@Param("id") Long id);

  /**
   * 查询一个字段
   * @param id
   * @return
   */
  @Query(value = "SELECT A.name,A.account FROM AccountEntity A WHERE A.id = :id")
  String enqrNameByJpql(@Param("id") Long id);

  /**
   * 查询多个字段方式1
   * @param id
   * @return
   */
  @Query(value = "SELECT A.name,A.account FROM AccountEntity A WHERE A.id = :id")
  List<Object[]> enqrMultipleColumnByJpql1(@Param("id") Long id);

  /**
   * 查询多个字段方式1
   * @param id
   * @return
   */
  @Query(value = "SELECT NEW com.demo.dto.RtnDto(A.name,A.account) FROM AccountEntity A WHERE A.id = :id")
  List<RtnDto> enqrMultipleColumnByJpql2(@Param("id") Long id);

  /**
   * 删除
   * @param id
   */
  @Query(value = "DELETE FROM AccountEntity WHERE id = :id")
  @Modifying
  void delByJpql(@Param("id") Long id);
}



```

**dto:**
RtnDto.java
```java
package com.demo.dto;

/**
 * 自定义返回结果
 *
 * @author Anna.
 * @date 2025/2/24 0:00
 */
public class RtnDto {
    private String name;
    private String account;
    
    public RtnDto(String name, String account) {
      this.name = name;
      this.account = account;
    }
    // GET / SET
}

```

> 创建实体的时候必须提供`NEW com.demo.dto.RtnDto(A.name,A.account)`相应构造方法,否则编译会报错

**测试代码：**
JpqlDemoTest.java

1. 修改

```
@Test
@Transactional
public void testUpdate(){
    accountRepository.updateByJpql("zhangsan3", new Date(System.currentTimeMillis()) ,1L);
}
```

![img_8.png](images/img_8.png)

数据库：

![img_9.png](images/img_9.png)

2. 查询所有属性

```
@Test
public void testSelect(){
    AccountEntity account = accountRepository.enqrByJpql(1L);
    System.out.println("执行结果：" + account);
}
```

![img_10.png](images/img_10.png)

3. 查询单个属性
```
@Test
public void testSelect2(){
    String name = accountRepository.enqrNameByJpql(1L);
    System.out.println("执行结果：" + name);
}
```

![img_11.png](images/img_11.png)

> 查询单个属性时，我们可以使用基本属性及String直接接收

4. 查询多个属性方式1
```
@Test
public void testSelect3(){
    List<Object[]> rtn = accountRepository.enqrMultipleColumnByJpql1(1L);
    for (Object[] result : rtn) {
        String name = (String) result[0];
        String account = (String) result[1];
        System.out.println("name: " + name + ", account: " + account);
    }
}
```

![img_12.png](images/img_12.png)

6. 查询多个属性方式2
```
@Test
public void testSelect4(){
    List<RtnDto> rtn = accountRepository.enqrMultipleColumnByJpql2(1L);
    System.out.println("执行结果：" + rtn);
}
```

![img_13.png](images/img_13.png)

> 需要注意的时，查询多个字段时，实体类必须与查询字段个数一一对应否则执行会报错。

8. 删除

```
@Test
@Transactional
public void testDel(){
    accountRepository.delByJpql(1L);
}
```

![img_14.png](images/img_14.png)

数据库：

![img_15.png](images/img_15.png)

## gitee源码
> [git clone https://gitee.com/anna_spaces/JavaStudyWorkSpaces.git](https://gitee.com/anna_spaces/JavaStudyWorkSpaces.git)


