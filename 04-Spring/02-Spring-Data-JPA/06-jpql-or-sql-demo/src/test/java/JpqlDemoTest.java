import com.demo.SpringBootApplication;
import com.demo.dao.AccountRepository;
import com.demo.dto.RtnDto;
import com.demo.entity.AccountEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

/**
 * 测试类
 *
 * @author Anna.
 * @date 2025/2/23 22:57
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {SpringBootApplication.class})
public class JpqlDemoTest {
    @Autowired
    private AccountRepository accountRepository;

  
    @Test
    @Transactional
    public void testUpdate(){
        accountRepository.updateByJpql("zhangsan3", new Date(System.currentTimeMillis()) ,1L);
    }

    @Test
    public void testSelect(){
        AccountEntity account = accountRepository.enqrAllColumnByJpql(1L);
        System.out.println("执行结果：" + account);
    }

    @Test
    public void testSelect2(){
        String name = accountRepository.enqrNameByJpql(1L);
        System.out.println("执行结果：" + name);
    }

    @Test
    public void testSelect3(){
        List<Object[]> rtn = accountRepository.enqrMultipleColumnByJpql1(1L);
        for (Object[] result : rtn) {
            String name = (String) result[0];
            String account = (String) result[1];
            System.out.println("name: " + name + ", account: " + account);
        }
    }

    @Test
    public void testSelect4(){
        List<RtnDto> rtn = accountRepository.enqrMultipleColumnByJpql2(1L);
        System.out.println("执行结果：" + rtn);
    }

    @Test
    @Transactional
    public void testDel(){
        accountRepository.delByJpql(1L);
    }
}
