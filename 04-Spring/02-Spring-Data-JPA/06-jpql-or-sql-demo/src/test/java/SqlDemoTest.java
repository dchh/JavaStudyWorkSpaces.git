import com.demo.SpringBootApplication;
import com.demo.dao.AccountRepository;
import com.demo.entity.AccountEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;

/**
 * 测试类
 *
 * @author Anna.
 * @date 2025/2/23 22:57
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {SpringBootApplication.class})
public class SqlDemoTest {
    @Autowired
    private AccountRepository accountRepository;

    @Test
    @Transactional
    public void testInsert(){
        accountRepository.insertBySql("zhangsan",new Date(System.currentTimeMillis()) , 1000F, "张三");
    }

    @Test
    @Transactional
    public void testUpdate(){
        accountRepository.updateBySql("zhangsan1",1L);
    }

    @Test
    public void testSelect(){
        AccountEntity account = accountRepository.enqrBySql(1L);
        System.out.println("执行结果：" + account);
    }

    @Test
    @Transactional
    public void testDel(){
        accountRepository.delBySql(1L);
    }
}
