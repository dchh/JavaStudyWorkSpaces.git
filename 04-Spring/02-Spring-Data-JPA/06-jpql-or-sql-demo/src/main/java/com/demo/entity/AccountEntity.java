package com.demo.entity;

import javax.persistence.*;
import java.sql.Date;

/**
 * 实体类
 *
 * @author Anna.
 * @date 2025/2/19 9:00
 */
@Entity
@Table(name = "account")
public class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "account")
    private String account;

    @Column(name = "name")
    private String name;

    @Column(name = "money")
    private Float money;

    @Column(name = "createDate")
    private Date createDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getMoney() {
        return money;
    }

    public void setMoney(Float money) {
        this.money = money;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public AccountEntity() {
    }

    public AccountEntity(Long id) {
        this.id = id;
    }

    public AccountEntity(String account, String name) {
        this.account = account;
        this.name = name;
        this.createDate = new Date(System.currentTimeMillis());
    }

    public AccountEntity(String account, String name, Float money) {
        this.account = account;
        this.name = name;
        this.money = money;
        this.createDate = new Date(System.currentTimeMillis());
    }

    public AccountEntity(Long id, String account, String name, Float money) {
        this.id = id;
        this.account = account;
        this.name = name;
        this.money = money;
        this.createDate = new Date(System.currentTimeMillis());
    }

    @Override
    public String toString() {
        return "AccountEntity{" +
                "id=" + id +
                ", account='" + account + '\'' +
                ", name='" + name + '\'' +
                ", money=" + money +
                ", createDate=" + createDate +
                '}';
    }
}
