package com.demo.dto;

/**
 * 自定义返回结果
 *
 * @author Anna.
 * @date 2025/2/24 0:00
 */
public class RtnDto {
    private String name;
    private String account;

    public RtnDto(String name, String account) {
        this.name = name;
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "RtnDto{" +
                "name='" + name + '\'' +
                ", account='" + account + '\'' +
                '}';
    }
}
