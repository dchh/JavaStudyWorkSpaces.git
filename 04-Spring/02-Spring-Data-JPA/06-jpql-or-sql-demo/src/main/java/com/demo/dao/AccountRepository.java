package com.demo.dao;

import com.demo.dto.RtnDto;
import com.demo.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;

/**
 * Account DAO 接口
 *   JpaRepository封装了基本查询方法，JpaSpecificationExecutor封装了复杂查询的方法
 * @author Anna.
 * @date 2025/2/19 9:39
 */
public interface AccountRepository extends JpaRepository<AccountEntity,Long>, JpaSpecificationExecutor<AccountEntity> {

    /**
     * 新增
     * @param account
     * @param date
     * @param money
     * @param name
     */
    @Query(nativeQuery = true,value = "INSERT INTO `account`( `account`, `create_date`, `money`, `name`) VALUES (:account, :createDate, :money, :name)")
    @Modifying
    void insertBySql(@Param("account") String account, @Param("createDate") Date date, @Param("money") Float money, @Param("name") String name);

    /**
     * 修改
     * @param account
     * @param id
     */
    @Query(nativeQuery = true,value = "UPDATE `account` SET `account` = :account WHERE `id` = :id")
    @Modifying
    void updateBySql(@Param("account") String account, @Param("id") Long id);

    /**
     * 查询
     * @param id
     */
    @Query(nativeQuery = true, value = "SELECT * FROM account WHERE id = :id")
    AccountEntity enqrBySql(@Param("id") Long id);

    /**
     * 删除
     * @param id
     */
    @Query(nativeQuery = true, value = "DELETE FROM account WHERE id = :id")
    @Modifying
    void delBySql(@Param("id") Long id);

    /**
     * 修改
     * @param account
     * @param id
     */
    @Query(value = "UPDATE AccountEntity SET createDate = :createDate, account = :account WHERE id = :id")
    @Modifying
    void updateByJpql(@Param("account") String account, @Param("createDate") Date date, @Param("id") Long id);

    /**
     * 查询所有字段
     * @param id
     */
    @Query(value = "SELECT A.name,A.account FROM AccountEntity A WHERE A.id = :id")
    AccountEntity enqrAllColumnByJpql(@Param("id") Long id);

    /**
     * 查询一个字段
     * @param id
     * @return
     */
    @Query(value = "SELECT A.name,A.account FROM AccountEntity A WHERE A.id = :id")
    String enqrNameByJpql(@Param("id") Long id);

    /**
     * 查询多个字段方式1
     * @param id
     * @return
     */
    @Query(value = "SELECT A.name,A.account FROM AccountEntity A WHERE A.id = :id")
    List<Object[]> enqrMultipleColumnByJpql1(@Param("id") Long id);

    /**
     * 查询多个字段方式1
     * @param id
     * @return
     */
    @Query(value = "SELECT NEW com.demo.dto.RtnDto(A.name,A.account) FROM AccountEntity A WHERE A.id = :id")
    List<RtnDto> enqrMultipleColumnByJpql2(@Param("id") Long id);

    /**
     * 删除
     * @param id
     */
    @Query(value = "DELETE FROM AccountEntity WHERE id = :id")
    @Modifying
    void delByJpql(@Param("id") Long id);
}
