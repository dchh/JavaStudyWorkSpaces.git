# Spring Data JPA中的查询方式

1. 使用Spring Data JPA中接口定义的方法进行查询
2. 使用JPQL的方式进行查询
3. 使用SQL语句查询

## JpaSpecificationExecutor接口方法使用

JpaSpecificationExecutor接口是Spring Data JPA提供的一个用于支持复杂查询的接口。它基于JPA Criteria API，允许开发者以类型安全的方式构建动态查询。适用于需要构建复杂查询的场景，如多条件查询、动态查询等。

### 注意事项
1. **性能问题：**
   在使用JpaSpecificationExecutor进行复杂查询时，需要注意性能问题。尽量避免在查询中使用过多的条件或复杂的连接操作，以免导致查询性能下降。

2. **安全性问题：**
   在构建查询条件时，需要注意防止SQL注入等安全问题。建议使用JPA Criteria API提供的条件构建方法，而不是直接拼接SQL语句。
3. **分页与排序：**
   JpaSpecificationExecutor接口提供了分页和排序的查询方法。在使用这些方法时，需要注意分页参数和排序参数的设置，以确保查询结果的准确性。

### 定义方法介绍

| 方法名称 | 作用 | 使用方法 | 注意事项 |
| --- | --- | --- | --- |
| findOne(Specification<T> spec) | 根据指定的查询条件（Specification）查找符合条件的单个实体 | 1. 创建Specification对象并定义查询条件。 2. 调用findOne方法并传入Specification对象。 | 1. 若查询结果为空，则返回Optional.empty()。 2. 注意处理Optional对象，避免NullPointerException。 |
| findAll(@Nullable Specification<T> spec) | 查询所有符合指定条件的实体，并返回一个List | 1. 创建Specification对象并定义查询条件。 2. 调用findAll方法并传入Specification对象。 | 1. 若无查询条件，可传入null。 2. 返回结果可能包含大量数据，注意内存使用。 |
| findAll(@Nullable Specification<T> spec, Pageable pageable) | 根据指定的查询条件和分页参数，查询符合条件的实体，并返回分页结果 | 1. 创建Specification对象并定义查询条件。 2. 创建Pageable对象并设置分页参数。 3. 调用findAll方法并传入Specification和Pageable对象。 | 1. Pageable对象包含分页信息，如页码、每页数量等。 2. 注意处理分页结果，如总页数、当前页码等。 |
| findAll(@Nullable Specification<T> spec, Sort sort) | 查询符合条件的实体，并按指定的排序参数返回结果 | 1. 创建Specification对象并定义查询条件。 2. 创建Sort对象并设置排序参数。 3. 调用findAll方法并传入Specification和Sort对象。 | 1. Sort对象包含排序信息，如排序字段、排序方向等。 2. 注意排序字段在实体类中的存在性和数据类型。 |
| count(@Nullable Specification<T> spec) | 计算符合指定条件的实体总数 | 1. 创建Specification对象并定义查询条件。 2. 调用count方法并传入Specification对象。 | 1. 若无查询条件，可传入null。 2. 返回结果为long类型，表示符合条件的实体总数。 |
| exists(@Nullable Specification<T> spec) | 检查是否存在符合条件的实体 | 1. 创建Specification对象并定义查询条件。 2. 调用exists方法并传入Specification对象。 | 1. 若存在至少一个符合条件的实体，则返回true；否则返回false。 2. 适用于快速验证是否存在符合特定条件的记录。 |

#### 什么是Specification

Specification是一个函数式接口，其toPredicate方法用于定义查询条件。在业务逻辑中，通常需要构建不同的Specification对象，并将其传递给JpaSpecificationExecutor的查询方法。

```
@Nullable
Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder);
```

+ **root：** 代表了可以查询和操作的实体对象的根。如果将实体对象比喻成数据库中的表，那么root里面就是这张表里面的字段。通过root，可以访问实体类的属性并构建查询条件。 
+ **query：** 代表一个具体的查询对象。它包含了查询的各个部分，如select、from、where等。然而，在直接使用Specification接口时，通常不需要直接操作query对象。查询的构建主要是在toPredicate方法中通过CriteriaBuilder完成的。 
+ **criteriaBuilder：** 用于构建CriteriaQuery的构建器对象。它提供了丰富的API来创建查询条件和表达式。通过criteriaBuilder，可以构建如等于、大于、小于、模糊匹配等查询条件。 
+ **返回值：** 返回一个Predicate对象，该对象表示了构建的查询条件。

**案例1：**
使用通过定义类实现Specification实现

```
// 定义一个实现Specification的内部内
class AccountSpecification implements Specification<AccountEntity>{
  private String name;

  public AccountSpecification() {
  }

  public AccountSpecification(String name) {
      this.name = name;
  }

  @Override
  public Predicate toPredicate(Root root, CriteriaQuery query, CriteriaBuilder criteriaBuilder) {
      if(name != null && !name.isEmpty()) {
          return criteriaBuilder.like(root.get("name"), "%" + name + "%");
      }
      // 返回一个总是为真的条件，表示没有限制 实际就是 查询条件 1 = 1
      return criteriaBuilder.conjunction();
  }
}

@Test
public void testFindAll(){
  List<AccountEntity> all = accountRepository.findAll(new AccountSpecification("张"));
  System.out.println(all);
}
```

![img.png](img.png)


**案例2：**
因为Specification是一个函数式接口，因此可以使用Lambda表达式实现

```
@Test
public void testFindAll2(){
  String name = "";
  List<AccountEntity> all = accountRepository.findAll(( root,  query, criteriaBuilder) -> {
      if(!name.isEmpty()) {
          return criteriaBuilder.like(root.get("name"), "%" + name + "%");
      }
      // 返回一个总是为真的条件，表示没有限制 实际就是 查询条件 1 = 1
      return criteriaBuilder.conjunction();
  });
  System.out.println(all);
}
```

![img_1.png](img_1.png)

#### CriteriaBuilder方法概述

CriteriaBuilder是JPA标准中的一个接口，用于构建查询条件。它提供了丰富的静态方法，用于创建各种查询条件，并支持动态构建查询，提高查询的灵活性和可维护性。

**方法列表及详细说明：**

| 方法名 | 作用 | 调用方式 | 案例 | 注意事项 |
| --- | --- | --- | --- | --- |
| between | 在两个值之间创建范围条件 | `criteriaBuilder.between(expression, lowerBound, upperBound)` | `query.add(Restrictions.between("age", 18, 30))` | 匹配值和范围需相同数据类型；范围是闭区间 |
| equal | 创建等于条件 | `criteriaBuilder.equal(expression, value)` | `Predicate predicate = criteriaBuilder.equal(root.get("id"), 1)` | 确保比较双方数据类型一致 |
| notEqual | 创建不等于条件 | `criteriaBuilder.notEqual(expression, value)` | - | 同上 |
| greaterThan | 创建大于条件 | `criteriaBuilder.greaterThan(expression, value)` | - | 同上 |
| greaterThanOrEqualTo | 创建大于等于条件 | `criteriaBuilder.greaterThanOrEqualTo(expression, value)` | - | 同上 |
| lessThan | 创建小于条件 | `criteriaBuilder.lessThan(expression, value)` | - | 同上 |
| lessThanOrEqualTo | 创建小于等于条件 | `criteriaBuilder.lessThanOrEqualTo(expression, value)` | - | 同上 |
| like | 创建模糊匹配条件 | `criteriaBuilder.like(expression, pattern)` | - | 使用通配符（%或_）进行模糊匹配 |
| notLike | 创建不匹配条件 | `criteriaBuilder.notLike(expression, pattern)` | - | 同上 |
| isNull | 创建为空条件 | `criteriaBuilder.isNull(expression)` | - | 检查字段是否为空 |
| isNotNull | 创建不为空条件 | `criteriaBuilder.isNotNull(expression)` | - | 检查字段是否不为空 |
| isEmpty | 检查集合属性是否为空 | `criteriaBuilder.isEmpty(collectionExpression)` | `criteriaQuery.where(criteriaBuilder.isEmpty(employee.get(Employee_.tasks)))` | 仅适用于集合属性 |
| isNotEmpty | 检查集合属性是否不为空 | `criteriaBuilder.isNotEmpty(collectionExpression)` | - | 同上 |
| and | 组合多个条件为与操作 | `criteriaBuilder.and(condition1, condition2, ...)` | `Predicate whereCondition = criteriaBuilder.and(cb.like(...), cb.between(...))` | 所有条件需同时满足 |
| or | 组合多个条件为或操作 | `criteriaBuilder.or(condition1, condition2, ...)` | - | 任意条件满足即可 |
| not | 对条件取反 | `criteriaBuilder.not(condition)` | - | 对给定条件取反 |
| createQuery | 创建SELECT查询 | `criteriaBuilder.createQuery(Class<T> resultClass)` | `CriteriaQuery<Employee> criteriaQuery = criteriaBuilder.createQuery(Employee.class)` | 指定查询返回类型 |
| createTupleQuery | 创建返回Tuple格式的SELECT查询 | `criteriaBuilder.createTupleQuery()` | - | 返回结果以Tuple形式表示 |
| createCriteriaDelete | 创建DELETE查询 | `criteriaBuilder.createCriteriaDelete(Class<T> targetEntity)` | - | 用于构建DELETE操作 |
| createCriteriaUpdate | 创建UPDATE查询 | `criteriaBuilder.createCriteriaUpdate(Class<T> targetEntity)` | - | 用于构建UPDATE操作 |
| literal | 返回指定的常量表达式 | `criteriaBuilder.literal(Object value)` | - | 用于在查询中插入常量值 |
| asc | 返回一个升序排列的表达式 | `criteriaBuilder.asc(expression)` | `query.orderBy(criteriaBuilder.asc(root.get("fieldName")))` | 用于指定升序排序 |
| desc | 返回一个降序排列的表达式 | `criteriaBuilder.desc(expression)` | `query.orderBy(criteriaBuilder.desc(root.get("fieldName")))` | 用于指定降序排序 |
| sum | 返回用于计算和的表达式 | `criteriaBuilder.sum(expression)` | `query.select(criteriaBuilder.sum(root.get("fieldName")))` | 用于计算字段值的和 |
| avg | 返回用于计算平均值的表达式 | `criteriaBuilder.avg(expression)` | `query.select(criteriaBuilder.avg(root.get("fieldName")))` | 用于计算字段值的平均值 |
| max | 返回用于计算最大值的表达式 | `criteriaBuilder.max(expression)` | `query.select(criteriaBuilder.max(root.get("fieldName")))` | 用于计算字段值的最大值 |
| min | 返回用于计算最小值的表达式 | `criteriaBuilder.min(expression)` | `query.select(criteriaBuilder.min(root.get("fieldName")))` | 用于计算字段值的最小值 |
| count | 返回用于计算行数的表达式 | `criteriaBuilder.count(expression)` | `query.select(criteriaBuilder.count(root.get("id")))` | 用于计算查询结果的行数 |
| distinct | 返回一个用于查询不同值的表达式 | `criteriaBuilder.distinct(expression)` | - | 用于去除查询结果中的重复值 |
| concat | 返回连接两个字符串的表达式 | `criteriaBuilder.concat(string1, string2)` | - | 用于连接两个字符串字段 |
| length | 返回一个字符串长度的表达式 | `criteriaBuilder.length(expression)` | - | 用于获取字符串字段的长度 |
| substring | 返回一个从指定位置开始的子串的表达式 | `criteriaBuilder.substring(expression, from)` | - | 用于获取字符串字段的子串 |
| trim | 返回去除字符串首尾空格的表达式 | `criteriaBuilder.trim(expression)` | - | 用于去除字符串字段的首尾空格 |
| lower | 返回将字符串转换为小写的表达式 | `criteriaBuilder.lower(expression)` | - | 用于将字符串字段转换为小写 |
| upper | 返回将字符串转换为大写的表达式 | `criteriaBuilder.upper(expression)` | - | 用于将字符串字段转换为大写 |

## gitee源码
> [git clone https://gitee.com/anna_spaces/JavaStudyWorkSpaces.git](https://gitee.com/anna_spaces/JavaStudyWorkSpaces.git)