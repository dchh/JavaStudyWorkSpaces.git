import com.demo.SpringBootApplication;
import com.demo.dao.AccountRepository;
import com.demo.entity.AccountEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * 测试类
 *
 * @author Anna.
 * @date 2025/2/21 10:15
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {SpringBootApplication.class})
public class JpaSpecificationExecutorTest {

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void testFindAll(){
        List<AccountEntity> all = accountRepository.findAll(new AccountSpecification("张"));
        System.out.println(all);
    }

    class AccountSpecification implements Specification<AccountEntity>{
        private String name;

        public AccountSpecification() {
        }

        public AccountSpecification(String name) {
            this.name = name;
        }

        @Override
        public Predicate toPredicate(Root root, CriteriaQuery query, CriteriaBuilder criteriaBuilder) {
            if(name != null && !name.isEmpty()) {
                return criteriaBuilder.like(root.get("name"), "%" + name + "%");
            }
            // 返回一个总是为真的条件，表示没有限制 实际就是 查询条件 1 = 1
            return criteriaBuilder.conjunction();
        }
    }

    @Test
    public void testFindAll2(){
        String name = "";
        List<AccountEntity> all = accountRepository.findAll(( root,  query, criteriaBuilder) -> {
            if(!name.isEmpty()) {
                return criteriaBuilder.like(root.get("name"), "%" + name + "%");
            }
            // 返回一个总是为真的条件，表示没有限制 实际就是 查询条件 1 = 1
            return criteriaBuilder.conjunction();
        });
        System.out.println(all);
    }
}
