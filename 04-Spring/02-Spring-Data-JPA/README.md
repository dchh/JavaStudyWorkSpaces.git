01-hibernate-jpa-demo: 基于hibernate实现的的JPA案例
02-spring-data-jpa-demo: 基于spring整合Hibernate及Spring Data JPA的基础测试
03-springboot-hibernate-jpa-demo: 基于springboot整合Hibernate及Spring Data JPA的基础测试
04-JpaRepository-method-demo：JpaRepository接口方法的使用
05-JpaSpecificationExecutor-method-demo：JpaSpecificationExecutor接口方法的使用
06-jpql-or-sql-demo：使用JPQL或者SQL的方式查询
