# Spring Data JPA中的查询方式

1. 使用Spring Data JPA中接口定义的方法进行查询
2. 使用JPQL的方式进行查询
3. 使用SQL语句查询

## JpaRepository接口方法使用

### 常用方法介绍
以下是对JpaRepository内部所有方法（包括继承的方法）的作用、调用方式及方法使用注意事项的详细说明，以列表形式展示：

#### JpaRepository方法列表

| 方法/接口 | 作用 | 调用方式 | 注意事项 |
| --- | --- | --- | --- |
| **新增或修改** | - | - | - |
| **save** | 保存单个实体对象到数据库中 | userRepository.save(user); | 若实体对象的主键已存在，则执行更新操作。 |
| **saveAll** | 保存多个实体对象到数据库中 | userRepository.saveAll(users); | 通过批量保存多个实体对象，可以减少与数据库的交互次数，提高数据保存的效率。 |
| **saveAndFlush** | 保存实体对象并立即刷新到数据库 | userRepository.saveAndFlush(user); | 结合了save和flush两个操作，确保数据立即持久化。 |
| **查询** | - | - | - |
| **findById** | 根据主键查询实体对象 | userRepository.findById(id); | 若实体对象不存在，则返回Optional.empty()。 |
| **findAll** | 查询所有实体对象 | userRepository.findAll(); | 返回所有实体对象的集合。 |
| **findAllById** | 根据主键集合查询实体对象集合 | userRepository.findAllById(ids); | 可以根据主键集合批量查询实体对象。 |
| **getOne** | 根据主键快速访问实体对象（可能不立即从数据库加载） | userRepository.getOne(id); | 返回一个代理对象，在实际使用时才会加载数据。 |
| **findBy**（自定义方法） | 根据方法名自动生成查询语句 | 如：findByUsername(String username) | 方法名需遵循Spring Data的命名规范，支持多种查询条件。 |
| **分页和排序** | - | - | - |
| **findAll(Pageable pageable)** | 分页查询所有实体对象 | userRepository.findAll(pageable); | Pageable对象用于指定分页信息。 |
| **findAll(Sort sort)** | 排序查询所有实体对象 | userRepository.findAll(sort); | Sort对象用于指定排序信息。 |
| **删除** | - | - | - |
| **delete** | 根据实体对象删除数据 | userRepository.delete(user); | 若实体对象不存在，则不执行任何操作。 |
| **deleteAll** | 删除所有实体对象 | userRepository.deleteAll(); | 会删除表中所有数据，操作需谨慎。 |
| **deleteById** | 根据主键删除数据 | userRepository.deleteById(id); | 可以通过主键快速删除数据。 |
| **deleteInBatch** | 批量删除数据 | userRepository.deleteInBatch(users); | 可以批量删除多个实体对象，提高删除效率。 |
| **其他** | - | - | - |
| **existsById** | 根据主键判断实体对象是否存在 | userRepository.existsById(id); | 返回布尔值，表示实体对象是否存在。 |
| **count** | 统计实体对象的数量 | userRepository.count(); | 返回实体对象的总数。 |
| **flush** | 强制将数据库会话中的更改刷新到数据库 | userRepository.flush(); | 一般在需要立即将更改保存到数据库时使用。 |

#### 基础代码
以下是基础代码介绍

**MAVEN依赖：**
pom.xml
```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-data-jpa</artifactId>
        <version>2.6.7</version>
    </dependency>
    <dependency>
        <groupId>org.hibernate</groupId>
        <artifactId>hibernate-core</artifactId>
        <version>5.6.8.Final</version>
    </dependency>
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>5.1.46</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <version>2.6.7</version>
    </dependency>
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.13.2</version>
        <scope>test</scope>
    </dependency>
</dependencies>
```

**DB:**
account.sql
```sql
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `account`;
CREATE TABLE `account`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id主键',
  `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `money` float NULL DEFAULT NULL COMMENT '金额',
  `createDate` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`, `account`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

```
**ENTITY:**
AccountEntity.java
```java
package com.demo.entity;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "account")
public class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "account")
    private String account;

    @Column(name = "name")
    private String name;

    @Column(name = "money")
    private Float money;

    @Column(name = "createDate")
    private Date createDate;

    // GET / SET
}

```
**DAO:**
AccountRepository.java
```java
package com.demo.dao;

import com.demo.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface AccountRepository extends JpaRepository<AccountEntity,Long>, JpaSpecificationExecutor<AccountEntity> {
}

```
**配置文件：**
application.yml
```
spring:
  # 配置数据源
  datasource:
    driver-class-name: com.mysql.jdbc.Driver
    url: jdbc:mysql:///testdb
    username: root
    password: root
  # 配置JPA
  jpa:
    hibernate:
      ddl-auto: update
    show-sql: true
    properties:
      hibernate:
        dialect: org.hibernate.dialect.MySQL5Dialect
```

#### save()

保存单个实体对象到数据库中。

**案例：**

1. 当对象不设置主键时，则直接执行INSERT
    ```
    @Test
    public void testSave(){
        AccountEntity account = new AccountEntity("zhangsan","张三", 1000F);
        AccountEntity save = accountRepository.save(account);
        System.out.println("返回结果：" + save);
    }
    ```

![img.png](imagse/img.png)

2. 当对象设置主键时,会先根据主键执行查询，如果数据不存在，则执行INSERT
    ```
    @Test
    public void testSave2(){
        AccountEntity account = new AccountEntity(1L, "zhangsan","张三", 1000F);
        AccountEntity save = accountRepository.save(account);
        System.out.println("返回结果：" + save);
    }
    ```

![img_1.png](imagse/img_1.png)

3. 当对象设置主键时,会先根据主键执行查询，如果数据存在且数据一致，则不会执行UPDATE
    ```
    @Test
    public void testSave2(){
        AccountEntity account = new AccountEntity(1L, "zhangsan","张三", 1000F);
        AccountEntity save = accountRepository.save(account);
        System.out.println("返回结果：" + save);
    }
    ```

![img_2.png](imagse/img_2.png)

4. 当对象设置主键时,会先根据主键执行查询，如果数据存在且数据不一致，则会执行UPDATE
```
@Test
public void testSave2(){
    AccountEntity account = new AccountEntity(1L, "zhangsan2","张三", 1000F);
    AccountEntity save = accountRepository.save(account);
    System.out.println("返回结果：" + save);
}
```

![img_3.png](imagse/img_3.png)

#### saveAll

保存提供的所有实体。实际上是循环执行save()方法，返回的是实体对象的列表。

**案例：**

1. 当对象不设置主键时，则直接执行INSERT
```
@Test
public void testSaveAll(){
    List<AccountEntity> dataList = new ArrayList<>();
    dataList.add(new AccountEntity("zhangsan","张三", 1000F));
    dataList.add(new AccountEntity("lixi","李四", 1000F));
    List<AccountEntity> rtn = accountRepository.saveAll(dataList);
    System.out.println("返回结果：" + rtn);
}
```
2. 当对象设置主键时,会先根据主键执行查询，如果数据不存在，则执行INSERT
```
@Test
public void testSaveAll2(){
    List<AccountEntity> dataList = new ArrayList<>();
    dataList.add(new AccountEntity(1L,"zhangsan","张三", 1000F));
    dataList.add(new AccountEntity(2L,"lixi","李四", 1000F));
    List<AccountEntity> rtn = accountRepository.saveAll(dataList);
    System.out.println("返回结果：" + rtn);
}
```

![img_4.png](imagse/img_4.png)

3. 当对象设置主键时,会先根据主键执行查询，如果数据存在且数据一致，则不会执行UPDATE
```
   @Test
   public void testSaveAll2(){
   List<AccountEntity> dataList = new ArrayList<>();
   dataList.add(new AccountEntity(1L,"zhangsan","张三", 1000F));
   dataList.add(new AccountEntity(2L,"lixi","李四", 1000F));
   List<AccountEntity> rtn = accountRepository.saveAll(dataList);
   System.out.println("返回结果：" + rtn);
   }
```

![img_5.png](imagse/img_5.png)

4. 当对象设置主键时,会先根据主键执行查询，如果数据存在且数据不一致，则会执行UPDATE
```
@Test
public void testSaveAll2(){
   List<AccountEntity> dataList = new ArrayList<>();
   dataList.add(new AccountEntity(1L,"zhangsan","张三", 1000F));
   dataList.add(new AccountEntity(2L,"lixi","李四", 1000F));
   List<AccountEntity> rtn = accountRepository.saveAll(dataList);
   System.out.println("返回结果：" + rtn);
}
```

![img_6.png](imagse/img_6.png)

#### deleteById(ID id)

根据ID进行删除

**案例：**

1. 存在数据，先执行查询，在执行delete
```
public void testDeleteById(){
    accountRepository.deleteById(1L);
}
```

![img_7.png](imagse/img_7.png)

2. 不存在数据，先执行查询，如果数据不存在则不再执行delete,并抛出异常`org.springframework.dao.EmptyResultDataAccessException`

![img_8.png](imagse/img_8.png)

#### deleteAllByIdInBatch(Iterable<ID> ids)

根据ID批量删除

**案例：**

```
@Test
public void testDeleteAllByIdInBatch(){
    accountRepository.deleteAllByIdInBatch(Arrays.asList(1L,2L));
}
```

![img_9.png](imagse/img_9.png)

> 无论数据是否存在，直接根据ID执行DELETE批量进行删除，数据不存在时，也不会报错

#### void delete(T entity)

根据实体删除

**案例：**

```
@Test
 public void testDelete(){
     AccountEntity account = new AccountEntity(1L);
     accountRepository.delete(account);
 }
```

![img_10.png](imagse/img_10.png)

> 先根据主键查询数据是否存在，如果存在则执行DELETE，如果不存在则不再继续执行，但也不会抛出异常

#### void deleteAll(Iterable<? extends T> entities)

根据实体批量删除，实际是循环执行`void delete(T entity)`

**案例：**

```
@Test
 public void testDeleteAll(){
     ArrayList<AccountEntity> dataList = new ArrayList<>();
     dataList.add(new AccountEntity(1L));
     dataList.add(new AccountEntity(4L));
     accountRepository.deleteAll(dataList);
 }
```

![img_11.png](imagse/img_11.png)

> 实际是循环执行`void delete(T entity)`,先执行查询，数据存在时，在执行DELETE

#### void deleteAll()

删除所有数据，该方法会先查询表中所有数据，然后循环遍历执行`void delete(T entity)`方法

**案例：**
```
@Test
 public void testDeleteAll2(){
     accountRepository.deleteAll();
 }
```

![img_12.png](imagse/img_12.png)

> 该方法会先查询表中所有数据，然后循环遍历执行`void delete(T entity)`方法

#### void deleteAllInBatch()

删除所有数据

**案例：**
```
@Test
 public void testDeleteAllInBatch(){
     accountRepository.deleteAllInBatch();
 }
```

![img_13.png](imagse/img_13.png)

> 该方法直接执行，DELETE方法，执行效率相对`void deleteAll()`较高

#### List<T> findAll()

查询所有数据

**案例：**
```
@Test
public void testFindAll(){
  List<AccountEntity> all = accountRepository.findAll();
  System.out.println(all);
}
```

![img_14.png](imagse/img_14.png)

#### Optional<T> findById(ID id)

根据主键查询,该查询返回一个Optional对象

**案例：**
```
@Test
public void testFindById(){
  Optional<AccountEntity> rtn = accountRepository.findById(1L);
  System.out.println("返回结果：" + rtn);
}
```

![img_15.png](imagse/img_15.png)

##### Optional对象介绍
以下是将上述关于Optional的使用说明以表格形式返回的结果：

| 类别 | 方法/属性 | 说明 | 示例代码 |
| --- | --- | --- | --- |
| **创建** | `Optional.of(T t)` | 创建一个包含非空值的Optional实例，若传入空值则抛出NullPointerException | `Optional<String> optional = Optional.of("Hello");` |
|  | `Optional.empty()` | 创建一个空的Optional实例 | `Optional<String> emptyOptional = Optional.empty();` |
|  | `Optional.ofNullable(T t)` | 创建一个Optional实例，允许传入空值 | `Optional<String> nullableOptional = Optional.ofNullable(null);` |
| **判断** | `boolean isPresent()` | 判断Optional容器中的值是否存在 | `boolean present = optional.isPresent();` |
| **消费** | `void ifPresent(Consumer<? super T> consumer)` | 若值存在，则执行消费操作 | `optional.ifPresent(System.out::println);` |
| **获取** | `T get()` | 获取Optional容器中的值，若值为空则抛出NoSuchElementException | `String value = optional.get();` |
| **默认值** | `T orElse(T other)` | 若值存在则返回该值，否则返回指定的默认值 | `String defaultValue = nullableOptional.orElse("Default");` |
|  | `T orElseGet(Supplier<? extends T> other)` | 若值存在则返回该值，否则返回由Supplier提供的默认值 | `String lazyDefaultValue = nullableOptional.orElseGet(() -> "Lazy Default");` |
| **异常处理** | `T orElseThrow(Supplier<? extends X> exceptionSupplier)` | 若值存在则返回该值，否则抛出由Supplier提供的异常 | `try { String exceptionValue = nullableOptional.orElseThrow(() -> new IllegalArgumentException("Value is missing!")); } catch (Exception e) { e.printStackTrace(); }` |
| **过滤** | `<U> Optional<U> filter(Predicate<? super T> predicate)` | 若值存在且匹配给定的谓词，则返回一个包含该值的Optional，否则返回空Optional | `Optional<String> filteredOptional = optional.filter(s -> s.startsWith("Hello"));` |
| **映射** | `<U> Optional<U> map(Function<? super T, ? extends U> mapper)` | 若值存在，则对其执行映射函数，并返回包含映射结果的Optional，否则返回空Optional | `Optional<Integer> lengthOptional = optional.map(String::length);` |
| **扁平映射** | `<U> Optional<U> flatMap(Function<? super T, Optional<U>> mapper)` | 若值存在，则对其执行映射函数（返回Optional类型），并返回该Optional，否则返回空Optional | `Optional<String> upperCaseOptional = optional.flatMap(s -> Optional.ofNullable(s.toUpperCase()));` |

#### List<T> findAll(Sort sort)

所有数据,并排序

**案例：**
```
@Test
 public void testFindAll2(){
     List<AccountEntity> rtn = accountRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));
     System.out.println("返回结果：" + rtn);
 }
```

![img_16.png](imagse/img_16.png)

> 需要注意的是，这里的`id`,填的是属性名称，而非数据库字段名称，多个在字段排序，以逗号分隔`Sort.by(Sort.Direction.DESC, "id","name")`

#### `<S extends T> List<S> findAll(Example<S> example)`

根据条件查询数据

**案例：**
```
@Test
public void testFindAll3(){
  // 创建一个实例，只设置作为查询条件的属性
  AccountEntity account = new AccountEntity();
  account.setName("张三");
  // 使用Example.of方法创建Example对象
  Example<AccountEntity> example = Example.of(account);
  List<AccountEntity> rtn = accountRepository.findAll(example);
  System.out.println("返回结果：" + rtn);
}
```

![img_17.png](imagse/img_17.png)

#### `Page<T> findAll(Pageable pageable)`

分页查询所有数据

**案例：**
```
@Test
public void testFindAll4(){
  // 其中PageRequest.of(0,3,Sort.Direction.DESC,"id") 第1个参数是页码（从0开始），第2个参数是每页显示的记录数
  Page<AccountEntity> all = accountRepository.findAll(PageRequest.of(0,3,Sort.Direction.DESC,"id"));
  System.out.println(all);
}
```

![img_21.png](imagse/img_21.png)

> Spring Data JPA会根据这个Pageable对象的信息，执行相应的分页查询，并返回一个Page<T>对象，其中包含了查询结果以及分页的相关信息（如总页数、总记录数等）。

#### T getById(ID id)

(延迟加载),该方法调用时，并不会立即查询数据库，只要在使用时才会去真正查询

**案例：**
```
@Test
 public void testGetById(){
     AccountEntity rtn = accountRepository.getById(1L);
     System.out.println("目前为止并没有是否执行查询");
     System.out.println("返回结果：" + rtn);
 }
```

![img_18.png](imagse/img_18.png)

> 注意：使用该方法需配置hibernate，设置`spring.jpa.properties.hibernate.enable_lazy_load_no_trans=true`,否则会抛出如下异常。

![img_19.png](imagse/img_19.png)

#### findBy（自定义）方法命名查询

顾名思义，方法命名规则查询就是根据方法的名字，就能创建查询。只需要按照Spring Data JPA提供的方法命名规则定义方法的名称，就可以完成查询工作。

以下是根据您提供的关键词、示例方法名和JPQL查询语句，以列表形式重新整理的结果：

| Keyword        | Sample                          | JPQL                                                |
|----------------|---------------------------------|-----------------------------------------------------|
| And            | `findByLastnameAndFirstname`    | `… where x.lastname = ?1 and x.firstname = ?2`      |
| Or             | `findByLastnameOrFirstname`     | `… where x.lastname = ?1 or x.firstname = ?2`       |
| Is, Equals     | `findByFirstnameIs`, `findByFirstnameEquals` | `… where x.firstname = ?1`             |
| Between        | `findByStartDateBetween`        | `… where x.startDate between ?1 and ?2`             |
| LessThan       | `findByAgeLessThan`             | `… where x.age < ?1`                                |
| LessThanEqual  | `findByAgeLessThanEqual`        | `… where x.age <= ?1`                               |
| GreaterThan    | `findByAgeGreaterThan`          | `… where x.age > ?1`                                |
| GreaterThanEqual | `findByAgeGreaterThanEqual`   | `… where x.age >= ?1`                               |
| After          | `findByStartDateAfter`          | `… where x.startDate > ?1`                          |
| Before         | `findByStartDateBefore`         | `… where x.startDate < ?1`                          |
| IsNull         | `findByAgeIsNull`               | `… where x.age is null`                             |
| IsNotNull, NotNull | `findByAgeIsNotNull`, `findByAgeNotNull` | `… where x.age is not null`            |
| Like           | `findByFirstnameLike`           | `… where x.firstname like ?1`                       |
| NotLike        | `findByFirstnameNotLike`        | `… where x.firstname not like ?1`                   |

> 对于`IsNull`和`IsNotNull`（或`NotNull`），虽然示例中提供了`findByAgeIsNull`，
> 但通常Spring Data JPA也支持`findByAgeIsNotNull`或简写为`findByAgeNotNull`来表示非空查询。
> 实际应用中，应该根据所使用的Spring Data JPA版本和配置来确定正确的方法名。 
> 其中，`?1`, `?2`, 等是JPQL中的参数占位符，它们在实际查询执行时会被替换为具体的值。在Spring Data JPA中，这些值通常是通过方法参数传递的。
> 查询条件使用的是实体的属性名称，而非字段名称。且定义接口即可。但需要注意的是参数的顺序不能出错，必须按照方法名指定写参数。

**案例：**
AccountRepository.java
```java
package com.demo.dao;

import com.demo.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface AccountRepository extends JpaRepository<AccountEntity,Long>, JpaSpecificationExecutor<AccountEntity> {
    /**
     * 根据account及name属性查询
     * @return
     */
    List<AccountEntity> findByAccountAndName(String account,String name);
}

```
**测试代码：**
```
@Test
public void test(){
  List<AccountEntity> rtn = accountRepository.findByAccountAndName("zhangsan", "张三");
  System.out.println("返回结果：" + rtn);
}
```

![img_20.png](imagse/img_20.png)

> 注意接口方法名称必须是实体属性名称，但是参数名称并没有要求，但是参数顺序必须根据设置查询方式一致

## gitee源码
> [git clone https://gitee.com/anna_spaces/JavaStudyWorkSpaces.git](https://gitee.com/anna_spaces/JavaStudyWorkSpaces.git)


