import com.demo.SpringBootApplication;
import com.demo.dao.AccountRepository;
import com.demo.entity.AccountEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * 测试类
 *
 * @author Anna.
 * @date 2025/2/20 10:55
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {SpringBootApplication.class})
public class TestDemo {
    @Autowired
    private AccountRepository accountRepository;

    /**
     *  不设置主键
     */
    @Test
    public void testSave(){
        AccountEntity account = new AccountEntity("zhangsan","张三", 1000F);
        AccountEntity save = accountRepository.save(account);
        System.out.println("返回结果：" + save);
    }

    /**
     *  设置主键
     */
    @Test
    public void testSave2(){
        AccountEntity account = new AccountEntity(1L, "zhangsan2","张三", 1000F);
        AccountEntity save = accountRepository.save(account);
        System.out.println("返回结果：" + save);
    }

    /**
     * 当对象不设置主键时，则直接执行INSERT
     */
    @Test
    public void testSaveAll(){
        List<AccountEntity> dataList = new ArrayList<>();
        dataList.add(new AccountEntity("zhangsan","张三", 1000F));
        dataList.add(new AccountEntity("lixi","李四", 1000F));
        List<AccountEntity> rtn = accountRepository.saveAll(dataList);
        System.out.println("返回结果：" + rtn);
    }

    /**
     * 当对象不设置主键时，则直接执行INSERT
     */
    @Test
    public void testSaveAll2(){
        List<AccountEntity> dataList = new ArrayList<>();
        dataList.add(new AccountEntity(1L,"zhangsan2","张三", 1000F));
        dataList.add(new AccountEntity(2L,"lixi","李四", 1000F));
        List<AccountEntity> rtn = accountRepository.saveAll(dataList);
        System.out.println("返回结果：" + rtn);
    }

    /**
     * 当对象不设置主键时，则直接执行INSERT
     */
    @Test
    public void testSaveAll3(){
        List<AccountEntity> dataList = new ArrayList<>();
        dataList.add(new AccountEntity(1L,"zhangsan2","张三", 1000F));
        dataList.add(new AccountEntity(2L,"lixi","李四", 1000F));
        List<AccountEntity> rtn = accountRepository.saveAll(dataList);
        System.out.println("返回结果：" + rtn);
    }

    /**
     * 根据ID删除
     */
    @Test
    public void testDeleteById(){
        accountRepository.deleteById(1L);
    }

    /**
     * 根据ID批量删除
     */
    @Test
    public void testDeleteAllByIdInBatch(){
        accountRepository.deleteAllByIdInBatch(Arrays.asList(1L,2L));
    }

    /**
     * 根据实体删除
     */
    @Test
    public void testDelete(){
        AccountEntity account = new AccountEntity(1L);
        accountRepository.delete(account);
    }

    /**
     * 根据实体批量删除
     */
    @Test
    public void testDeleteAll(){
        ArrayList<AccountEntity> dataList = new ArrayList<>();
        dataList.add(new AccountEntity(1L));
        dataList.add(new AccountEntity(6L));
        accountRepository.deleteAll(dataList);
    }

    /**
     * 删除所有
     */
    @Test
    public void testDeleteAll2(){
        accountRepository.deleteAll();
    }

    /**
     * 根据实体批量删除
     */
    @Test
    public void testDeleteAllInBatch(){
        accountRepository.deleteAllInBatch();
    }

    /**
     * 查询所有数据
     */
    @Test
    public void testFindAll(){
        List<AccountEntity> all = accountRepository.findAll();
        System.out.println(all);
    }

    /**
     * 根据ID查询
     */
    @Test
    public void testFindById(){
        Optional<AccountEntity> rtn = accountRepository.findById(1L);
        System.out.println("返回结果：" + rtn);
    }

    /**
     * 查询所有数据,排序
     */
    @Test
    public void testFindAll2(){
        List<AccountEntity> rtn = accountRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));
        System.out.println("返回结果：" + rtn);
    }

    /**
     * 根据条件查询数据
     */
    @Test
    public void testFindAll3(){
        // 创建一个实例，只设置作为查询条件的属性
        AccountEntity account = new AccountEntity();
        account.setName("张三");
        // 使用Example.of方法创建Example对象
        Example<AccountEntity> example = Example.of(account);
        List<AccountEntity> rtn = accountRepository.findAll(example);
        System.out.println("返回结果：" + rtn);
    }

    /**
     * 分页查询所有数据
     */
    @Test
    public void testFindAll4(){
        Page<AccountEntity> all = accountRepository.findAll(PageRequest.of(0,3,Sort.Direction.DESC,"id"));
        System.out.println(all);
    }

    /**
     * 查询一条数据(延迟加载)
     */
    @Test
    public void testGetById(){
        AccountEntity rtn = accountRepository.getById(1L);
        System.out.println("目前为止并没有是否执行查询");
        System.out.println("返回结果：" + rtn);
    }

    /**
     * findBy（自定义）方法命名查询
     */
    @Test
    public void test(){
        List<AccountEntity> rtn = accountRepository.findByAccountAndName("zhangsan", "张三");
        System.out.println("返回结果：" + rtn);
    }
}
