# 基于springboot整合Hibernate及Spring Data JPA的基础测试

**DB:**
account.sql
```sql
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id主键',
  `account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `money` float NULL DEFAULT NULL COMMENT '金额',
  `createDate` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`, `account`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

```
**MAVEN依赖：**
pom.xml
```xml
<properties>
    <maven.compiler.source>8</maven.compiler.source>
    <maven.compiler.target>8</maven.compiler.target>
</properties>

<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-data-jpa</artifactId>
        <version>2.6.7</version>
    </dependency>
    <dependency>
        <groupId>org.hibernate</groupId>
        <artifactId>hibernate-core</artifactId>
        <version>5.6.8.Final</version>
    </dependency>
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>5.1.46</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <version>2.6.7</version>
    </dependency>
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.13.2</version>
        <scope>test</scope>
    </dependency>
</dependencies>
```
**ENTITY:**
AccountEntity.java
```java
package com.demo.entity;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "account")
public class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "account")
    private String account;

    @Column(name = "name")
    private String name;

    @Column(name = "money")
    private Float money;

    @Column(name = "createDate")
    private Date createDate;

    // get / set
}

```
**DAO:**
AccountRepository.java
```java
package com.demo.dao;

import com.demo.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * Account DAO 接口
 *   JpaRepository封装了基本查询方法，JpaSpecificationExecutor封装了复杂查询的方法
 * @author Anna.
 * @date 2025/2/19 9:39
 */
public interface AccountRepository extends JpaRepository<AccountEntity,Long>, JpaSpecificationExecutor<AccountEntity> {
}

```
**SERVICE:**
IAccountService.java
```java
public interface IAccountService {
    List<AccountEntity> findAll();
}

```
AccountServiceImpl.java
```java
@Service("accountService")
public class AccountServiceImpl implements IAccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public List<AccountEntity> findAll() {
        return accountRepository.findAll();
    }
}
```
**配置文件：**
application.yml
```yml
spring:
  # 配置数据源
  datasource:
    driver-class-name: com.mysql.jdbc.Driver
    url: jdbc:mysql:///testdb
    username: root
    password: root
  # 配置JPA
  jpa:
    hibernate:
      ddl-auto: update
    show-sql: true
    properties:
      hibernate:
        dialect: org.hibernate.dialect.MySQL5Dialect
```
**测试代码：**
SpringBootJpaTest.java
```java
import com.demo.SpringBootApplication;
import com.demo.entity.AccountEntity;
import com.demo.service.IAccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {SpringBootApplication.class})
public class SpringBootJpaTest {

    @Autowired
    private IAccountService accountService;

    @Test
    public void test() {
        List<AccountEntity> all = accountService.findAll();
        for (AccountEntity account : all) {
            System.out.println(account);
        }

    }
}

```
**执行结果：**

![img.png](img.png)


## gitee源码
> [git clone https://gitee.com/anna_spaces/JavaStudyWorkSpaces.git](https://gitee.com/anna_spaces/JavaStudyWorkSpaces.git)