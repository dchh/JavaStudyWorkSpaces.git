package com.demo.service;

import com.demo.entity.AccountEntity;

import java.util.List;

/**
 * 接口类
 *
 * @author Anna.
 * @date 2025/2/19 23:12
 */
public interface IAccountService {

    List<AccountEntity> findAll();
}
