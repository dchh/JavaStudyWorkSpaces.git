package com.demo;

import org.springframework.boot.SpringApplication;

/**
 * 启动类
 *
 * @author Anna.
 * @date 2025/2/19 23:05
 */
@org.springframework.boot.autoconfigure.SpringBootApplication
public class SpringBootApplication {
    public static void main(String[] args) {
        // 启动Spring应用上下文
        SpringApplication.run(SpringBootApplication.class, args);
    }
}
