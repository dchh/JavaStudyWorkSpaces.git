package com.demo.service.impl;

import com.demo.dao.AccountRepository;
import com.demo.entity.AccountEntity;
import com.demo.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 实现类
 *
 * @author Anna.
 * @date 2025/2/19 23:13
 */
@Service("accountService")
public class AccountServiceImpl implements IAccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public List<AccountEntity> findAll() {
        return accountRepository.findAll();
    }
}
