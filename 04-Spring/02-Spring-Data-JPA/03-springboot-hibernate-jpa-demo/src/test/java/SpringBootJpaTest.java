import com.demo.SpringBootApplication;
import com.demo.entity.AccountEntity;
import com.demo.service.IAccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * 测试类
 *
 * @author Anna.
 * @date 2025/2/19 23:00
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {SpringBootApplication.class})
public class SpringBootJpaTest {

    @Autowired
    private IAccountService accountService;

    @Test
    public void test() {
        List<AccountEntity> all = accountService.findAll();
        for (AccountEntity account : all) {
            System.out.println(account);
        }

    }
}
