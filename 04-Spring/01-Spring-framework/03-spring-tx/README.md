# Spring事物管理

Spring事务管理是Spring框架提供的一种机制，用于管理应用程序中的事务操作，确保数据的一致性和完整性。

## 基本概念

### 什么是事务
事务由事务开始与事务结束之间执行的全部数据库操作组成。是访问并可能操作各种数据项的一个数据库操作序列，这些操作要么全部执行，要么全部不执行，是一个不可分割的工作单位。

### 事务的四大特性（ACID）
+ **原子性（Atomicity）：** 事务作为一个整体操作，要么全部执行成功，要么全部失败回滚，不允许部分执行成功部分失败。 
+ **一致性（Consistency）：** 事务执行前后，数据库从一个一致状态转换为另一个一致状态。事务执行过程中，数据库的数据保持一致性约束。 
+ **隔离性（Isolation）：** 事务的执行应该与其他事务隔离开来，每个事务在执行过程中看到的数据应该是一致的。 
+ **持久性（Durability）：** 一旦事务提交，对数据库的修改应该是永久性的，即使发生系统故障也不会丢失。

## 案例-基于spring自带JdbcTemplate实现转账
基于spring自带JdbcTemplate实现转账，手动设置运行时异常，演示事务不一致情况。
**数据库：**
accountbook.sql
```sql
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS `accountbook`;
CREATE TABLE `accountbook`  (
                              `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                              `money` float NULL DEFAULT NULL,
                              PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

INSERT INTO `accountbook` VALUES ('lisi', 1000);
INSERT INTO `accountbook` VALUES ('zhangsan', 1000);

SET FOREIGN_KEY_CHECKS = 1;
```
**代码：**
AccountBookDo.java
```java
package demo1.entity;

public class AccountBookDo {
  private String name;
  private float money;
  // ... GET/SET
}
```
IAccountBookDao.java
```java
package demo1.dao;

import demo1.entity.AccountBookDo;

public interface IAccountBookDao {

    AccountBookDo selectAccountBookByName(String name);

    void updateAccountBook(float money,String name);
}
```
AccountBookDaoImpl.java
```java
package demo1.dao.impl;

import demo1.dao.IAccountBookDao;
import demo1.entity.AccountBookDo;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

public class AccountBookDaoImpl implements IAccountBookDao {

    private QueryRunner runner;

    public AccountBookDaoImpl(QueryRunner runner) {
        this.runner = runner;
    }

    @Override
    public AccountBookDo selectAccountBookByName(String name) {
        try {
            return runner.query("select * from accountbook where name = ? ", new BeanHandler<AccountBookDo>(AccountBookDo.class), name);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateAccountBook(float money, String name) {
        try {
            runner.update("update accountbook set money=? where name=?", money, name);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
```
IAccountBookService.java
```java
package demo1.service;

import demo1.entity.AccountBookDo;
public interface IAccountBookService {
    void transferAccounts();
}

```
AccountBookServiceImpl.java
```java
package demo1.service.impl;

import demo1.dao.IAccountBookDao;
import demo1.entity.AccountBookDo;
import demo1.service.IAccountBookService;

public class AccountBookServiceImpl implements IAccountBookService {

    private IAccountBookDao accountBookDao;

    public void setAccountBookDao(IAccountBookDao accountBookDao) {
        this.accountBookDao = accountBookDao;
    }

    @Override
    public void transferAccounts() {
        // 1. 查询账户zhangsan
        AccountBookDo zhangsan = accountBookDao.selectAccountBookByName("zhangsan");
        // 2.查询账户lisi
        AccountBookDo lisi = accountBookDao.selectAccountBookByName("lisi");
        // 定义金融
        float money = 100f;
        // 3.账户zhangsan - 100
        accountBookDao.updateAccountBook(zhangsan.getMoney() - money, zhangsan.getName());

        // 运行异常导致事物不一致
        int i = 1 / 0;

        // 4.账户lisi + 100
        accountBookDao.updateAccountBook(lisi.getMoney() + money, lisi.getName());

        System.out.println("执行完成");
    }
}
```

**配置文件：**
demo1.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    <!-- 配置MySQL数据源 -->
    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://127.0.0.1:3306/testdb"/>
        <property name="username" value="root"/>
        <property name="password" value="root"/>
    </bean>
    <!--配置QueryRunner-->
    <bean id="queryRunner" class="org.apache.commons.dbutils.QueryRunner">
        <constructor-arg ref="dataSource"/>
    </bean>
    <!-- 配置DAO -->
    <bean id="accountBookDao" class="demo1.dao.impl.AccountBookDaoImpl">
        <constructor-arg ref="queryRunner"></constructor-arg>
    </bean>
    <!--配置Service-->
    <bean id="accountService" class="demo1.service.impl.AccountBookServiceImpl">
        <property name="accountBookDao" ref="accountBookDao"></property>
    </bean>
</beans>
```

**测试类：**
TestClient.java
```java
package demo1;

import demo1.service.IAccountBookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestClient {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("demo1.xml");
        // 获取service
        IAccountBookService accountService = (IAccountBookService) ac.getBean("accountBookService");
        accountService.transferAccounts();
    }
}
```

**执行结果：**
![img.png](images/img.png)

数据库：

![img_1.png](images/img_1.png)

## Spring基于XML声明式事务控制
Spring中基于XML的声明式事务控制配置步骤：
1. 配置事务管理器 
```xml
<bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
    <property name="dataSource" ref="dataSource"></property>
</bean>
```
2. 配置事务的通知
   + 需要导入事务的约束 tx和aop的命名空间及约束
       ![img_2.png](images/img_2.png)
   + 使用tx:advice标签配置事务通知：
     id属性：事务的唯一标识
     transaction-manager属性：指定事务管理器的bean的id（与ref作用类似）   
    ```xml
    <!-- 配置事务的通知 -->
    <tx:advice id="txAdvice" transaction-manager="transactionManager"></tx:advice>
    ```
3. 配置AOP中的通用切点表达式
4. 建立事务通知和切点表达式的对应关系
```xml
<!-- 配置AOP切点表达式 -->
<aop:config>
    <!-- 配置切点表达式 -->
    <aop:pointcut id="pt1" expression="execution(* demo1.service.impl.AccountBookServiceImpl.*(..))"/>
    <!--建立切入点表达式和事务通知的对应关系 -->
    <aop:advisor pointcut-ref="pt1" advice-ref="txAdvice"></aop:advisor>
</aop:config>
```
5. 配置事务的属性（在事务的通知tx:advice标签的内部）
```xml
<!-- 配置事务的通知 -->
<tx:advice id="txAdvice" transaction-manager="transactionManager">
    <!-- 配置事务 -->
    <tx:attributes>
        <!-- 配置管理方法的属性
            name: 指定需要管理的方法.可以使用通配符*
                transferAccounts： 指定明确的方法名，则值管理transferAccounts作为方法名的方法
                transfer*：指定前缀，表示管理方法名以transfer开头的方法名的方法
                *：表示管理所有方法，通常情况下我们并不会这么使用

            isolation：用于指定事务的隔离级别。默认是DEFAULT,表示使用数据库的默认隔离级别。
            propagation：用于指定事务的传播行为。默认值是REQUIRED,表示一定会有事务。查询方法可以选择SUPPORTS.
            read-only: 用于指定事务是否只读。默认值是false.只有查询方法才能设置为true.
            timeout: 用于指定事务的超时时间，默认值是-1，表示永不超时。如果指定了数值单位s(秒)
            rollback-for: 用于指定一个异常，当产生该异常时，事务回滚，产生其他异常时，事务不回滚。没有默认值，表示任何异常都回滚。
            no-rollback-for：用于指定一个异常，当产生该异常时，事务不回滚，产生其他异常时事务回滚。没有默认值，表示任何异常都回滚。
        -->
        <tx:method name="transferAccounts"/>
    </tx:attributes>
</tx:advice>
```

**配置文件:**
demo2.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop.xsd
                           http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd
">

    <!-- 配置数据源 -->
    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://127.0.0.1:3306/testdb"/>
        <property name="username" value="root"/>
        <property name="password" value="root"/>
    </bean>

    <!-- 配置JdbcTemplate-->
    <bean id="jdbcTemplate" class="org.springframework.jdbc.core.JdbcTemplate">
        <property name="dataSource" ref="dataSource"></property>
    </bean>

    <!-- 配置Dao-->
    <bean id="accountBookDao" class="demo1.dao.impl.AccountBookDaoImpl">
        <property name="jdbcTemplate" ref="jdbcTemplate"></property>
    </bean>
    <!-- 配置service-->
    <bean id="accountBookService" class="demo1.service.impl.AccountBookServiceImpl">
        <property name="accountBookDao" ref="accountBookDao"></property>
    </bean>

    <!-- Spring中基于XML的声明式事务控制配置步骤：
        1. 配置事务管理器
        2. 配置事务的通知
            需要导入事务的约束 tx和aop的命名空间及约束
            使用tx:advice标签配置事务通知：
                id属性：事务的唯一标识
                transaction-manager属性：指定事务管理器的bean的id（与ref作用类似）
        3. 配置AOP中的通用切点表达式
        4. 建立事务通知和切点表达式的对应关系
        5. 配置事务的属性（在事务的通知tx:advice标签的内部）
    -->
    <!-- 配置事务管理器 -->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <property name="dataSource" ref="dataSource"></property>
    </bean>

    <!-- 配置事务的通知 -->
    <tx:advice id="txAdvice" transaction-manager="transactionManager">
        <!-- 配置事务 -->
        <tx:attributes>
            <!-- 配置管理方法的属性
                name: 指定需要管理的方法.可以使用通配符*
                    transferAccounts： 指定明确的方法名，则值管理transferAccounts作为方法名的方法
                    transfer*：指定前缀，表示管理方法名以transfer开头的方法名的方法
                    *：表示管理所有方法，通常情况下我们并不会这么使用

                isolation：用于指定事务的隔离级别。默认是DEFAULT,表示使用数据库的默认隔离级别。
                propagation：用于指定事务的传播行为。默认值是REQUIRED,表示一定会有事务。查询方法可以选择SUPPORTS.
                read-only: 用于指定事务是否只读。默认值是false.只有查询方法才能设置为true.
                timeout: 用于指定事务的超时时间，默认值是-1，表示永不超时。如果指定了数值单位s(秒)
                rollback-for: 用于指定一个异常，当产生该异常时，事务回滚，产生其他异常时，事务不回滚。没有默认值，表示任何异常都回滚。
                no-rollback-for：用于指定一个异常，当产生该异常时，事务不回滚，产生其他异常时事务回滚。没有默认值，表示任何异常都回滚。
            -->
            <tx:method name="transferAccounts"/>
        </tx:attributes>
    </tx:advice>

    <!-- 配置AOP切点表达式 -->
    <aop:config>
        <!-- 配置切点表达式 -->
        <aop:pointcut id="pt1" expression="execution(* demo1.service.impl.AccountBookServiceImpl.*(..))"/>
        <!--建立切入点表达式和事务通知的对应关系 -->
        <aop:advisor pointcut-ref="pt1" advice-ref="txAdvice"></aop:advisor>
    </aop:config>

</beans>
```
**测试代码：**
TestClient.java
```java
package demo2;

import demo2.service.IAccountBookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestClient {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("demo2.xml");
        // 获取service
        IAccountBookService accountService = (IAccountBookService) ac.getBean("accountBookService");
        accountService.transferAccounts();
    }
}
```
**执行结果：**
![img_3.png](images/img_3.png)

数据库：

![img_4.png](images/img_4.png)

### XML配置中的事务通知详解
在Spring框架中，`<tx:advice>`标签是用于配置事务管理的一个重要组成部分。它属于Spring的声明式事务管理范畴，允许开发者在不需要编写大量事务管理代码的情况下，通过配置来管理事务。以下是关于`<tx:advice>`标签及其子标签的详细描述：

#### `<tx:advice>`标签

* **作用**：创建一个`TransactionInterceptor`对象，该对象作为事务切面的通知方法。通过该标签，可以设置一系列事务属性，这些属性将应用于被拦截的方法上。
* **使用场景**：当需要对特定的方法进行事务管理时，可以使用`<tx:advice>`标签来配置这些方法的事务属性。
* **主要属性**：

    * `id`：为事务通知设置一个唯一的标识符，以便在其它地方引用。
    * `transaction-manager`：指定要使用的事务管理器。事务管理器是Spring框架中用于管理事务的核心组件，它提供了开启、提交和回滚事务的能力。

##### `<tx:attributes>`子标签

* **作用**：`<tx:attributes>`标签用于包含一系列`<tx:method>`子标签，每个`<tx:method>`标签都定义了一个特定方法的事务属性。

###### `<tx:method>`子标签

* **作用**：`<tx:method>`标签用于指定某个或某些方法的事务属性。通过匹配方法名或模式，可以为特定的方法设置事务的传播行为、隔离级别、超时时间等。
* **主要属性**：
    * `name`：指定要应用事务属性的方法名或模式。例如，“insert*”可以匹配所有以“insert”开头的方法。
    * `propagation`：设置事务的传播行为。传播行为定义了事务方法被嵌套调用时的处理方式。默认值`REQUIRED`。常用的取值包括：
        + `REQUIRED`：如果当前有事务，则加入当前事务；如果没有事务，则开启一个新事务。
        + `REQUIRES_NEW`：每次都开启一个新事务，并暂停当前事务（如果存在）。
        + `SUPPORTS`：如果当前有事务，则加入当前事务；如果没有事务，则以非事务方式执行。
        + `NOT_SUPPORTED`：以非事务方式执行操作，如果当前存在事务，则把当前事务挂起。
        + `MANDATORY`：支持当前事务，如果当前没有事务，则抛出异常。
        + `NEVER`：以非事务方式执行，如果当前存在事务，则抛出异常。
    * `isolation`：设置事务的隔离级别。隔离级别定义了事务之间的隔离程度，以避免脏读、不可重复读和幻读等问题。默认值`DEFAULT`。常用的取值包括：
        + `DEFAULT`：使用数据库默认的隔离级别。
        + `READ_UNCOMMITTED`：允许读取未提交的数据，可能会导致脏读。
        + `READ_COMMITTED`：只能读取已提交的数据，避免脏读。
        + `REPEATABLE_READ`：确保在同一事务中多次读取同一数据时，数据是一致的，避免不可重复读。
        + `SERIALIZABLE`：最高级别的隔离，完全串行化执行，避免脏读、不可重复读和幻读，但性能开销最大。
    * `timeout`：设置事务的超时时间（以秒为单位）。默认值-1。如果事务在指定的时间内没有完成，则会被回滚。
    * `read-only`：设置事务是否为只读事务。默认值false。只读事务可以优化数据库性能，因为它不允许修改数据。
    * `rollback-for`和`no-rollback-for`：配置哪些异常可以导致或不会导致事务回滚。默认情况下，`RuntimeException`及其子类会导致事务回滚，而其它异常（即checked异常）不会导致回滚。通过这两个属性，可以自定义回滚行为。

#### 使用示例

以下是一个使用`<tx:advice>`标签配置事务属性的示例：

```xml
<aop:config>
    <aop:pointcut id="serviceMethods" expression="execution(* com.example.service.*.*(..))"/>
    <aop:advisor advice-ref="txAdvice" pointcut-ref="serviceMethods"/>
</aop:config>

<tx:advice id="txAdvice" transaction-manager="transactionManager">
    <tx:attributes>
        <tx:method name="insert*" propagation="REQUIRED" rollback-for="Exception"/>
        <tx:method name="update*" propagation="REQUIRED" rollback-for="Exception"/>
        <tx:method name="delete*" propagation="REQUIRED" rollback-for="Exception"/>
        <tx:method name="*" propagation="SUPPORTS" read-only="true"/>
    </tx:attributes>
</tx:advice>
```

示例中，我们定义了一个名为`txAdvice`的事务通知，并将其应用于`com.example.service`包下所有方法的切点上。对于以“insert”、“update”和“delete”开头的方法，我们设置了`REQUIRED`的传播行为和`Exception`的回滚规则；对于其它方法，设置了`SUPPORTS`的传播行为和只读事务。

#### 事务的传播特性

应用场景：方法A调用方法B和方法C，其中-1表示方法B或方法C进行异常操作。

![img_5.png](images/img_5.png)

| 执行顺序 | B（-1操作）| C（-1操作）| A（调用BC后-1操作）| 执行结果 |
| --- | --- | --- | --- | --- |
| A-->BC | REQUIRED（-1） | REQUIRED | REQUIRED | B执行，存在当前事务A，使用当前事务A，B执行报错，事务A回滚，因此B回滚，C不执行 |
| A-->BC | REQUIRED | REQUIRED（-1） | REQUIRED | B执行，存在当前事务A使用当前事务A，B执行成功，A事务暂未提交，C执行，存在当前事务A，使用当前事务A，C执行报错，事务A回滚，因此BC回滚 |
| A-->BC | REQUIRED | REQUIRED | REQUIRED（-1） | B执行，存在当前事务A使用当前事务A，B执行成功，A事务暂未提交，C执行，存在当前事务A，使用当前事务A，C执行成功，A事务暂未提交，执行A方法调用BC后方法报错，事务A回滚，因此BC回滚 |
| A-->BC | REQUIRES_NEW（-1） | REQUIRED | REQUIRED | B执行，存在当前事务A，因此把事务A挂起，新建事务B事务，B执行异常，事务B回滚，若方法A中通过try-catch处理了方法B的异常，则继续执行方法C，结果C执行成功，B执行失败数据回滚，若方法A未处理异常，C不执行，A事务回滚，结果B回滚C不执行 |
| A-->BC | REQUIRES_NEW | REQUIRED（-1） | REQUIRED | B执行，存在当前事务A，因此把事务A挂起，新建事务B事务，B执行成功，B事务提交，C执行存在当前事务A，使用当前事务A，C执行报错，则A事务回滚，结果B执行成功，C回滚 |
| A-->BC | REQUIRES_NEW | REQUIRED | REQUIRED（-1） | B执行，存在当前事务A，因此把事务A挂起，新建事务B事务，B执行成功，B事务提交，C执行存在当前事务A，使用当前事务A，C执行成功，A事务暂未提交，执行A方法调用BC后方法报错，事务A回滚，因此B执行成功，C回滚 |
| A-->BC | REQUIRED（-1） | REQUIRES_NEW | REQUIRED | B执行，当前存在事务A，使用当前事务，B执行报错，则C不执行，B回滚 |
| A-->BC | REQUIRED | REQUIRES_NEW（-1） | REQUIRED | B执行，当前存在事务A，使用当前事务，B执行成功，A事务暂未提交，C执行，存在当前事务A，A事务挂起，新建事务C，C执行失败，则事务C回滚，若方法A中通过try-catch处理了方法C的异常，则B执行成功，C回滚，若方法A未处理异常，则BC回滚 |
| A-->BC | REQUIRED | REQUIRES_NEW | REQUIRED（-1） | B执行，当前存在事务A，使用当前事务，B执行成功，A事务暂未提交，C执行，存在当前事务A，A事务挂起，新建事务C，C执行成功，事务C提交，执行A方法调用BC后方法报错，A事务回滚，则B回滚，C执行成功 |

## 基于注解声明式事务控制
**Spring中基于注解的事务控制配置步骤：**
1. 配置事务管理器
2. 开启spring对注解事务的支持。需要导入事务的约束 tx和context的命名空间及约束
3. 在需要事务支持的地方使用@Transactional注解

**SERVICE:**
AccountBookServiceImpl.java
```java
package demo3.service.impl;

import demo3.dao.IAccountBookDao;
import demo3.entity.AccountBookDo;
import demo3.service.IAccountBookService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

// 只读型事务的配置
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AccountBookServiceImpl implements IAccountBookService {

    private IAccountBookDao accountBookDao;

    public void setAccountBookDao(IAccountBookDao accountBookDao) {
        this.accountBookDao = accountBookDao;
    }

    // 读写型事务的配置
    @Transactional(propagation = Propagation.REQUIRED,readOnly = false)
    @Override
    public void transferAccounts() {
        // 1. 查询账户zhangsan
        AccountBookDo zhangsan = accountBookDao.selectAccountBookByName("zhangsan");
        // 2.查询账户lisi
        AccountBookDo lisi = accountBookDao.selectAccountBookByName("lisi");
        // 定义金融
        float money = 100f;
        // 3.账户zhangsan - 100
        accountBookDao.updateAccountBook(zhangsan.getMoney() - money, zhangsan.getName());

        // 运行异常导致事物不一致
        int i = 1 / 0;

        // 4.账户lisi + 100
        accountBookDao.updateAccountBook(lisi.getMoney() + money, lisi.getName());

        System.out.println("执行完成");
    }
}

```
**配置文件：**
demo3.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd
                           http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx.xsd
">
    <!-- 配置数据源 -->
    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://127.0.0.1:3306/testdb"/>
        <property name="username" value="root"/>
        <property name="password" value="root"/>
    </bean>

    <!-- 配置JdbcTemplate-->
    <bean id="jdbcTemplate" class="org.springframework.jdbc.core.JdbcTemplate">
        <property name="dataSource" ref="dataSource"></property>
    </bean>

    <!-- 配置Dao-->
    <bean id="accountBookDao" class="demo3.dao.impl.AccountBookDaoImpl">
        <property name="jdbcTemplate" ref="jdbcTemplate"></property>
    </bean>
    <!-- 配置service-->
    <bean id="accountBookService" class="demo3.service.impl.AccountBookServiceImpl">
        <property name="accountBookDao" ref="accountBookDao"></property>
    </bean>

    <!-- Spring中基于注解的事务控制配置步骤：
        1. 配置事务管理器
        2. 开启spring对注解事务的支持。需要导入事务的约束 tx和context的命名空间及约束
        3. 在需要事务支持的地方使用@Transactional注解
    -->
    <!-- 配置spring创建容器时要扫描的包-->
    <context:component-scan base-package="demo3"></context:component-scan>

    <!-- 配置事务管理器 -->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <property name="dataSource" ref="dataSource"></property>
    </bean>

    <!-- 开启spring对注解事务的支持 -->
    <tx:annotation-driven transaction-manager="transactionManager"></tx:annotation-driven>
</beans>
```
**测试代码：**
TestClient.java
```java
package demo3;

import demo3.service.IAccountBookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestClient {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("demo3.xml");
        // 获取service
        IAccountBookService accountService = (IAccountBookService) ac.getBean("accountBookService");
        accountService.transferAccounts();
    }
}
```
**执行结果：**
![img_6.png](images/img_6.png)

数据库：

![img_7.png](images/img_7.png)

## 基于纯注解声明式事务控制

**配置文件：**
JdbcConfig.java
数据连接配置文件
```java
package demo4.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
public class JdbcConfig {

    // 配置数据源
    @Bean("dataSource")
    public DataSource createDataSource() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUrl("jdbc:mysql://127.0.0.1:3306/testdb");
        ds.setUsername("root");
        ds.setPassword("root");
        return ds;
    }

    // 配置JdbcTemplate
    @Bean("jdbcTemplate")
    public JdbcTemplate createJdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}

```
TransactionManagerConfig.java
事务管理器配置类
```java
package demo4.config;

import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

public class TransactionManagerConfig {

    // 配置事务管理器
    @Bean("transactionManager")
    public PlatformTransactionManager createTransactionManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
}

```
SpringConfig.java
Spring配置类
```java
package demo4.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration // 标注是配置文件
@ComponentScan("demo4") // 需要扫描的包
@Import({JdbcConfig.class,TransactionManagerConfig.class})// 配置需要引入的配置文件
@EnableTransactionManagement // 开始支持注解式事务
public class SpringConfig {
}

```
**DAO:**
AccountBookDaoImpl.java
```java
package demo4.dao.impl;

import demo4.dao.IAccountBookDao;
import demo4.entity.AccountBookDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("accountBookDao")
public class AccountBookDaoImpl implements IAccountBookDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public AccountBookDo selectAccountBookByName(String name) {
        List<AccountBookDo> rtn = jdbcTemplate.query("select * from accountbook where name = ? ", new BeanPropertyRowMapper<AccountBookDo>(AccountBookDo.class), name);
        if(rtn.size() > 1) {
            throw new RuntimeException("查结果过多");
        }
        return rtn.get(0);
    }

    @Override
    public void updateAccountBook(float money, String name) {
        jdbcTemplate.update("update accountbook set money=? where name=?", money, name);
    }


}

```
**SERVICE:**
AccountBookServiceImpl.java
```java
package demo4.service.impl;

import demo4.dao.IAccountBookDao;
import demo4.entity.AccountBookDo;
import demo4.service.IAccountBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("accountBookService")
// 只读型事务的配置
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AccountBookServiceImpl implements IAccountBookService {

    @Autowired
    private IAccountBookDao accountBookDao;

    // 读写型事务的配置
    @Transactional(propagation = Propagation.REQUIRED,readOnly = false)
    @Override
    public void transferAccounts() {
        // 1. 查询账户zhangsan
        AccountBookDo zhangsan = accountBookDao.selectAccountBookByName("zhangsan");
        // 2.查询账户lisi
        AccountBookDo lisi = accountBookDao.selectAccountBookByName("lisi");
        // 定义金融
        float money = 100f;
        // 3.账户zhangsan - 100
        accountBookDao.updateAccountBook(zhangsan.getMoney() - money, zhangsan.getName());

        // 运行异常导致事物不一致
        int i = 1 / 0;

        // 4.账户lisi + 100
        accountBookDao.updateAccountBook(lisi.getMoney() + money, lisi.getName());

        System.out.println("执行完成");
    }
}

```
**测试类：**
TestClient.java
```java
package demo4;

import demo4.service.IAccountBookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestClient {
    public static void main(String[] args) {
        ApplicationContext ac = new AnnotationConfigApplicationContext("demo4");
        // 获取service
        IAccountBookService accountService = (IAccountBookService) ac.getBean("accountBookService");
        accountService.transferAccounts();
    }
}
```
**执行结果：**
![img_8.png](images/img_8.png)

数据库：

![img_9.png](images/img_9.png)

## gitee源码
> [git clone https://gitee.com/anna_spaces/JavaStudyWorkSpaces.git](https://gitee.com/anna_spaces/JavaStudyWorkSpaces.git)