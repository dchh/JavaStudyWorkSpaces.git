package demo4.service.impl;

import demo4.dao.IAccountBookDao;
import demo4.entity.AccountBookDo;
import demo4.service.IAccountBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * AccountBookService 接口实现类
 *
 * @author Anna.
 * @date 2025/2/11 20:03
 */
@Service("accountBookService")
// 只读型事务的配置
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AccountBookServiceImpl implements IAccountBookService {

    @Autowired
    private IAccountBookDao accountBookDao;

    // 读写型事务的配置
    @Transactional(propagation = Propagation.REQUIRED,readOnly = false)
    @Override
    public void transferAccounts() {
        // 1. 查询账户zhangsan
        AccountBookDo zhangsan = accountBookDao.selectAccountBookByName("zhangsan");
        // 2.查询账户lisi
        AccountBookDo lisi = accountBookDao.selectAccountBookByName("lisi");
        // 定义金融
        float money = 100f;
        // 3.账户zhangsan - 100
        accountBookDao.updateAccountBook(zhangsan.getMoney() - money, zhangsan.getName());

        // 运行异常导致事物不一致
        int i = 1 / 0;

        // 4.账户lisi + 100
        accountBookDao.updateAccountBook(lisi.getMoney() + money, lisi.getName());

        System.out.println("执行完成");
    }
}
