package demo4.dao.impl;

import demo4.dao.IAccountBookDao;
import demo4.entity.AccountBookDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * AccountBookDao 实现类
 *
 * @author Anna.
 * @date 2025/2/11 19:42
 */
@Repository("accountBookDao")
public class AccountBookDaoImpl implements IAccountBookDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public AccountBookDo selectAccountBookByName(String name) {
        List<AccountBookDo> rtn = jdbcTemplate.query("select * from accountbook where name = ? ", new BeanPropertyRowMapper<AccountBookDo>(AccountBookDo.class), name);
        if(rtn.size() > 1) {
            throw new RuntimeException("查结果过多");
        }
        return rtn.get(0);
    }

    @Override
    public void updateAccountBook(float money, String name) {
        jdbcTemplate.update("update accountbook set money=? where name=?", money, name);
    }


}
