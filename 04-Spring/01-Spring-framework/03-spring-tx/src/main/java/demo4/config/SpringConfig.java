package demo4.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Spring配置类
 *
 * @author Anna.
 * @date 2025/2/17 17:37
 */
@Configuration // 标注是配置文件
@ComponentScan("demo4") // 需要扫描的包
@Import({JdbcConfig.class,TransactionManagerConfig.class})// 配置需要引入的配置文件
@EnableTransactionManagement // 开始支持注解式事务
public class SpringConfig {
}
