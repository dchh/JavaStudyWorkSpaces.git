package demo4;

import demo4.service.IAccountBookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 测试类
 *
 * @author Anna.
 * @date 2025/2/11 19:30
 */
public class TestClient {
    public static void main(String[] args) {
        ApplicationContext ac = new AnnotationConfigApplicationContext("demo4");
        // 获取service
        IAccountBookService accountService = (IAccountBookService) ac.getBean("accountBookService");
        accountService.transferAccounts();
    }
}
