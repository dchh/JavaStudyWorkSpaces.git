package demo3.service;

/**
 * AccountBookService 接口类
 *
 * @author Anna.
 * @date 2025/2/11 20:02
 */
public interface IAccountBookService {

    /**
     * 转账
     */
    void transferAccounts();
}
