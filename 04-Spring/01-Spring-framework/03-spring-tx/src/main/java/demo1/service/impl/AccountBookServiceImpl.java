package demo1.service.impl;

import demo1.dao.IAccountBookDao;
import demo1.entity.AccountBookDo;
import demo1.service.IAccountBookService;

/**
 * AccountBookService 接口实现类
 *
 * @author Anna.
 * @date 2025/2/11 20:03
 */
public class AccountBookServiceImpl implements IAccountBookService {

    private IAccountBookDao accountBookDao;

    public void setAccountBookDao(IAccountBookDao accountBookDao) {
        this.accountBookDao = accountBookDao;
    }

    @Override
    public void transferAccounts() {
        // 1. 查询账户zhangsan
        AccountBookDo zhangsan = accountBookDao.selectAccountBookByName("zhangsan");
        // 2.查询账户lisi
        AccountBookDo lisi = accountBookDao.selectAccountBookByName("lisi");
        // 定义金融
        float money = 100f;
        // 3.账户zhangsan - 100
        accountBookDao.updateAccountBook(zhangsan.getMoney() - money, zhangsan.getName());

        // 运行异常导致事物不一致
        int i = 1 / 0;

        // 4.账户lisi + 100
        accountBookDao.updateAccountBook(lisi.getMoney() + money, lisi.getName());

        System.out.println("执行完成");
    }
}
