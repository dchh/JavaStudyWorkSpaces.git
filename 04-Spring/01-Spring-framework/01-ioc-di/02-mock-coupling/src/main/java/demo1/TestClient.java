package demo1;

import demo1.service.IAccountService;
import demo1.service.impl.AccountServiceImpl;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/26 23:41
 */
public class TestClient {
    public static void main(String[] args) {
        // 创建Service
        IAccountService accountService = new AccountServiceImpl();
        accountService.saveAccount();
    }
}
