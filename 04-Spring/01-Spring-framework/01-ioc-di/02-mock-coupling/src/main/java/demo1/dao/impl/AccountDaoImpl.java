package demo1.dao.impl;

import demo1.dao.IAccountDao;

/**
 * DAO 接口实现类
 *
 * @author Anna.
 * @date 2024/4/26 23:40
 */
public class AccountDaoImpl implements IAccountDao {

    @Override
    public void saveAccount() {
        System.out.println("Account 保存成功");
    }
}
