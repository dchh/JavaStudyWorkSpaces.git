package demo3;

import demo3.dao.IAccountDao;
import demo3.service.IAccountService;
import demo3.utils.BeanFactory;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/26 23:41
 */
public class TestClient {
    public static void main(String[] args) {
        // 创建Service
        IAccountService accountService = (IAccountService) BeanFactory.getBean("accountService");
        accountService.saveAccount();
    }
}
