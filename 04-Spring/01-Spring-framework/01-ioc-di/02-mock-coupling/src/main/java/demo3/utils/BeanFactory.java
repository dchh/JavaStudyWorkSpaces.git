package demo3.utils;

import java.io.InputStream;
import java.util.*;

/**
 * 创建Bean对象
 *   bean 在计算机英语中有可重用组件的含义
 *
 * @author Anna.
 * @date 2024/4/27 1:29
 */
public class BeanFactory {
    // 定义一个静态Properties
    private static Properties prop;
    // 定义Map存储反射实例
    private static Map<String, Object> beans = new HashMap<String, Object>();

    static {
        // 实例化对象
        prop = new Properties();
        // 获取文件流
        try (InputStream is = BeanFactory.class.getClassLoader().getResourceAsStream("beans3.properties");) {
            // 加载prop
            prop.load(is);
            Enumeration<Object> keys = prop.keys();
            while(keys.hasMoreElements()){
                String key =  keys.nextElement().toString();
                // 单例情况下，存在依赖时，必须先初始化依赖
                if("accountService".equals(key)){
                    beans.put("accountDao", Class.forName(prop.getProperty("accountDao")).getDeclaredConstructor().newInstance());
                }
                String path = prop.getProperty(key);
                Object obj = null;
                Class<?> aClass = Class.forName(path);
                obj = aClass.getDeclaredConstructor(null).newInstance();
                beans.put(key, obj);
            }
            System.out.println(beans);
        } catch (Exception e) {
            System.err.println("初始化Properties失败");
        }
    }

    /**
     * 根据bean名称获取实例对象
     *
     * @param beanName
     * @return java.lang.Object
     * @author Anna.
     * @date 2024/4/27 1:34
     */
    public static Object getBean(String beanName) {
        return beans.get(beanName);
    }
}
