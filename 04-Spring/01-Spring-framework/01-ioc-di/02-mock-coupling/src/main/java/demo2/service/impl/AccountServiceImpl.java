package demo2.service.impl;

import demo2.dao.IAccountDao;
import demo2.service.IAccountService;
import demo2.utils.BeanFactory;

/**
 * SERVICE 接口实现类
 *
 * @author Anna.
 * @date 2024/4/26 23:39
 */
public class AccountServiceImpl implements IAccountService {

    IAccountDao accountDao = (IAccountDao) BeanFactory.getBean("accountDao");

    @Override
    public void saveAccount() {
        accountDao.saveAccount();
    }
}
