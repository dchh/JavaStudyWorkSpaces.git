package demo2.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 创建Bean对象
 *
 * @author Anna.
 * @date 2024/4/27 1:29
 */
public class BeanFactory {
    // 定义一个静态Properties
    private static Properties prop;

    static {
        // 实例化对象
        prop = new Properties();
        // 获取文件流
        try (InputStream is = BeanFactory.class.getClassLoader().getResourceAsStream("beans.properties");){
            // 加载prop
            prop.load(is);
        } catch (IOException e) {
            System.err.println("初始化Properties失败");
        }
    }

    /**
     * 根据bean名称获取实例对象
     *
     * @param beanName
     * @return java.lang.Object
     * @author Anna.
     * @date 2024/4/27 1:34
     */
    public static Object getBean(String beanName){
        String beanPath= prop.getProperty(beanName);
        Object obj = null;
        try {
            Class<?> aClass = Class.forName(beanPath);
            obj = aClass.getDeclaredConstructor(null).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return obj;
    }
}
