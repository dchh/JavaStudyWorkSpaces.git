spring bean的相关介绍
demo1：spring创建和管理bean
deno2: BeanFactory与ApplicationContext区别
demo3:ApplicationContext的三个常用实例
demo4:IOC 中 bean 标签和管理对象细节
demo5:spring bean的注入方式
demo6:使用 p 名称空间注入数据
demo7:测试案例-@Component注解使用
demo8:测试案例-@Controller @Service @Repository @Autowired注解使用
demo9:测试案例- @Autowired 自动按照类型注入时，Spring核心容器中包含两个相同类型
demo10:测试案例- @Autowired 自动按照类型注入时，Spring核心容器中包含两个相同类型-解决方案
demo11:创建Bean的三种方式
demo12:注解的综合运用
