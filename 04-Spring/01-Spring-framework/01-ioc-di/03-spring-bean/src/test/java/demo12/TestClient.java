package demo12;

import demo12.config.SpringConfig;
import demo12.controller.AccountController;
import demo12.entity.AccountDo;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestClient {

    @Test
    public void testQuery(){
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        AccountController accountController = (AccountController) context.getBean("accountController");
        AccountDo accountDo = accountController.findAccountDoById(1);
        System.out.println(accountDo);
    }

}
