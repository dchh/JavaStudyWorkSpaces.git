package demo12.service;

import demo12.entity.AccountDo;

/**
 * SERVICE 接口
 *
 * @author Anna.
 * @date 2024/4/26 23:38
 */
public interface IAccountService {
    AccountDo findAccountDoById(Integer id);
}
