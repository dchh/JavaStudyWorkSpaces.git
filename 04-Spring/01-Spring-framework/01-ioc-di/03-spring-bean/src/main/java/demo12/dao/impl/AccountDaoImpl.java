package demo12.dao.impl;

import demo12.dao.IAccountDao;
import demo12.entity.AccountDo;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * DAO 接口实现类
 *
 * @author Anna.
 * @date 2024/4/26 23:40
 */
@Repository
public class AccountDaoImpl implements IAccountDao {

    @Autowired
    private QueryRunner runner;

    @Override
    public AccountDo findAccountDoById(Integer id) {
        try {
            return runner.query("select * from account where id = ? ", new BeanHandler<AccountDo>(AccountDo.class), id);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<AccountDo> findAllAccount() {
        try{
            return runner.query("select * from account",new BeanListHandler<AccountDo>(AccountDo.class));
        }catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public void saveAccount(AccountDo account) {
        try{
            runner.update("insert into account(account,name,createDate)values(?,?,?)",account.getAccount(),account.getName(),account.getCreateDate());
        }catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateAccount(AccountDo account) {
        try{
            runner.update("update account set account=?,name=? where id=?",account.getAccount(),account.getName(),account.getId());
        }catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteAccount(Integer id) {
        try{
            runner.update("delete from account where id=?",id);
        }catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
