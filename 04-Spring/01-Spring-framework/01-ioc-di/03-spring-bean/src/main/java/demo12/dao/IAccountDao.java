package demo12.dao;

import demo12.entity.AccountDo;

import java.util.List;

/**
 * DAO 接口
 *
 * @author Anna.
 * @date 2024/4/26 23:40
 */
public interface IAccountDao {
    AccountDo findAccountDoById(Integer id);

    /**
     * 查询所有
     * @return
     */
    List<AccountDo> findAllAccount();


    /**
     * 保存
     * @param account
     */
    void saveAccount(AccountDo account);

    /**
     * 更新
     * @param account
     */
    void updateAccount(AccountDo account);

    /**
     * 删除
     * @param id
     */
    void deleteAccount(Integer id);
}
