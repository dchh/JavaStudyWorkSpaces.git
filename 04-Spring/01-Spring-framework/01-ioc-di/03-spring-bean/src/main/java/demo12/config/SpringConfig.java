package demo12.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackages = "demo12")
@Import(DataSourceConfig.class)
public class SpringConfig {
}
