package demo12;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

public class TestClient {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext("demo12");
        String[] beanDefinitionNames = context.getBeanDefinitionNames();
        System.out.println("args = " + beanDefinitionNames);
    }
}
