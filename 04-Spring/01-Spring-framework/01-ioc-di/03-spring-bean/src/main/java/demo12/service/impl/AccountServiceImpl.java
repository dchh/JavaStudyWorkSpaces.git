package demo12.service.impl;

import demo12.dao.IAccountDao;
import demo12.entity.AccountDo;
import demo12.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * SERVICE 接口实现类
 *
 * @author Anna.
 * @date 2024/4/26 23:39
 */
@Service
public class AccountServiceImpl implements IAccountService {

    @Autowired
    private IAccountDao accountDao;

    @Override
    public AccountDo findAccountDoById(Integer id) {
        return accountDao.findAccountDoById(id);
    }
}
