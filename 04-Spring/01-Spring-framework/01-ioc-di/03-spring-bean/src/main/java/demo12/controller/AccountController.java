package demo12.controller;

import demo12.entity.AccountDo;
import demo12.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class AccountController {

    @Autowired
    private IAccountService accountService;

    public AccountDo findAccountDoById(Integer id) {
        return accountService.findAccountDoById(id);
    }
}
