package demo4;

import demo4.entity.AccountDo;
import demo4.entity.AccountDo2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/26 23:41
 */
public class TestClient2 {
    public static void main(String[] args) {
        // 通过ClassPathXmlApplicationContext获取ApplicationContext
        System.out.println("=======获取ApplicationContext=======");
        ClassPathXmlApplicationContext ac = new ClassPathXmlApplicationContext("bean4-02.xml");
        System.out.println("======scope:singleton单例=========");
        AccountDo accountDo1 = (AccountDo) ac.getBean("accountDo");
        AccountDo accountDo2 = (AccountDo) ac.getBean("accountDo");
        System.out.printf("对比获取bean是否是同一对象：%s%n",accountDo1 == accountDo2);
        System.out.printf("手动关闭容器-触发销毁方法%n");
        ac.destroy();
        System.out.println("");
        System.out.println("=======获取ApplicationContext =======");
        ClassPathXmlApplicationContext ac2 = new ClassPathXmlApplicationContext("bean4-02.xml");
        System.out.println("======scope:prototype多例=========");
        AccountDo2 accountDo3 = (AccountDo2) ac2.getBean("accountDo2");
        AccountDo2 accountDo4 = (AccountDo2) ac2.getBean("accountDo2");
        System.out.printf("对比获取bean是否是同一对象：%s%n",accountDo3 == accountDo4);
        System.out.printf("手动关闭容器-触发销毁方法%n");
        ac2.destroy();
        System.out.printf("手动执行GC%n");
    }
}
