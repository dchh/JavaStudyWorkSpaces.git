package demo4.entity;

/**
 * UserDo
 * 提供有参构造方法，但是不重新无参构造方法
 * @author Anna.
 * @date 2024/4/28 11:14
 */
public class UserDo {

    private String name;

    public UserDo(String name) {
        this.name = name;
    }
}
