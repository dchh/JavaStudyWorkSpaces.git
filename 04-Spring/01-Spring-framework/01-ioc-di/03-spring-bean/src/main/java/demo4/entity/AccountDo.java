package demo4.entity;

import java.util.Date;

/**
 * AccountDo
 *
 * @author Anna.
 * @date 2024/4/28 15:10
 */
public class AccountDo {

    private Integer id;

    private String name;

    private Date createDate;

    public AccountDo() {
        System.out.println("执行 AccountDo 无参构造方法");
    }

    public void init(){
        System.out.println("执行 AccountDo - init()方法");
    }

    public void destroy(){
        System.out.println("执行 AccountDo - destroy()方法");
    }
}
