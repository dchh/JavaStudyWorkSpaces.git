package demo4.entity;

import java.util.Date;

/**
 * AccountDo
 *
 * @author Anna.
 * @date 2024/4/28 15:10
 */
public class AccountDo2 {

    private Integer id;

    private String name;

    private Date createDate;

    public AccountDo2() {
        System.out.println("执行 AccountDo2 无参构造方法");
    }

    public void init(){
        System.out.println("执行 AccountDo2 -init()方法");
    }

    public void destroy(){
        System.out.println("执行 AccountDo2 -destroy()方法");
    }
}
