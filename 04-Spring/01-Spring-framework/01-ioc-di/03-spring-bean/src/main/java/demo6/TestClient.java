package demo6;

import demo6.entity.AccountDo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/26 23:41
 */
public class TestClient {
    public static void main(String[] args) {
        // 通过ClassPathXmlApplicationContext获取ApplicationContext
        System.out.println("=======1. 通过ClassPathXmlApplicationContext 获取ApplicationContext =======");
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean6-01.xml");
        AccountDo accountDo = (AccountDo) applicationContext.getBean("accountDo");
        System.out.println(accountDo);
    }
}
