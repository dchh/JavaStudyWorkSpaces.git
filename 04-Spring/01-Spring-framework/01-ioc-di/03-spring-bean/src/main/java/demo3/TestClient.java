package demo3;

import demo3.service.IAccountService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/26 23:41
 */
public class TestClient {
    public static void main(String[] args) {
        // 通过ClassPathXmlApplicationContext获取ApplicationContext
        System.out.println("=======1. 通过ClassPathXmlApplicationContext 获取ApplicationContext =======");
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean3.xml");
        System.out.println("=======2. 获取AccountService =======");
        // 获取AccountDao
        IAccountService accountService = (IAccountService) applicationContext.getBean("accountService");
        System.out.println("accountService = " + accountService);
        System.out.println("");
        System.out.println("================================================================================");
        System.out.println("");
        System.out.println("=======1. 通过FileSystemXmlApplicationContext 获取ApplicationContext =======");
        ApplicationContext applicationContext2 = new FileSystemXmlApplicationContext("D:/temp/bean3.xml");
        System.out.println("=======2. 获取AccountService =======");
        // 获取AccountDao
        IAccountService accountService2 = (IAccountService) applicationContext.getBean("accountService");
        System.out.println("accountService2 = " + accountService2);
    }
}
