package demo3.service.impl;

import demo3.dao.IAccountDao;
import demo3.dao.impl.AccountDaoImpl;
import demo3.service.IAccountService;

/**
 * SERVICE 接口实现类
 *
 * @author Anna.
 * @date 2024/4/26 23:39
 */
public class AccountServiceImpl implements IAccountService {

    IAccountDao accountDao = new AccountDaoImpl();

    public AccountServiceImpl() {
        System.out.println("AccountServiceImpl 被创建了");
    }

    @Override
    public void saveAccount() {
        accountDao.saveAccount();
    }
}
