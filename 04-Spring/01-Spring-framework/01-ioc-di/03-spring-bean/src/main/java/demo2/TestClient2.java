package demo2;

import demo2.service.IAccountService;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/26 23:41
 */
public class TestClient2 {
    public static void main(String[] args) {
        // 获取BeanFactory获取获取spring bean创建并管理的对象
        System.out.println("=======1. 通过BeanFactory获取获取spring bean创建并管理的对象 =======");
        // 该方式已不推荐使用，这里只为展示区别使用
        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("bean2.xml"));
        System.out.println("=======2. 获取AccountService =======");
        // 获取AccountDao
        IAccountService accountService = (IAccountService) beanFactory.getBean("accountService");
        System.out.println("accountService = " + accountService);
    }
}
