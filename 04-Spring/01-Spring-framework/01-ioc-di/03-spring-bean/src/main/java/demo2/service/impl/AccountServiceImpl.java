package demo2.service.impl;

import demo2.dao.IAccountDao;
import demo2.dao.impl.AccountDaoImpl;
import demo2.service.IAccountService;

/**
 * SERVICE 接口实现类
 *
 * @author Anna.
 * @date 2024/4/26 23:39
 */
public class AccountServiceImpl implements IAccountService {

    IAccountDao accountDao = new AccountDaoImpl();

    public AccountServiceImpl() {
        System.out.println("AccountServiceImpl 被创建了");
    }

    @Override
    public void saveAccount() {
        accountDao.saveAccount();
    }
}
