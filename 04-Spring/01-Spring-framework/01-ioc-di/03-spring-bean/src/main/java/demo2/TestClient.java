package demo2;

import demo2.service.IAccountService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/26 23:41
 */
public class TestClient {
    public static void main(String[] args) {
        // 通过ApplicationContext获取spring bean创建并管理的对象
        System.out.println("=======1. 通过ApplicationContext获取spring bean创建并管理的对象 =======");
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean2.xml");
        System.out.println("=======2. 获取AccountService =======");
        // 获取AccountDao
        IAccountService accountService = (IAccountService) applicationContext.getBean("accountService");
        System.out.println("accountService = " + accountService);
    }
}
