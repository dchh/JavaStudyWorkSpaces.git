package demo9.service.impl;

import demo9.dao.IAccountDao;
import demo9.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * SERVICE 接口实现类
 *
 * @author Anna.
 * @date 2024/4/26 23:39
 */
@Service
public class AccountServiceImpl implements IAccountService {

//    @Autowired
//    private IAccountDao accountDao;

    @Override
    public void saveAccount() {
//        accountDao.saveAccount();
    }
}
