package demo1.dao;

/**
 * DAO 接口
 *
 * @author Anna.
 * @date 2024/4/26 23:40
 */
public interface IAccountDao {
    void saveAccount();
}
