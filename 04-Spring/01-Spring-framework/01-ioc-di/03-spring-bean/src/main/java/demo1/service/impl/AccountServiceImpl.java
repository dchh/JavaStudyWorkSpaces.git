package demo1.service.impl;

import demo1.dao.IAccountDao;
import demo1.dao.impl.AccountDaoImpl;
import demo1.service.IAccountService;

/**
 * SERVICE 接口实现类
 *
 * @author Anna.
 * @date 2024/4/26 23:39
 */
public class AccountServiceImpl implements IAccountService {

    IAccountDao accountDao = new AccountDaoImpl();

    @Override
    public void saveAccount() {
        accountDao.saveAccount();
    }
}
