package demo1;

import demo1.dao.IAccountDao;
import demo1.service.IAccountService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/26 23:41
 */
public class TestClient {
    public static void main(String[] args) {
        // 获取通过 ClassPathXmlApplicationContext（默认在resources目录下查找文件） 获取 ApplicationContext
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean.xml");
        // 创建Service
        IAccountService accountService = (IAccountService) applicationContext.getBean("accountService");
        IAccountDao accountDao = (IAccountDao) applicationContext.getBean("accountDao");
        System.out.println("accountService = " + accountService);
        System.out.println("accountDao = " + accountDao);
    }
}
