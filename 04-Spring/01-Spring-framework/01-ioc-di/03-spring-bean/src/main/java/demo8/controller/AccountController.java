package demo8.controller;

import demo8.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class AccountController {

    @Autowired
    private IAccountService accountService;

    public void saveAccount(){
        accountService.saveAccount();
    }
}
