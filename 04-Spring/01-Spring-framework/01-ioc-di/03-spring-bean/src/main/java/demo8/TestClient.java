package demo8;

import demo8.controller.AccountController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/5/8 16:36
 */
public class TestClient {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext("demo8");
        AccountController accountController = (AccountController) context.getBean("accountController");
        accountController.saveAccount();
    }
}
