package demo8.service.impl;

import demo8.dao.IAccountDao;
import demo8.dao.impl.AccountDaoImpl;
import demo8.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * SERVICE 接口实现类
 *
 * @author Anna.
 * @date 2024/4/26 23:39
 */
@Service
public class AccountServiceImpl implements IAccountService {

    @Autowired
    private IAccountDao accountDao;

    @Override
    public void saveAccount() {
        accountDao.saveAccount();
    }
}
