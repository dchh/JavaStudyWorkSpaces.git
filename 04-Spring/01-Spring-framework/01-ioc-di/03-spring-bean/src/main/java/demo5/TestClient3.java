package demo5;

import demo5.entity.AccountDo2;
import demo5.entity.AccountDo3;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/26 23:41
 */
public class TestClient3 {
    public static void main(String[] args) {
        // 通过ClassPathXmlApplicationContext获取ApplicationContext
        System.out.println("=======1. 通过ClassPathXmlApplicationContext 获取ApplicationContext =======");
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean5-03.xml");
        // 输出AccountDo2
        System.out.println("=======set方法注入 - 复杂类型/集合类型注入 =======");
        AccountDo3 accountDo3 = (AccountDo3) applicationContext.getBean("accountDo3");
        System.out.println(accountDo3);

    }
}
