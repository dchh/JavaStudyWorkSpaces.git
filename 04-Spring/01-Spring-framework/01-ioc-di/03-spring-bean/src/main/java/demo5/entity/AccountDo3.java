package demo5.entity;

import java.util.*;

/**
 * AccountDo
 *
 * @author Anna.
 * @date 2024/4/28 15:10
 */
public class AccountDo3 {

    private String[] arr;
    private List<String> strList;
    private Set<String> strSet;
    private Map<String,String> map;
    private Properties props;

    public void setArr(String[] arr) {
        this.arr = arr;
    }

    public void setStrList(List<String> strList) {
        this.strList = strList;
    }

    public void setStrSet(Set<String> strSet) {
        this.strSet = strSet;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }

    public void setProps(Properties props) {
        this.props = props;
    }

    @Override
    public String toString() {
        return "AccountDo3{" +
                "arr=" + Arrays.toString(arr) +
                ", strList=" + strList +
                ", strSet=" + strSet +
                ", map=" + map +
                ", props=" + props +
                '}';
    }
}
