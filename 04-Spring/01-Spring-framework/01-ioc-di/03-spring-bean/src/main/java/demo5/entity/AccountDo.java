package demo5.entity;

import java.util.Date;

/**
 * AccountDo
 *
 * @author Anna.
 * @date 2024/4/28 15:10
 */
public class AccountDo {

    private Integer id;

    private String name;

    private Date createDate;

    public AccountDo(Integer id, String name, Date createDate) {
        System.out.println("使用构造函数注入");
        this.id = id;
        this.name = name;
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return "AccountDo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", createDate=" + createDate +
                '}';
    }
}
