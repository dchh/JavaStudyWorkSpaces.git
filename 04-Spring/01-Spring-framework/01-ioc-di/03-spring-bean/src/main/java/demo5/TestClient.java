package demo5;

import demo5.entity.AccountDo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/26 23:41
 */
public class TestClient {
    public static void main(String[] args) {
        // 通过ClassPathXmlApplicationContext获取ApplicationContext
        System.out.println("=======1. 通过ClassPathXmlApplicationContext 获取ApplicationContext =======");
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("bean5-01.xml");
        // 输出AccountDo、
        System.out.println("=======构造函数注入 - type =======");
        AccountDo accountDo = (AccountDo) applicationContext.getBean("accountDo");
        System.out.println(accountDo);

        System.out.println("=======构造函数注入 - index =======");
        AccountDo accountDo1 = (AccountDo) applicationContext.getBean("accountDo1");
        System.out.println(accountDo1);

        System.out.println("=======构造函数注入 - name =======");
        AccountDo accountDo2 = (AccountDo) applicationContext.getBean("accountDo2");
        System.out.println(accountDo2);
    }
}
