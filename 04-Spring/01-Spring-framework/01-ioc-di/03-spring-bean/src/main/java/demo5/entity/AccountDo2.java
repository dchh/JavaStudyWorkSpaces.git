package demo5.entity;

import java.util.Date;

/**
 * AccountDo
 *
 * @author Anna.
 * @date 2024/4/28 15:10
 */
public class AccountDo2 {

    private Integer id;

    private String name;

    private Date createDate;

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return "AccountDo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", createDate=" + createDate +
                '}';
    }
}
