package demo11.factory;


import demo11.dao.IAccountDao;
import demo11.dao.impl.AccountDaoImpl;

public class StaticFactory {

    public static IAccountDao getAccountService(){
        System.out.println("使用类中的静态方法创建bean");
        return new AccountDaoImpl();
    }
}
