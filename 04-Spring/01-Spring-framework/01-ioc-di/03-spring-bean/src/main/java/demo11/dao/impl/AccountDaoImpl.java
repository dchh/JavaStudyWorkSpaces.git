package demo11.dao.impl;

import demo11.dao.IAccountDao;
import org.springframework.stereotype.Repository;

/**
 * DAO 接口实现类
 *
 * @author Anna.
 * @date 2024/4/26 23:40
 */
public class AccountDaoImpl implements IAccountDao {

    public AccountDaoImpl() {
        System.out.println(" 调用 AccountDaoImpl 默认构造函数");
    }

    @Override
    public void saveAccount() {
        System.out.println("AccountDaoImpl Account 保存成功");
    }
}
