package demo11.factory;


import demo11.dao.IAccountDao;
import demo11.dao.impl.AccountDaoImpl;

public class InstanceFactory {

    public IAccountDao getAccountService(){
        System.out.println("使用类中方法创建Bean");
        return new AccountDaoImpl();
    }
}
