package demo7;

import demo7.entity.AccountDo;
import demo7.entity.AccountDo2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/26 23:41
 */
public class TestClient2 {
    public static void main(String[] args) {
        // 通过AnnotationConfigApplicationContext获取ApplicationContext
        System.out.println("=======1. 通过AnnotationConfigApplicationContext 获取ApplicationContext =======");
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext("demo7.entity");
        AccountDo accountDo = (AccountDo) applicationContext.getBean("accountDo");
        System.out.println(accountDo);
        AccountDo2 accountDo2 = (AccountDo2) applicationContext.getBean("account2");
        System.out.println(accountDo2);
    }
}
