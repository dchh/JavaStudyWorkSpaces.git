package demo10.service.impl;

import demo10.dao.IAccountDao;
import demo10.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * SERVICE 接口实现类
 *
 * @author Anna.
 * @date 2024/4/26 23:39
 */
@Service
public class AccountServiceImpl2 implements IAccountService {

    @Autowired
    @Qualifier("accountDaoImpl2")
    private IAccountDao accountDao;

    @Override
    public void saveAccount() {
        accountDao.saveAccount();
    }
}
