package demo10.service;

/**
 * SERVICE 接口
 *
 * @author Anna.
 * @date 2024/4/26 23:38
 */
public interface IAccountService {
    void saveAccount();
}
