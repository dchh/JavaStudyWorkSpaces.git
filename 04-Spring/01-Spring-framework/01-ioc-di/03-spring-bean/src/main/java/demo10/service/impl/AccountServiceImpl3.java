package demo10.service.impl;

import demo10.dao.IAccountDao;
import demo10.service.IAccountService;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * SERVICE 接口实现类
 *
 * @author Anna.
 * @date 2024/4/26 23:39
 */
@Service("accountServiceImpl3")
public class AccountServiceImpl3 implements IAccountService {

    @Resource(name = "accountDaoImpl2")
    private IAccountDao accountDao;

    @Override
    public void saveAccount() {
        accountDao.saveAccount();
    }
}
