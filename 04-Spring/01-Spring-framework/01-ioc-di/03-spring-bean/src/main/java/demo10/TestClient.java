package demo10;

import demo10.service.IAccountService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/5/8 16:36
 */
public class TestClient {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext("demo10");
        IAccountService accountService = (IAccountService) context.getBean("accountServiceImpl");
        accountService.saveAccount();
    }
}
