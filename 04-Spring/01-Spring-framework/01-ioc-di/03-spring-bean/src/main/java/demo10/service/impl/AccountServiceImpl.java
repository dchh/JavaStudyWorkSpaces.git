package demo10.service.impl;

import demo10.dao.IAccountDao;
import demo10.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * SERVICE 接口实现类
 *
 * @author Anna.
 * @date 2024/4/26 23:39
 */
@Service
public class AccountServiceImpl implements IAccountService {

    @Autowired
    private IAccountDao accountDaoImpl2;

    @Override
    public void saveAccount() {
        accountDaoImpl2.saveAccount();
    }
}
