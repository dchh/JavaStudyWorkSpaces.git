package demo10.dao.impl;

import demo10.dao.IAccountDao;
import org.springframework.stereotype.Repository;

/**
 * DAO 接口实现类
 *
 * @author Anna.
 * @date 2024/4/26 23:40
 */
@Repository
public class AccountDaoImpl2 implements IAccountDao {

    @Override
    public void saveAccount() {
        System.out.println("AccountDaoImpl2 Account 保存成功");
    }
}
