package demo2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * 测试使用Class.forName反射注册驱动
 *
 * @author Anna.
 * @date 2024/4/26 22:53
 */
public class OldJdbcClientTest2 {
    public static void main(String[] args) throws Exception {
        //1.注册驱动
//        DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
        Class.forName("com.mysql.cj.jdbc.Driver");
        //2.获取连接
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/testdb","root","root");
        //3.获取预处理 sql 语句对象
        PreparedStatement preparedStatement = connection.prepareStatement("select * from account;");
        //4.获取结果集
        ResultSet resultSet = preparedStatement.executeQuery();
        //5.遍历结果集
        while(resultSet.next()){
            System.out.printf("id:%s-account:%s-name:%s-createDate:%s%n",
                    resultSet.getInt("id"),
                    resultSet.getString("account"),
                    resultSet.getString("name"),
                    resultSet.getDate("createDate"));
        }
        //6.释放资源
        preparedStatement.close();
        connection.close();
    }
}
