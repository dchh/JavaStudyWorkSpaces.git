# AOP与事务

## 案例
### 转账运行时异常导致事物不一致问题
业务逻辑：
1. 获取张三账户信息
2. 获取李四账户信息
3. 张三账户扣除100
4. 1/0 模拟运行时异常
5. 李四账户加100

**数据库：**
accountbook.sql
```sql
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS `accountbook`;
CREATE TABLE `accountbook`  (
                              `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
                              `money` float NULL DEFAULT NULL,
                              PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

INSERT INTO `accountbook` VALUES ('lisi', 1000);
INSERT INTO `accountbook` VALUES ('zhangsan', 1000);

SET FOREIGN_KEY_CHECKS = 1;
```
**代码：**
AccountBookDo.java
```java
package demo1.entity;

public class AccountBookDo {
  private String name;
  private float money;
  // ... GET/SET
}
```
IAccountBookDao.java
```java
package demo1.dao;

import demo1.entity.AccountBookDo;

public interface IAccountBookDao {

    AccountBookDo selectAccountBookByName(String name);

    void updateAccountBook(float money,String name);
}
```
AccountBookDaoImpl.java
```java
package demo1.dao.impl;

import demo1.dao.IAccountBookDao;
import demo1.entity.AccountBookDo;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

public class AccountBookDaoImpl implements IAccountBookDao {

    private QueryRunner runner;

    public AccountBookDaoImpl(QueryRunner runner) {
        this.runner = runner;
    }

    @Override
    public AccountBookDo selectAccountBookByName(String name) {
        try {
            return runner.query("select * from accountbook where name = ? ", new BeanHandler<AccountBookDo>(AccountBookDo.class), name);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateAccountBook(float money, String name) {
        try {
            runner.update("update accountbook set money=? where name=?", money, name);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
```
IAccountBookService.java
```java
package demo1.service;

import demo1.entity.AccountBookDo;
public interface IAccountBookService {

    AccountBookDo selectAccountBookByName(String name);

    void updateAccountBook(float money,String name);
}

```
AccountBookServiceImpl.java
```java
package demo1.service.impl;

import demo1.dao.IAccountBookDao;
import demo1.entity.AccountBookDo;
import demo1.service.IAccountBookService;

public class AccountBookServiceImpl implements IAccountBookService {

    private IAccountBookDao accountBookDao;

    public void setAccountBookDao(IAccountBookDao accountBookDao) {
        this.accountBookDao = accountBookDao;
    }

    @Override
    public AccountBookDo selectAccountBookByName(String name) {
        return accountBookDao.selectAccountBookByName(name);
    }

    @Override
    public void updateAccountBook(float money, String name) {
        accountBookDao.updateAccountBook(money, name);
    }
}

```

**配置文件：**
demo1.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">
    <!-- 配置MySQL数据源 -->
    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://127.0.0.1:3306/testdb"/>
        <property name="username" value="root"/>
        <property name="password" value="root"/>
    </bean>
    <!--配置QueryRunner-->
    <bean id="queryRunner" class="org.apache.commons.dbutils.QueryRunner">
        <constructor-arg ref="dataSource"/>
    </bean>
    <!-- 配置DAO -->
    <bean id="accountBookDao" class="demo1.dao.impl.AccountBookDaoImpl">
        <constructor-arg ref="queryRunner"></constructor-arg>
    </bean>
    <!--配置Service-->
    <bean id="accountService" class="demo1.service.impl.AccountBookServiceImpl">
        <property name="accountBookDao" ref="accountBookDao"></property>
    </bean>
</beans>
```

**测试类：**
TestClient.java
```java
package demo1;

import demo1.entity.AccountBookDo;
import demo1.service.IAccountBookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.Assert;

import java.util.Arrays;

public class TestClient {
  public static void main(String[] args) {
    ApplicationContext ac = new ClassPathXmlApplicationContext("demo1.xml");
    // 获取service
    IAccountBookService accountService = (IAccountBookService) ac.getBean("accountService");
    // 1. 查询账户zhangsan
    AccountBookDo zhangsan = accountService.selectAccountBookByName("zhangsan");
    // 2.查询账户lisi
    AccountBookDo lisi = accountService.selectAccountBookByName("lisi");
    // 定义金融
    float money = 100f;
    // 3.账户zhangsan - 100
    accountService.updateAccountBook(zhangsan.getMoney() - money, zhangsan.getName());

    // 运行异常导致事物不一致
    int i = 1 / 0;

    // 4.账户lisi + 100
    accountService.updateAccountBook(lisi.getMoney() + money, lisi.getName());

    System.out.println("执行完成");
  }
}
```

**执行结果：**
![img_1.png](images/img_1.png)
数据库：
![img.png](images/img.png)

> 从运行结果看，由于事务不一致导致，数据库结果与预期不符。

### 使用本地线程ThreadLocal处理转账运行时异常导致事物不一致问题
这里只演示ThreadLocal处理转账运行时异常导致事物不一致问题的基本方案，对不工具类进行进一步封装处理(不使用单例及考虑多线程情况)

**工具类：**
ConnectionUtils.java
```java
package demo2.utils;

import com.mysql.jdbc.Connection;

import javax.sql.DataSource;
import java.sql.SQLException;

public class ConnectionUtils {
    private ThreadLocal<Connection> local = new ThreadLocal<>();

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Connection getThreadConnection() {
        // 1 先从ThreadLocal中获取
        Connection connection = local.get();
        // 2 判断当前线程是否有链接
        if(connection == null) {
            // 3 从数据源中获取一个链接，并且设置到ThreadLocal中
            try {
                connection = (Connection) dataSource.getConnection();
                local.set(connection);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        // 4 返回当前线程上的连接
        return connection;
    }

    /**
     * 把链接和线程解绑
     */
    public void removeThreadConnection(){
        local.remove();
    }
}
```

> 连接的工具类，用于实现从数据源中获取一个链接，并且实现和线程的绑定与解绑

TransactionManager.java
```java
package demo2.utils;

import java.sql.SQLException;

public class TransactionManager {

    private ConnectionUtils connectionUtils;

    public void setConnectionUtils(ConnectionUtils connectionUtils) {
        this.connectionUtils = connectionUtils;
    }

    /**
     * 开启事务
     */
    public void beginTransaction() throws SQLException {
        connectionUtils.getThreadConnection().setAutoCommit(false);
    }

    /**
     * 提交事务
     */
    public void commit() throws SQLException {
        connectionUtils.getThreadConnection().commit();
    }

    /**
     * 回滚事务
     */
    public void rollback(){
        try {
            connectionUtils.getThreadConnection().rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 释放连接
     */
    public void release() {
        try {
            connectionUtils.getThreadConnection().close();
            // 把链接和线程解绑
            connectionUtils.removeThreadConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
```
> 事物管理相关工具类

**DAO/SERVICE改造：**
AccountBookDaoImpl.java
```java
package demo2.dao.impl;

import demo2.dao.IAccountBookDao;
import demo2.entity.AccountBookDo;
import demo2.utils.ConnectionUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.sql.SQLException;

/**
 * AccountBookDao 实现类
 *
 * @author Anna.
 * @date 2025/2/11 19:42
 */
public class AccountBookDaoImpl implements IAccountBookDao {

    private QueryRunner runner;

    private ConnectionUtils connectionUtils;

    public AccountBookDaoImpl(QueryRunner runner, ConnectionUtils connectionUtils) {
        this.runner = runner;
        this.connectionUtils = connectionUtils;
    }

    @Override
    public AccountBookDo selectAccountBookByName(String name) throws SQLException {
        return runner.query(connectionUtils.getThreadConnection(), "select * from accountbook where name = ? ", new BeanHandler<AccountBookDo>(AccountBookDo.class), name);
    }

    @Override
    public void updateAccountBook(float money, String name) throws SQLException {
        runner.update(connectionUtils.getThreadConnection(), "update accountbook set money=? where name=?", money, name);
    }
}
```
IAccountBookService.java
```java
package demo2.service;

import demo2.entity.AccountBookDo;

import java.sql.SQLException;

public interface IAccountBookService {

    /**
     * 查询账单
     * @param name
     * @return
     */
    AccountBookDo selectAccountBookByName(String name) throws SQLException;

    /**
     * 更新账单
     * @param money
     * @param name
     */
    void updateAccountBook(float money,String name) throws SQLException;

    /**
     * 转账
     */
    void transferAccounts();
}
```
AccountBookServiceImpl.java
```java
package demo2.service.impl;

import demo2.dao.IAccountBookDao;
import demo2.entity.AccountBookDo;
import demo2.service.IAccountBookService;
import demo2.utils.TransactionManager;

import java.sql.SQLException;

public class AccountBookServiceImpl implements IAccountBookService {

    private IAccountBookDao accountBookDao;

    private TransactionManager txManager;

    public void setAccountBookDao(IAccountBookDao accountBookDao) {
        this.accountBookDao = accountBookDao;
    }

    public void setTxManager(TransactionManager txManager) {
        this.txManager = txManager;
    }

    @Override
    public AccountBookDo selectAccountBookByName(String name) throws SQLException {
        return accountBookDao.selectAccountBookByName(name);
    }

    @Override
    public void updateAccountBook(float money, String name) throws SQLException {
        accountBookDao.updateAccountBook(money, name);
    }

    @Override
    public void transferAccounts() {
        // 1 开启事务
        try {
            txManager.beginTransaction();
            // 2 执行操作
            // 2.1. 查询账户zhangsan
            AccountBookDo zhangsan = this.selectAccountBookByName("zhangsan");
            // 2.2.查询账户lisi
            AccountBookDo lisi = this.selectAccountBookByName("lisi");
            // 定义金融
            float money = 100f;
            // 2.3.账户zhangsan - 100
            this.updateAccountBook(zhangsan.getMoney() - money, zhangsan.getName());
            System.out.println("账户zhangsan - 100");
            // 运行异常导致事物不一致
            int i = 1 / 0;

            // 2.4.账户lisi + 100
            this.updateAccountBook(lisi.getMoney() + money, lisi.getName());
            System.out.println("账户lisi + 100");
            System.out.println("执行完成");
            // 3 提交事物
            txManager.commit();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("执行回退");
            // 4 回滚事务
            txManager.rollback();
        }
        finally {
            // 5 关闭连接
            txManager.release();
        }
    }

}
```

**配置类：**
demo2.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

    <!-- 配置MySQL数据源 -->
    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://127.0.0.1:3306/testdb"/>
        <property name="username" value="root"/>
        <property name="password" value="root"/>
    </bean>

    <!--配置QueryRunner-->
    <bean id="queryRunner" class="org.apache.commons.dbutils.QueryRunner"></bean>

    <!-- 配置DAO -->
    <bean id="accountBookDao" class="demo2.dao.impl.AccountBookDaoImpl">
        <constructor-arg ref="queryRunner"></constructor-arg>
        <constructor-arg ref="connectionUtils"></constructor-arg>
    </bean>
    <!--配置Service-->
    <bean id="accountService" class="demo2.service.impl.AccountBookServiceImpl">
        <property name="accountBookDao" ref="accountBookDao"></property>
        <property name="txManager" ref="transactionManager"></property>
    </bean>
    
    <!--配置ConnectionUtils-->
    <bean id="connectionUtils" class="demo2.utils.ConnectionUtils">
        <property name="dataSource" ref="dataSource"></property>
    </bean>
    
    <!-- 配置TransactionManager -->
    <bean id="transactionManager" class="demo2.utils.TransactionManager">
        <property name="connectionUtils" ref="connectionUtils"></property>
    </bean>
</beans>
```

**测试代码：**
TestClient.java
```java
package demo2;

import demo2.service.IAccountBookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestClient {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("demo2.xml");
        // 获取service
        IAccountBookService accountService = (IAccountBookService) ac.getBean("accountService");
        accountService.transferAccounts();
    }
}
```

**执行结果：**
![img_2.png](images/img_2.png)
数据库：<br/>
![img_3.png](images/img_3.png)

### 使用动态代理优化
设计模式动态代理参考[JAVA-设计模式-代理模式](https://gitee.com/anna_spaces/JavaStudyWorkSpaces/tree/master/03-gof23/03-gof23-06-proxy-pattern)

**抽象接口：**
IProxyService.java
```java
package demo3.utils;

/**
 * 抽象角色
 *
 * @author Anna.
 * @date 2025/2/14 9:56
 */
public interface IProxyService {
    // 代理方法
    void dealProxyMethod() throws Exception;
}

```
**具体实现：**
AccountBookServiceImpl.java
```java
package demo3.service.impl;

import demo3.dao.IAccountBookDao;
import demo3.entity.AccountBookDo;
import demo3.service.IAccountBookService;
import demo3.utils.IProxyService;

import java.sql.SQLException;

/**
 * AccountBookService 接口实现类
 *
 * @author Anna.
 * @date 2025/2/11 20:03
 */
public class AccountBookServiceImpl implements IAccountBookService, IProxyService {

    private IAccountBookDao accountBookDao;


    public void setAccountBookDao(IAccountBookDao accountBookDao) {
        this.accountBookDao = accountBookDao;
    }


    @Override
    public AccountBookDo selectAccountBookByName(String name) throws SQLException {
        return accountBookDao.selectAccountBookByName(name);
    }

    @Override
    public void updateAccountBook(float money, String name) throws SQLException {
        accountBookDao.updateAccountBook(money, name);
    }

    @Override
    public void dealProxyMethod() throws Exception {
        // 2.1. 查询账户zhangsan
        AccountBookDo zhangsan = accountBookDao.selectAccountBookByName("zhangsan");
        // 2.2.查询账户lisi
        AccountBookDo lisi = accountBookDao.selectAccountBookByName("lisi");
        // 定义金融
        float money = 100f;
        // 2.3.账户zhangsan - 100
        accountBookDao.updateAccountBook(zhangsan.getMoney() - money, zhangsan.getName());
        System.out.println("账户zhangsan - 100");
        // 运行异常导致事物不一致
        int i = 1 / 0;

        // 2.4.账户lisi + 100
        accountBookDao.updateAccountBook(lisi.getMoney() + money, lisi.getName());
        System.out.println("账户lisi + 100");
        System.out.println("执行完成");
    }
}

```
**代理方法：**
ProxyUtils.java
```java
package demo3.utils;

/**
 * JDK实现动态代理
 *
 * @author Anna.
 * @date 2025/2/14 9:45
 */
public class ProxyUtils implements IProxyService {

    private IProxyService proxyService;

    private TransactionManager txManager;

    public ProxyUtils(TransactionManager txManager) {
        this.txManager = txManager;
    }

    public void setProxyService(IProxyService proxyService) {
        this.proxyService = proxyService;
    }

    @Override
    public void dealProxyMethod() throws Exception {
        // 1 开启事务
        try {
            txManager.beginTransaction();
            // 2 执行操作
            proxyService.dealProxyMethod();
            // 3 提交事物
            txManager.commit();
        } catch (Exception e) {
            System.out.println("执行回退");
            // 4 回滚事务
            txManager.rollback();
            throw new RuntimeException(e);
        } finally {
            // 5 关闭连接
            txManager.release();
        }
    }
}

```
**配置文件：**
demo3.xml
```demo3.xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

    <!-- 配置MySQL数据源 -->
    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://127.0.0.1:3306/testdb"/>
        <property name="username" value="root"/>
        <property name="password" value="root"/>
    </bean>

    <!--配置QueryRunner-->
    <bean id="queryRunner" class="org.apache.commons.dbutils.QueryRunner"></bean>

    <!-- 配置DAO -->
    <bean id="accountBookDao" class="demo3.dao.impl.AccountBookDaoImpl">
        <constructor-arg ref="queryRunner"></constructor-arg>
        <constructor-arg ref="connectionUtils"></constructor-arg>
    </bean>
    <!--配置Service-->
    <bean id="accountService" class="demo3.service.impl.AccountBookServiceImpl">
        <property name="accountBookDao" ref="accountBookDao"></property>
    </bean>
    
    <!--配置ConnectionUtils-->
    <bean id="connectionUtils" class="demo3.utils.ConnectionUtils">
        <property name="dataSource" ref="dataSource"></property>
    </bean>
    
    <!-- 配置TransactionManager -->
    <bean id="transactionManager" class="demo3.utils.TransactionManager">
        <property name="connectionUtils" ref="connectionUtils"></property>
    </bean>
    <!-- 配置动态代理类-->
    <bean id="proxyUtils" class="demo3.utils.ProxyUtils">
        <constructor-arg ref="transactionManager"></constructor-arg>
    </bean>
</beans>
```
**测试方法：**
```java
package demo3;

import demo3.service.impl.AccountBookServiceImpl;
import demo3.utils.ProxyUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestClient {
    public static void main(String[] args) throws Exception {
        ApplicationContext ac = new ClassPathXmlApplicationContext("demo3.xml");
        // 获取service
        AccountBookServiceImpl accountService = (AccountBookServiceImpl) ac.getBean("accountService");
        ProxyUtils proxyUtils = (ProxyUtils) ac.getBean("proxyUtils");
        proxyUtils.setProxyService(accountService);
        proxyUtils.dealProxyMethod();
    }
}

```
**执行结果：**
![img_4.png](images/img_4.png)
数据库：<br/>
![img_5.png](images/img_5.png)

### 使用JDK自带动态代理类实现优化
**SERVICE:**
IAccountBookService.java
```java
package demo4.service;

import demo4.entity.AccountBookDo;

import java.sql.SQLException;

public interface IAccountBookService {

    /**
     * 查询账单
     * @param name
     * @return
     */
    AccountBookDo selectAccountBookByName(String name) throws SQLException;

    /**
     * 更新账单
     * @param money
     * @param name
     */
    void updateAccountBook(float money,String name) throws Exception;

    /**
     * 转账
     */
    void transferAccounts() throws SQLException, Exception;
}

```
AccountBookServiceImpl.java
```java
package demo4.service.impl;

import demo4.dao.IAccountBookDao;
import demo4.entity.AccountBookDo;
import demo4.service.IAccountBookService;

import java.sql.SQLException;

public class AccountBookServiceImpl implements IAccountBookService {

    private IAccountBookDao accountBookDao;

    public void setAccountBookDao(IAccountBookDao accountBookDao) {
        this.accountBookDao = accountBookDao;
    }

    @Override
    public AccountBookDo selectAccountBookByName(String name) throws SQLException {
        return accountBookDao.selectAccountBookByName(name);
    }

    @Override
    public void updateAccountBook(float money, String name) throws SQLException {
        accountBookDao.updateAccountBook(money, name);
    }
    
    @Override
    public void transferAccounts() throws Exception {
        // 2.1. 查询账户zhangsan
        AccountBookDo zhangsan = accountBookDao.selectAccountBookByName("zhangsan");
        // 2.2.查询账户lisi
        AccountBookDo lisi = accountBookDao.selectAccountBookByName("lisi");
        // 定义金融
        float money = 100f;
        // 2.3.账户zhangsan - 100
        accountBookDao.updateAccountBook(zhangsan.getMoney() - money, zhangsan.getName());
        System.out.println("账户zhangsan - 100");
        // 运行异常导致事物不一致
        int i = 1 / 0;

        // 2.4.账户lisi + 100
        accountBookDao.updateAccountBook(lisi.getMoney() + money, lisi.getName());
        System.out.println("账户lisi + 100");
        System.out.println("执行完成");
    }
}
```
**UTIL:**
ProxyUtils.java
```java
package demo4.utils;

import demo4.service.IAccountBookService;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * JDK实现动态代理
 *
 * @author Anna.
 * @date 2025/2/14 9:45
 */
public class ProxyUtils{

    private IAccountBookService accountBookService;

    private TransactionManager txManager;

    public ProxyUtils(TransactionManager txManager) {
        this.txManager = txManager;
    }

    public void setAccountBookService(IAccountBookService accountBookService) {
        this.accountBookService = accountBookService;
    }

    /**
     * 获取Service代理对象
     * @return
     */
    public IAccountBookService getAccountBookService(){
        return (IAccountBookService) Proxy.newProxyInstance(accountBookService.getClass().getClassLoader(), accountBookService.getClass().getInterfaces(), new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                Object rtn = null;
                // 1 开启事务
                try {
                    txManager.beginTransaction();
                    // 2 执行操作
                    rtn = method.invoke(accountBookService,args);
                    // 3 提交事物
                    txManager.commit();
                    return rtn;
                } catch (Exception e) {
                    System.out.println("执行回退");
                    // 4 回滚事务
                    txManager.rollback();
                    throw new RuntimeException(e);
                } finally {
                    // 5 关闭连接
                    txManager.release();
                }
            }
        });
    }
}
```
**配置文件：**
demo4.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

    <!-- 配置MySQL数据源 -->
    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://127.0.0.1:3306/testdb"/>
        <property name="username" value="root"/>
        <property name="password" value="root"/>
    </bean>

    <!--配置QueryRunner-->
    <bean id="queryRunner" class="org.apache.commons.dbutils.QueryRunner"></bean>

    <!-- 配置DAO -->
    <bean id="accountBookDao" class="demo4.dao.impl.AccountBookDaoImpl">
        <constructor-arg ref="queryRunner"></constructor-arg>
        <constructor-arg ref="connectionUtils"></constructor-arg>
    </bean>
    <!--配置Service-->
    <bean id="accountService" class="demo4.service.impl.AccountBookServiceImpl">
        <property name="accountBookDao" ref="accountBookDao"></property>
    </bean>
    
    <!--配置ConnectionUtils-->
    <bean id="connectionUtils" class="demo4.utils.ConnectionUtils">
        <property name="dataSource" ref="dataSource"></property>
    </bean>
    
    <!-- 配置TransactionManager -->
    <bean id="transactionManager" class="demo4.utils.TransactionManager">
        <property name="connectionUtils" ref="connectionUtils"></property>
    </bean>
    <!-- 配置动态代理类-->
    <bean id="proxyUtils" class="demo4.utils.ProxyUtils">
        <constructor-arg ref="transactionManager"></constructor-arg>
    </bean>
</beans>
```
**测试类：**
TestClient.java
```java
package demo4;

import demo4.service.IAccountBookService;
import demo4.service.impl.AccountBookServiceImpl;
import demo4.utils.ProxyUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestClient {
    public static void main(String[] args) throws Exception {
        ApplicationContext ac = new ClassPathXmlApplicationContext("demo4.xml");
        // 获取需要代理类
        AccountBookServiceImpl accountService = (AccountBookServiceImpl) ac.getBean("accountService");
        // 获取代理工具类
        ProxyUtils proxyUtils = (ProxyUtils) ac.getBean("proxyUtils");
        // 设置真实代理类
        proxyUtils.setAccountBookService(accountService);
        // 获取代理对象
        IAccountBookService accountBookService = proxyUtils.getAccountBookService();
        // 执行代理方法
        accountBookService.transferAccounts();
    }
}

```
**执行结果：**
![img_10.png](images/img_10.png)

![img_11.png](images/img_11.png)

## AOP 

### 什么是AOP
Spring AOP（Aspect-Oriented Programming）是Spring框架中实现面向切面编程的模块。面向切面编程是一种编程范式，旨在将程序中的横切关注点（如日志记录、事务管理、安全检查等）从业务逻辑代码中分离出来，以提高代码的可重用性和可维护性。

#### AOP 的作用及优势
+ 作用：
    - 在程序运行期间，不修改源码对已有方法进行增强。
+ 优势：
    - 减少重复代码
    - 提高开发效率
    - 维护方便

#### AOP 的实现方式
使用动态代理技术

### 基于spring自带JdbcTemplate实现转账

**数据库：**
accountbook.sql
```sql
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `accountbook`;
CREATE TABLE `accountbook`  (
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` float NULL DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

INSERT INTO `accountbook` VALUES ('lisi', 1000);
INSERT INTO `accountbook` VALUES ('zhangsan', 1000);

SET FOREIGN_KEY_CHECKS = 1;

```
**依赖：**
```xml
<dependencies>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>5.3.22</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
            <version>5.3.22</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-tx</artifactId>
            <version>5.3.22</version>
        </dependency>

        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>5.1.6</version>
        </dependency>
    </dependencies>
```
**entity:**
AccountBookDo.java
```java
package demo1.entity;

public class AccountBookDo {
    private String name;
    private float money;
    // GET / SET
}

```
**DAO:**
AccountBookDo.java
```java
package demo1.dao;

import demo1.entity.AccountBookDo;

public interface IAccountBookDao {

    /**
     * 查询账单
     * @param name
     * @return
     */
    AccountBookDo selectAccountBookByName(String name);

    /**
     * 更新账单
     * @param money
     * @param name
     */
    void updateAccountBook(float money,String name);
}

```
AccountBookDaoImpl.java
```java
package demo1.dao.impl;

import demo1.dao.IAccountBookDao;
import demo1.entity.AccountBookDo;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class AccountBookDaoImpl implements IAccountBookDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public AccountBookDo selectAccountBookByName(String name) {
        List<AccountBookDo> rtn = jdbcTemplate.query("select * from accountbook where name = ? ", new BeanPropertyRowMapper<AccountBookDo>(AccountBookDo.class), name);
        if(rtn.size() > 1) {
            throw new RuntimeException("查结果过多");
        }
        return rtn.get(0);
    }

    @Override
    public void updateAccountBook(float money, String name) {
        jdbcTemplate.update("update accountbook set money=? where name=?", money, name);
    }
}

```
**SERVICE:**
IAccountBookService.java
```java
package demo1.service;

import demo1.entity.AccountBookDo;

public interface IAccountBookService {

    /**
     * 查询账单
     * @param name
     * @return
     */
    AccountBookDo selectAccountBookByName(String name);

    /**
     * 更新账单
     * @param money
     * @param name
     */
    void updateAccountBook(float money,String name);
}

```
AccountBookServiceImpl.java
```java
package demo1.service.impl;

import demo1.dao.IAccountBookDao;
import demo1.entity.AccountBookDo;
import demo1.service.IAccountBookService;

public class AccountBookServiceImpl implements IAccountBookService {

    private IAccountBookDao accountBookDao;

    public void setAccountBookDao(IAccountBookDao accountBookDao) {
        this.accountBookDao = accountBookDao;
    }

    @Override
    public AccountBookDo selectAccountBookByName(String name) {
        return accountBookDao.selectAccountBookByName(name);
    }

    @Override
    public void updateAccountBook(float money, String name) {
        accountBookDao.updateAccountBook(money, name);
    }
}
```
**配置文件：**
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd">

    <!-- 配置数据源 -->
    <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://127.0.0.1:3306/testdb"/>
        <property name="username" value="root"/>
        <property name="password" value="root"/>
    </bean>

    <!-- 配置JdbcTemplate-->
    <bean id="jdbcTemplate" class="org.springframework.jdbc.core.JdbcTemplate">
        <property name="dataSource" ref="dataSource"></property>
    </bean>

    <!-- 配置Dao-->
    <bean id="accountBookDao" class="demo1.dao.impl.AccountBookDaoImpl">
        <property name="jdbcTemplate" ref="jdbcTemplate"></property>
    </bean>
    <!-- 配置service-->
    <bean id="accountBookService" class="demo1.service.impl.AccountBookServiceImpl">
        <property name="accountBookDao" ref="accountBookDao"></property>
    </bean>

</beans>
```
**测试方法：**
TestClient.java
```java
package demo1;

import demo1.entity.AccountBookDo;
import demo1.service.IAccountBookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestClient {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("demo1.xml");
        // 获取service
        IAccountBookService accountService = (IAccountBookService) ac.getBean("accountBookService");
        // 1. 查询账户zhangsan
        AccountBookDo zhangsan = accountService.selectAccountBookByName("zhangsan");
        // 2.查询账户lisi
        AccountBookDo lisi = accountService.selectAccountBookByName("lisi");
        // 定义金融
        float money = 100f;
        // 3.账户zhangsan - 100
        accountService.updateAccountBook(zhangsan.getMoney() - money, zhangsan.getName());

        // 运行异常导致事物不一致
        int i = 1 / 0;

        // 4.账户lisi + 100
        accountService.updateAccountBook(lisi.getMoney() + money, lisi.getName());

        System.out.println("执行完成");
    }
}

```
**执行结果：**
![img_6.png](images/img_6.png)
数据库：<br/>
![img_7.png](images/img_7.png)

### 基于ThreadLoacl及XML实现spring aop事务控制
由于JdbcTemplate不太方便实现，手动获取一个Connection对象并将其与JdbcTemplate的某个操作关联起来。因此改用QueryRunner实现。
**DAO**
IAccountBookDao.java
```java
package demo2.dao;

import demo2.entity.AccountBookDo;

import java.sql.SQLException;

public interface IAccountBookDao {

    AccountBookDo selectAccountBookByName(String name) throws SQLException;

    void updateAccountBook(float money,String name) throws SQLException;
}
```
AccountBookDaoImpl.java
```java
package demo2.dao.impl;

import demo2.dao.IAccountBookDao;
import demo2.entity.AccountBookDo;
import demo2.utils.ConnectionUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.sql.SQLException;

/**
 * AccountBookDao 实现类
 *
 * @author Anna.
 * @date 2025/2/11 19:42
 */
public class AccountBookDaoImpl implements IAccountBookDao {

  private QueryRunner runner;

  private ConnectionUtils connectionUtils;

  public AccountBookDaoImpl(QueryRunner runner, ConnectionUtils connectionUtils) {
    this.runner = runner;
    this.connectionUtils = connectionUtils;
  }

  @Override
  public AccountBookDo selectAccountBookByName(String name) throws SQLException {
    return runner.query(connectionUtils.getThreadConnection(), "select * from accountbook where name = ? ", new BeanHandler<AccountBookDo>(AccountBookDo.class), name);
  }

  @Override
  public void updateAccountBook(float money, String name) throws SQLException {
    runner.update(connectionUtils.getThreadConnection(), "update accountbook set money=? where name=?", money, name);
  }
}
```


**SERVICE:**
IAccountBookService.java
```java
package demo2.service;

import demo2.entity.AccountBookDo;

public interface IAccountBookService {

    AccountBookDo selectAccountBookByName(String name);

    void updateAccountBook(float money,String name);

    /**
     * 转账
     */
    void transferAccounts();
}

```
AccountBookServiceImpl.java
```java
package demo2.service.impl;

import demo2.dao.IAccountBookDao;
import demo2.entity.AccountBookDo;
import demo2.service.IAccountBookService;

public class AccountBookServiceImpl implements IAccountBookService {

    private IAccountBookDao accountBookDao;

    public void setAccountBookDao(IAccountBookDao accountBookDao) {
        this.accountBookDao = accountBookDao;
    }

    @Override
    public AccountBookDo selectAccountBookByName(String name) {
        return accountBookDao.selectAccountBookByName(name);
    }

    @Override
    public void updateAccountBook(float money, String name) {
        accountBookDao.updateAccountBook(money, name);
    }

    @Override
    public void transferAccounts() {
        // 1. 查询账户zhangsan
        AccountBookDo zhangsan = accountBookDao.selectAccountBookByName("zhangsan");
        // 2.查询账户lisi
        AccountBookDo lisi = accountBookDao.selectAccountBookByName("lisi");
        // 定义金融
        float money = 100f;
        // 3.账户zhangsan - 100
        accountBookDao.updateAccountBook(zhangsan.getMoney() - money, zhangsan.getName());

        // 运行异常导致事物不一致
        int i = 1 / 0;

        // 4.账户lisi + 100
        accountBookDao.updateAccountBook(lisi.getMoney() + money, lisi.getName());

        System.out.println("执行完成");
    }
}

```
**工具类：**
ConnectionUtils.java
```java
package demo2.utils;

import com.mysql.jdbc.Connection;

import javax.sql.DataSource;
import java.sql.SQLException;

public class ConnectionUtils {
    private ThreadLocal<Connection> local = new ThreadLocal<>();

    private DataSource dataSource;

    public ConnectionUtils(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * 线程绑定链接
     * @return
     * @throws SQLException
     */
    public Connection getThreadConnection() throws SQLException {
        // 从线程中获取链接
         Connection connection = local.get();
         // 判断链接是否为空
         if(connection == null){
             // 重新获取一个链接，并存储在ThreadLocal中
             connection = (Connection) dataSource.getConnection();
             local.set(connection);
         }
         return connection;
    }

    /**
     * 线程解绑链接
     */
    public void removeThreadConnection(){
        local.remove();
    }
}
```
> 连接管理类，负责线程绑定与解绑链接

TransactionManager.java
```java
package demo2.utils;

import java.sql.SQLException;

public class TransactionManager {

    private ConnectionUtils connectionUtils;

    public void setConnectionUtils(ConnectionUtils connectionUtils) {
        this.connectionUtils = connectionUtils;
    }

    /**
     * 开启事务
     *
     * @throws SQLException
     */
    public void beginTransaction() throws SQLException {
        System.out.println("开启事务");
        connectionUtils.getThreadConnection().setAutoCommit(false);
    }

    /**
     * 提交事务
     *
     * @throws SQLException
     */
    public void commit() throws SQLException {
        System.out.println("提交事务");
        connectionUtils.getThreadConnection().commit();
    }

    /**
     * 回滚事务
     */
    public void rollback() {
        try {
            System.out.println("回滚事务");
            connectionUtils.getThreadConnection().rollback();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * 关闭连接及线程连接解绑
     */
    public void release() {
        try {
            System.out.println("关闭连接");
            connectionUtils.getThreadConnection().close();
            connectionUtils.removeThreadConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
```
> 事务管理工具类

**配置文件：**
demo2.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop.xsd
">
  <!-- 配置MySQL数据源 -->
  <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
    <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
    <property name="url" value="jdbc:mysql://127.0.0.1:3306/testdb"/>
    <property name="username" value="root"/>
    <property name="password" value="root"/>
  </bean>

  <!--配置QueryRunner-->
  <bean id="queryRunner" class="org.apache.commons.dbutils.QueryRunner"></bean>

  <!-- 配置DAO -->
  <bean id="accountBookDao" class="demo2.dao.impl.AccountBookDaoImpl">
    <constructor-arg ref="queryRunner"></constructor-arg>
    <constructor-arg ref="connectionUtils"></constructor-arg>
  </bean>
  <!--配置Service-->
  <bean id="accountService" class="demo2.service.impl.AccountBookServiceImpl">
    <property name="accountBookDao" ref="accountBookDao"></property>
    <property name="txManager" ref="txManager"></property>
  </bean>

  <!--配置ConnectionUtils-->
  <bean id="connectionUtils" class="demo2.utils.ConnectionUtils">
    <property name="dataSource" ref="dataSource"></property>
  </bean>

  <!-- 配置TransactionManager -->
  <bean id="txManager" class="demo2.utils.TransactionManager">
    <property name="connectionUtils" ref="connectionUtils"></property>
  </bean>

  <!--配置aop-->
  <aop:config>
    <!--配置通用切入点表达式-->
    <aop:pointcut id="pt1" expression="execution(public void demo2.service.impl.AccountBookServiceImpl.transferAccounts())"/>
    <aop:aspect id="txAdvice" ref="txManager">
      <!--配置前置通知：开启事务-->
      <aop:before method="beginTransaction" pointcut-ref="pt1"></aop:before>
      <!--配置后置通知：提交事务-->
      <aop:after-returning method="commit" pointcut-ref="pt1"></aop:after-returning>
      <!--配置异常通知：回滚事务-->
      <aop:after-throwing method="rollback" pointcut-ref="pt1"></aop:after-throwing>
      <!--配置最终通知：释放连接-->
      <aop:after method="release" pointcut-ref="pt1"></aop:after>
    </aop:aspect>
  </aop:config>
</beans>
```

**测试类：**
TestClient.java
```java
package demo2;

import demo2.service.IAccountBookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestClient {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("demo2.xml");
        // 获取service
        IAccountBookService accountService = (IAccountBookService) ac.getBean("accountBookService");
        accountService.transferAccounts();
    }
}

```
**执行结果：**
![img_8.png](images/img_8.png)

![img_9.png](images/img_9.png)

### AOP 相关术语
| 术语 | 描述 | Spring/AspectJ 相关信息 |
| :---: | :---: | :---: |
| Joinpoint（连接点） | 被拦截到的点 | 在 Spring 中，这些点指的是方法，因为 Spring 只支持方法类型的连接点 |
| Pointcut（切入点） | 要对哪些 Joinpoint 进行拦截的定义 | - |
| Advice（通知/增强） | 拦截到 Joinpoint 之后所要做的事情 | 通知的类型：前置通知, 后置通知, 异常通知, 最终通知, 环绕通知 |
| Introduction（引介） | 在不修改类代码的前提下，为类动态地添加一些方法或 Field | - |
| Target（目标对象） | 代理的目标对象 | - |
| Weaving（织入） | 把增强应用到目标对象来创建新的代理对象的过程 | Spring 采用动态代理织入，AspectJ 采用编译期织入和类装载期织入 |
| Proxy（代理） | 一个类被 AOP 织入增强后产生的结果代理类 | - |
| Aspect（切面） | 切入点和通知（引介）的结合 | - |

对比使用JDK自带动态代理类实现优化实现事务控制代码：
![img_12.png](images/img_12.png)
**Target（目标对象）：** IAccountBookService.java

**Joinpoint（连接点）:** selectAccountBookByName、updateAccountBook等待即是连接点
![img_13.png](images/img_13.png)

**Pointcut（切入点）** selectAccountBookByName、updateAccountBook等待即是切入点

> 注意：如下新增一个接口test()，然后增强方法中，不对test()方法进行事务控制

IAccountBookService.java
```java
public interface IAccountBookService {

    AccountBookDo selectAccountBookByName(String name) throws SQLException;

    void updateAccountBook(float money,String name) throws Exception;

    void transferAccounts() throws SQLException, Exception;

    /**
     * 新增一个test接口
     */
    void test();
}
```
ProxyUtils.java
```java
package demo4.utils;

import demo4.service.IAccountBookService;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * JDK实现动态代理
 *
 * @author Anna.
 * @date 2025/2/14 9:45
 */
public class ProxyUtils{
  // ...
  public IAccountBookService getAccountBookService(){
    return (IAccountBookService) Proxy.newProxyInstance(accountBookService.getClass().getClassLoader(), accountBookService.getClass().getInterfaces(), new InvocationHandler() {
      @Override
      public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object rtn = null;
        if("test".equals(method.getName())) {
          return method.invoke(accountBookService,args);
        }
        // 1 开启事务
        try {
          txManager.beginTransaction();
          // 2 执行操作
          rtn = method.invoke(accountBookService,args);
          // 3 提交事物
          txManager.commit();
          return rtn;
        } catch (Exception e) {
          System.out.println("执行回退");
          // 4 回滚事务
          txManager.rollback();
          throw new RuntimeException(e);
        } finally {
          // 5 关闭连接
          txManager.release();
        }
      }
    });
  }
}
```
> 那么 这里的test方法即使连接点，但是并没有对其进行增强，因此test方法并不是切入点。

**Advice（通知/增强）：** 如下图所示：
![img_14.png](images/img_14.png)

**Weaving（织入）：** 给需要执行的方法进行增强的过程叫做织入。例如：对`rtn = method.invoke(accountBookService,args);`方法，前后加入事务控制代码的过程叫做织入。

**Proxy（代理）：** Proxy.newProxyInstance()方法创建出来的对象就是代理对象。

**Aspect（切面）：** 建立切入点方法和执行方法在执行时的对应关系就是切面。如以下代码就是一个切面：

![img_15.png](images/img_15.png)

#### AOP四种常用通知类型
**SERVICE:**
ISendMassageService.java
```java
package demo3.service;

public interface ISendMassageService {
    void sendMessage(String msg);
    void acceptMessage();
}

```
SendMassageServiceImpl.java
```java
package demo3.service.impl;

import demo3.service.ISendMassageService;

public class SendMassageServiceImpl implements ISendMassageService {
    @Override
    public void sendMessage(String msg) {
        System.out.println("发送消息:" + msg);
    }
    @Override
    public void acceptMessage() {
        System.out.println("接收消息:Hello World!");
        throw new RuntimeException("接收消息-测试异常");
    }
}
```
**UTILS:**
LoggerUtils.java
```java
package demo3.utils;

public class LoggerUtils {
    public void beforeLog(){
        System.out.println("前置通知执行");
    }
    public void afterLog(){
        System.out.println("后置通知执行");
    }
    public void exceptionLog(){
        System.out.println("异常通知执行");
    }
    public void finalLog(){
        System.out.println("最终通知执行");
    }
}
```
**配置文件：**
demo3.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop.xsd
">
  <bean id="sendMassageService" class="demo3.service.impl.SendMassageServiceImpl"></bean>
  <bean id="loggerUtils" class="demo3.utils.LoggerUtils"></bean>

  <!-- Spring基于XML配置AOP的步骤
      1. 把通知Bean交给spring管理
      2. 使用aop:config标签表明开始AOP的配置
      3. 使用aop:aspect标签表明配置切面
          id属性：是切面提供的一个唯一标识
          ref属性：是指定通知类的bean的id
      4. 在aop:aspect标签的内部使用对应标签来配置通知的类型
          aop:before 属性: 表示配置前置通知
              method属性：用于指定通知类的那个方法是前置通知
              pointcut属性：用于指定切入点表达式，该表达式的含义指的是对业务层中哪些方法增强
          aop:after-returning 属性: 表示配置后置通知
          aop:after-throwing 属性: 表示配置异常通知
          aop:after 属性: 表示配置最终通知

          切入点表达式的写法：execution(表达式)
              表达式：
                  访问修饰符 返回值 包名.包名...类名.方法名(参数列表)
              例如：增强SendMassageServiceImpl类中sendMessage()方法
              标准写法：
                  public void demo3.service.impl.SendMassageServiceImpl.sendMessage(java.lang.String)
              访问修饰符可以省略：
                  void demo3.service.impl.SendMassageServiceImpl.sendMessage(java.lang.String)
              返回值可以使用通配符*，表示任意返回值：
                  * demo3.service.impl.SendMassageServiceImpl.sendMessage(java.lang.String)
              包名可以使用通配符表示任意包，但是有几级包，就需要写几个*：
                  * *.*.*.SendMassageServiceImpl.sendMessage(java.lang.String)
              包名可以使用..表示当前包及其子包：
                  * *..SendMassageServiceImpl.sendMessage(java.lang.String)
              类名及方法名也可以使用*表示任意：
                  * *..*.*(java.lang.String)
              参数列表：
                  可以直接写数据类型：基本类型直接写：名称（int），引用类型写：包名.类名(java.lang.String)
                      * *..*.*(java.lang.String)
                  可以使用通配符表示任意类型，但是必须有参数
                      * *..*.*(*)
                  可以使用..表示有误参数均可，有参数可以是任意类型
                      * *..*.*(..)
              全统配写法：
                  * *..*.*(..)
              实际开发中通常切到业务层实现类下的所有方法：
                  * demo3.service.impl.SendMassageServiceImpl.*(..)
              注意：使用通配符表达式写切入点表达式时，需要引入aspectjweaver依赖
  -->
  <aop:config>
    <!-- 配置切面 -->
    <aop:aspect id="loggerAdvice" ref="loggerUtils">
      <!--配置前置通知: 在切入点方法执行之前执行 -->
      <!--            <aop:before method="beforeLog" pointcut="execution(public void demo3.service.impl.SendMassageServiceImpl.sendMessage(java.lang.String))"></aop:before>-->
      <aop:before method="beforeLog" pointcut="execution(void demo3.service.impl.SendMassageServiceImpl.sendMessage(java.lang.String))"></aop:before>
      <!-- 配置后置通知：在切入点方法执行之后执行，它和异常通知永远只能执行其中一个 -->
      <aop:after-returning method="afterLog" pointcut="execution(* *..*.*(*))"></aop:after-returning>
      <!-- 异常通知：在切入点方法执行发生异常时执行，它和后置通知永远只能执行其中一个 -->
      <aop:after-throwing method="exceptionLog" pointcut="execution(* demo3.service.impl.SendMassageServiceImpl.*(..))"></aop:after-throwing>
      <!-- 最终通知：无论切入点方法是否正常执行，它都会在其后执行 -->
      <aop:after method="finalLog" pointcut="execution(* demo3.service.impl.SendMassageServiceImpl.*(..))"></aop:after>
    </aop:aspect>
  </aop:config>
</beans>
```
>  注意：使用通配符表达式写切入点表达式时，需要引入aspectjweaver依赖:
```xml
<dependency>
    <groupId>org.aspectj</groupId>
    <artifactId>aspectjweaver</artifactId>
    <version>1.8.13</version>
</dependency>
```
**测试方法：**
TestClient.java
```java
package demo3;

import demo3.service.ISendMassageService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestClient {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("demo3.xml");
        // 获取service
        ISendMassageService sendMassageService = (ISendMassageService) ac.getBean("sendMassageService");
        sendMassageService.sendMessage("你好！");
        System.out.println("================================");
        sendMassageService.acceptMessage();
    }
}
```
**执行结果：**
![img_16.png](images/img_16.png)

#### 配置切入点表达式
```xml
    <aop:config>
        <!--配置切入点表达式
            id属性: 配置切入点表达式的唯一标志
            expression属性：配置切入点表达式
        -->
        <aop:pointcut id="ptlog" expression="execution(* demo3.service.impl.SendMassageServiceImpl.*(..))"/>
        <!-- 配置切面 -->
        <aop:aspect id="loggerAdvice" ref="loggerUtils">
            <!--配置前置通知: 在切入点方法执行之前执行 -->
            <aop:before method="beforeLog" pointcut-ref="ptlog"></aop:before>
            <!-- 配置后置通知：在切入点方法执行之后执行，它和异常通知永远只能执行其中一个 -->
            <aop:after-returning method="afterLog"  pointcut-ref="ptlog"></aop:after-returning>
            <!-- 异常通知：在切入点方法执行发生异常时执行，它和后置通知永远只能执行其中一个 -->
            <aop:after-throwing method="exceptionLog"  pointcut-ref="ptlog"></aop:after-throwing>
            <!-- 最终通知：无论切入点方法是否正常执行，它都会在其后执行 -->
            <aop:after method="finalLog"  pointcut-ref="ptlog"></aop:after>
            <!-- 注意：aop:pointcut标签写在aop:aspect标签内部，只能当前标签使用 -->
<!--            <aop:pointcut id="ptlog" expression="execution(* demo3.service.impl.SendMassageServiceImpl.*(..))"/>-->
        </aop:aspect>
    </aop:config>
```
> 注意：aop:pointcut标签写在aop:aspect标签内部，只能当前标签使用。<br/>
> aop:pointcut标签写在aop:config标签下，则所有切面均可使用。当时该标签必须出现在切面之前，否则会报错。

#### spring环绕通知
**SERVICE:**
ISendMassageService.java
```java
package demo4.service;
public interface ISendMassageService {
    void sendMessage(String msg);
    void acceptMessage();
}
```
SendMassageServiceImpl.java
```java
package demo4.service.impl;

import demo4.service.ISendMassageService;

public class SendMassageServiceImpl implements ISendMassageService {
    @Override
    public void sendMessage(String msg) {
        System.out.println("发送消息:" + msg);
    }
    @Override
    public void acceptMessage() {
        System.out.println("接收消息:Hello World!");
        throw new RuntimeException("接收消息-测试异常");
    }
}
```
**UTILS:**
LoggerUtils.java
```java
package demo4.utils;

public class LoggerUtils {
    public void aroundMeaasge(){
        System.out.println("环绕通知");
    }
}
```
**配置文件：**
demo4.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop.xsd
">
    <bean id="sendMassageService" class="demo4.service.impl.SendMassageServiceImpl"></bean>
    <bean id="loggerUtils" class="demo4.utils.LoggerUtils"></bean>

    <aop:config>
        <aop:pointcut id="ptlog" expression="execution(* demo4.utils.LoggerUtils.*(..))"/>
        <!-- 配置切面 -->
        <aop:aspect id="loggerAdvice" ref="loggerUtils">
            <!-- spring环绕通知-->
            <aop:around method="aroundMeaasge" pointcut-ref="ptlog"></aop:around>
        </aop:aspect>
    </aop:config>
</beans>
```
**测试方法：**
TestClient.java
```java
package demo4;

import demo4.service.ISendMassageService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestClient {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("demo4.xml");
        // 获取service
        ISendMassageService sendMassageService = (ISendMassageService) ac.getBean("sendMassageService");
        sendMassageService.sendMessage("你好！");
        System.out.println("================================");
        sendMassageService.acceptMessage();
    }
}
```
**执行结果：**
![img_17.png](images/img_17.png)

> 从执行结果可以看出与预期并不相符,只执行了增强方法但是切入点方法并没有执行。spring环绕通知与前置通知，后置通知等不同。spring环绕通知是spring框架为我们提供的一种可以在代码中手动控制增强方法何时执行的方式。
> <br/>spring框架为我们提供了一个接口：ProceedingJoinPoint，该接口有一个proceed()方法，此方法相当于明确调用切入点的方法。
> 该接口作为环绕通知方法的参数，在程序执行时，可以手动控制增强方法何时执行。

#### 优化代码如下：
LoggerUtils.java
```java
package demo4.utils;

import org.aspectj.lang.ProceedingJoinPoint;

public class LoggerUtils {

    /**
     * spring环绕通知是spring框架为我们提供的一种可以在代码中手动控制增强方法何时执行的方式。
     *    spring框架为我们提供了一个接口：ProceedingJoinPoint，该接口有一个proceed()方法，此方法相当于明确调用切入点的方法。
     *    该接口可以作为环绕通知方法的参数，在程序执行时，可以手动控制增强方法何时执行。
     *    在proceed()方法执行前执行的代码则是前置通知
     *    proceed()方法执行之后执行的则是后置通知
     *    proceed()方法异常时执行的代码则是异常通知
     *    proceed()抛出Throwable异常后通过finally明确最终执行的代码则是最终通知
     * @param pjp
     * @return
     */
    public Object aroundMeaasge(ProceedingJoinPoint pjp){
        Object rtn = null;
        Object[] args = pjp.getArgs();
        try {
            System.out.println("执行前置通知");
            rtn = pjp.proceed(args);
            System.out.println("执行后置通知");
            return rtn;
        } catch (Throwable e) {
            System.out.println("执行异常通知");
            throw new RuntimeException(e);
        }
        finally {
            System.out.println("执行最终通知");
        }
    }
}
```
**执行结果：**
![img_18.png](images/img_18.png)

#### 基于注解配置spring aop环绕通知
**SERVICE：**
ISendMassageService.java
```java
package demo5.service;

public interface ISendMassageService {

    void sendMessage(String msg);

    void acceptMessage();
}
```
ISendMassageService.java
```java
package demo5.service.impl;

import demo5.service.ISendMassageService;
import org.springframework.stereotype.Service;

@Service("sendMassageService")
public class SendMassageServiceImpl implements ISendMassageService {

    @Override
    public void sendMessage(String msg) {
        System.out.println("发送消息:" + msg);
    }
    @Override
    public void acceptMessage() {
        System.out.println("接收消息:Hello World!");
        throw new RuntimeException("接收消息-测试异常");
    }
}

```
**UTILS：**
LoggerUtils.java
```java
package demo5.utils;

import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Component("loggerUtils")
@Aspect // 表示当前类是一个切面类
public class LoggerUtils {

    // 配置切入点
    @Pointcut("execution(* demo5.service.impl.SendMassageServiceImpl.*(..)))")
    public void pt1(){}

    @Before("pt1()")
    public void beforeLog(){
        System.out.println("前置通知执行");
    }

    @AfterReturning("pt1()")
    public void afterLog(){
        System.out.println("后置通知执行");
    }

    @AfterThrowing("pt1()")
    public void exceptionLog(){
        System.out.println("异常通知执行");
    }

    @After("pt1()")
    public void finalLog(){
        System.out.println("最终通知执行");
    }
}
```
**配置文件：**
demo5.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop.xsd
                           http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd
">
    <!-- 配置需要扫描的包-->
    <context:component-scan base-package="demo5"></context:component-scan>
    <!-- 配置开启aop注解支持-->
    <aop:aspectj-autoproxy></aop:aspectj-autoproxy>
</beans>
```
**测试方法：**
ISendMassageService.java
```java
package demo5;

import demo5.service.ISendMassageService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestClient {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("demo5.xml");
        // 获取service
        ISendMassageService sendMassageService = (ISendMassageService) ac.getBean("sendMassageService");
        sendMassageService.sendMessage("你好！");
        System.out.println("================================");
        sendMassageService.acceptMessage();

    }
}
```
**执行结果：**
![img_19.png](images/img_19.png)

> 注意：在 Spring AOP 中，基于注解配置的后置通知（@AfterReturning）和最终通知（@After）在某些版本中存在执行顺序问题。具体来说，@AfterReturning 和 @After 的执行顺序可能与预期不符。例如：5.0.2.RELEASE版本
> 我们会得到如下结果：
![img_20.png](images/img_20.png)
> 如图可以看出，在代码没有改变的情况下，我们只调整了Spring的版本，但是出现了@AfterReturning 和 @After 的执行顺序与预期不符的情况。
> 因此，一种常见的做法是使用环绕通知（@Around）来替代单独的后置通知和最终通知。

#### 基于注解配置spring aop环绕通知
**SERVICE：**
ISendMassageService.java
```java
package demo6.service;

public interface ISendMassageService {

    void sendMessage(String msg);

    void acceptMessage();
}
```
ISendMassageService.java
```java
package demo6.service.impl;

import demo6.service.ISendMassageService;
import org.springframework.stereotype.Service;

@Service("sendMassageService")
public class SendMassageServiceImpl implements ISendMassageService {

    @Override
    public void sendMessage(String msg) {
        System.out.println("发送消息:" + msg);
    }
    @Override
    public void acceptMessage() {
        System.out.println("接收消息:Hello World!");
        throw new RuntimeException("接收消息-测试异常");
    }
}

```
**UTILS：**
LoggerUtils.java
```java
package demo6.utils;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Component("loggerUtils")
@Aspect // 表示当前类是一个切面类
public class LoggerUtils {

  // 配置切入点
  @Pointcut("execution(* demo6.service.impl.SendMassageServiceImpl.*(..)))")
  public void pt1(){}

  // 配置环绕通知
  @Around("pt1()")
  public Object aroundMeaasge(ProceedingJoinPoint pjp){
    Object rtn = null;
    Object[] args = pjp.getArgs();
    try {
      System.out.println("执行前置通知");
      rtn = pjp.proceed(args);
      System.out.println("执行后置通知");
      return rtn;
    } catch (Throwable e) {
      System.out.println("执行异常通知");
      throw new RuntimeException(e);
    }
    finally {
      System.out.println("执行最终通知");
    }
  }
}
```
**配置文件：**
demo6.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
                           http://www.springframework.org/schema/aop http://www.springframework.org/schema/aop/spring-aop.xsd
                           http://www.springframework.org/schema/context http://www.springframework.org/schema/context/spring-context.xsd
">
    <!-- 配置需要扫描的包-->
    <context:component-scan base-package="demo6"></context:component-scan>
    <!-- 配置开启aop注解支持-->
    <aop:aspectj-autoproxy></aop:aspectj-autoproxy>
</beans>
```
**测试方法：**
ISendMassageService.java
```java
package demo6;

import demo6.service.ISendMassageService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestClient {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("demo6.xml");
        // 获取service
        ISendMassageService sendMassageService = (ISendMassageService) ac.getBean("sendMassageService");
        sendMassageService.sendMessage("你好！");
        System.out.println("================================");
        sendMassageService.acceptMessage();

    }
}
```
**执行结果：**
![img_21.png](images/img_21.png)

## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)