package demo4;

import demo4.service.ISendMassageService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 测试类
 *
 * @author Anna.
 * @date 2025/2/11 19:30
 */
public class TestClient {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("demo4.xml");
        // 获取service
        ISendMassageService sendMassageService = (ISendMassageService) ac.getBean("sendMassageService");
        sendMassageService.sendMessage("你好！");
        System.out.println("================================");
        sendMassageService.acceptMessage();
    }
}
