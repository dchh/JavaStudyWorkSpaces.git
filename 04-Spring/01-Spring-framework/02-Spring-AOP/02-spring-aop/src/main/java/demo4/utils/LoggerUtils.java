package demo4.utils;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * 日志工具类
 *
 * @author Anna.
 * @date 2025/2/14 22:12
 */
public class LoggerUtils {

    /**
     * spring环绕通知是spring框架为我们提供的一种可以在代码中手动控制增强方法何时执行的方式。
     *    spring框架为我们提供了一个接口：ProceedingJoinPoint，该接口有一个proceed()方法，此方法相当于明确调用切入点的方法。
     *    该接口可以作为环绕通知方法的参数，在程序执行时，可以手动控制增强方法何时执行。
     *    在proceed()方法执行前执行的代码则是前置通知
     *    proceed()方法执行之后执行的则是后置通知
     *    proceed()方法异常时执行的代码则是异常通知
     *    proceed()抛出Throwable异常后通过finally明确最终执行的代码则是最终通知
     * @param pjp
     * @return
     */
    public Object aroundMeaasge(ProceedingJoinPoint pjp){
        Object rtn = null;
        Object[] args = pjp.getArgs();
        try {
            System.out.println("执行前置通知");
            rtn = pjp.proceed(args);
            System.out.println("执行后置通知");
            return rtn;
        } catch (Throwable e) {
            System.out.println("执行异常通知");
            throw new RuntimeException(e);
        }
        finally {
            System.out.println("执行最终通知");
        }
    }
}
