package demo6.service.impl;

import demo6.service.ISendMassageService;
import org.springframework.stereotype.Service;

/**
 * 接口实现类
 *
 * @author Anna.
 * @date 2025/2/14 22:05
 */
@Service("sendMassageService")
public class SendMassageServiceImpl implements ISendMassageService {

    @Override
    public void sendMessage(String msg) {
        System.out.println("发送消息:" + msg);
    }

    @Override
    public void acceptMessage() {
        System.out.println("接收消息:Hello World!");
        throw new RuntimeException("接收消息-测试异常");
    }
}
