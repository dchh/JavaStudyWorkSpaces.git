package demo6.utils;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * 日志工具类
 *
 * @author Anna.
 * @date 2025/2/14 22:12
 */
@Component("loggerUtils")
@Aspect // 表示当前类是一个切面类
public class LoggerUtils {

    // 配置切入点
    @Pointcut("execution(* demo6.service.impl.SendMassageServiceImpl.*(..)))")
    public void pt1(){}

    // 配置环绕通知
    @Around("pt1()")
    public Object aroundMeaasge(ProceedingJoinPoint pjp){
        Object rtn = null;
        Object[] args = pjp.getArgs();
        try {
            System.out.println("执行前置通知");
            rtn = pjp.proceed(args);
            System.out.println("执行后置通知");
            return rtn;
        } catch (Throwable e) {
            System.out.println("执行异常通知");
            throw new RuntimeException(e);
        }
        finally {
            System.out.println("执行最终通知");
        }
    }
}
