package demo5.utils;

import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * 日志工具类
 *
 * @author Anna.
 * @date 2025/2/14 22:12
 */
@Component("loggerUtils")
@Aspect // 表示当前类是一个切面类
public class LoggerUtils {

    // 配置切入点
    @Pointcut("execution(* demo5.service.impl.SendMassageServiceImpl.*(..)))")
    public void pt1(){}

    @Before("pt1()")
    public void beforeLog(){
        System.out.println("前置通知执行");
    }

    @AfterReturning("pt1()")
    public void afterLog(){
        System.out.println("后置通知执行");
    }

    @AfterThrowing("pt1()")
    public void exceptionLog(){
        System.out.println("异常通知执行");
    }

    @After("pt1()")
    public void finalLog(){
        System.out.println("最终通知执行");
    }
}
