package demo5.service;

/**
 * 接口
 *
 * @author Anna.
 * @date 2025/2/14 22:01
 */
public interface ISendMassageService {

    void sendMessage(String msg);

    void acceptMessage();
}
