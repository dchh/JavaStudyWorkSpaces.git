package demo1.dao;

import demo1.entity.AccountBookDo;

/**
 * AccountBookDao 接口类
 *
 * @author Anna.
 * @date 2025/2/11 19:42
 */
public interface IAccountBookDao {

    /**
     * 查询账单
     * @param name
     * @return
     */
    AccountBookDo selectAccountBookByName(String name);

    /**
     * 更新账单
     * @param money
     * @param name
     */
    void updateAccountBook(float money,String name);
}
