package demo1.service;

import demo1.entity.AccountBookDo;

/**
 * AccountBookService 接口类
 *
 * @author Anna.
 * @date 2025/2/11 20:02
 */
public interface IAccountBookService {

    /**
     * 查询账单
     * @param name
     * @return
     */
    AccountBookDo selectAccountBookByName(String name);

    /**
     * 更新账单
     * @param money
     * @param name
     */
    void updateAccountBook(float money,String name);
}
