package demo2.utils;

import java.sql.SQLException;

/**
 * 事物管理相关工具类
 *
 * @author Anna.
 * @date 2025/2/13 11:50
 */
public class TransactionManager {

    private ConnectionUtils connectionUtils;

    public void setConnectionUtils(ConnectionUtils connectionUtils) {
        this.connectionUtils = connectionUtils;
    }

    /**
     * 开启事务
     */
    public void beginTransaction() throws SQLException {
        System.out.println("开启事务");
        connectionUtils.getThreadConnection().setAutoCommit(false);
    }

    /**
     * 提交事务
     */
    public void commit() throws SQLException {
        System.out.println("提交事务");
        connectionUtils.getThreadConnection().commit();
    }

    /**
     * 回滚事务
     */
    public void rollback(){
        try {
            System.out.println("回滚事务");
            connectionUtils.getThreadConnection().rollback();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 释放连接
     */
    public void release() {
        try {
            System.out.println("释放连接");
            connectionUtils.getThreadConnection().close();
            // 把链接和线程解绑
            connectionUtils.removeThreadConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
