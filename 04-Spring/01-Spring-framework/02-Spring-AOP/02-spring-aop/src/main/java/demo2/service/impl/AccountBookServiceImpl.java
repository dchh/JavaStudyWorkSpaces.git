package demo2.service.impl;

import demo2.dao.IAccountBookDao;
import demo2.entity.AccountBookDo;
import demo2.service.IAccountBookService;
import demo2.utils.TransactionManager;

import java.sql.SQLException;

/**
 * AccountBookService 接口实现类
 *
 * @author Anna.
 * @date 2025/2/11 20:03
 */
public class AccountBookServiceImpl implements IAccountBookService {

    private IAccountBookDao accountBookDao;

    private TransactionManager txManager;

    public void setAccountBookDao(IAccountBookDao accountBookDao) {
        this.accountBookDao = accountBookDao;
    }

    public void setTxManager(TransactionManager txManager) {
        this.txManager = txManager;
    }

    @Override
    public AccountBookDo selectAccountBookByName(String name) throws SQLException {
        return accountBookDao.selectAccountBookByName(name);
    }

    @Override
    public void updateAccountBook(float money, String name) throws SQLException {
        accountBookDao.updateAccountBook(money, name);
    }

    @Override
    public void transferAccounts() throws SQLException {
        // 2.1. 查询账户zhangsan
        AccountBookDo zhangsan = accountBookDao.selectAccountBookByName("zhangsan");
        // 2.2.查询账户lisi
        AccountBookDo lisi = accountBookDao.selectAccountBookByName("lisi");
        // 定义金融
        float money = 100f;
        // 2.3.账户zhangsan - 100
        accountBookDao.updateAccountBook(zhangsan.getMoney() - money, zhangsan.getName());
        System.out.println("账户zhangsan - 100");
        // 运行异常导致事物不一致
        int i = 1 / 0;

        // 2.4.账户lisi + 100
        accountBookDao.updateAccountBook(lisi.getMoney() + money, lisi.getName());
        System.out.println("账户lisi + 100");
        System.out.println("执行完成");
    }

}
