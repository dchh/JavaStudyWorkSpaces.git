package demo2;

import demo2.service.IAccountBookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.SQLException;

/**
 * 测试类
 *
 * @author Anna.
 * @date 2025/2/11 19:30
 */
public class TestClient {
    public static void main(String[] args) throws SQLException {
        ApplicationContext ac = new ClassPathXmlApplicationContext("demo2.xml");
        // 获取service
        IAccountBookService accountService = (IAccountBookService) ac.getBean("accountService");
        accountService.transferAccounts();
    }
}
