package demo3.utils;

/**
 * 日志工具类
 *
 * @author Anna.
 * @date 2025/2/14 22:12
 */
public class LoggerUtils {

    public void beforeLog(){
        System.out.println("前置通知执行");
    }

    public void afterLog(){
        System.out.println("后置通知执行");
    }

    public void exceptionLog(){
        System.out.println("异常通知执行");
    }

    public void finalLog(){
        System.out.println("最终通知执行");
    }
}
