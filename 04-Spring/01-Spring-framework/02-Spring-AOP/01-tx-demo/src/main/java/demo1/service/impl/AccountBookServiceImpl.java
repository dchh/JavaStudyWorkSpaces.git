package demo1.service.impl;

import demo1.dao.IAccountBookDao;
import demo1.entity.AccountBookDo;
import demo1.service.IAccountBookService;

/**
 * AccountBookService 接口实现类
 *
 * @author Anna.
 * @date 2025/2/11 20:03
 */
public class AccountBookServiceImpl implements IAccountBookService {

    private IAccountBookDao accountBookDao;

    public void setAccountBookDao(IAccountBookDao accountBookDao) {
        this.accountBookDao = accountBookDao;
    }

    @Override
    public AccountBookDo selectAccountBookByName(String name) {
        return accountBookDao.selectAccountBookByName(name);
    }

    @Override
    public void updateAccountBook(float money, String name) {
        accountBookDao.updateAccountBook(money, name);
    }
}
