package demo1;

import demo1.entity.AccountBookDo;
import demo1.service.IAccountBookService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.Assert;

import java.util.Arrays;

/**
 * 测试类
 *
 * @author Anna.
 * @date 2025/2/11 19:30
 */
public class TestClient {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("demo1.xml");
        // 获取service
        IAccountBookService accountService = (IAccountBookService) ac.getBean("accountService");
        // 1. 查询账户zhangsan
        AccountBookDo zhangsan = accountService.selectAccountBookByName("zhangsan");
        // 2.查询账户lisi
        AccountBookDo lisi = accountService.selectAccountBookByName("lisi");
        // 定义金融
        float money = 100f;
        // 3.账户zhangsan - 100
        accountService.updateAccountBook(zhangsan.getMoney() - money, zhangsan.getName());

        // 运行异常导致事物不一致
        int i = 1 / 0;

        // 4.账户lisi + 100
        accountService.updateAccountBook(lisi.getMoney() + money, lisi.getName());

        System.out.println("执行完成");
    }
}
