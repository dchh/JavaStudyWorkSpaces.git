package demo1.dao.impl;

import demo1.dao.IAccountBookDao;
import demo1.entity.AccountBookDo;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

/**
 * AccountBookDao 实现类
 *
 * @author Anna.
 * @date 2025/2/11 19:42
 */
public class AccountBookDaoImpl implements IAccountBookDao {

    private QueryRunner runner;

    public AccountBookDaoImpl(QueryRunner runner) {
        this.runner = runner;
    }

    @Override
    public AccountBookDo selectAccountBookByName(String name) {
        try {
            return runner.query("select * from accountbook where name = ? ", new BeanHandler<AccountBookDo>(AccountBookDo.class), name);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateAccountBook(float money, String name) {
        try {
            runner.update("update accountbook set money=? where name=?", money, name);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
