package demo3;

import demo3.service.impl.AccountBookServiceImpl;
import demo3.utils.ProxyUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 测试类
 *
 * @author Anna.
 * @date 2025/2/11 19:30
 */
public class TestClient {
    public static void main(String[] args) throws Exception {
        ApplicationContext ac = new ClassPathXmlApplicationContext("demo3.xml");
        // 获取service
        AccountBookServiceImpl accountService = (AccountBookServiceImpl) ac.getBean("accountService");
        ProxyUtils proxyUtils = (ProxyUtils) ac.getBean("proxyUtils");
        proxyUtils.setProxyService(accountService);
        proxyUtils.dealProxyMethod();
    }
}
