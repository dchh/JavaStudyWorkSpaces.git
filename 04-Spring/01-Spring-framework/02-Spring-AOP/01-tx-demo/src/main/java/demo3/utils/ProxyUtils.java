package demo3.utils;

/**
 * JDK实现动态代理
 *
 * @author Anna.
 * @date 2025/2/14 9:45
 */
public class ProxyUtils implements IProxyService {

    private IProxyService proxyService;

    private TransactionManager txManager;

    public ProxyUtils(TransactionManager txManager) {
        this.txManager = txManager;
    }

    public void setProxyService(IProxyService proxyService) {
        this.proxyService = proxyService;
    }

    @Override
    public void dealProxyMethod() throws Exception {
        // 1 开启事务
        try {
            txManager.beginTransaction();
            // 2 执行操作
            proxyService.dealProxyMethod();
            // 3 提交事物
            txManager.commit();
        } catch (Exception e) {
            System.out.println("执行回退");
            // 4 回滚事务
            txManager.rollback();
            throw new RuntimeException(e);
        } finally {
            // 5 关闭连接
            txManager.release();
        }
    }
}
