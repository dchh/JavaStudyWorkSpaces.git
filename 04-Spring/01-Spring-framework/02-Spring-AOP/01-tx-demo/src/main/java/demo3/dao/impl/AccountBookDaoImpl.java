package demo3.dao.impl;

import demo3.dao.IAccountBookDao;
import demo3.entity.AccountBookDo;
import demo3.utils.ConnectionUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;

import java.sql.SQLException;

/**
 * AccountBookDao 实现类
 *
 * @author Anna.
 * @date 2025/2/11 19:42
 */
public class AccountBookDaoImpl implements IAccountBookDao {

    private QueryRunner runner;

    private ConnectionUtils connectionUtils;

    public AccountBookDaoImpl(QueryRunner runner, ConnectionUtils connectionUtils) {
        this.runner = runner;
        this.connectionUtils = connectionUtils;
    }

    @Override
    public AccountBookDo selectAccountBookByName(String name) throws SQLException {
        return runner.query(connectionUtils.getThreadConnection(), "select * from accountbook where name = ? ", new BeanHandler<AccountBookDo>(AccountBookDo.class), name);
    }

    @Override
    public void updateAccountBook(float money, String name) throws SQLException {
        runner.update(connectionUtils.getThreadConnection(), "update accountbook set money=? where name=?", money, name);
    }
}
