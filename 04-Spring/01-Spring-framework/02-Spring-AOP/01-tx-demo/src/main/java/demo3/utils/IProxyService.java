package demo3.utils;

/**
 * 抽象角色
 *
 * @author Anna.
 * @date 2025/2/14 9:56
 */
public interface IProxyService {
    // 代理方法
    void dealProxyMethod() throws Exception;
}
