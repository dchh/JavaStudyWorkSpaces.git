package demo3.service;

import demo3.entity.AccountBookDo;

import java.sql.SQLException;

/**
 * AccountBookService 接口类
 *
 * @author Anna.
 * @date 2025/2/11 20:02
 */
public interface IAccountBookService {

    /**
     * 查询账单
     * @param name
     * @return
     */
    AccountBookDo selectAccountBookByName(String name) throws SQLException;

    /**
     * 更新账单
     * @param money
     * @param name
     */
    void updateAccountBook(float money,String name) throws Exception;
}
