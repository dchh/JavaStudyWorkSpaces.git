/*
 Navicat Premium Data Transfer

 Source Server         : localshot
 Source Server Type    : MySQL
 Source Server Version : 50710
 Source Host           : 127.0.0.1:3306
 Source Schema         : testdb

 Target Server Type    : MySQL
 Target Server Version : 50710
 File Encoding         : 65001

 Date: 11/02/2025 19:38:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for accountbook
-- ----------------------------
DROP TABLE IF EXISTS `accountbook`;
CREATE TABLE `accountbook`  (
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `money` float NULL DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of accountbook
-- ----------------------------
INSERT INTO `accountbook` VALUES ('lisi', 1000);
INSERT INTO `accountbook` VALUES ('zhangsan', 1000);

SET FOREIGN_KEY_CHECKS = 1;
