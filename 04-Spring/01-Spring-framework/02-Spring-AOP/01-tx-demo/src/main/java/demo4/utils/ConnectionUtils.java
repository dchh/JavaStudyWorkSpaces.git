package demo4.utils;

import com.mysql.jdbc.Connection;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * 连接的工具类，用于实现从数据源中获取一个链接，并且实现和线程的绑定
 *
 * @author Anna.
 * @date 2025/2/13 11:42
 */
public class ConnectionUtils {
    private ThreadLocal<Connection> local = new ThreadLocal<>();

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Connection getThreadConnection() {
        // 1 先从ThreadLocal中获取
        Connection connection = local.get();
        // 2 判断当前线程是否有链接
        if(connection == null) {
            // 3 从数据源中获取一个链接，并且设置到ThreadLocal中
            try {
                connection = (Connection) dataSource.getConnection();
                local.set(connection);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        // 4 返回当前线程上的连接
        return connection;
    }

    /**
     * 把链接和线程解绑
     */
    public void removeThreadConnection(){
        local.remove();
    }
}
