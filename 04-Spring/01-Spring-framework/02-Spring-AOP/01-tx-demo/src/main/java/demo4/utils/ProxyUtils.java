package demo4.utils;

import demo4.service.IAccountBookService;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * JDK实现动态代理
 *
 * @author Anna.
 * @date 2025/2/14 9:45
 */
public class ProxyUtils{

    private IAccountBookService accountBookService;

    private TransactionManager txManager;

    public ProxyUtils(TransactionManager txManager) {
        this.txManager = txManager;
    }

    public void setAccountBookService(IAccountBookService accountBookService) {
        this.accountBookService = accountBookService;
    }

    /**
     * 获取Service代理对象
     * @return
     */
    public IAccountBookService getAccountBookService(){
        return (IAccountBookService) Proxy.newProxyInstance(accountBookService.getClass().getClassLoader(), accountBookService.getClass().getInterfaces(), new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                Object rtn = null;
                // 1 开启事务
                try {
                    txManager.beginTransaction();
                    // 2 执行操作
                    rtn = method.invoke(accountBookService,args);
                    // 3 提交事物
                    txManager.commit();
                    return rtn;
                } catch (Exception e) {
                    System.out.println("执行回退");
                    // 4 回滚事务
                    txManager.rollback();
                    throw new RuntimeException(e);
                } finally {
                    // 5 关闭连接
                    txManager.release();
                }
            }
        });
    }
}
