package demo4;

import demo4.service.IAccountBookService;
import demo4.service.impl.AccountBookServiceImpl;
import demo4.utils.ProxyUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 测试类
 *
 * @author Anna.
 * @date 2025/2/11 19:30
 */
public class TestClient {
    public static void main(String[] args) throws Exception {
        ApplicationContext ac = new ClassPathXmlApplicationContext("demo4.xml");
        // 获取需要代理类
        AccountBookServiceImpl accountService = (AccountBookServiceImpl) ac.getBean("accountService");
        // 获取代理工具类
        ProxyUtils proxyUtils = (ProxyUtils) ac.getBean("proxyUtils");
        // 设置真实代理类
        proxyUtils.setAccountBookService(accountService);
        // 获取代理对象
        IAccountBookService accountBookService = proxyUtils.getAccountBookService();
        // 执行代理方法
        accountBookService.transferAccounts();
    }
}
