package demo4.entity;

/**
 * DO
 *
 * @author Anna.
 * @date 2025/2/11 19:39
 */
public class AccountBookDo {
    private String name;
    private float money;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "AccountBookDo{" +
                "name='" + name + '\'' +
                ", money=" + money +
                '}';
    }
}
