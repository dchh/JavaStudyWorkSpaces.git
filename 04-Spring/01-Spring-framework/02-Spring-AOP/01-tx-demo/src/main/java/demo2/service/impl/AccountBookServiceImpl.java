package demo2.service.impl;

import demo2.dao.IAccountBookDao;
import demo2.entity.AccountBookDo;
import demo2.service.IAccountBookService;
import demo2.utils.TransactionManager;

import java.sql.SQLException;

/**
 * AccountBookService 接口实现类
 *
 * @author Anna.
 * @date 2025/2/11 20:03
 */
public class AccountBookServiceImpl implements IAccountBookService {

    private IAccountBookDao accountBookDao;

    private TransactionManager txManager;

    public void setAccountBookDao(IAccountBookDao accountBookDao) {
        this.accountBookDao = accountBookDao;
    }

    public void setTxManager(TransactionManager txManager) {
        this.txManager = txManager;
    }

    @Override
    public AccountBookDo selectAccountBookByName(String name) throws SQLException {
        return accountBookDao.selectAccountBookByName(name);
    }

    @Override
    public void updateAccountBook(float money, String name) throws SQLException {
        accountBookDao.updateAccountBook(money, name);
    }

    @Override
    public void transferAccounts() {
        // 1 开启事务
        try {
            txManager.beginTransaction();
            // 2 执行操作
            // 2.1. 查询账户zhangsan
            AccountBookDo zhangsan = this.selectAccountBookByName("zhangsan");
            // 2.2.查询账户lisi
            AccountBookDo lisi = this.selectAccountBookByName("lisi");
            // 定义金融
            float money = 100f;
            // 2.3.账户zhangsan - 100
            this.updateAccountBook(zhangsan.getMoney() - money, zhangsan.getName());
            System.out.println("账户zhangsan - 100");
            // 运行异常导致事物不一致
            int i = 1 / 0;

            // 2.4.账户lisi + 100
            this.updateAccountBook(lisi.getMoney() + money, lisi.getName());
            System.out.println("账户lisi + 100");
            System.out.println("执行完成");
            // 3 提交事物
            txManager.commit();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("执行回退");
            // 4 回滚事务
            txManager.rollback();
            throw new RuntimeException(e);
        }
        finally {
            // 5 关闭连接
            txManager.release();
        }
    }

}
