package demo4;

import javassist.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class JavassistProxyFactory {

    /**
     * 创建代理类：相当于实现demo3中StartProxy
     *
     * @param target   真实角色
     * @param methodHandler  处理类（具体实现）
     * @return T
     * @author Anna.
     * @date 2024/4/9 17:26
     */
    public static <T> T createProxy(T target, MethodHandler methodHandler) throws Exception {
        // 创建 ClassPool，它是 Javassist 的核心类，用于处理类
        ClassPool pool = ClassPool.getDefault();
        // 获取目标类的 CtClass 对象
        CtClass ctClass = pool.get(target.getClass().getName());
        // 获取目标类的 CtClass 对象
        CtClass methodHandlerCtClass = pool.get(methodHandler.getClass().getName());
        // 创建代理类的名称
        String proxyClassName = target.getClass().getName() + "$Proxy";
        // 创建代理类的 CtClass 对象
        CtClass proxyCtClass = pool.makeClass(proxyClassName);
        // 设置代理类继承自目标类 用于实现其所有方法
        proxyCtClass.setSuperclass(ctClass);

        // 添加字段
        CtField aField = new CtField(ctClass, "a", proxyCtClass);
        aField.setModifiers(javassist.Modifier.PRIVATE);
        CtField bField = new CtField(methodHandlerCtClass, "b", proxyCtClass);
        bField.setModifiers(javassist.Modifier.PRIVATE);

        // 添加属性
        proxyCtClass.addField(aField);
        proxyCtClass.addField(bField);

        // 创建一个构造函数来初始化name属性
        CtConstructor constructor = new CtConstructor(new CtClass[]{ctClass,methodHandlerCtClass}, proxyCtClass);
        constructor.setModifiers(javassist.Modifier.PUBLIC);
        // $0 this $1 第一个参数 $2 第二个参数 依次类推
        constructor.setBody("{this.a = $1;this.b = $2;}");
        proxyCtClass.addConstructor(constructor);

        // 遍历目标类的所有方法，并创建相应的方法在代理类中
        for (Method method : target.getClass().getDeclaredMethods()) {
            // 创建方法
            CtMethod ctMethod = new CtMethod(pool.get(method.getReturnType().getName()), method.getName(), getParameterTypes(method), proxyCtClass);
            ctMethod.setModifiers(Modifier.PUBLIC);
            // 设置方法体，调用 MethodHandler 的 invoke 方法
            ctMethod.setBody("{ b.invoke(a, \"" + method.getName() + "\", $args); }");
            // 添加方法到代理类
            proxyCtClass.addMethod(ctMethod);
        }

        // 反射
        Class<?>  aClass = proxyCtClass.toClass();
        Constructor<?>[] constructors = aClass.getConstructors();
        Object obj = constructors[0].newInstance(target, methodHandler);
        // 创建代理类的实例
        return (T) obj;
    }

    /**
     * 获取方法参数列表
     *
     * @param method
     * @return javassist.CtClass[]
     * @author Anna.
     * @date 2024/4/9 17:40
     */
    private static CtClass[] getParameterTypes(Method method) throws NotFoundException {
        CtClass[] ctClasses = new CtClass[method.getParameterTypes().length];
        for (int i = 0; i < method.getParameterTypes().length; i++) {
            ctClasses[i] = ClassPool.getDefault().get(method.getParameterTypes()[i].getName());
        }
        return ctClasses;
    }

}