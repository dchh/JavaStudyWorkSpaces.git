package demo4;

/**
 * 抽象角色
 * 明星：具备唱歌能力
 *
 * @author Anna.
 * @date 2024/4/9 10:09
 */
public interface Start {
    void sing() ;
}
