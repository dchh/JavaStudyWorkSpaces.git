package demo4;


/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/9 17:41
 */
public class TestClient4 {
    public static void main(String[] args) throws Exception {
        Start realStart = new RealStart();
        StartMethodHandle startMethodHandle = new StartMethodHandle();
        Start proxy = (Start) JavassistProxyFactory.createProxy(realStart, startMethodHandle);
        proxy.sing();
    }
}
