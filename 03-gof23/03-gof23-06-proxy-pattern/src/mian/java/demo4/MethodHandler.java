package demo4;

/**
 * 中转处理接口
 *
 * @author Anna.
 * @date 2024/4/9 16:17
 */
public interface MethodHandler {
    Object invoke(Object self, String methodName, Object[] args) throws Exception;
}
