package demo3;

import java.lang.reflect.Method;

/**
 * 处理类（具体实现）
 *
 * @author Anna.
 * @date 2024/4/9 16:17
 */
public class StartMethodHandle implements MethodHandler {
    @Override
    public Object invoke(Object self, String methodName, Object[] args) throws Throwable {
        System.out.println("代理执行之前");
        Method method = self.getClass().getMethod(methodName);
        Object invoke = method.invoke(self, args);
        System.out.println("代理执行之后");
        return invoke;
    }
}
