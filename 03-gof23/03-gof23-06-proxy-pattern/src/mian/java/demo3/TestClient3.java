package demo3;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/9 16:48
 */
public class TestClient3 {
    public static void main(String[] args) {
        // 创建真实角色
        Start start = new RealStart();
        // 创建代理处理类
        StartMethodHandle startMethodHandle = new StartMethodHandle();
        // 创建代理对象
        StartProxy startProxy = new StartProxy(startMethodHandle, start);
        try {
            startProxy.sing();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }
}
