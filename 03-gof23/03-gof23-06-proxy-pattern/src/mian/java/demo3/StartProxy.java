package demo3;

/**
 * 代理对象
 *
 * @author Anna.
 * @date 2024/4/9 16:32
 */
public class StartProxy implements Start {
    private MethodHandler methodHandler;
    private Start start;

    public StartProxy(MethodHandler methodHandler, Start start) {
        this.methodHandler = methodHandler;
        this.start = start;
    }

    @Override
    public void sing() throws Throwable {
        methodHandler.invoke(start,"sing",null);
    }
}
