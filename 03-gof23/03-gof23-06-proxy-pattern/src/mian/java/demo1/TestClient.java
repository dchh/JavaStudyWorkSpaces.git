package demo1;

/**
 * 测试静态代理模式
 *
 * @author Anna.
 * @date 2024/4/9 10:17
 */
public class TestClient {
    public static void main(String[] args) {
        // 创建真实角色
        RealStart realStart = new RealStart();
        // 创建代理角色
        ProxyStart proxyStart = new ProxyStart(realStart);
        // 方法调用
        // 面谈
        proxyStart.interview();
        // 合同起草
        proxyStart.contractDrafting();
        // 签字
        proxyStart.signature();
        // 前期准备
        proxyStart.preliminaryPreparation();
        // 唱歌
        proxyStart.sing();
        // 收尾款
        proxyStart.closingPayment();
    }
}
