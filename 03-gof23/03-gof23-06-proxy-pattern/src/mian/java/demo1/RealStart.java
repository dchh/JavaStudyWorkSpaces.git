package demo1;

/**
 * 真实角色
 * 实现明星唱歌
 *
 * @author Anna.
 * @date 2024/4/9 10:10
 */
public class RealStart implements Start{
    @Override
    public void sing() {
        System.out.println("周杰伦唱歌");
    }
}
