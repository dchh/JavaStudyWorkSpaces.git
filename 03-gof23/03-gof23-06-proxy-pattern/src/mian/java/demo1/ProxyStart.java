package demo1;

/**
 * 代理明星
 * 通过实现Star方法代理明星唱歌
 * @author Anna.
 * @date 2024/4/9 10:12
 */
public class ProxyStart implements Start{
    // 代理明星
    private Start start;

    // 通过构造器传入设置代理明星
    public ProxyStart(Start start) {
        this.start = start;
    }

    // 重写唱歌，代理实现唱歌
    @Override
    public void sing() {
        System.out.print("5. ");
        start.sing();
    }

    public void interview(){
        System.out.println("1. 面谈");
    }

    public void contractDrafting(){
        System.out.println("2. 合同起草");
    }

    public void signature(){
        System.out.println("3. 签字");
    }

    public void preliminaryPreparation(){
        System.out.println("4. 前期准备");
    }

    public void closingPayment(){
        System.out.println("6. 收尾款");
    }
}
