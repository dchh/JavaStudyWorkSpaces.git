package demo2;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * JDK 实现动态代理
 *
 * @author Anna.
 * @date 2024/4/9 11:32
 */
public class StartHandle implements InvocationHandler {

    // 代理明星
    private Start start;

    // 通过构造器传入设置代理明星
    public StartHandle(Start start) {
        this.start = start;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("代理方法执行之前");
        System.out.println("1. 面谈");
        System.out.println("2. 合同起草");
        System.out.println("3. 签字");
        System.out.println("4. 前期准备");
        System.out.print("5. ");
        method.invoke(start,args);
        System.out.println("代理方法执行之后");
        System.out.println("6. 收尾款");
        return true;
    }
}
