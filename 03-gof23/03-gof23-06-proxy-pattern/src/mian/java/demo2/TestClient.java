package demo2;

import java.lang.reflect.Proxy;

/**
 * 动态代理测试
 *
 * @author Anna.
 * @date 2024/4/9 11:34
 */
public class TestClient {
    public static void main(String[] args) {
        // 创建真实角色
        RealStart realStart = new RealStart();
        // 创建动态代理处理类
        StartHandle startHandle = new StartHandle(realStart);

        Start proxy = (Start) Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(), new Class[]{Start.class}, startHandle);

        // 处理业务
        proxy.sing();
    }
}
