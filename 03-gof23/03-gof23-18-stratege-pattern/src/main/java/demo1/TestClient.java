package demo1;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/25 8:46
 */
public class TestClient {
    public static void main(String[] args) {
        Context context = new Context(new OperationAdd());
        System.out.printf("选择加法-执行结果：%s%n",context.executeStrategy(5,2));
        context = new Context(new OperationMultiply());
        System.out.printf("选择乘法-执行结果：%s%n",context.executeStrategy(5,2));
    }
}
