package demo1;

/**
 * 具体策略类-加法
 *
 * @author Anna.
 * @date 2024/4/25 8:40
 */
public class OperationAdd implements Strategy{
    @Override
    public int dealOperation(int num1, int num2) {
        return num1 + num2;
    }
}
