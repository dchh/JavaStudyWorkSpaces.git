package demo1;

/**
 * 策略接口
 *
 * @author Anna.
 * @date 2024/4/25 8:39
 */
public interface Strategy {
    /**
     * 定义操作接口
     *
     * @param num1
     * @param num2
     * @return int
     * @author Anna.
     * @date 2024/4/25 8:40
     */
    int dealOperation(int num1,int num2);
}
