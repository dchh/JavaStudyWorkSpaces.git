package demo1;

/**
 * 上限文类
 *
 * @author Anna.
 * @date 2024/4/25 8:45
 */
public class Context {

    private Strategy strategy;

    public Context(Strategy strategy) {
        this.strategy = strategy;
    }

    public int executeStrategy(int num1, int num2) {
        return strategy.dealOperation(num1, num2);
    }
}
