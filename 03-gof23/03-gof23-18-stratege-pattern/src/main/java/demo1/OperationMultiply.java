package demo1;

/**
 * 具体策略类-乘法
 *
 * @author Anna.
 * @date 2024/4/25 8:43
 */
public class OperationMultiply implements Strategy {

    @Override
    public int dealOperation(int num1, int num2) {
        return num1 * num2;
    }
}
