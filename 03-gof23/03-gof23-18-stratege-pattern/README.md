# 策略模式
## 什么是策略模式
策略模式（Strategy Pattern）是行为设计模式之一，它使你能在运行时改变对象的行为。在策略模式中，一个类的行为或其算法可以在运行时更改。这种类型的设计模式属于行为模式。

在策略模式中，我们创建表示各种策略的对象和一个行为随着策略对象改变而改变的上下文对象。策略对象更改上下文对象的执行算法。

**优点：**
+ 策略模式提供了管理相关的算法族的办法。策略类的等级结构定义了一个算法族，恰当使用继承可以把公共的代码移到父类里面，从而避免代码重复。 
+ 策略模式提供了可以替换继承关系的办法。继承可以处理多种算法或行为。如果不是用策略模式，那么在这些算法或行为特别多的情况下，使用继承方式，会造成子类膨胀，使用策略模式可以避免这个问题。 
+ 使用策略模式可以避免使用多重条件转移语句。多重条件转移语句不易维护，它把采取哪一种算法或采取哪一种行为的逻辑与算法或行为的逻辑混合在一起，统统列在一个多重条件选择语句里面，比使用继承的办法还要原始和落后。

**缺点：**
+ 客户端必须知道不同策略的存在，并自行决定使用哪一个策略。 
+ 策略模式将造成产生很多策略类，增加了代码的复杂性。

> 策略模式对应于解决某一个问题的算法族，允许用户从该算法族中任选一个算法解决某一问题，同时可以方便的更换算法或者增加新的算法，并且有客户端决定调用那个算法。

## 案例
使用策略模式，对应两个数进行加减乘除操作

### UML
![img.png](images/img.png)

实现步骤：
+ 定义操作接口，为具体实现算法的创建提供统一的接口类
+ 创建具体的策略类，实现加减乘除不同的算法
+ 创建上限文对象，对象中持有策略算法的引用，提供调用方法，通过持有的策略算法的引用实现不同算法的调用

### 实现代码
Strategy.java
```java
// 策略接口
public interface Strategy {
    // 定义操作接口
    int dealOperation(int num1,int num2);
}

```
OperationAdd.java
```java
// 具体策略类-加法
public class OperationAdd implements Strategy{
    @Override
    public int dealOperation(int num1, int num2) {
        return num1 + num2;
    }
}
```

OperationSubtract.java
```java
// 具体策略类-减法
public class OperationSubtract implements Strategy {
    @Override
    public int dealOperation(int num1, int num2) {
        return num1 - num2;
    }
}
```

OperationMultiply.java
```java
// 具体策略类-乘法
public class OperationMultiply implements Strategy {
    @Override
    public int dealOperation(int num1, int num2) {
        return num1 * num2;
    }
}

```

OperationDivide.java
```java
// 具体策略类-除法
public class OperationDivide implements Strategy {

    @Override
    public int dealOperation(int num1, int num2) {
        return num1 / num2;
    }
}

```

Context.java
```java
// 上限文类
public class Context {

    private Strategy strategy;

    public Context(Strategy strategy) {
        this.strategy = strategy;
    }

    public int executeStrategy(int num1, int num2) {
        return strategy.dealOperation(num1, num2);
    }
}

```

TestClient.java
```java
public class TestClient {
    public static void main(String[] args) {
        Context context = new Context(new OperationAdd());
        System.out.printf("选择加法-执行结果：%s%n",context.executeStrategy(5,2));
        context = new Context(new OperationMultiply());
        System.out.printf("选择乘法-执行结果：%s%n",context.executeStrategy(5,2));
    }
}
```

执行结果：

![img_1.png](images/img_1.png)

## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)