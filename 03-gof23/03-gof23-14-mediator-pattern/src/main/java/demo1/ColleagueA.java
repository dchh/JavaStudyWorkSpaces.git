package demo1;

/**
 * 同事A
 *
 * @author Anna.
 * @date 2024/4/16 16:06
 */
public class ColleagueA implements Colleague{

    /** 持有中介者的引用 **/
    private Mediator mediator;

    public ColleagueA(Mediator mediator) {
        this.mediator = mediator;
        // 注册到中介者中
        mediator.register("A",this);
    }

    @Override
    public void selfAction() {
        System.out.printf("%s-发出信息%n",this.getClass().getSimpleName());
    }

    @Override
    public void receiveAction() {
        System.out.printf("%s-接收信息进行处理%n",this.getClass().getSimpleName());
    }

    @Override
    public void outAction(String name) {
        System.out.printf("%s-向中介者发出申请%n",this.getClass().getSimpleName());
        this.mediator.command(name);
    }
}
