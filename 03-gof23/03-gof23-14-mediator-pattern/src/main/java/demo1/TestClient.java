package demo1;

/**
 * 测试类
 *
 * @author Anna.
 * @date 2024/4/16 16:15
 */
public class TestClient {
    public static void main(String[] args) {
        // 定义中介者
        Mediator mediator = new ConcreteMediator();
        // 定义同事类
        Colleague a = new ColleagueA(mediator);
        Colleague b = new ColleagueB(mediator);

        // A发出消息
        a.selfAction();
        // 通过中介者转发给B接收消息
        a.outAction("B");
    }
}
