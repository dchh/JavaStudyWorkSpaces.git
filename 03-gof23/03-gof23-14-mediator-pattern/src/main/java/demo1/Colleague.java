package demo1;

/**
 * 同事类接口
 *
 * @author Anna.
 * @date 2024/4/16 16:01
 */
public interface Colleague {
    /**
     * 处理自己的事
     *
     * @param
     * @return void
     * @author Anna.
     * @date 2024/4/16 16:02
     */
    void selfAction();

    /**
     * 接收消息
     *
     * @param
     * @return void
     * @author Anna.
     * @date 2024/4/16 16:14
     */
    void receiveAction();

    /**
     * 向中介者发出申请
     *
     * @param name
     * @return void
     * @author Anna.
     * @date 2024/4/16 16:02
     */
    void outAction(String name);
}
