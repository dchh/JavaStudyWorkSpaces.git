package demo1;

import java.util.HashMap;
import java.util.Map;

/**
 * 中介者的具体实现
 *
 * @author Anna.
 * @date 2024/4/16 16:10
 */
public class ConcreteMediator implements Mediator {

    /** 定义存储同事集合 */
    private Map<String,Colleague> map = new HashMap<String,Colleague>();

    @Override
    public void register(String name, Colleague colleague) {
        map.put(name,colleague);
    }

    @Override
    public void command(String name) {
        // 传递给接收者
        map.get(name).receiveAction();
    }
}
