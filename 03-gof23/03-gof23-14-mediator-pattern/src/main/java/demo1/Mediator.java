package demo1;

/**
 * 中介者接口
 *
 * @author Anna.
 * @date 2024/4/16 16:00
 */
public interface Mediator {

    /**
     * 注册接口
     *
     * @param name  注册名称
     * @param colleague
     * @return void
     * @author Anna.
     * @date 2024/4/16 16:04
     */
    void register(String name,Colleague colleague);

    /**
     * 处理接口
     *
     * @param name  注册名称
     * @return void
     * @author Anna.
     * @date 2024/4/16 16:05
     */
    void command(String name);
}
