# 中介者模式
## 什么是中介者模式
中介者模式（Mediator Pattern）是一种行为设计模式，用于减少对象之间的直接依赖关系，降低它们之间的耦合度，并使得一个对象改变时，所有依赖于它的对象都得到通知并自动更新。在中介者模式中，一个中介者对象来封装一系列对象之间的交互，从而使这些对象不需要显式地相互引用，降低了它们的耦合度。

在 Java 中实现中介者模式，通常涉及以下几个角色：
+ 中介者（Mediator）：定义了对象之间的交互方式，协调多个对象之间的行为。
+ 同事类（Colleague）：知道中介者的存在，与中介者进行交互，而不需要与其他同事类直接交互。

## 中介者模式的本质
解耦各个同事对象之间的交互关系。每个对象都持有中介者对象的引用，只跟中介者对象打交道。我们通过中介者对象统一管理这些交互关系。

常见使用场景：
+ MVC模式其中C，控制器就是一个中介者对象，M和V都和它打交道。
+ 窗口游戏程序，窗口软件开发中窗口对象也是一个中介者对象
+ 图形界面卡法GUI中，多个组件只见的交互，可以通过引入一个中介者对象来解决，可以是整体的窗口对象或者DOM对象
+ java.lang.reflect.Method#invoke()

## 案例
A发送消息通过中介者转发给B接收

### UML
![img.png](images/img.png)

实现步骤：
+ 定义同事类接口，接口中定义处理自己事情，接收消息及向中介者发送申请接口
+ 定义中介者接口，接口中定义注册及处理接口
+ 定义同事A，构造方法中传入中介者获取中介者的引用，及完成注册，实现同事类接口
+ 定义同事B，构造方法中传入中介者获取中介者的引用，及完成注册，实现同事类接口
+ 定义中介者实现类，通过MAP存储注册信息及完成转发动作

### 实现代码
Colleague.java
```java
// 同事类接口
public interface Colleague {
    // 处理自己的事
    void selfAction();
    // 接收消息
    void receiveAction();
    // 向中介者发出申请
    void outAction(String name);
}
```
Mediator.java
```java
// 中介者接口
public interface Mediator {
    // 注册接口
    void register(String name,Colleague colleague);
    // 处理接口
    void command(String name);
}

```
ColleagueA.java
```java
// 同事A
public class ColleagueA implements Colleague{

    /** 持有中介者的引用 **/
    private Mediator mediator;

    public ColleagueA(Mediator mediator) {
        this.mediator = mediator;
        // 注册到中介者中
        mediator.register("A",this);
    }

    @Override
    public void selfAction() {
        System.out.printf("%s-发出信息%n",this.getClass().getSimpleName());
    }

    @Override
    public void receiveAction() {
        System.out.printf("%s-接收信息进行处理%n",this.getClass().getSimpleName());
    }

    @Override
    public void outAction(String name) {
        System.out.printf("%s-向中介者发出申请%n",this.getClass().getSimpleName());
        this.mediator.command(name);
    }
}
```
ColleagueB.java
```java
// 同事B
public class ColleagueB implements Colleague{

    /** 持有中介者的引用 **/
    private Mediator mediator;

    public ColleagueB(Mediator mediator) {
        this.mediator = mediator;
        // 注册到中介者中
        mediator.register("B",this);
    }

    @Override
    public void selfAction() {
        System.out.printf("%s-发出信息%n",this.getClass().getSimpleName());
    }

    @Override
    public void receiveAction() {
        System.out.printf("%s-接收信息进行处理%n",this.getClass().getSimpleName());
    }

    public void outAction(String name) {
        System.out.printf("%s-向中介者发出申请%n",this.getClass().getSimpleName());
        this.mediator.command(name);
    }
}

```
ConcreteMediator.java
```java
import java.util.HashMap;
import java.util.Map;

// 中介者的具体实现
public class ConcreteMediator implements Mediator {

    /** 定义存储同事集合 */
    private Map<String,Colleague> map = new HashMap<String,Colleague>();

    @Override
    public void register(String name, Colleague colleague) {
        map.put(name,colleague);
    }

    @Override
    public void command(String name) {
        // 传递给接收者
        map.get(name).receiveAction();
    }
}
```
TestClient.java
```java
package demo1;

/**
 * 测试类
 *
 * @author Anna.
 * @date 2024/4/16 16:15
 */
public class TestClient {
    public static void main(String[] args) {
        // 定义中介者
        Mediator mediator = new ConcreteMediator();
        // 定义同事类
        Colleague a = new ColleagueA(mediator);
        Colleague b = new ColleagueB(mediator);
        // A发出消息
        a.selfAction();
        // 通过中介者转发给B接收消息
        a.outAction("B");
    }
}
```

执行结果：

![img_1.png](images/img_1.png)

## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)