package demo1;

import java.util.ArrayList;
import java.util.List;

/**
 * 容器节点
 *
 * @author Anna.
 * @date 2024/4/10 11:59
 */
public class Composite implements Component{

    // 作为容器存储子节点信息
    private List<Component> components = new ArrayList<>();

    public Composite(List<Component> components) {
        this.components = components;
    }

    public List<Component> getComponents() {
        return components;
    }

    public void setComponents(List<Component> components) {
        this.components = components;
    }

    @Override
    public void operation() {
        System.out.println("容器构件操作");
        for (Component component : components) {
            // 递归执行叶子节点操作
            component.operation();
        }
    }


}
