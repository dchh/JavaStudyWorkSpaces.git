package demo1;

/**
 * 抽象构件
 *
 * @author Anna.
 * @date 2024/4/10 11:57
 */
public interface Component {
    // 相同行为操作
    void operation();
}
