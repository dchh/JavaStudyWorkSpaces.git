package demo1;

/**
 * 叶子
 *
 * @author Anna.
 * @date 2024/4/10 11:58
 */
public class Leaf implements Component {
    @Override
    public void operation() {
        System.out.println("叶子单独操作");
    }
}
