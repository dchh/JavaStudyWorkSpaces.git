package demo2;

/**
 * 抽象组件
 *
 * @author Anna.
 * @date 2024/4/10 12:47
 */
public interface AbstractFile {
    // 杀毒功能
    void killVirus();
}
