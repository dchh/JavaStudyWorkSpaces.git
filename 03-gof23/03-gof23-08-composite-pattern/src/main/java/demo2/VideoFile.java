package demo2;

/**
 * 视频类型文件
 *
 * @author Anna.
 * @date 2024/4/10 12:49
 */
public class VideoFile implements AbstractFile{
    private String name;

    public VideoFile(String name) {
        this.name = name;
    }

    @Override
    public void killVirus() {
        System.out.printf("视频类型文件-%s-进行扫毒%n",this.name);
    }
}
