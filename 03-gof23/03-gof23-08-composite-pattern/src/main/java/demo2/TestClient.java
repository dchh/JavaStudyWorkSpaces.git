package demo2;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/10 12:53
 */
public class TestClient {
    public static void main(String[] args) {
        AbstractFile file1 = new TxtFile("花无缺.TXT");
        AbstractFile file2 = new ImagesFile("花无缺.png");
        AbstractFile file3 = new ImagesFile("花无缺.avi");
        List<AbstractFile> list = new ArrayList<AbstractFile>();
        list.add(file1);
        list.add(file2);
        list.add(file3);
        Floder floder = new Floder("huawuque",list);

        AbstractFile file4 = new TxtFile("小鱼儿.TXT");
        AbstractFile file5 = new ImagesFile("小鱼儿.png");
        AbstractFile file6 = new ImagesFile("小鱼儿.avi");
        List<AbstractFile> list1 = new ArrayList<AbstractFile>();
        list1.add(file4);
        list1.add(file5);
        list1.add(file6);
        Floder floder2 = new Floder("xuaiyuer",list1);

        List<AbstractFile> list3 = new ArrayList<AbstractFile>();
        list3.add(floder);
        list3.add(floder2);

        Floder floder3 = new Floder("绝代双骄",list3);

        floder3.killVirus();
    }
}
