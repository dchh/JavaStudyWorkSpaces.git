package demo2;

/**
 * 图片类型文件
 *
 * @author Anna.
 * @date 2024/4/10 12:49
 */
public class ImagesFile implements AbstractFile{
    private String name;

    public ImagesFile(String name) {
        this.name = name;
    }

    @Override
    public void killVirus() {
        System.out.printf("图片类型文件-%s-进行扫毒%n",this.name);
    }
}
