package demo2;

import java.util.ArrayList;
import java.util.List;

/**
 * 容器构件：目录
 *
 * @author Anna.
 * @date 2024/4/10 12:51
 */
public class Floder implements AbstractFile{
    private String name;

    // 子节点
    private List<AbstractFile> lists = new ArrayList<AbstractFile>();

    public Floder(String name, List<AbstractFile> lists) {
        this.name = name;
        this.lists = lists;
    }

    @Override
    public void killVirus() {
        System.out.printf("扫描当前目录-%s%n",this.name);
        for(AbstractFile abstractFile : lists){
            abstractFile.killVirus();
        }
    }
}
