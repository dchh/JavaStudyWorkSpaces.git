package demo3;

/**
 * 观察者接口
 *
 * @author Anna.
 * @date 2024/4/25 16:24
 */
public interface Observer {
    // 定义拉取信息的接口
    void update();
}
