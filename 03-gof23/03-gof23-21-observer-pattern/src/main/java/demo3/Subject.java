package demo3;

/**
 * 被观察者
 *
 * @author Anna.
 * @date 2024/4/25 16:25
 */
public class Subject {
    // 定义当前状态
    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
