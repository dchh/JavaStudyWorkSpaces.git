package demo3;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/25 16:28
 */
public class TestClient {
    public static void main(String[] args) {
        // 创建被观察者
        Subject subject = new Subject();
        // 创建观察者
        ConcreteObserver observer = new ConcreteObserver(subject);
        // 修改状态
        subject.setState("RUN");
        // 观察者主动拉取
        observer.update();
    }
}
