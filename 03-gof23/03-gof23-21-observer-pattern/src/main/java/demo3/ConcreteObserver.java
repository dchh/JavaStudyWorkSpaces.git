package demo3;

/**
 * 观察者具体实现
 *
 * @author Anna.
 * @date 2024/4/25 16:26
 */
public class ConcreteObserver implements Observer {
    // 定义被观察者，持有被观察者的引用
    private Subject subject;

    public ConcreteObserver(Subject subject) {
        this.subject = subject;
    }

    @Override
    public void update() {
        System.out.printf("观察者主动拉取状态-state:%s%n ", subject.getState());
    }
}
