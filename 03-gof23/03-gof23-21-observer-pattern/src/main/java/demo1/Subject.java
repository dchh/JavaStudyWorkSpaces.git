package demo1;

import java.util.ArrayList;
import java.util.List;

/**
 * 被观察者
 *
 * @author Anna.
 * @date 2024/4/25 15:42
 */
public class Subject {
    // 定义存储观察者的集合
    private List<Observer> observerList = new ArrayList<Observer>();
    // 定义状态属性
    private String state;

    /**
     * 注册观察者
     *
     * @param observer
     * @return void
     * @author Anna.
     * @date 2024/4/25 15:45
     */
    public void registerObserver(Observer observer){
        observerList.add(observer);
    }

    /**
     * 移除观察者
     *
     * @param observer
     * @return void
     * @author Anna.
     * @date 2024/4/25 15:45
     */
    public void removeObserver(Observer observer){
        observerList.remove(observer);
    }

    /**
     * 通知所有观察者
     *
     * @param
     * @return void
     * @author Anna.
     * @date 2024/4/25 15:46
     */
    public void notifyObservers(){
        for(Observer observer : observerList){
            observer.update(this);
        }
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
        notifyObservers();
    }
}
