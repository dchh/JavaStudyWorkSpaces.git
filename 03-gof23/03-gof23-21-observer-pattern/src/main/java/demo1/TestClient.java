package demo1;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/25 15:49
 */
public class TestClient {
    public static void main(String[] args) {
        Subject subject = new Subject();
        subject.registerObserver(new ObserverA());
        subject.registerObserver(new ObserverB());

        subject.setState("START");
    }
}
