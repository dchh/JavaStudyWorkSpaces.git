package demo1;

/**
 * 观察者接口
 *
 * @author Anna.
 * @date 2024/4/25 15:41
 */
public interface Observer {
    void update(Subject subject);
}
