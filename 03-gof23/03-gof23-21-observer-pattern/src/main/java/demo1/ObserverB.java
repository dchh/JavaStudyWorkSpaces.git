package demo1;

/**
 * 观察者B
 *
 * @author Anna.
 * @date 2024/4/25 15:48
 */
public class ObserverB implements Observer{
    @Override
    public void update(Subject subject) {
        System.out.printf("观察者B接收到消息-state：%s%n",subject.getState());
    }
}
