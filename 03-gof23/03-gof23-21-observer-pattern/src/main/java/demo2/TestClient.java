package demo2;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/25 16:17
 */
public class TestClient {
    public static void main(String[] args) {
        ConcreteSubject subject = new ConcreteSubject();
        subject.addObserver(new ObserverA());
        subject.addObserver(new ObserverB());

        subject.setState("STOP");
    }
}
