package demo2;

import java.util.Observable;

/**
 * 目标对象
 *
 * @author Anna.
 * @date 2024/4/25 16:09
 */
public class ConcreteSubject extends Observable {

    private String state;

    public void setState(String state) {
        this.state = state;
        // 表示目标对象已被改变
        setChanged();
        // 通知所有观察者
        notifyObservers();
    }

    public String getState() {
        return state;
    }
}
