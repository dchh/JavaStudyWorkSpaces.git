package demo2;

import java.util.Observable;
import java.util.Observer;

/**
 * 观察者B
 *
 * @author Anna.
 * @date 2024/4/25 16:15
 */
public class ObserverB implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        System.out.printf("观察者B接收到消息-state：%s%n", ((ConcreteSubject) o).getState());
    }
}
