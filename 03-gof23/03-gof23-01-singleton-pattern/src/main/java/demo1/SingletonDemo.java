package demo1;

/**
 *  单例模式：饿汉式
 *  1 提供一个私有静态的SingletonDemo类型属性
 *  2 构造器私有化
 *  3 getInstance
 *
 * @author Anna.
 * @date 2024/4/5 20:34
 */
public class SingletonDemo {

    // 静态的SingletonDemo类型属性，初始化时，立即加载这个对象
    // 由于JVM类加载规则，加载class时进行初始化，保证了线程安全，但是没有延时加载的优势
    // 因此可能会造成内存浪费，因为无论是否使用到该实例，它都会在类加载时就被创建
    private static SingletonDemo instance = new SingletonDemo();

    // 构造器私有化
    private SingletonDemo() {
    }

    // 提供一个public的初始化方法 getInstance()方法
    public static SingletonDemo getInstance(){
        return instance;
    }
}
