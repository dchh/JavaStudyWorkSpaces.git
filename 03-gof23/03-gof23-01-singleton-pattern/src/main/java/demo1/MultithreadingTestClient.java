package demo1;

/**
 * 多线程测试
 *
 * @author Anna.
 * @date 2024/4/5 20:52
 */
public class MultithreadingTestClient {
    public static void main(String[] args) {
        // 多线程模式
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                SingletonDemo instance3 = SingletonDemo.getInstance();
                SingletonDemo instance4 = SingletonDemo.getInstance();

                if (instance3.hashCode() != instance4.hashCode()) {
                    System.out.printf("多线程模式下，判断获取的两个单例模式对象，判断是否是同一个对象-线程名称%s：%s%n",
                            Thread.currentThread().getName(), instance3.hashCode() == instance4.hashCode());
                }

            }, "线程" + i).start();
        }
    }
}
