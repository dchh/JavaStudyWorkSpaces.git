package demo1;

/**
 * 单线程测试
 *
 * @author Anna.
 * @date 2024/4/5 20:38
 */
public class SingleThreadedTestClient {
    public static void main(String[] args) {
        // 单线程模式
        SingletonDemo instance1 = SingletonDemo.getInstance();
        SingletonDemo instance2 = SingletonDemo.getInstance();
        System.out.printf("单线程模式下，判断获取的两个单例模式对象，判断是否是同一个对象：%s%n", instance1.hashCode() == instance2.hashCode());
    }
}
