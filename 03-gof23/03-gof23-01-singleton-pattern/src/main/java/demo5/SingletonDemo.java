package demo5;

/**
 * 单例模式：枚举
 *
 * 1 定义一个枚举的元素
 * 2 枚举本省就是单例模式。有JVM从根本上提供保证。避免通过反射和反序列化的漏洞
 * 3 如需使用提供相应public 方法即可
 *
 * @author Anna.
 * @date 2024/4/5 21:31
 */
public enum SingletonDemo {
    /** 定义一个枚举元素，它就代表了单例的一个实例*/
    INSTANCD;

    public void test(){System.out.println("枚举单例内部方法调用了");}
}
