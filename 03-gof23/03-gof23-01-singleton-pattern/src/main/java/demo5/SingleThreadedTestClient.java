package demo5;

/**
 * 单线程测试
 *
 * @author Anna.
 * @date 2024/4/5 20:38
 */
public class SingleThreadedTestClient {
    public static void main(String[] args) throws InterruptedException {
        // 单线程模式·
        System.out.println("========== 单线程测试枚举单例模式是否线程安全 ==========");
        SingletonDemo instance1 = SingletonDemo.INSTANCD;
        SingletonDemo instance2 = SingletonDemo.INSTANCD;
        System.out.printf("单线程模式下，判断获取的两个单例模式对象，判断是否是同一个对象：%s%n", instance1.hashCode() == instance2.hashCode());

        // 调用方式
        SingletonDemo.INSTANCD.test();
    }
}
