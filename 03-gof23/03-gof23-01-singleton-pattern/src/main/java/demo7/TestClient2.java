package demo7;

import java.lang.reflect.Constructor;

/**
 * 通过反射破坏单例模式
 *
 * @author Anna.
 * @date 2024/4/5 22:07
 */
public class TestClient2 {

    public static void main(String[] args) throws Exception {
        // 静态内部内实现单例模式
        SingletonDemo instance1 = SingletonDemo.getInstance();
        SingletonDemo instance2 = SingletonDemo.getInstance();

        System.out.printf("单线程模式下，判断获取的两个单例模式对象，判断是否是同一个对象：%s%n", instance1.hashCode() == instance2.hashCode());
        
        // 反射获取静态内部内实现单例模式
        Class<SingletonDemo> clazz = (Class<SingletonDemo>) Class.forName("demo7.SingletonDemo");
        // 获取构造器
        Constructor<SingletonDemo> declaredConstructor = clazz.getDeclaredConstructor(null);
        // 设置跳过安全检查
        declaredConstructor.setAccessible(true);
        // 初始化对象
        SingletonDemo singletonDemo1 = declaredConstructor.newInstance();
        SingletonDemo singletonDemo2 = declaredConstructor.newInstance();

        System.out.printf("反射方式，判断获取的两个单例模式对象，判断是否是同一个对象：%s%n", singletonDemo1.hashCode() == singletonDemo2.hashCode());
    }
}
