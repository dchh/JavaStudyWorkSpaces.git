package demo7;

/**
 * 单例模式：静态内部类方式
 *
 * 1 定义一个静态内部类，内部类中提供一个静态final的单例对象属性
 * 2 构造函数私有化
 * 3 提供一个public静态的初始化方法getInstance()方法
 *
 * @author Anna.
 * @date 2024/4/5 21:31
 */
public class SingletonDemo {

    // 1 定义一个静态内部类，内部类中提供一个静态final的单例对象属性
    static class SingletonDemoInstance{
        // 静态内部类 利用 JVM类加载器 加载类时 首先初始化 类中静态常量到常量池中且一个class只会被初始化一次这一特性保证线程安全
        private static final SingletonDemo instance = new SingletonDemo();
    }

    // 2 构造函数私有化
    private SingletonDemo() {
        // 反射通过调用SingletonDemo()进行初始化时，判断属性 SingletonDemoInstance.instance 是否为空，如果为空则直接抛出异常
        if(SingletonDemoInstance.instance != null){
            throw new RuntimeException();
        }
    }

    // 3 提供一个public静态的初始化方法getInstance()方法
    public static SingletonDemo getInstance() {
        return SingletonDemoInstance.instance;
    }
}
