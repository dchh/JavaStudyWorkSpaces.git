package demo6;

import demo1.SingletonDemo;

import java.util.concurrent.CountDownLatch;

/**
 * 测试5中单例模式 多线程下的执行效率
 *
 * @author Anna.
 * @date 2024/4/5 21:53
 */
public class TestClient {
    public static void main(String[] args) throws InterruptedException {
        long start = System.currentTimeMillis();
        int threadNum = 10;
        CountDownLatch countDownLatch1 = new CountDownLatch(threadNum);
        for (int i = 0; i < threadNum; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000000000; j++) {
                    SingletonDemo instance = SingletonDemo.getInstance();
                }
                countDownLatch1.countDown();
            }).start();
        }
        countDownLatch1.await();
        long end = System.currentTimeMillis();
        System.out.printf("饿汉式-%s个多线程各自创建1000000000次-耗时：%s%n", threadNum, end - start);

        CountDownLatch countDownLatch2 = new CountDownLatch(threadNum);
        for (int i = 0; i < threadNum; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000000000; j++) {
                    demo2.SingletonDemo instance = demo2.SingletonDemo.getInstance();
                }
                countDownLatch2.countDown();
            }).start();
        }
        countDownLatch2.await();
        end = System.currentTimeMillis();
        System.out.printf("懒汉式-%s个多线程各自创建1000000000次-耗时：%s%n", threadNum, end - start);

        CountDownLatch countDownLatch3 = new CountDownLatch(threadNum);
        for (int i = 0; i < threadNum; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000000000; j++) {
                    demo2.SingletonDemo instance = demo2.SingletonDemo.getInstance();
                }
                countDownLatch3.countDown();
            }).start();
        }
        countDownLatch3.await();
        end = System.currentTimeMillis();
        System.out.printf("双重检锁方式-%s个多线程各自创建1000000000次-耗时：%s%n", threadNum, end - start);

        CountDownLatch countDownLatch4 = new CountDownLatch(threadNum);
        for (int i = 0; i < threadNum; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000000000; j++) {
                    demo2.SingletonDemo instance = demo2.SingletonDemo.getInstance();
                }
                countDownLatch4.countDown();
            }).start();
        }
        countDownLatch4.await();
        end = System.currentTimeMillis();
        System.out.printf("静态内部类方式-%s个多线程各自创建1000000000次-耗时：%s%n", threadNum, end - start);

        CountDownLatch countDownLatch5 = new CountDownLatch(threadNum);
        for (int i = 0; i < threadNum; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000000000; j++) {
                    demo2.SingletonDemo instance = demo2.SingletonDemo.getInstance();
                }
                countDownLatch5.countDown();
            }).start();
        }
        countDownLatch5.await();
        end = System.currentTimeMillis();
        System.out.printf("枚举单例-%s个多线程各自创建1000000000次-耗时：%s%n", threadNum, end - start);
    }
}
