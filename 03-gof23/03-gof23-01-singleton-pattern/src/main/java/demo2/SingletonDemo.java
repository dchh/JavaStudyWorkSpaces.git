package demo2;

/**
 * 单例模式：懒汉式
 * 1 提供一个私有的静态SingletonDemo属性
 * 2 构造函数私有化
 * 3 提供一个public静态的初始化方法getInstance()方法
 *
 * @author Anna.
 * @date 2024/4/5 20:59
 */
public class SingletonDemo {

    // 1 提供一个私有的静态SingletonDemo属性
    // 这里在加载时，不进行初始化，调用getInstance()方法时进行初始化。以达到延时加载的目的
    private static SingletonDemo instance;

    // 2 构造函数私有化
    private SingletonDemo(){}

    // 3 提供一个public静态的初始化方法getInstance()方法
    public static SingletonDemo getInstance(){
        // 判断属性是否为空
        if(instance == null){
            // 延时加载，初始化对象
            instance = new SingletonDemo();
        }
        // 返回对象
        return instance;
    }
}
