package demo8;

import demo8.SingletonDemo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * 通过反序列化单例模式
 *
 * @author Anna.
 * @date 2024/4/5 22:07
 */
public class TestClient {

    public static void main(String[] args) throws Exception {
        // 静态内部内实现单例模式
        SingletonDemo instance1 = SingletonDemo.getInstance();
        SingletonDemo instance2 = SingletonDemo.getInstance();

        System.out.printf("单线程模式下，判断获取的两个单例模式对象，判断是否是同一个对象：%s%n", instance1.hashCode() == instance2.hashCode());
        
        // 序列化instance1到本地文件中 序列化时必须实现序列化接口
        try(FileOutputStream fos = new FileOutputStream("D:/temp/a.txt"); ObjectOutputStream oos = new ObjectOutputStream(fos);){
            oos.writeObject(instance1);
        }

        // 反序列化获取instance1对象
        try(FileInputStream fis = new FileInputStream("D:/temp/a.txt"); ObjectInputStream ois = new ObjectInputStream(fis);){
            SingletonDemo instance3 = (SingletonDemo) ois.readObject();
            System.out.printf("反序列化方式，判断获取的两个单例模式对象，判断是否是同一个对象：%s%n", instance1.hashCode() == instance3.hashCode());
        }
    }
}
