package demo8;

import java.io.ObjectStreamException;
import java.io.Serializable;

/**
 * 单例模式：静态内部类方式
 *
 * 1 定义一个静态内部类，内部类中提供一个静态final的单例对象属性
 * 2 构造函数私有化
 *
 * 3 提供一个public静态的初始化方法getInstance()方法
 *
 * @author Anna.
 * @date 2024/4/5 21:31
 */
// 实现Serializable接口
public class SingletonDemo implements Serializable {

    // 序列化ID，用于版本控制
    private static final long serialVersionUID = 1L;

    // 1 定义一个静态内部类，内部类中提供一个静态final的单例对象属性
    static class SingletonDemoInstance{
        // 静态内部类 利用 JVM类加载器 加载类时 首先初始化 类中静态常量到常量池中且一个class只会被初始化一次这一特性保证线程安全
        private static final SingletonDemo instance = new SingletonDemo();
    }

    // 2 构造函数私有化
    private SingletonDemo() {
    }

    // 3 提供一个public静态的初始化方法getInstance()方法
    public static SingletonDemo getInstance() {
        return SingletonDemoInstance.instance;
    }

    // readResolve方法是java.io.Serializable接口中的一个特殊方法，
    // 当反序列化对象时，如果对象所属的类定义了readResolve方法，
    // JVM会调用这个方法，并返回其返回的对象，而不是新创建的对象。
    // 因此，我们可以在单例类的readResolve方法中返回单例的原始实例，以确保反序列化不会创建新的实例。
    protected Object readResolve() throws ObjectStreamException {
        return getInstance();
    }
}
