package demo1;

/**
 * 个人业务
 *
 * @author Anna.
 * @date 2024/4/25 10:20
 */
public class PersonalBusiness extends Business{
    @Override
    public void handleBsiness() {
        System.out.println("办理个人业务");
    }
}
