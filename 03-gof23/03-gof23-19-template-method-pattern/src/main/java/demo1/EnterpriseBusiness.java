package demo1;

/**
 * 企业业务
 *
 * @author Anna.
 * @date 2024/4/25 10:22
 */
public class EnterpriseBusiness extends Business{
    @Override
    public void handleBsiness() {
        System.out.println("办理企业业务");
    }
}
