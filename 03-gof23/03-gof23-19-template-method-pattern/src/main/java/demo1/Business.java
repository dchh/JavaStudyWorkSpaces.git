package demo1;

/**
 * 业务
 *
 * @author Anna.
 * @date 2024/4/25 9:34
 */
public abstract class Business {

    public final void offerNumber(){
        System.out.println("取号");
    }

    /**
     * 办理业务抽象方法
     *
     * @author Anna.
     * @date 2024/4/25 10:18
     */
    public abstract void handleBsiness();

    public final void evaluate(){
        System.out.println("评价");
    }

    public final void handle(){
        offerNumber();
        handleBsiness();
        evaluate();
    }
}
