package demo1;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/25 10:23
 */
public class TestClient {
    public static void main(String[] args) {
        Business business = new PersonalBusiness();
        business.handle();
    }
}
