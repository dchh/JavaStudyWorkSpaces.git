# 模板方法模式
## 什么是模板方法
模板方法模式（Template Method Pattern）在Java中是一种行为型设计模式，它定义了一个操作中的算法骨架，而将一些步骤延迟到子类中。模板方法使得子类可以不改变一个算法的结构即可重新定义该算法的某些特定步骤。

模板方法模式的结构
+ 抽象类（Abstract Class）：
  - 定义了一个或多个抽象操作。 
  - 定义了一个模板方法，它调用了这些抽象操作。
+ 具体子类（Concrete Subclass）：
  - 实现抽象类中定义的抽象操作。
  - 继承抽象类并可以调用模板方法以执行算法。

**优点:**
+ 代码复用：模板方法定义了算法的骨架，因此相同的代码不需要在多个地方重复编写。
+ 算法结构清晰：通过将算法分解为一系列步骤，模板方法使得算法结构清晰，易于理解。 
+ 扩展性：子类可以通过实现不同的抽象操作来扩展算法的行为，而无需改变算法的整体结构。 
+ 灵活性：模板方法模式允许在算法的不同部分之间进行灵活的组合。

**缺点:**
+ 子类数量增加：如果算法中的步骤很多，并且它们都可能被子类覆盖，那么可能会导致子类数量急剧增加，从而增加系统的复杂性。 
+ 不易于进行大的变动：如果模板方法中的某个步骤不适合作为抽象方法，那么就无法通过子类来修改它，这可能会限制算法的灵活性。 
+ 可能导致违反开闭原则：如果抽象类定义得不够稳定，经常需要修改，那么每次修改都可能影响到所有的子类。

## 案例
在银行办理业务时，一般通过取号-> 办理业务 -> 评价，通过模板方法实现银行办理不同业务

### UML
![img.png](images/img.png)

实现步骤：
+ 定义银行办理业务类，类中定义通用的final方法(不允许子类覆盖)，对外调用的处理方法，以及变化的抽象方法接口
+ 定义不同的业务实现，通过继承实现变化的抽象方法接口

### 实现代码
Business.java
```java
// 业务
public abstract class Business {

    public final void offerNumber(){
        System.out.println("取号");
    }

    // 办理业务抽象方法
    public abstract void handleBsiness();

    public final void evaluate(){
        System.out.println("评价");
    }

    public final void handle(){
        offerNumber();
        handleBsiness();
        evaluate();
    }
}

```
EnterpriseBusiness.java
```java
// 企业业务
public class EnterpriseBusiness extends Business{
    @Override
    public void handleBsiness() {
        System.out.println("办理企业业务");
    }
}
```
PersonalBusiness.java
```java
// 个人业务
public class PersonalBusiness extends Business{
    @Override
    public void handleBsiness() {
        System.out.println("办理个人业务");
    }
}
```
TestClient.java
```java
public class TestClient {
    public static void main(String[] args) {
        Business business = new PersonalBusiness();
        business.handle();
    }
}
```

执行结果：

![img_1.png](images/img_1.png)

## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)