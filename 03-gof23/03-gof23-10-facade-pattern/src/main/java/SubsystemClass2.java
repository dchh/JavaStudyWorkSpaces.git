/**
 * 子系统类2
 *
 * @author Anna.
 * @date 2024/4/11 12:31
 */
public class SubsystemClass2 {
    public void method2() {
        System.out.println("SubsystemClass2 method2");
    }
}
