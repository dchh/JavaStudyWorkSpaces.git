/**
 * 子系统类1
 *
 * @author Anna.
 * @date 2024/4/11 12:30
 */
public class SubsystemClass1 {
    public void method1() {
        System.out.println("SubsystemClass1 method1");
    }
}
