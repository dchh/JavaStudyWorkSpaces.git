/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/11 12:32
 */
public class FacadePatternDemo {
    public static void main(String[] args) {
        Facade facade = new Facade(new SubsystemClass1(), new SubsystemClass2());
        facade.dealMethod();
    }
}
