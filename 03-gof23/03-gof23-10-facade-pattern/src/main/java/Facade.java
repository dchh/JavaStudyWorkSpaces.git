/**
 * 外观类
 *
 * @author Anna.
 * @date 2024/4/11 12:31
 */
public class Facade {
    private SubsystemClass1 sub1;
    private SubsystemClass2 sub2;

    public Facade(SubsystemClass1 sub1, SubsystemClass2 sub2) {
        this.sub1 = sub1;
        this.sub2 = sub2;
    }

    // 提供简单接口
    public void dealMethod() {
        sub1.method1();
        sub2.method2();
        // 可能还有其他子系统的调用和逻辑处理
    }
}
