# 外观模式
## 什么是外观模式
外观模式（Facade Pattern）是面向对象设计模式中的一种，它为子系统中的一组接口提供了一个统一的高级接口，使得子系统更容易使用。外观模式定义了一个高层接口，让子系统更容易使用。子系统中的很多类往往不是面向用户使用的，而是相互之间通过协作完成一项任务。这些类之间的协作关系是比较复杂且难以理解的。外观模式为这些类提供了一个高层接口，使得用户只需要跟外观类打交道，而不需要跟子系统中众多的类打交道。
> 外观模式是开发中最常用的模式之一。外观模式遵循迪米特法则（最少知识原则），一个软件实体应当尽可能的少与其他实体发生相互作用。

## 在Java中实现外观模式，一般涉及以下几个步骤：
+ 定义子系统接口和类：这些类实现了子系统的具体功能，但通常不会直接暴露给外部使用。
+ 创建外观类：这个类提供了一组简单的方法来访问子系统的功能。外观类通常会调用子系统中的多个类或接口，以完成一个特定的任务。
+ 客户端使用外观类：客户端不需要了解子系统的细节，只需要通过外观类提供的接口与子系统交互。

## 案例
SubsystemClass1.java
```java
// 子系统类1
public class SubsystemClass1 {
    public void method1() {
        System.out.println("SubsystemClass1 method1");
    }
}
```
SubsystemClass2.java
```java
// 子系统类2
public class SubsystemClass2 {
    public void method2() {
        System.out.println("SubsystemClass2 method2");
    }
}
```
Facade.java
```java
// 外观类
public class Facade {
    private SubsystemClass1 sub1;
    private SubsystemClass2 sub2;

    public Facade(SubsystemClass1 sub1, SubsystemClass2 sub2) {
        this.sub1 = sub1;
        this.sub2 = sub2;
    }

    // 提供简单接口
    public void dealMethod() {
        sub1.method1();
        sub2.method2();
        // 可能还有其他子系统的调用和逻辑处理
    }
}
```
FacadePatternDemo.java
```java
public class FacadePatternDemo {
    public static void main(String[] args) {
        Facade facade = new Facade(new SubsystemClass1(), new SubsystemClass2());
        facade.dealMethod();
    }
}
```

执行结果：

![img.png](images/img.png)

> 在上面的示例中，SubsystemClass1 和 SubsystemClass2 是子系统的类，它们提供了具体的功能。Facade 类是外观类，它组合了子系统的类，并提供了一个简单的接口 dealMethod()。客户端代码只需要与 Facade 类交互，而不需要了解子系统的具体实现细节。


## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)