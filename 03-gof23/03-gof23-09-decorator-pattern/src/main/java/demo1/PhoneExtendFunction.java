package demo1;

/**
 * 抽象装饰器
 *  继承自抽象组件，并持有一个抽象组件的引用
 *
 * @author Anna.
 * @date 2024/4/10 22:04
 */
public class PhoneExtendFunction implements IPhone{
    // 持有一个抽象组件的引用,调用已具有的功能
    private IPhone phone;

    public PhoneExtendFunction(IPhone phone) {
        this.phone = phone;
    }

    @Override
    public void function() {
        phone.function();
    }
}
