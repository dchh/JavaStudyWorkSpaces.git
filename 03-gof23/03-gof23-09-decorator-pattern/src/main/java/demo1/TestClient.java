package demo1;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/10 22:14
 */
public class TestClient {
    public static void main(String[] args) {
        PhotographFunction phone = new PhotographFunction(new InternetFunction(new NfcFunction(new Phone())));
        phone.function();
    }
}
