package demo1;

/**
 * 具体组件：
 *  定义一个具体组件包含最基础的功能：普通手机
 *
 * @author Anna.
 * @date 2024/4/10 21:59
 */
public class Phone implements IPhone {
    /**
     * 具备功能
     *
     * @param
     * @return void
     * @author Anna.
     * @date 2024/4/10 22:00
     */
    @Override
    public void function(){
        System.out.println("具备通讯功能");
    };
}
