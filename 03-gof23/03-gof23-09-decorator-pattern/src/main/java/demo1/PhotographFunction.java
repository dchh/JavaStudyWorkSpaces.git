package demo1;

/**
 * 具体装饰器
 * 实现抽象装饰器所增加的功能，并在调用原有方法时，增加新的功能。
 *
 * @author Anna.
 * @date 2024/4/10 22:08
 */
public class PhotographFunction extends PhoneExtendFunction{

    public PhotographFunction(IPhone phone) {
        super(phone);
    }

    public void photograph(){
        System.out.println("摄影功能");
    }

    @Override
    public void function() {
        super.function();
        photograph();
    }
}
