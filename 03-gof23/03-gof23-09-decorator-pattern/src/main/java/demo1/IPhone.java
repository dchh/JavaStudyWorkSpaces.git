package demo1;

/**
 * 抽象组件：
 *  定义一个手机具备某种功能接口的抽象类
 *
 * @author Anna.
 * @date 2024/4/10 21:59
 */
public interface IPhone {
    /**
     * 具备功能
     *
     * @param
     * @return void
     * @author Anna.
     * @date 2024/4/10 22:00
     */
     void function();
}
