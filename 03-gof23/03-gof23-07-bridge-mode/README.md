# 桥接模式
## 什么是桥接模式
桥接模式（Bridge Pattern）是一种将抽象与实现解耦的设计模式，使得二者可以独立变化。在桥接模式中，抽象部分与实现部分通过一个桥接接口进行连接，从而允许抽象部分和实现部分独立演化。

## 场景案例
商城系统中常见的商品分类，以电脑为例，如何良好的处理商品分类销售的问题？

### 处理方案1：
可以使用多层继承结构实现下图关系：

![img.png](images/img.png)

### 问题：
+ 扩展性问题（类个数膨胀问题）
  - 如果要增加一个新的电脑类型（智能手机），则要增加各个品牌下面的类
  - 如果要增加一个新的品牌，也要增加各种电脑类型
+ 违反单一职责原则
  - 一个类：如联想笔记本，有两个引起这个类变化的原因（品牌及电脑类型）

### 场景分析：
该场景中有两个变化的维度：电脑类型、电脑品牌。
![img_1.png](images/img_1.png)

### 处理方案2
使用桥接模式，处理多层继承结构，处理多维度变化点场景，将各个维度设计成独立的继承结构，是各个维度可以独立的扩展在抽象层建立关联。

### UML
![img_2.png](images/img_2.png)

### 代码实现
品牌
```java
// 品牌
public interface Brand {
    String getBrand();
}
class Lenovo implements Brand {
    @Override
    public String getBrand() {
        return "联想";
    }
}
class Dell implements Brand {
    @Override
    public String getBrand() {
        return "戴尔";
    }
}
class Huawei implements Brand {
    @Override
    public String getBrand() {
        return "华为";
    }
}
```
电脑类型
```java
// 电脑类型
public abstract class Computer {
    // 品牌
    protected Brand brand;

    // 通过构造器注入
    public Computer(Brand brand) {
        this.brand = brand;
    }

    public abstract String getType();
}

class Desktop extends Computer {
    public Desktop(Brand brand) {
        super(brand);
    }

    @Override
    public String getType() {
       return brand.getBrand() + "台式机";
    }
}

class Notebook extends Computer {
    public Notebook(Brand brand) {
        super(brand);
    }

    @Override
    public String getType() {
        return brand.getBrand() + "笔记本";
    }
}

class Tablets extends Computer {
    public Tablets(Brand brand) {
        super(brand);
    }

    @Override
    public String getType() {
        return brand.getBrand() + "平板电脑";
    }
}
```
TestClient.java
```java
// 测试
public class TestClient {
    public static void main(String[] args) {
        sale(new Desktop(new Lenovo()));
        sale(new Desktop(new Huawei()));
        sale(new Notebook(new Huawei()));
    }
    // 销售方法
    public static void sale(Computer computer){
        System.out.println("销售" + computer.getType());
    }
}
```
执行结果

![img_3.png](images/img_3.png)

## 桥接模式核心
处理多层继承结构，处理多维度变化场景，将各个维度设计成独立的继承结构，是各个维度可以独立的扩展在抽象层建立联系。

## 应用场景
+ JDBC驱动程序
+ 银行日志管理
  - 格式分类：操作日志、交易日志、异常日志
  - 距离分类：本地记录日志、异地记录日志
+ 人力资源系统中的奖金计算模块
  - 奖金分类：个人奖金、团体奖金、激励奖金
  - 部门分类：人事部门、销售部门、研发部门
+ OA系统中的消息处理
  - 业务类型：普通消息、加急消息、特急消息
  - 发送消息方法：系统内消息、手机短信、邮件

## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)
