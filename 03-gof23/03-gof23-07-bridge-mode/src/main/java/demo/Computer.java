package demo;

/**
 * 电脑类型
 *
 * @author Anna.
 * @date 2024/4/10 9:58
 */
public abstract class Computer {
    // 品牌
    protected Brand brand;

    // 通过构造器注入
    public Computer(Brand brand) {
        this.brand = brand;
    }

    public abstract String getType();
}

class Desktop extends Computer {
    public Desktop(Brand brand) {
        super(brand);
    }

    @Override
    public String getType() {
       return brand.getBrand() + "台式机";
    }
}

class Notebook extends Computer {
    public Notebook(Brand brand) {
        super(brand);
    }

    @Override
    public String getType() {
        return brand.getBrand() + "笔记本";
    }
}

class Tablets extends Computer {
    public Tablets(Brand brand) {
        super(brand);
    }

    @Override
    public String getType() {
        return brand.getBrand() + "平板电脑";
    }
}