package demo;

/**
 * 品牌
 *
 * @author Anna.
 * @date 2024/4/10 9:55
 */
public interface Brand {
    String getBrand();
}

class Lenovo implements Brand {
    @Override
    public String getBrand() {
        return "联想";
    }
}
class Dell implements Brand {
    @Override
    public String getBrand() {
        return "戴尔";
    }
}
class Huawei implements Brand {
    @Override
    public String getBrand() {
        return "华为";
    }
}