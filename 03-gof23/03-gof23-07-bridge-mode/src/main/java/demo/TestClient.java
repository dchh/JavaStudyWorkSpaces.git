package demo;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/10 10:34
 */
public class TestClient {
    public static void main(String[] args) {
        sale(new Desktop(new Lenovo()));
        sale(new Desktop(new Huawei()));
        sale(new Notebook(new Huawei()));
    }

    // 销售方法
    public static void sale(Computer computer){
        System.out.println("销售" + computer.getType());
    }
}
