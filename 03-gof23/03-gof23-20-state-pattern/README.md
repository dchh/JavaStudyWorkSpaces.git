# 状态模式
## 什么是状态模式
Java中的状态模式（State Pattern）是一种行为型设计模式，主要用于解决系统中复杂对象的状态转换以及不同状态下行为的封装问题。状态模式允许一个对象在其内部状态改变时改变它的行为，使得对象看起来似乎修改了它的类。

**优点：**
+ 结构清晰：通过将状态转换逻辑分布到独立的状态类中，状态模式将对象的行为与对应的状态分离，使得代码结构更加清晰，易于理解。 
+ 灵活性和可扩展性：状态模式使得在修改对象状态时，不需要修改对象的行为方法，从而提高了系统的灵活性和可扩展性。同时，新的状态可以很容易地引入，而无需修改已有的状态类或上下文。 
+ 降低耦合度：状态模式将对象的状态从主体中分离出来并将其封装在独立的状态类中，使得主体和状态之间的耦合度降低，提高了系统的可维护性。

**缺点：**
+ 增加类和对象的个数：由于每个状态都被封装成独立的对象，因此状态模式可能会导致系统中类和对象的个数增加，从而增加系统的复杂度和实现难度。 
+ 使用条件较为苛刻：状态模式要求将状态转换逻辑包含在具体状态类中，因此只适合状态不多且状态转换比较少的情况。如果状态机过于复杂，使用状态模式可能会导致系统的维护和扩展变得困难。

**常见场景：**
+ 任务状态管理：如任务的创建、进行中、暂停、完成等状态。 
+ 订单状态管理：如订单的待支付、已支付、待发货、已发货、已完成等状态。 
+ 线程状态管理：如线程的创建、就绪、运行、阻塞、终止等状态。

## 案例
通过状态模式实现订单状态管理

### UML
![img.png](images/img.png)

实现步骤：
+ 创建订单状态接口，定义通过上限文处理订单状态的接口
+ 创建上限文，持有订单状态的引用，提供通过订单状态引用调用订单状态实现类的具体方法
+ 创建不同订单状态，实现订单状态接口的方法

### 实现代码
OrderState.java
```java
// 订单状态接口
public interface OrderState {
    void handleState(Context context);
}
```
Context.java
```java
// 上下文
public class Context {
    private OrderState orderState;
    public Context(OrderState orderState) {
        this.orderState = orderState;
    }
    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }
    public void request(){
        this.orderState.handleState(this);
    }
}

```
PlaceOrder.java
```java
// 下单
public class PlaceOrder implements OrderState {
    @Override
    public void handleState(Context context) {
        System.out.println("下单-当前状态：待支付");
    }
}

```
PayOrder.java
```java
// 支付
public class PayOrder implements OrderState {
    @Override
    public void handleState(Context context) {
        System.out.println("支付-当前状态：已支付");
    }
}

```
SendGoods.java
```java
// 发货
public class SendGoods implements OrderState {
    @Override
    public void handleState(Context context) {
        System.out.println("发货-当前状态：已发货");
    }
}

```
TestClient.java
```java
public class TestClient {
    public static void main(String[] args) {
        // 创建上下文
        Context context = new Context(new PlaceOrder());
        context.request();
        context.setOrderState(new PayOrder());
        context.request();
        context.setOrderState(new SendGoods());
        context.request();
    }
}
```

执行结果：

![img_1.png](images/img_1.png)

## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)