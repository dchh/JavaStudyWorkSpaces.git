package demo1;

/**
 * 上下文
 *
 * @author Anna.
 * @date 2024/4/25 10:50
 */
public class Context {

    private OrderState orderState;

    public Context(OrderState orderState) {
        this.orderState = orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    public void request(){
        this.orderState.handleState(this);
    }
}
