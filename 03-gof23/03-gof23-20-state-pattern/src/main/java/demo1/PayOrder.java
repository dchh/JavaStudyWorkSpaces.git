package demo1;

/**
 * 支付
 *
 * @author Anna.
 * @date 2024/4/25 10:53
 */
public class PayOrder implements OrderState {
    @Override
    public void handleState(Context context) {
        System.out.println("支付-当前状态：已支付");
    }
}
