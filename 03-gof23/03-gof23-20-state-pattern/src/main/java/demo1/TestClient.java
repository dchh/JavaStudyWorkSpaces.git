package demo1;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/25 10:56
 */
public class TestClient {
    public static void main(String[] args) {
        // 创建上下文
        Context context = new Context(new PlaceOrder());
        context.request();
        context.setOrderState(new PayOrder());
        context.request();
        context.setOrderState(new SendGoods());
        context.request();
    }
}
