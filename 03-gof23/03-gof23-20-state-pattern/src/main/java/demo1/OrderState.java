package demo1;

/**
 * 订单状态接口
 *
 * @author Anna.
 * @date 2024/4/25 10:46
 */
public interface OrderState {
    void handleState(Context context);
}
