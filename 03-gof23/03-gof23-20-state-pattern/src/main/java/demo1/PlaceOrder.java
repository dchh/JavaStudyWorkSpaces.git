package demo1;

/**
 * 下单
 *
 * @author Anna.
 * @date 2024/4/25 10:53
 */
public class PlaceOrder implements OrderState {
    @Override
    public void handleState(Context context) {
        System.out.println("下单-当前状态：待支付");
    }
}
