package demo1;

/**
 * 发货
 *
 * @author Anna.
 * @date 2024/4/25 10:53
 */
public class SendGoods implements OrderState {
    @Override
    public void handleState(Context context) {
        System.out.println("发货-当前状态：已发货");
    }
}
