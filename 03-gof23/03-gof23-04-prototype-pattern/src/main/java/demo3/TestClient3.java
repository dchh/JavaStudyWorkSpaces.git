package demo3;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/8 11:49
 */
public class TestClient3 {
    public static void main(String[] args) throws CloneNotSupportedException, Exception {
        Person person = new Person("张三",new Address("北京"));
        // 通过序列化 反序列化实现深拷贝

        byte[] bytes = null;
        Person clone = null;

        // 序列化
        try( ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(baos);){
            oos.writeObject(person);
            bytes = baos.toByteArray();
        }

        // 反序列化
        try( ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
             ObjectInputStream ois = new ObjectInputStream(bais);){
            clone = (Person) ois.readObject();
        }

        System.out.printf("深拷贝-原型对象：%s%n",person);
        System.out.printf("深拷贝-拷贝对象值：%s%n",clone);

        System.out.println("======修改原型对象-address======");
        person.getAddress().setCity("重庆");
        System.out.printf("深拷贝-原型对象：%s%n",person);
        System.out.printf("深拷贝-拷贝对象值：%s%n",clone);

        System.out.println("======修改拷贝对象-address======");
        clone.getAddress().setCity("广州");
        System.out.printf("深拷贝-原型对象：%s%n",person);
        System.out.printf("深拷贝-拷贝对象值：%s%n",clone);
    }
}
