package demo3;

import java.io.Serializable;

/**
 * 反序列化实现深拷贝：
 * 实现Serializable接口
 *
 * @author Anna.
 * @date 2024/4/8 11:20
 */
public class Person implements Serializable {

    private String name;

    private Address address;

    public Person(String name, Address address) {
        this.name = name;
        this.address = address;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", address=" + address +
                '}';
    }
}
