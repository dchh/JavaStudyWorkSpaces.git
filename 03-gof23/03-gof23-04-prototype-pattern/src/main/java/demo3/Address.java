package demo3;

import java.io.Serializable;

/**
 * 地址
 *
 * @author Anna.
 * @date 2024/4/8 11:06
 */
public class Address implements Serializable {

    private String city;

    public Address(String city) {
        this.city = city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Address{" +
                "city='" + city + '\'' +
                '}';
    }
}
