package demo1;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/8 11:21
 */
public class TestClient {

    public static void main(String[] args) throws CloneNotSupportedException {
        Person person = new Person("张三",new Address("北京"));
        Person clone = (Person) person.clone();
        System.out.printf("浅拷贝-原型对象：%s%n",person);
        System.out.printf("浅拷贝-拷贝对象值：%s%n",clone);

        System.out.println("======修改原型对象-address======");
        person.getAddress().setCity("重庆");
        System.out.printf("浅拷贝-原型对象：%s%n",person);
        System.out.printf("浅拷贝-拷贝对象值：%s%n",clone);

        System.out.println("======修改拷贝对象-address======");
        clone.getAddress().setCity("广州");
        System.out.printf("浅拷贝-原型对象：%s%n",person);
        System.out.printf("浅拷贝-拷贝对象值：%s%n",clone);
    }
}
