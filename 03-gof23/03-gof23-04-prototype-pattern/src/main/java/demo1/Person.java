package demo1;

/**
 * 浅拷贝：
 * 实现Cloneable接口 重写protected Object clone()方法
 *
 * @author Anna.
 * @date 2024/4/8 11:20
 */
public class Person implements Cloneable{
    private String name;

    private Address address;

    public Person(String name, Address address) {
        this.name = name;
        this.address = address;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();   // 默认浅拷贝
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", address=" + address +
                '}';
    }
}
