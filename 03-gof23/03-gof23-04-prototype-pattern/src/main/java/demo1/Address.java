package demo1;

/**
 * 地址
 *
 * @author Anna.
 * @date 2024/4/8 11:06
 */
public class Address {
    private String city;

    public Address(String city) {
        this.city = city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Address{" +
                "city='" + city + '\'' +
                '}';
    }
}
