package demo1;

import java.util.Stack;

/**
 * 看护者
 * 对备忘录进行管理，提供保存与获取备忘录的功能。但它不能对备忘录的内容进行访问与修改。
 *
 * @author Anna.
 * @date 2024/4/25 17:15
 */
public class Caretaker {
    // 定义栈管理备忘录
    private Stack<ScannerInput> stack = new Stack<ScannerInput>();

    /**
     * 压栈
     *
     * @param scannerInput
     * @return void
     * @author Anna.
     * @date 2024/4/25 17:17
     */
    public void setScannerInput(ScannerInput scannerInput) {
        stack.push(scannerInput);
    }

    /**
     * 出栈
     * @return
     */
    public ScannerInput getScannerInput() {
        if(stack.size() == 0){
            System.out.println("已经不可以回退啦！");
        }
        return stack.pop();
    }
}
