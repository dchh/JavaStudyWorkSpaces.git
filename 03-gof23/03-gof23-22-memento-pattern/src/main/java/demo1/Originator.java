package demo1;

/**
 * 备忘录
 * 继承发起者，主要继承发起者的属性及get/set方法不再重复提供
 * 提供创建备忘录及通过备忘录恢复状态的方法
 * @author Anna.
 * @date 2024/4/25 17:09
 */
public class Originator extends ScannerInput{

    public Originator(String input) {
        super(input);
    }

    /**
     * 创建备忘录
     * @author Anna.
     * @date 2024/4/25 17:27
     */
    public ScannerInput createMemento() {
        return new ScannerInput(this.input);
    }

    /**
     * 恢复
     * @param scannerInput
     */
    public void restoreMemento(ScannerInput scannerInput) {
        this.input = scannerInput.getInput();
    }
}
