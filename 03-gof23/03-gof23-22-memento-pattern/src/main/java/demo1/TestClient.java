package demo1;

import java.util.Scanner;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/25 17:18
 */
public class TestClient {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入一些文本，输入'exit'退出：");

        // 创建原发器对象并设置默认内容
        Originator originator = new Originator("");
        // 创建并保存备忘录
        Caretaker caretaker = new Caretaker();
        // 初始化压栈
        caretaker.setScannerInput(originator.createMemento());

        while (true) {
            String input = scanner.nextLine();
            if ("exit".equalsIgnoreCase(input)) {
                System.out.printf("你输入了：%s%n" , input);
                break;
            }
            // 回退操作
            else if ("back".equalsIgnoreCase(input)) {
                // 回退
                originator.restoreMemento(caretaker.getScannerInput());
                System.out.printf("回退上一步-你输入：%s%n 输入：back(回退上一步) exit(退出)%n" , originator.getInput());
            } else {
                // 设置内容
                originator.setInput(input);
                // 设置备忘录
                caretaker.setScannerInput(originator.createMemento());
                System.out.printf("你输入了：%s%n 输入：back(回退上一步) exit(退出)%n" , originator.getInput());
            }
        }

        scanner.close();
        System.out.println("已退出。");
    }
}
