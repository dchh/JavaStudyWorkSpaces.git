package demo1;

/**
 * 发起者
 *
 * @author Anna.
 * @date 2024/4/25 17:07
 */
public class ScannerInput {
    // 输入内容
    protected String input;

    public ScannerInput(String input) {
        this.input = input;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }
}
