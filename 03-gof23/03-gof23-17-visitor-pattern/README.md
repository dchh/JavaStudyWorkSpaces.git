# 访问者模式
## 什么是访问者模式
访问者模式（Visitor Pattern）是软件设计模式中的一种行为模式，它用于将数据结构中的元素与操作这些元素的操作解耦。这种模式使得可以在不修改数据结构的情况下添加新的操作。

在访问者模式中，我们定义了两个主要角色：
+ **访问者（Visitor）：** 这个接口声明了一个访问元素的方法，这个方法的名字和参数表示了访问操作。 
+ **元素（Element）：** 这是一个定义了接受访问者（accept）方法的接口。这个方法的名字和参数表示了被访问的操作。 
+ **具体元素（ConcreteElement）：** 实现元素接口，并存储数据。同时，它实现接受操作，通常是通过调用访问者的访问方法。 
+ **具体访问者（ConcreteVisitor）：** 实现访问者接口，并定义了对特定元素的访问操作。 
+ **对象结构（Object Structure）：** 通常包含元素的集合，并提供一个方法，使得访问者可以访问其元素。

**常见应用场景：**
+ XML文档解析器设计 
+ 编译器的设计 
+ 复杂集合对象的处理

> 访问者模式表示一个作用于某对象结构中的各个元素的操作，他是我们可以在不改变各个元素的类的前提下定义作用域这些元素的新操作。
> 实际开发中应用范围非常窄。

## 示例
Element.java
```java
// 元素接口
public interface Element {
    void accept(Visitor visitor);
}
```
Visitor.java
```java
// 访问者接口
public interface Visitor {
    void visit(ConcreteElementA elementA);

    void visit(ConcreteElementB elementB);
}
```
ConcreteElementA.java
```java
// 具体元素A
public class ConcreteElementA implements Element {

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void operationA() {
        System.out.println("Element A: operationA()");
    }

}
```
ConcreteElementB.java
```java
// 具体元素B
public class ConcreteElementB implements Element {

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
    public void operationB() {
        System.out.println("Element B: operationB()");
    }
}

```
ConcreteVisitor.java
```java
// 具体访问者
public class ConcreteVisitor  implements Visitor {
    @Override
    public void visit(ConcreteElementA elementA) {
        elementA.operationA();
        System.out.println("Visited Element A");
    }

    @Override
    public void visit(ConcreteElementB elementB) {
        elementB.operationB();
        System.out.println("Visited Element B");
    }
}
```
ObjectStructure.java
```java
import java.util.ArrayList;
import java.util.List;

// 对象结构
public class ObjectStructure {
    private List<Element> elements = new ArrayList<>();

    public void attach(Element element) {
        elements.add(element);
    }

    public void detach(Element element) {
        elements.remove(element);
    }

    public void accept(Visitor visitor) {
        for (Element element : elements) {
            element.accept(visitor);
        }
    }
}
```
TestClient.java
```java
// 测试
public class TestClient {
    public static void main(String[] args) {
        ObjectStructure objectStructure = new ObjectStructure();
        objectStructure.attach(new ConcreteElementA());
        objectStructure.attach(new ConcreteElementB());

        ConcreteVisitor visitor = new ConcreteVisitor();
        objectStructure.accept(visitor);
    }
}
```

执行结果：

![img.png](img.png)

## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)