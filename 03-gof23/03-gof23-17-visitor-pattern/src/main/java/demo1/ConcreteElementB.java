package demo1;

/**
 * 具体元素B
 *
 * @author Anna.
 * @date 2024/4/24 22:12
 */
public class ConcreteElementB implements Element {

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void operationB() {
        System.out.println("Element B: operationB()");
    }

}
