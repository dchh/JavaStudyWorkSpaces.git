package demo1;

/**
 * 具体访问者
 *
 * @author Anna.
 * @date 2024/4/24 22:15
 */
public class ConcreteVisitor  implements Visitor {
    @Override
    public void visit(ConcreteElementA elementA) {
        elementA.operationA();
        System.out.println("Visited Element A");
    }

    @Override
    public void visit(ConcreteElementB elementB) {
        elementB.operationB();
        System.out.println("Visited Element B");
    }
}
