package demo1;

/**
 * 具体元素A
 *
 * @author Anna.
 * @date 2024/4/24 22:12
 */
public class ConcreteElementA implements Element {

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void operationA() {
        System.out.println("Element A: operationA()");
    }

}
