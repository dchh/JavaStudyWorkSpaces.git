package demo1;

/**
 * 访问者接口
 *
 * @author Anna.
 * @date 2024/4/24 22:12
 */
public interface Visitor {
    void visit(ConcreteElementA elementA);

    void visit(ConcreteElementB elementB);
}
