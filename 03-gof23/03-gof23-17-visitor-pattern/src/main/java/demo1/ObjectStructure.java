package demo1;

import java.util.ArrayList;
import java.util.List;

/**
 * 对象结构
 *
 * @author Anna.
 * @date 2024/4/24 22:16
 */
public class ObjectStructure {
    private List<Element> elements = new ArrayList<>();

    public void attach(Element element) {
        elements.add(element);
    }

    public void detach(Element element) {
        elements.remove(element);
    }

    public void accept(Visitor visitor) {
        for (Element element : elements) {
            element.accept(visitor);
        }
    }
}
