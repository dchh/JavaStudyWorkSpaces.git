package demo1;

/**
 * 元素接口
 *
 * @author Anna.
 * @date 2024/4/24 22:11
 */
public interface Element {
    void accept(Visitor visitor);
}
