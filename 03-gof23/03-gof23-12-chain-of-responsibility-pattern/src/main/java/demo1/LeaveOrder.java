package demo1;

/**
 * 请假单
 *
 * @author Anna.
 * @date 2024/4/13 21:24
 */
public class LeaveOrder {
    // 请假人
    private String name;
    // 请假天数
    private int days;
    // 原因
    private String reason;

    public LeaveOrder(String name, int days, String reason) {
        this.name = name;
        this.days = days;
        this.reason = reason;
    }

    public String getName() {
        return name;
    }

    public int getDays() {
        return days;
    }

    public String getReason() {
        return reason;
    }

}
