package demo1;

/**
 * 定义处理者接口
 *
 * @author Anna.
 * @date 2024/4/13 21:26
 */
public interface LeaveHandle {
    // 定义本人处理方式接口
    void handleRequest(LeaveOrder leaveOrder);
    // 定义下一处理者的引用
    void setNextHandle(LeaveHandle leaveHandle);
}
