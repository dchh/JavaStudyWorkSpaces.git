package demo1;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/13 21:34
 */
public class TestClient {
    public static void main(String[] args) {
        // 创建请假单
        LeaveOrder order = new LeaveOrder("张三",15,"回家");
        //// 创建处理人
        // 主任
        DirectorLeaveHandle directorLeaveHandle = new DirectorLeaveHandle();
        // 经理
        ManagerLeaveHandle managerLeaveHandle = new ManagerLeaveHandle();
        // 总经理
        GeneralManagerLeaveHandle generalManagerLeaveHandle = new GeneralManagerLeaveHandle();

        // 设置责任链
        directorLeaveHandle.setNextHandle(managerLeaveHandle);
        directorLeaveHandle.setNextHandle(generalManagerLeaveHandle);

        // 提交请假申请
        directorLeaveHandle.handleRequest(order);
    }
}
