package demo1;

/**
 * 总经理
 *
 * @author Anna.
 * @date 2024/4/13 21:28
 */
public class GeneralManagerLeaveHandle implements LeaveHandle{

    // 定义一个属性 用于持有下一处理者
    private LeaveHandle nextHandler;

    @Override
    public void handleRequest(LeaveOrder leaveOrder) {
        if(leaveOrder != null && leaveOrder.getDays() <= 30 && leaveOrder.getDays() > 10){
            System.out.printf("总经理审批-允许请假-请假人：%s-请假天数：%s-理由：%s%n",leaveOrder.getName(),leaveOrder.getDays(),leaveOrder.getReason());
        }
        else {
            System.out.println("超过30天拒绝请假");
        }
    }

    @Override
    public void setNextHandle(LeaveHandle leaveHandle) {
        this.nextHandler = leaveHandle;
    }
}
