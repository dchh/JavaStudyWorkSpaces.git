package demo1;

/**
 * 主任
 *
 * @author Anna.
 * @date 2024/4/13 21:28
 */
public class DirectorLeaveHandle implements LeaveHandle{

    // 定义一个属性 用于持有下一处理者
    private LeaveHandle nextHandler;

    @Override
    public void handleRequest(LeaveOrder leaveOrder) {
        if(leaveOrder != null && leaveOrder.getDays() <= 3 && leaveOrder.getDays() > 0){
            System.out.printf("主任审批-允许请假-请假人：%s-请假天数：%s-理由：%s%n",leaveOrder.getName(),leaveOrder.getDays(),leaveOrder.getReason());
        }
        else {
            // 通过持有引用 调用下已处理者进行处理
            nextHandler.handleRequest(leaveOrder);
        }
    }

    @Override
    public void setNextHandle(LeaveHandle leaveHandle) {
        this.nextHandler = leaveHandle;
    }
}
