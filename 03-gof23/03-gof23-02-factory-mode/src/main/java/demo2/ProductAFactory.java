package demo2;

/**
 * 创建一个ProductA的工厂，实现ProductFactory接口，用于生产ProductA
 *
 * @author Anna.
 * @date 2024/4/6 21:22
 */
public class ProductAFactory implements ProductFactory{
    @Override
    public Product createProduct() {
        return new ProductA();
    }
}
