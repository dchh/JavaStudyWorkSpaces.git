package demo2;

/**
 * 定义一个产品接口
 *
 * @author Anna.
 * @date 2024/4/6 21:18
 */
public interface Product {
    /**
     * 定义一个抽象的使用的方法
     *
     * @param
     * @return void
     * @author Anna.
     * @date 2024/4/6 21:18
     */
    void use();
}
