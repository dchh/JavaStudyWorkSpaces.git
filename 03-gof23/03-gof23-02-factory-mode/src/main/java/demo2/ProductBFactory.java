package demo2;

/**
 * 创建一个ProductB的工厂，实现ProductFactory接口，用于生产ProductB
 *
 * @author Anna.
 * @date 2024/4/6 21:22
 */
public class ProductBFactory implements ProductFactory{
    @Override
    public Product createProduct() {
        return new ProductB();
    }
}
