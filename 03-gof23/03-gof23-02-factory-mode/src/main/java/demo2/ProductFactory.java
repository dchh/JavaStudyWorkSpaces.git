package demo2;

/**
 * 定义一个ProductFactory工厂接口
 *  接口中定义一个创建Product的方法
 *
 * @author Anna.
 * @date 2024/4/6 21:20
 */
public interface ProductFactory {
    /**
     * 接口中定义一个创建Product的方法
     *
     * @param
     * @return demo2.Product
     * @author Anna.
     * @date 2024/4/6 21:21
     */
    Product createProduct();
}
