package demo2;

/**
 * ProductA实现Product接口
 *
 * @author Anna.
 * @date 2024/4/6 21:19
 */
public class ProductA implements Product{

    @Override
    public void use() {
        System.out.println("ProductA 使用了");
    }
}
