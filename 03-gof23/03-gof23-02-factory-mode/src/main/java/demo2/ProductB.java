package demo2;

/**
 * ProductB实现Product接口
 *
 * @author Anna.
 * @date 2024/4/6 21:19
 */
public class ProductB implements Product{

    @Override
    public void use() {
        System.out.println("ProductB 使用了");
    }
}
