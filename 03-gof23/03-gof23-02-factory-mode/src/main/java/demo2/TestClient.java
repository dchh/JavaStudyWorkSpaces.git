package demo2;

/**
 * 测试类
 *
 * @author Anna.
 * @date 2024/4/6 21:23
 */
public class TestClient {
    public static void main(String[] args) {
        // 创建ProductA
        Product product1 = new ProductAFactory().createProduct();
        product1.use();
        // 创建ProductB
        Product product2 = new ProductBFactory().createProduct();
        product2.use();
    }
}
