package demo3;

/**
 * 定义Car的接口
 *
 * @author Anna.
 * @date 2024/4/6 22:04
 */
public interface CarFactory {
    /**
     * 创建发动机
     *
     * @param
     * @return demo3.Engine
     * @author Anna.
     * @date 2024/4/6 22:05
     */
    Engine engine();

    /**
     * 创建车身
     *
     * @param
     * @return demo3.CarBody
     * @author Anna.
     * @date 2024/4/6 22:05
     */
    CarBody carBody();

    /**
     * 创建轮胎
     *
     * @param
     * @return demo3.Tyre
     * @author Anna.
     * @date 2024/4/6 22:05
     */
    Tyre tyre();
}
