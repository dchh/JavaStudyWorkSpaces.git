package demo3;

/**
 * 定义一个车身接口
 *
 * @author Anna.
 * @date 2024/4/6 21:55
 */
public interface CarBody {
    /**
     * 定义一个乘坐的方法
     *
     * @param
     * @return void
     * @author Anna.
     * @date 2024/4/6 21:56
     */
    void ride();
}
