package demo3;

/**
 * 定义一个轮胎接口
 *
 * @author Anna.
 * @date 2024/4/6 21:59
 */
public interface Tyre {
    /**
     * 定义轮胎转动的方法
     *
     * @param
     * @return void
     * @author Anna.
     * @date 2024/4/6 22:00
     */
    void run();
}
