package demo3;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/6 22:08
 */
public class TestClient {
    public static void main(String[] args) {
        // 使用高端汽车工厂类 创建高端汽车
        HighEndCarFactory highEndCar = new HighEndCarFactory();
        highEndCar.engine().stop();
        highEndCar.carBody().ride();
        highEndCar.tyre().run();

        System.out.println("==========================");

        // 使用低端汽车工厂类 创建低端汽车
        LowEndCarFactory lowEndCar = new LowEndCarFactory();
        lowEndCar.engine().stop();
        lowEndCar.carBody().ride();
        lowEndCar.tyre().run();
    }
}
