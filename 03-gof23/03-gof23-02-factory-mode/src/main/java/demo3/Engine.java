package demo3;

/**
 * 定义发动机接口
 *
 * @author Anna.
 * @date 2024/4/6 21:50
 */
public interface Engine {
    /**
     * 定义发动机 发动方法
     *
     * @param
     * @return void
     * @author Anna.
     * @date 2024/4/6 21:51
     */
    void run();
    /**
     * 定义发动机 停止方法
     *
     * @param
     * @return void
     * @author Anna.
     * @date 2024/4/6 21:52
     */
    void stop();
}
