package demo3;

/**
 * 低端汽车工厂实现汽车工厂
 *
 * @author Anna.
 * @date 2024/4/6 22:06
 */
public class LowEndCarFactory implements CarFactory{


    @Override
    public Engine engine() {
        return new LowEndEngine();
    }

    @Override
    public CarBody carBody() {
        return new LowEndCarBody();
    }

    @Override
    public Tyre tyre() {
        return new LowEndTyre();
    }
}
