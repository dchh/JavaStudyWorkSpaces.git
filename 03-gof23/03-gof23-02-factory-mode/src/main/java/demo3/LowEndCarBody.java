package demo3;

/**
 * 创建一个低端车身实现车身接口
 *
 * @author Anna.
 * @date 2024/4/6 21:53
 */
public class LowEndCarBody implements CarBody{

    @Override
    public void ride() {
        System.out.println("低端车身-朴素-看起来安全");
    }
}
