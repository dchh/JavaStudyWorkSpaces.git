package demo3;

/**
 * 创建一个低端轮胎实现轮胎
 *
 * @author Anna.
 * @date 2024/4/6 21:53
 */
public class LowEndTyre implements Tyre{

    @Override
    public void run() {
        System.out.println("低端轮胎-普通材料-易磨损");
    }
}
