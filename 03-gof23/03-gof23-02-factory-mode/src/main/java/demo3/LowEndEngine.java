package demo3;

/**
 * 创建一个低端发动机实现发动机
 *
 * @author Anna.
 * @date 2024/4/6 21:53
 */
public class LowEndEngine implements Engine{
    @Override
    public void run() {
        System.out.println("低端发动机-跑的慢");
    }

    @Override
    public void stop() {
        System.out.println("低端发动机-刹车性能弱");
    }
}
