package demo3;

/**
 * 创建一个高端车身实现车身
 *
 * @author Anna.
 * @date 2024/4/6 21:53
 */
public class HighEndCarBody implements CarBody{

    @Override
    public void ride() {
        System.out.println("高端车身-奢华-安全");
    }
}
