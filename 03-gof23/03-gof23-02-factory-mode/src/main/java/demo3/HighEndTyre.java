package demo3;

/**
 * 创建一个高端轮胎实现轮胎
 *
 * @author Anna.
 * @date 2024/4/6 21:53
 */
public class HighEndTyre implements Tyre{

    @Override
    public void run() {
        System.out.println("高端轮胎-太空材料-安全-耐磨");
    }
}
