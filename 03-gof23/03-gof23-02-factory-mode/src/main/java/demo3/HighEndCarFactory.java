package demo3;

/**
 * 高端汽车工厂实现汽车工厂
 *
 * @author Anna.
 * @date 2024/4/6 22:06
 */
public class HighEndCarFactory implements CarFactory{


    @Override
    public Engine engine() {
        return new HighEndEngine();
    }

    @Override
    public CarBody carBody() {
        return new HighEndCarBody();
    }

    @Override
    public Tyre tyre() {
        return new HighEndTyre();
    }
}
