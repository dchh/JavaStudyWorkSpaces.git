package demo1;

/**
 * 定义一个动物的接口
 *
 * @author Anna.
 * @date 2024/4/6 20:19
 */
public interface Animal {

    /**
     * 接口中定义一个抽象的方法：叫声
     *
     * @return void
     * @author Anna.
     * @date 2024/4/6 20:20
     */
    void makeSound();
}
