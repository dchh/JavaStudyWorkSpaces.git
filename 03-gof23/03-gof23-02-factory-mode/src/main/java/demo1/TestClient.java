package demo1;

/**
 * 测试类
 *
 * @author Anna.
 * @date 2024/4/6 20:27
 */
public class TestClient {
    public static void main(String[] args) {
        // 根据简单工厂创建不同的动物，执行动作
        // 生产一个Cat
        Animal cat = SimpleAnimalFactory.createAnimal("cat");
        cat.makeSound();

        // 生产一个Dog
        Animal dog = SimpleAnimalFactory.createAnimal("Dog");
        dog.makeSound();
    }
}
