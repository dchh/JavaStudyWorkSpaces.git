package demo1;

/**
 * 定义一个简单工厂类用于创建动物
 *
 * @author Anna.
 * @date 2024/4/6 20:22
 */
public class SimpleAnimalFactory {

    /**
     * 定义一个创建动物的方法用于生产不同的动物的静态方法
     *
     * @param type
     * @return demo1.Animal
     * @author Anna.
     * @date 2024/4/6 20:25
     */
    public static Animal createAnimal(String type) {
        if ("Cat".equalsIgnoreCase(type)) {
            return new Cat();
        }
        else if ("Dog".equalsIgnoreCase(type)) {
            return new Dog();
        }
        else  {
           return null;
        }
    }

}
