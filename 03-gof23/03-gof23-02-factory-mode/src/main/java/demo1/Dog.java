package demo1;

/**
 * 定义一个实现类实现Animal接口
 *  狗属于动物：实现发出叫声的接口
 *
 * @author Anna.
 * @date 2024/4/6 20:20
 */
public class Dog implements Animal{
    @Override
    public void makeSound() {
        System.out.println("汪汪汪");
    }
}
