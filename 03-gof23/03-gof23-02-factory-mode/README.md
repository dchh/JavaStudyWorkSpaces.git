# 工厂模式
工厂设计模式是一种创建型设计模式，它提供了一种封装对象创建过程的机制，将对象的创建与使用分离。
这种设计模式允许我们在不修改客户端代码的情况下引入新的对象类型。
在Java中，工厂设计模式主要有三种形式：简单工厂模式、工厂方法模式和抽象工厂模式。

## 简单工厂模式
用来生成同一等级结构中的任意产品。
> <font color="red">注：对增加新的产品需要修改已有的代码，这违背了面向对象设计原则中的开闭原则（对扩展开放，对修改关闭）</font>

### UML

![img.png](images/img.png)

### 实现代码
Animal.java
```java
// 定义一个动物的接口
public interface Animal {

    // 接口中定义一个抽象的方法：叫声
    void makeSound();
}
```
Cat.java
```java
// 定义一个实现类实现Animal接口
public class Cat implements Animal{
    // 猫属于动物：实现发出叫声的接口
    @Override
    public void makeSound() {
        System.out.println("喵喵喵");
    }
}
```
Dog.java
```java
// 定义一个实现类实现Animal接口
public class Dog implements Animal{
    // 狗属于动物：实现发出叫声的接口
    @Override
    public void makeSound() {
        System.out.println("汪汪汪");
    }
}

```
SimpleAnimalFactory.java
```java
// 定义一个简单工厂类用于创建动物
public class SimpleAnimalFactory {
    // 定义一个创建动物的方法用于生产不同的动物的静态方法
    public static Animal createAnimal(String type) {
        if ("Cat".equalsIgnoreCase(type)) {
            return new Cat();
        }
        else if ("Dog".equalsIgnoreCase(type)) {
            return new Dog();
        }
        else  {
           return null;
        }
    }
}
```
TestClient.java
```java
public class TestClient {
    public static void main(String[] args) {
        // 根据简单工厂创建不同的动物，执行动作
        // 生产一个Cat
        Animal cat = SimpleAnimalFactory.createAnimal("cat");
        cat.makeSound();
        // 生产一个Dog
        Animal dog = SimpleAnimalFactory.createAnimal("Dog");
        dog.makeSound();
    }
}

```

执行结果：

![img_1.png](images/img_1.png)

> 结论：<br/>
> 简单工厂好处在于，对于客户端调用时，我们不需要关心具体实现，只需要调用工厂方法，传入参数获取我们需要返回的结果即可。<br/>
> 但是对于同一个产品（动物），如果我们进行新增（猪），则必须要修改Factory中createAnimal(String type)方法。因此违背了开闭原则

## 工厂方法模式
用来生产同一等级结构中的固定产品。
> 支持增加任意产品，满足开闭原则，但设计相对于简单工厂复杂一些

### UML
![img_2.png](images/img_2.png)

### 实现代码
Product.java
```java
// 定义一个产品接口
public interface Product {
    //定义一个抽象的使用的方法
    void use();
}
```
ProductA.java
```java
// ProductA实现Product接口
public class ProductA implements Product{
    @Override
    public void use() {
        System.out.println("ProductA 使用了");
    }
}
```
ProductB.java
```java
// ProductB实现Product接口
public class ProductB implements Product{
    @Override
    public void use() {
        System.out.println("ProductB 使用了");
    }
}
```
ProductFactory.java
```java
// 定义一个ProductFactory工厂接口
public interface ProductFactory {
    // 接口中定义一个创建Product的方法
    Product createProduct();
}
```
ProductAFactory.java
```java
// 创建一个ProductA的工厂，实现ProductFactory接口，用于生产ProductA
public class ProductAFactory implements ProductFactory{
    @Override
    public Product createProduct() {
        return new ProductA();
    }
}
```
ProductBFactory.java
```java
// 创建一个ProductB的工厂，实现ProductFactory接口，用于生产ProductB
public class ProductBFactory implements ProductFactory{
    @Override
    public Product createProduct() {
        return new ProductB();
    }
}
```

TestClient.java
```java
public class TestClient {
    public static void main(String[] args) {
        // 创建ProductA
        Product product1 = new ProductAFactory().createProduct();
        product1.use();
        // 创建ProductB
        Product product2 = new ProductBFactory().createProduct();
        product2.use();
    }
}
```

执行结果：

![img_3.png](images/img_3.png)

> 从工厂方式模式，我们可以看出，我们可以任意增加<font color="red">同一产品</font>，而不会影响到原来已有产品
> （创建一个产品C继承Product接口，创建一个产品C的Factory类生产C,使用是通过相应Factory调用生产C即可）。
> 如果产品中新增一个方法，则所有实现了Product接口的方法都必须修改相应方法。

## 抽象工厂模式
用来生产不同产品族的全部产品。
> 对增加新的产品无能为力，支持增加产品族

### UML
![img_4.png](images/img_4.png)

### 实现代码
Engine.java
```java
// 定义发动机接口
public interface Engine {
    // 定义发动机 发动方法
    void run();
    // 定义发动机 停止方法
    void stop();
}

```
HighEndEngine.java
```java
// 创建一个高端发动机实现发动机
public class HighEndEngine implements Engine{
    @Override
    public void run() {
        System.out.println("高端发动机-跑的快");
    }
    @Override
    public void stop() {
        System.out.println("高端发动机-刹车性能强");
    }
}
```
LowEndEngine.java
```java
// 创建一个低端发动机实现发动机
public class LowEndEngine implements Engine{
    @Override
    public void run() {
        System.out.println("低端发动机-跑的慢");
    }
    @Override
    public void stop() {
        System.out.println("低端发动机-刹车性能弱");
    }
}
```
CarBody.java
```java
// 定义一个车身接口
public interface CarBody {
    // 定义一个乘坐的方法
    void ride();
}
```
HighEndCarBody.java
```java
// 创建一个高端车身实现车身
public class HighEndCarBody implements CarBody{
    @Override
    public void ride() {
        System.out.println("高端车身-奢华-安全");
    }
}
```
LowEndCarBody.java
```java
// 创建一个低端车身实现车身
public class LowEndCarBody implements CarBody{
    @Override
    public void ride() {
        System.out.println("低端车身-朴素-看起来安全");
    }
}
```
Tyre.java
```java
// 定义一个轮胎接口
public interface Tyre {
    // 定义轮胎转动的方法
    void run();
}

```
HighEndTyre.java
```java
// 创建一个高端轮胎实现轮胎
public class HighEndTyre implements Tyre{
    @Override
    public void run() {
        System.out.println("高端轮胎-太空材料-安全-耐磨");
    }
}
```
LowEndTyre.java
```java
// 创建一个低端轮胎实现轮胎
public class LowEndTyre implements Tyre{
    @Override
    public void run() {
        System.out.println("低端轮胎-普通材料-易磨损");
    }
}
```
CarFactory.java
```java
// 定义Car的接口
public interface CarFactory {
    // 创建发动机
    Engine engine();
    // 创建车身
    CarBody carBody();
    // 创建轮胎
    Tyre tyre();
}
```
HighEndCarBody.java
```java
// 高端汽车工厂实现汽车工厂
public class HighEndCarFactory implements CarFactory{
    @Override
    public Engine engine() {
        return new HighEndEngine();
    }
    @Override
    public CarBody carBody() {
        return new HighEndCarBody();
    }
    @Override
    public Tyre tyre() {
        return new HighEndTyre();
    }
}
```
LowEndCarFactory.java
```java
// 低端汽车工厂实现汽车工厂
public class LowEndCarFactory implements CarFactory{
    @Override
    public Engine engine() {
        return new LowEndEngine();
    }
    @Override
    public CarBody carBody() {
        return new LowEndCarBody();
    }
    @Override
    public Tyre tyre() {
        return new LowEndTyre();
    }
}
```
TestClient.java
```java
public class TestClient {
    public static void main(String[] args) {
        // 使用高端汽车工厂类 创建高端汽车
        HighEndCarFactory highEndCar = new HighEndCarFactory();
        highEndCar.engine().stop();
        highEndCar.carBody().ride();
        highEndCar.tyre().run();

        System.out.println("==========================");

        // 使用低端汽车工厂类 创建低端汽车
        LowEndCarFactory lowEndCar = new LowEndCarFactory();
        lowEndCar.engine().stop();
        lowEndCar.carBody().ride();
        lowEndCar.tyre().run();
    }
}

```

执行结果：

![img_5.png](images/img_5.png)

> 抽象工厂，不可以增加产品（比如：CarFactory一旦定下了，如果我们要新增新的部件则所有实现CarFactory的类都需实现该方法）。
> 但是抽象工厂，可以根据已有的接口，创建更多的产品族（比如：定义一个中端汽车工厂，调用高端发动机，低端轮胎，低端车身，等任意组合成新的Factory）

## 对比及应用场景
### 简单工厂模式
+ <b>优点：</b>
  - 实现了对象的创建和使用的责任分割，客户端只需要传入正确的参数，就可以获取需要的对象，无需知道创建细节。 
  - 工厂类中有必要的判断逻辑，可以决定根据当前的参数创建对应的产品实例，客户端可以免除直接创建产品对象的责任。
+ <b>缺点：</b>
  - 工厂类职责过重，如果产品种类增加，工厂类的代码会变得庞大且复杂，不利于维护。 
  - 简单工厂模式违背了开放封闭原则，因为每次增加新产品时，都需要修改工厂类的代码。
+ <b>适用场景：</b>
  - 创建对象较少，且对象的创建逻辑不复杂时。 
  - 客户端不关心对象的创建过程，只关心使用对象时。

### 工厂方法模式
+ <b>优点：</b>
    - 将对象的创建推迟到子类中进行，使得类的实例化更加灵活和可扩展。
    - 降低了客户端与具体产品类之间的耦合度，客户端只需要知道对应的工厂，无需知道具体的产品类。
+ <b>缺点：</b>
    - 增加了系统的抽象性和理解难度，需要引入额外的工厂接口和工厂类。
    - 如果产品类较少，使用工厂方法模式可能会增加不必要的复杂性。
+ <b>适用场景：</b>
    - 需要创建大量相似对象时，可以使用工厂方法模式来简化对象的创建过程。
    - 当一个类需要由其子类来指定创建哪个对象时，可以使用工厂方法模式。
    - 但实际开发中，简单工厂比工厂方法使用的更多

### 抽象工厂模式
+ <b>优点：</b>
    - 提供了创建一系列相关或相互依赖对象的接口，无需指定它们具体的类。
    - 增加了系统的灵活性和可扩展性，可以通过更换不同的工厂来实现不同的产品族。
+ <b>缺点：</b>
    - 规定了所有可能被创建的产品集合，产品族中扩展新的产品困难。
    - 如果产品族中的产品较少，使用抽象工厂模式可能会导致代码冗余和复杂性增加。
+ <b>适用场景：</b>
    - 当需要创建一组相互关联或相互依赖的对象时，可以使用抽象工厂模式。
    - 当一个系统需要独立地变化其创建的对象时，抽象工厂模式是一个很好的选择。

## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)
