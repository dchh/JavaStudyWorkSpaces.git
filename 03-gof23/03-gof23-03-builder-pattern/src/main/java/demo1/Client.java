package demo1;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/7 22:33
 */
public class Client {
    public static void main(String[] args) {
        Phone phone = new Phone.Builder()
                .setCamera("华为摄像头")
                .setFuselage("华为机身")
                .setSystemKernel("鸿蒙系统")
                .createPhone();
        System.out.println(phone);

        Phone phone2 = new Phone.Builder()
                .setCamera("小米摄像头")
                .setFuselage("小米机身")
                .setSystemKernel("小米系统")
                .createPhone();
        System.out.println(phone2);
    }
}
