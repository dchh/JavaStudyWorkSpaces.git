package demo1;


/**
 * 手机D0
 *
 * @author Anna.
 * @date 2024/4/7 22:24
 */
public class Phone {
    // 机身
    private String fuselage;
    // 摄像头
    private String camera;
    // 系统内核
    private String systemKernel;

    // 私有构造方法，只能通过建造者创建
    private Phone(Builder builder) {
        this.fuselage = builder.fuselage;
        this.camera = builder.camera;
        this.systemKernel = builder.systemKernel;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "fuselage='" + fuselage + '\'' +
                ", camera='" + camera + '\'' +
                ", systemKernel='" + systemKernel + '\'' +
                '}';
    }

    // 静态内部类作为建造者
    static class Builder {
        // 机身
        private String fuselage;
        // 摄像头
        private String camera;
        // 系统内核
        private String systemKernel;

        public Builder setFuselage(String fuselage) {
            this.fuselage = fuselage;
            return this;    // 链式调用
        }

        public Builder setCamera(String camera) {
            this.camera = camera;
            return this;    // 链式调用
        }

        public Builder setSystemKernel(String systemKernel) {
            this.systemKernel = systemKernel;
            return this;    // 链式调用
        }

        // 创建手机
        public Phone createPhone(){
            return new Phone(this);
        }
    }
}
