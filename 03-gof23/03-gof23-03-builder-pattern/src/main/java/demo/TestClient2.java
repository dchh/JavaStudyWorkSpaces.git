package demo;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/7 22:11
 */
public class TestClient2 {
    public static void main(String[] args) {
        // 创建建造具体建造者
        PhoneBuilder phoneBuilder = new PhoneBuilder();

        Phone phone = phoneBuilder
                .setCamera("装配小米摄像头")
                .setFuselage("装配小米机身")
                .setSystemKernel("装配小米系统")
                .buildPhone();

        System.out.println(phone.toString());

    }
}
