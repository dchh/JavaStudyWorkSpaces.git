package demo;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/7 22:11
 */
public class TestClient {
    public static void main(String[] args) {
        // 创建建造具体建造者
        PhoneBuilder phoneBuilder = new PhoneBuilder();

        // 创建导演者 传入具体建造者
        HuaweiDirector huaweiDirector = new HuaweiDirector(phoneBuilder);

        // 通过导演者构建手机
        Phone phone = huaweiDirector.createPhone();

        System.out.println(phone.toString());
    }
}
