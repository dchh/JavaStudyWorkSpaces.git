package demo;

/**
 * 华为手机导演者
 *
 * @author Anna.
 * @date 2024/4/7 22:04
 */
public class HuaweiDirector {
    // 定义建造者属性
    private IPhoneBuider phoneBuider;

    public HuaweiDirector(IPhoneBuider phoneBuider) {
        this.phoneBuider = phoneBuider;
    }

    public Phone createPhone(){
        Phone phone = phoneBuider.setCamera("装配华为摄像头")
                .setFuselage("装配华为机身")
                .setSystemKernel("装配鸿蒙系统")
                .buildPhone();
        return phone;
    }
}
