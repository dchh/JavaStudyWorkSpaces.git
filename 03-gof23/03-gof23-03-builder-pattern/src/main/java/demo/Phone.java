package demo;

/**
 * 手机DO
 *
 * @author Anna.
 * @date 2024/4/7 21:56
 */
public class Phone {
    // 机身
    private String fuselage;
    // 摄像头
    private String camera;
    // 系统内核
    private String systemKernel;

    public void setFuselage(String fuselage) {
        this.fuselage = fuselage;
    }

    public void setCamera(String camera) {
        this.camera = camera;
    }

    public void setSystemKernel(String systemKernel) {
        this.systemKernel = systemKernel;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "fuselage=" + fuselage +
                ", camera=" + camera +
                ", systemKernel=" + systemKernel +
                '}';
    }
}
