package demo;

/**
 * 建造者具体实现
 *
 * @author Anna.
 * @date 2024/4/7 21:59
 */
public class PhoneBuilder implements IPhoneBuider {
    // 定义建造者返回对象
    private Phone phone;

    public PhoneBuilder() {
        // 初始化Phone
        this.phone = new Phone();
    }

    @Override
    public IPhoneBuider setFuselage(String fuselage) {
        phone.setFuselage(fuselage);
        return this;
    }

    @Override
    public IPhoneBuider setCamera(String camera) {
        phone.setCamera(camera);
        return this;
    }

    @Override
    public IPhoneBuider setSystemKernel(String systemKernel) {
        phone.setSystemKernel(systemKernel);
        return this;
    }

    @Override
    public Phone buildPhone() {
        return phone;
    }
}
