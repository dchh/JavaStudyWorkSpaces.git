package demo;

/**
 * 手机 抽象建造者定义接口
 *
 * @author Anna.
 * @date 2024/4/7 21:43
 */
public interface IPhoneBuider {
    // 设置机身
    IPhoneBuider setFuselage(String fuselage);
    // 设置摄像头
    IPhoneBuider setCamera(String camera);
    // 设置系统内核
    IPhoneBuider setSystemKernel(String systemKernel);

    // 返回创建对象
    Phone buildPhone();

}
