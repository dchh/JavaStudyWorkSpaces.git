# 迭代器模式
## 什么是迭代器模式
迭代器模式（demo1.Iterator Pattern）是Java中一种常用的设计模式，它提供了一种顺序访问一个聚合对象中各个元素，而又不需要暴露该对象的内部表示的方法。迭代器模式将遍历逻辑从聚合对象中分离出来，使得聚合对象本身更加简洁，同时也使得遍历操作更加灵活和独立。

在Java中，迭代器模式通常通过实现Iterator接口来实现。
Iterator接口定义了访问和遍历元素的方法，如hasNext()用于判断是否还有下一个元素，next()用于获取下一个元素等。

**迭代器模式的主要优点包括：**
+ 它支持以不同的方式遍历一个聚合对象。
+ 迭代器简化了聚合对象的接口。
+ 在同一个聚合上可以有多个遍历。
+ 在遍历的同时更改聚合对象结构是安全的。

> 需要注意的是，Java集合框架（Collections Framework）已经内置了迭代器模式，因此在实际开发中，我们通常会直接使用Java集合类提供的迭代器，而不需要手动实现迭代器接口。例如，ArrayList、HashSet等类都实现了Iterable接口，可以通过iterator()方法获取迭代器。

## 案例
实现逆向迭代

### UML
![img.png](images/img.png)

实现步骤：
+ 定义迭代器接口Iterator类，类中定义判断是否还有下一个及获取下一个数据的接口
+ 定义接口实现类ReverseIterator，类中定义集合List<Object> 用于存储数据，定义游标属性用于获取当前集合所在位置， 然后实现Iterator接口

### 实现代码
Iterator.java
```java
// 定义迭代器接口
public interface Iterator {
    boolean hasNext();
    Object next();
}
```
ReverseIterator.java
```java
import java.util.List;

// 逆向迭代器具体实现方案
public class ReverseIterator implements Iterator{
    // 定义存储数据
    private List<Object> items;
    // 游标由于遍历
    private int position = 0;
    public ReverseIterator(List<Object> items) {
        this.items = items;
    }
    @Override
    public boolean hasNext() {
        return position < items.size();
    }
    @Override
    public Object next() {
        position++;
        return items.get(items.size() - position);
    }
}
```
TestClient.java
```java
public class TestClient {
    public static void main(String[] args) {
        List<Object> list = new ArrayList<Object>();
        list.add("1");
        list.add("2");
        list.add("3");

        ReverseIterator reverseIterator = new ReverseIterator(list);
        while(reverseIterator.hasNext()){
            System.out.println(reverseIterator.next());
        }
    }
}
```

执行结果：
![img_1.png](images/img_1.png)


## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)