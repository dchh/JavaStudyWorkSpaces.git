package demo1;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/13 22:26
 */
public class TestClient {
    public static void main(String[] args) {
        List<Object> list = new ArrayList<Object>();
        list.add("1");
        list.add("2");
        list.add("3");

        ReverseIterator reverseIterator = new ReverseIterator(list);
        while(reverseIterator.hasNext()){
            System.out.println(reverseIterator.next());
        }
    }
}
