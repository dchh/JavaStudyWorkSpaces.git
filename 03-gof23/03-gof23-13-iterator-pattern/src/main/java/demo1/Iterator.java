package demo1;

/**
 * 定义迭代器接口
 *
 * @author Anna.
 * @date 2024/4/13 22:23
 */
public interface Iterator {
    boolean hasNext();
    Object next();
}
