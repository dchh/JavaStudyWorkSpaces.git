package demo1;

import java.util.List;

/**
 * 逆向迭代器具体实现方案
 *
 * @author Anna.
 * @date 2024/4/13 22:23
 */
public class ReverseIterator implements Iterator{
    // 定义存储数据
    private List<Object> items;
    // 游标由于遍历
    private int position = 0;

    public ReverseIterator(List<Object> items) {
        this.items = items;
    }

    @Override
    public boolean hasNext() {
        return position < items.size();
    }

    @Override
    public Object next() {
        position++;
        return items.get(items.size() - position);
    }
}
