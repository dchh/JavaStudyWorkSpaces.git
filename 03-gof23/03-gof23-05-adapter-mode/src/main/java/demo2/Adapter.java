package demo2;

import demo1.Adaptee;

/**
 * 适配器：这是适配器模式的核心。适配器类实现了目标接口，并在内部持有适配者类的一个实例。
 * 当客户端调用目标接口的方法时，适配器类会将调用委托给适配者类的相应方法（可能需要经过一些转换）。
 *
 * 对象适配器模式
 *
 * @author Anna.
 * @date 2024/4/8 16:59
 */
public class Adapter implements Target {
    private Adaptee adaptee;

    @Override
    public void handlePrint() {
        adaptee.print();
    }
}
