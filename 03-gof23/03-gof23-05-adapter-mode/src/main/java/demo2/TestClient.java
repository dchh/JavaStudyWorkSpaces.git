package demo2;

import demo1.Adapter;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/8 17:00
 */
public class TestClient {
    public static void main(String[] args) {
        Adapter adapter = new Adapter();
        adapter.handlePrint();
    }

}
