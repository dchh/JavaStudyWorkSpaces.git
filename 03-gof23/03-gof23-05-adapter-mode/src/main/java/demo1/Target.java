package demo1;

/**
 * 这是我们期望得到的接口。客户端针对这个接口编程，而不需要知道具体的实现细节
 * 笔记本：USB接口
 *
 * @author Anna.
 * @date 2024/4/8 16:58
 */
public interface Target {

    void handlePrint();
}
