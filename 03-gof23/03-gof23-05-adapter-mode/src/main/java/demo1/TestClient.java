package demo1;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/8 17:00
 */
public class TestClient {
    public static void main(String[] args) {
        // 创建适配器
        Adapter adapter = new Adapter();
        // 测试
        adapter.handlePrint();
    }
}
