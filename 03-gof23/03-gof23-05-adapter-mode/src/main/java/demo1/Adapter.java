package demo1;

/**
 * 适配器：这是适配器模式的核心。适配器类实现了目标接口，并在内部持有适配者类的一个实例。
 * 当客户端调用目标接口的方法时，适配器类会将调用委托给适配者类的相应方法（可能需要经过一些转换）。
 *
 * 类适配器
 *
 * @author Anna.
 * @date 2024/4/8 16:59
 */
public class Adapter extends Adaptee implements Target{
    @Override
    public void handlePrint() {
        super.print();
    }
}
