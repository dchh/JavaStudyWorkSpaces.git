package demo1;

/**
 * 这是需要被适配的类，它可能有一个不兼容的接口。
 *  比如：不兼容USB接口的旧键盘
 *
 * @author Anna.
 * @date 2024/4/8 16:56
 */
public class Adaptee {

    public void print(){
        System.out.println("键盘输出");
    }
}
