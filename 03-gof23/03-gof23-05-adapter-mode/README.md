# 适配器模式
## 设么是适配器模式
它属于结构型模式，主要用于将一个类的接口转换成客户端所期望的另一种接口，从而使得原本由于接口不兼容而无法协同工作的类能够一起工作。

适配器模式主要解决的是不兼容接口的问题。在软件开发中，经常会有这样的情况：我们有一个现有的类，它的接口（方法、属性等）不符合我们的需求，但我们又无法直接修改这个类（可能是因为它是第三方库的一部分，或者出于其他原因）。此时，我们可以使用适配器模式来“包装”这个类，使其具有我们期望的接口。

## 适配器模式的主要形式
+ 类适配器模式：通过多重继承的方式，适配器类继承自目标接口和适配者类。由于Java不支持多重继承（除了接口），因此在实际应用中，我们通常会使用对象组合的方式来实现类适配器模式的效果。 
+ 对象适配器模式：适配器类持有适配者类的一个实例，并实现了目标接口。当客户端调用目标接口的方法时，适配器类会调用适配者类的相应方法。
适配器模式的优点包括：

## 适配器模式特点
+ 提高了类的复用性：通过适配器，我们可以复用那些原本不兼容的类。
+ 增加了灵活性：适配器模式使得代码更加灵活，我们可以很容易地更换适配者类，而不需要修改客户端代码。
+ 遵循了“开闭原则”：适配器模式对修改关闭，对扩展开放。我们可以通过添加新的适配器类来支持新的适配者类，而不需要修改现有的代码。
> 然而，适配器模式也有其局限性。例如，如果适配者类的接口与目标接口的差别太大，那么适配器类的实现可能会变得非常复杂和难以维护。此外，如果过度使用适配器模式，可能会导致系统结构变得复杂和混乱。

## 类适配器
### UML
![img.png](images/img.png)

- Adaptee（适配者键盘）具有打印功能，但是由于是接口并不适用。
- Target（目标接口）目标接口需要通过USB插入
- Adapter（适配器）实现目标接口，重新输出方法（通过继承调用适配者输出方法）
- 测试时，通过创建适配者Adapter调用目标接口方法即可实现调用Adaptee（适配者）相应方法

### 实现代码
Adaptee.java
```java
// 这是需要被适配的类，它可能有一个不兼容的接口。
// 比如：不兼容USB接口的旧键盘
public class Adaptee {
    public void print(){
        System.out.println("键盘输出");
    }
}
```
Target.java
```java
// 这是我们期望得到的接口。客户端针对这个接口编程，而不需要知道具体的实现细节
// 笔记本：USB接口
public interface Target {
    void handlePrint();
}
```
Adapter.java
```java
// 类适配器
// 适配器：这是适配器模式的核心。适配器类实现了目标接口，并在内部持有适配者类的一个实例。
// 当客户端调用目标接口的方法时，适配器类会将调用委托给适配者类的相应方法（可能需要经过一些转换）。
public class Adapter extends Adaptee implements Target{
    @Override
    public void handlePrint() {
        super.print();
    }
}
```
TestClient.java
```java
public class TestClient {
    public static void main(String[] args) {
        // 创建适配器
        Adapter adapter = new Adapter();
        // 测试
        adapter.handlePrint();
    }
}
```

执行结果

![img_1.png](images/img_1.png)

## 对象适配器模式
### UML
![img_2.png](images/img_2.png)
该模式只需要修改Adapter(适配器)，将继承改为对象组合。将Adapter属性设置为Adaptee,通过Adaptee调用其方法
Adapter.java
```java
// 对象适配器模式
// 适配器：这是适配器模式的核心。适配器类实现了目标接口，并在内部持有适配者类的一个实例。
//当客户端调用目标接口的方法时，适配器类会将调用委托给适配者类的相应方法（可能需要经过一些转换）。
public class Adapter implements Target {
    private Adaptee adaptee;
    @Override
    public void handlePrint() {
        adaptee.print();
    }
}
```

执行结果：

![img_3.png](images/img_3.png)

## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)
