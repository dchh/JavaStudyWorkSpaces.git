package demo1;

/**
 * 享元接口：
 *  提供操作外部状态的接口
 *
 * @author Anna.
 * @date 2024/4/11 16:03
 */
public interface Flyweight {

    /**
     * 获取棋子信息：
     *  通过传入地址获取外部状态地址的信息
     *
     * @param
     * @return void
     * @author Anna.
     * @date 2024/4/11 16:04
     */
    void getInfo(Address address);
}
