package demo1;

/**
 * 定义享元接口的实现：
 * 围棋：
 *  通过属性设置内部状态，并提供内部状态的设置及调用方法（这里只提供了设置方法）、
 *  通过实现享元接口，传入外部状态调用外部状态方法或获取外部状态属性
 *
 * @author Anna.
 * @date 2024/4/11 15:58
 */
public class ChessPieces implements Flyweight{
    private String color;

    public ChessPieces(String color) {
        this.color = color;
    }

    @Override
    public void getInfo(Address address) {
        System.out.printf("颜色：%s,坐标：x=%s,y=%s%n",color,address.getX(),address.getY());
    }
}
