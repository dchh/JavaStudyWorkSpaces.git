package demo1;

import java.util.HashMap;
import java.util.Map;

/**
 * 享元工厂
 * 定义享元池（通常以键值对形式缓存），用于缓存共享内部状态（该方法只适用单线程，多线程需要进行优化）
 * 提供获取内部状态的方法，如果享元池中已存在，则享元池中对象，否则新建一个对象并设置到享元池中
 *
 * @author Anna.
 * @date 2024/4/11 16:05
 */
public class FlyweightFactory {

    // 定义享元池
    private Map<String, Flyweight> mapPool = new HashMap<String, Flyweight>();

    /**
     * 提供获取内部状态的方法
     *
     * @param key
     * @return demo1.Flyweight
     * @author Anna.
     * @date 2024/4/11 17:15
     */
    public Flyweight getChessPieces(String key){
        Flyweight flyweight = mapPool.get(key);
        if(flyweight == null){
            flyweight = new ChessPieces(key);
            mapPool.put(key, flyweight);
        }
        return flyweight;
    }
}
