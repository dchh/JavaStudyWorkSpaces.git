package demo1;

/**
 * 定义外部状态
 *  地址
 *
 * @author Anna.
 * @date 2024/4/11 15:59
 */
public class Address {
    private int x;
    private int y;

    public Address(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
}
