package demo1;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/11 16:20
 */
public class TestClient {
    public static void main(String[] args) {
        FlyweightFactory flyweightFactory = new FlyweightFactory();
        Flyweight chessPieces = flyweightFactory.getChessPieces("白色");
        chessPieces.getInfo(new Address(1,2));
    }
}
