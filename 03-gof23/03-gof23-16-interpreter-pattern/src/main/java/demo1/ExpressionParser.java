package demo1;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * 解析器类
 *
 * @author Anna.
 * @date 2024/4/24 15:27
 */
public class ExpressionParser{
    private String expression;
    private int index = 0;
    private Deque<Expression> queue = new ArrayDeque<Expression>();

    public ExpressionParser(String expression) {
        this.expression = expression;
        parse();
    }

    /**
     * 解析表达式
     *
     * @param
     * @return void
     * @author Anna.
     * @date 2024/4/24 16:12
     */
    public void parse(){
        // 判断是否连续数字
        boolean markDigit = false;
        // 判断是否连续运算符
        boolean markOperation = false;
        while (index < expression.length()){
            char c = expression.charAt(index);
            // 字符判断
            if(Character.isDigit(c) && !markDigit){
                markDigit = true;
                markOperation = false;
                parseNumber();
                continue;
            }
            else if((c == '+' || c == '-') && !markOperation){
                markDigit = false;
                markOperation = true;
                parseOperation();
            } else {
                throw new RuntimeException("输入字符错误: " + c);
            }
            index++;
        }
    }

    /**
     * 解析数字
     *
     * @param
     * @return void
     * @author Anna.
     * @date 2024/4/24 16:11
     */
    private void parseNumber() {
        StringBuilder sb = new StringBuilder();
        while (index < expression.length() && Character.isDigit(expression.charAt(index))) {
            sb.append(expression.charAt(index));
            index++;
        }
        int number = Integer.parseInt(sb.toString());
        queue.offer(new NumberExpression(number));
    }

    /**
     * 解析符号
     *
     * @param
     * @return
     * @author Anna.
     * @date 2024/4/24 16:11
     */
    private void parseOperation() {
        queue.offer(new OperatorExpression(expression.charAt(index)));
    }

    /**
     * 计算
     *
     * @return int
     * @author Anna.
     * @date 2024/4/24 16:46
     */
    public int interpret() {
        Expression result = null;
        int len = queue.size();
        for (int i = 0; i < len; i++) {
            Expression pop = queue.poll();
            if(result == null){
                result = pop;
            }
            else if (pop instanceof OperatorExpression &&  pop.interpret() == '+') {
                result = new AddExpression(result, queue.poll());
                i++;
            }
            else if (pop instanceof OperatorExpression &&  pop.interpret() == '-') {
                result = new SubtractionExpression(result, queue.poll());
                i++;
            }
            else {
                throw new RuntimeException("表达式异常");
            }
        }

        return result.interpret();
    }
}
