package demo1;

/**
 * 具体的表达式-加法
 *
 * @author Anna.
 * @date 2024/4/22 12:50
 */
public class AddExpression implements Expression{
    // 解析器左侧表达式
    private Expression left;
    // 解析器右侧表达式
    private Expression right;

    public AddExpression(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public int interpret() {
        return left.interpret() + right.interpret();
    }
}
