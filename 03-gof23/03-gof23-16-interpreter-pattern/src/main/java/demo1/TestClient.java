package demo1;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/24 15:50
 */
public class TestClient {
    public static void main(String[] args) {
        String expression = "10+30-25";
        ExpressionParser parser = new ExpressionParser(expression);
        System.out.println("Result: " + parser.interpret());
    }
}
