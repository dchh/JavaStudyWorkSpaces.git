package demo1;

/**
 * 具体的表达式-符号
 *
 * @author Anna.
 * @date 2024/4/22 12:50
 */
public class OperatorExpression implements Expression{

    // 定义接收值
    private char value;

    public OperatorExpression(char value) {
        this.value = value;
    }

    @Override
    public int interpret() {
        return value;
    }
}
