package demo1;

/**
 * 具体的表达式-数字
 *
 * @author Anna.
 * @date 2024/4/22 12:50
 */
public class NumberExpression implements Expression{
    
    // 定义接收值
    private Integer value;

    public NumberExpression(Integer value) {
        this.value = value;
    }

    @Override
    public int interpret() {
        return value;
    }
}
