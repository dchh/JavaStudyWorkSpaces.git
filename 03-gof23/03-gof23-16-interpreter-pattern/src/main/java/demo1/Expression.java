package demo1;

/**
 * 抽象表达式接口
 *
 * @author Anna.
 * @date 2024/4/22 12:49
 */
public interface Expression {

    /**
     * 定义解释接口及返回值
     *
     * @param
     * @return int
     * @author Anna.
     * @date 2024/4/22 12:49
     */
    int interpret();
}
