package demo1;

/**
 * 命令接口的具体实现
 *
 * @author Anna.
 * @date 2024/4/22 10:52
 */
public class LightOffCommand implements Command{

    /** 持有命令接受者的引用*/
    private Light light;

    public LightOffCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.off();
    }
}
