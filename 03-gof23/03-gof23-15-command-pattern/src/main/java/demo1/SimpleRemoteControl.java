package demo1;

/**
 * 请求者：持有命令接口的实现，用于处理命令请求
 *
 * @author Anna.
 * @date 2024/4/22 10:57
 */
public class SimpleRemoteControl {

    /** 持有命令接口的实现 */
    private Command command;

    public void setCommand(Command command) {
        this.command = command;
    }

    /**
     * 执行动作
     *
     * @param
     * @return void
     * @author Anna.
     * @date 2024/4/22 11:00
     */
    public void excute(){
        command.execute();
    }
}
