package demo1;

/**
 * 命令接口
 *
 * @author Anna.
 * @date 2024/4/22 10:52
 */
public interface Command {
    /**
     * 执行动作
     *
     * @param
     * @return void
     * @author Anna.
     * @date 2024/4/22 10:52
     */
    void execute();
}
