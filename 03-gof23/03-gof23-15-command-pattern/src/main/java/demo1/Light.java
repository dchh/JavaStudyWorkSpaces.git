package demo1;

/**
 * 命令的接收者
 *
 * @author Anna.
 * @date 2024/4/22 10:52
 */
public class Light {

    public void on() {
        System.out.println("Light is on");
    }

    public void off() {
        System.out.println("Light is off");
    }
}
