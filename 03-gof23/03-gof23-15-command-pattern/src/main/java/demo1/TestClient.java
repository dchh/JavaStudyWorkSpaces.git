package demo1;

/**
 * 测试
 *
 * @author Anna.
 * @date 2024/4/22 11:00
 */
public class TestClient {

    public static void main(String[] args) {
        // 创建命令的接受者
        Light light = new Light();
        // 创建命令指令
        LightOnCommand lightOnCommand = new LightOnCommand(light);
        LightOffCommand lightOffCommand = new LightOffCommand(light);
        // 创建发出指令的请求者
        SimpleRemoteControl control = new SimpleRemoteControl();
        control.setCommand(lightOnCommand);
        control.excute();

        control.setCommand(lightOffCommand);
        control.excute();
    }
}
