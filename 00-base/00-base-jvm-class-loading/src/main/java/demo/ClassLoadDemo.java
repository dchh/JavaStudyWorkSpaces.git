package demo;

/**
 * 测试JVM类的加载机制
 *
 * @author Anna.
 * @date 2024/4/5 14:14
 */
public class ClassLoadDemo {

    static {
        System.out.println("mian方法所在类的 static 代码块被调用了");
    }

    public static void main(String[] args) {
        new B();

        System.out.println("初始化完成后-第二次调用不会重复进行初始化");
        new B();
    }
}
