package demo;

/**
 * 测试主动引用,一定会发生类的初始化
 *
 * @author Anna.
 * @date 2024/4/5 14:14
 */
public class ClassLoadDemo1 {

    public static void main(String[] args) throws Exception {
//        System.out.println("========1 通过new========");
//        new A();
//
//        System.out.println("========2 调用类的静态成员（除了final常量）和静态方法========");
//        System.out.println("调用final常量：");
//        System.out.println("A.AGE:" + A.AGE);
//        System.out.println("调用非final静态成员常量：");
//        System.out.println("A.NAME:" + A.NAME);

        System.out.println("========3 反射调用========");
        Class.forName("demo.A");
    }
}
