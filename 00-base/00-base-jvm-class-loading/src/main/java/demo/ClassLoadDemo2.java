package demo;

/**
 * 类的被动引用,不会发生类的初始化
 *
 * @author Anna.
 * @date 2024/4/5 14:14
 */
public class ClassLoadDemo2 {

    public static void main(String[] args) {
//        System.out.println("========通过子类引用父类的静态变量，不会导致子类初始化========");
//        System.out.println("========子类引用父类final常量，既不初始化父类，也不初始化子类========");
//        System.out.println("B.AGE" + B.AGE);
//        System.out.println("========子类引用父类非final常量，初始化父类，但不初始化子类========");
//        System.out.println("B.NAME" + B.NAME);

        System.out.println("========2 通过数组定义引用，不会触发此类的初始化========");
        A[] arr = new A[10];

    }
}
