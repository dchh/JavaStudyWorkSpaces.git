package demo4;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 取反加密工具
 *
 * @author Anna.
 * @date 2024/4/5 17:36
 */
public class EncrptUtils {

    /**
     * 文件字节取反加密
     *
     * @param src  原路径
     * @param dest 目标路径
     * @return void
     * @author Anna.
     * @date 2024/4/5 17:39
     */
    public static void inversion(File src, File dest) throws IOException {
        try (FileInputStream is = new FileInputStream(src); FileOutputStream baos = new FileOutputStream(dest)) {
            int temp = -1;
            while ((temp = is.read()) != -1) {
                // 取反
                baos.write(temp ^ 0xff);
            }
        } catch (Exception e) {
            throw e;
        }
    }
}
