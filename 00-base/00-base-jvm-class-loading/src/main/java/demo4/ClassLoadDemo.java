package demo4;

import java.io.File;
import java.io.IOException;

/**
 * 自定义解密类加载器
 * * 1 加密UserDo.class文件
 * * 2 使用上一案例中类加载器加载加密后的class
 * * 3 使用解密类加载器加载
 *
 * @author Anna.
 * @date 2024/4/5 17:36
 */
public class ClassLoadDemo {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        // 1 加密.class文件
        EncrptUtils.inversion(new File("D://demo3/UserDo.class"), new File("D://temp/demo3/UserDo.class"));

        // 获取根路径
        String path = "D:/temp";

        // 2 使用上一案例中类加载器加载加密后的class
        // 获取自定义类加载器    报错ClassNotFoundException
//        CustomClassLoader customClassLoader = new CustomClassLoader(path);
//        Class<?> clazz1 = customClassLoader.loadClass("demo3.UserDo");

        // 3 使用解密类加载器加载
        DecrptClassLoader decrptClassLoader = new DecrptClassLoader(path);
        Class<?> clazz2 = decrptClassLoader.loadClass("demo3.UserDo");
        System.out.printf("获取clazz3的类加载器:%s%n", clazz2.getClassLoader());
    }
}
