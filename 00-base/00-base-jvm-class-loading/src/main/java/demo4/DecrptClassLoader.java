package demo4;

import java.io.*;

/**
 * 自定义解密类加载器
 *
 * @author Anna.
 * @date 2024/4/5 16:07
 */
public class DecrptClassLoader extends ClassLoader {

    /**
     * 定义一个加载根路径
     */
    private String rootDir;

    public DecrptClassLoader(String rootDir) {
        this.rootDir = rootDir;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        // 加载该类是否已经加载到命名空间
        Class<?> clazz = findLoadedClass(name);
        if (clazz != null) {
            return clazz;
        }

        // 尝试自己加载类
        byte[] classData;
        try {
            classData = getClassData(name);
        } catch (IOException e) {
            throw new ClassNotFoundException();
        }

        // 如果自己也加载不到该类，则抛出异常
        if (classData == null) {
            throw new ClassNotFoundException();
        }

        // 调用defineClass()导入类型到方法区
        clazz = defineClass(name, classData, 0, classData.length);

        return clazz;
    }

    /**
     * 根据路径读取.class文件
     *
     * @param name
     * @return byte[]
     * @author Anna.
     * @date 2024/4/5 16:24
     */
    private byte[] getClassData(String name) throws IOException {
        // 将包路径转换为类路径
        String path = rootDir + File.separator + name.replace(".", File.separator) + ".class";
        // try-with-resources
        try (InputStream is = new FileInputStream(path); ByteArrayOutputStream baos = new ByteArrayOutputStream();) {
            // 使用字节流读取class文件 字节取反
            int temp = -1;
            while ((temp = is.read()) != -1) {
                // 字节取反
                baos.write(temp ^ 0xff);
            }
            return baos.toByteArray();
        }
    }
}
