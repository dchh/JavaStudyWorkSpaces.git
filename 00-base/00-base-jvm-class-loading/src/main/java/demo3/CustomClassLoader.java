package demo3;

import java.io.*;

/**
 * 自定义类加载器
 *
 * 首先检查请求的类型是否已经被这个加载装载到了命名空间，如果已加载，则直接返回
 * + 委派类加载器请求给父类加载器，如果父类加载器能够完成加载，则直接返回加载器加载的Class实例
 * + 调用本类加载器的findClass()方法，试图获取对应的字节码，如果获取的到，则调用defineClass()导入类型到方法区；如果获取不到对应的字节码或者其他原因失败，返回异常给loadClass(),loadClass()转抛异常，终止加载过程
 * @author Anna.
 * @date 2024/4/5 16:07
 */
public class CustomClassLoader extends ClassLoader{

    /** 定义一个加载根路径 */
    private String rootDir;

    public CustomClassLoader(String rootDir){
        this.rootDir = rootDir;
    }

    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        // 加载该类是否已经加载到命名空间
        Class<?> clazz = findLoadedClass(name);
        if (clazz != null) {
            return clazz;
        }

        ///// 尝试加载类
        // 交给父类进行加载
        ClassLoader parentClassLoader = this.getParent();
        clazz = parentClassLoader.loadClass(name);
        if(clazz != null){
            return clazz;
        }

        // 尝试自己加载类
        byte[] classData;
        try {
            classData = getClassData(name);
        } catch (IOException e) {
            throw new ClassNotFoundException();
        }

        // 如果自己也加载不到该类，则抛出异常
        if(classData == null){
            throw new ClassNotFoundException();
        }

        // 调用defineClass()导入类型到方法区
        clazz = defineClass(name, classData, 0, classData.length);

        return clazz;
    }

    /**
     * 根据路径读取.class文件
     *
     * @param name
     * @return byte[]
     * @author Anna.
     * @date 2024/4/5 16:24
     */
    private byte[] getClassData(String name) throws IOException {
        // 将包路径转换为类路径
        String path = rootDir + File.separator + name.replace(".", File.separator) + ".class";
        // 使用字节流读取class文件
        InputStream is  = null;
        ByteArrayOutputStream baos  = null;
        try{
            is = new FileInputStream(path);
            baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int temp = -1;
            while ((temp = is.read(buffer)) != -1){
                baos.write(buffer,0,temp);
            }
        }
        catch (Exception e){
            throw e;
        }
        finally {
            if(is != null){
                is.close();
            }
            if(baos != null){
                baos.close();
            }
        }

        return baos.toByteArray();
    }
}
