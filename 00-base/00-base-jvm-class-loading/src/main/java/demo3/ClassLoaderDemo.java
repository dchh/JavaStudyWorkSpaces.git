package demo3;

/**
 * 自定义类加载器 测试
 *
 * @author Anna.
 * @date 2024/4/5 16:33
 */
public class ClassLoaderDemo {

    public static void main(String[] args) throws ClassNotFoundException {

        // 获取根路径
        String path = "D:";
        System.out.println(path);

        // 获取自定义类加载器
        CustomClassLoader customClassLoader = new CustomClassLoader(path);

        // 使用同一个类加载器加载UserDo
        Class<?> clazz1 = customClassLoader.loadClass("demo3.UserDo");
        Class<?> clazz2 = customClassLoader.loadClass("demo3.UserDo");
        System.out.printf("clazz1 hashCode:%s%n", clazz1.hashCode());
        System.out.printf("clazz2 hashCode:%s%n", clazz2.hashCode());
        System.out.printf("判断同一个类加载器加载同一个对象，class是否相同:%s%n", clazz1 == clazz2);
        System.out.printf("获取clazz1的类加载器:%s%n", clazz1.getClassLoader());

        // 重新创建一个自定义类加载器
        CustomClassLoader customClassLoader2 = new CustomClassLoader(path);

        // 使用不同类加载器加载UserDo
        Class<?> clazz3 = customClassLoader2.loadClass("demo3.UserDo");
        System.out.printf("clazz3 hashCode:%s%n", clazz3.hashCode());
        System.out.printf("判断不同类加载器加载同一个对象，class是否相同:%s%n", clazz1 == clazz3);
        System.out.printf("获取clazz3的类加载器:%s%n", clazz1.getClassLoader());
    }

}
