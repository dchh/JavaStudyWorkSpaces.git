package demo3;

/**
 * UserDo 实体
 * @author Anna.
 * @date 2024/4/5 16:31
 */
public class UserDo1 {

    private String name;

    public UserDo1(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "UserDo{" +
                "name='" + name + '\'' +
                '}';
    }
}
