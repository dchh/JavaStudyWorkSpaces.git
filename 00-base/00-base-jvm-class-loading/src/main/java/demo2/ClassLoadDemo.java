package demo2;

/**
 * 获取加载器及应用类路径
 *
 * @author Anna.
 * @date 2024/4/5 15:30
 */
public class ClassLoadDemo {
    public static void main(String[] args) {
        System.out.printf("获取当前应用程序类加载器：%s%n", ClassLoader.getSystemClassLoader());
        System.out.printf("获取应用程序类加载器父类加载器：%s%n", ClassLoader.getSystemClassLoader().getParent());
        System.out.printf("获取根加载器：%s%n", ClassLoader.getSystemClassLoader().getParent().getParent());

        System.out.printf("获取应用类路径：%s%n", System.getProperty("java.class.path"));
    }
}
