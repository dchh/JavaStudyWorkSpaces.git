package demo2;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * 使用Runtime编译,通过反射调用main方法
 *
 * @author Anna.
 * @date 2024/4/4 11:13
 */
public class DyanmaicDemo {

    public static void main(String[] args) throws Exception {
        String path = DyanmaicDemo.class.getResource("").getPath();
        String javaName = "Hello";

        // 创建文件
        File tempFile = createFile(path, javaName);

        // 使用Runtime 编译
        compilation(tempFile);

        // 反射执行文件
        extracted(path, javaName);
    }

    /**
     * 创建文件
     *
     * @param path
     * @param javaName
     * @return java.io.File
     * @author Anna.
     * @date 2024/4/4 13:05
     */
    private static File createFile(String path, String javaName) throws IOException {
        // 创建编译内容
        StringBuffer sb = new StringBuffer();
        sb.append("public class ").append(javaName).append("{")
                .append("public static void main(String[] args){")
                .append("System.out.println(\"hello world !!!\");")
                .append("}")
                .append("}");

        // 写入临时文件
        File tempFile = new File(path + javaName + ".java");
        FileWriter fileWriter = new FileWriter(tempFile);
        fileWriter.write(sb.toString());
        fileWriter.close();
        return tempFile;
    }

    /**
     * 编译
     *
     * @param tempFile
     * @return void
     * @author Anna.
     * @date 2024/4/4 13:05
     */
    private static void compilation(File tempFile) throws IOException, InterruptedException {
        String replace = tempFile.getAbsolutePath().replace(tempFile.getName(), "");
        replace = replace.substring(0, replace.length() - 1);

        String str = "javac " + tempFile.getAbsolutePath();
        Runtime runtime = Runtime.getRuntime();

        Process process = runtime.exec(str);

        // 读取命令的标准输出
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }

        // 读取命令的错误输出
        BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
        while ((line = errorReader.readLine()) != null) {
            System.err.println(line);
        }

        // 等待进程结束并获取退出值
        int exitValue = process.waitFor();
        if (exitValue == 0) {
            System.out.println("Compilation is successful");
        } else {
            System.out.println("Compilation Failed");
        }
    }

    /**
     * 反射执行main方法
     *
     * @param path
     * @param javaName
     * @return void
     * @author Anna.
     * @date 2024/4/4 13:05
     */
    private static void extracted(String path, String javaName) throws MalformedURLException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{new URL("file:/" + path)});
        Class<?> clazz = urlClassLoader.loadClass(javaName);
        Method mainMethod = clazz.getMethod("main", String[].class);
        // 注意：由于可变参数是JDK5之后才有的，下面代码如果new String[]{"1","2"}强制在转换才Object，则会被编译成 mainMethod.invoke(null,"1","2")，从而导致找不到方法。
        // 因此，如果传参则徐亚加上(Object)，避免这个问题
        mainMethod.invoke(null, (Object) new String[]{"1", "2"});
    }

}
