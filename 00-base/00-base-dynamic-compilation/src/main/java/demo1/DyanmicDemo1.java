package demo1;

import javax.tools.*;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.Collections;

/**
 * 使用JavaCompiler编译,通过反射调用main方法
 *
 * @author Anna.
 * @date 2024/4/4 10:55
 */
public class DyanmicDemo1 {
    public static void main(String[] args) throws Exception {
        // 创建编译内容
        String javaName = "Hello";

        // 获取JavaFileObject
        JavaFileObject source = getJavaFileObject(javaName);

        // 编译
        compiler(source);

        // 执行
        extracted(javaName);
    }

    /**
     * 获取JavaFileObject
     *
     * @param javaName
     * @return javax.tools.JavaFileObject
     * @author Anna.
     * @date 2024/4/4 13:56
     */
    private static JavaFileObject getJavaFileObject(String javaName) {
        StringBuffer sb = new StringBuffer();
        sb.append("public class ").append(javaName).append("{")
                .append("public static void main(String[] args){")
                .append("System.out.println(\"hello world !!!\");")
                .append("}")
                .append("}");

        // 创建一个Java源代码文件 string:///是一个特殊的URI协议，用于表示源代码内容直接来自一个字符串，而不是来自文件系统中的一个文件。 好处是，你可以完全在内存中处理源代码，无需涉及文件系统的I/O操作。
        JavaFileObject source = new SimpleJavaFileObject(URI.create("string:///" + javaName + ".java"), JavaFileObject.Kind.SOURCE) {
            @Override
            public CharSequence getCharContent(boolean ignoreEncodingErrors) {
                return sb.toString();
            }
        };
        return source;
    }

    /**
     * 编译
     *
     * @param source
     * @return void
     * @author Anna.
     * @date 2024/4/4 13:49
     */
    private static void compiler(JavaFileObject source) {
        // 获取系统Java编译器
        JavaCompiler systemJavaCompiler = ToolProvider.getSystemJavaCompiler();
        // 创建诊断收集器
        DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();

        // 创建编译任务
        StandardJavaFileManager fileManager = systemJavaCompiler.getStandardFileManager(diagnostics, null, null);

        // 设置输出目录
        Iterable<? extends File> locations = fileManager.getLocation(StandardLocation.CLASS_OUTPUT);
        try {
            fileManager.setLocation(StandardLocation.CLASS_OUTPUT, Collections.singleton(new File(DyanmicDemo1.class.getClassLoader().getResource("").getPath())));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        Iterable<? extends JavaFileObject> compilationUnits = Arrays.asList(source);
        JavaCompiler.CompilationTask task = systemJavaCompiler.getTask(null, fileManager, diagnostics, null, null, compilationUnits);

        // 执行编译任务
        boolean success = task.call();
        // 关闭文件管理器
        try {
            fileManager.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 处理编译诊断信息
        for (Diagnostic<?> diagnostic : diagnostics.getDiagnostics()) {
            System.out.println(diagnostic);
            System.out.println(diagnostic.getKind() + ": " + diagnostic.getMessage(null));
        }

        System.out.println(success ? "编译通过" : "编译失败");
    }

    /**
     * 反射执行
     *
     * @param javaName
     * @return void
     * @author Anna.
     * @date 2024/4/4 13:50
     */
    private static void extracted(String javaName) throws MalformedURLException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{new URL("file:/" + DyanmicDemo1.class.getClassLoader().getResource("").getPath())});
        Class<?> clazz = urlClassLoader.loadClass(javaName);
        Method mainMethod = clazz.getMethod("main", String[].class);
        // 注意：由于可变参数是JDK5之后才有的，下面代码如果new String[]{"1","2"}强制在转换才Object，则会被编译成 mainMethod.invoke(null,"1","2")，从而导致找不到方法。
        // 因此，如果传参则徐亚加上(Object)，避免这个问题
        mainMethod.invoke(null, (Object) new String[]{"1", "2"});
    }

}
