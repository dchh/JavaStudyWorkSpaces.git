package demo1;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

/**
 * Java 脚本引擎及简单使用
 *
 * @author Anna.
 * @date 2024/4/4 15:37
 */
public class ScriptEngineDemo {
    public static void main(String[] args) throws Exception {
        // 创建ScriptEngineManager实例
        ScriptEngineManager manager = new ScriptEngineManager();
        // 通过ScriptEngineManager获取特定的脚本引擎。调用getEngineByName传入需要使用的脚本引擎名称（比如JavaScript）即可
        ScriptEngine engineByName = manager.getEngineByName("JavaScript");

        // 使用ScriptEngine执行脚本。一旦你有了ScriptEngine的实例，你可以使用eval方法来执行脚本
        Object evalRtn = engineByName.eval("'1' + 20");
        System.out.println(evalRtn);
    }
}
