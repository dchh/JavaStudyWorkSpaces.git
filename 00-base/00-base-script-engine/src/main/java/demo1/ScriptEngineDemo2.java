package demo1;

import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;

/**
 * 查看系统中可用的所有脚本引擎的信息
 *
 * @author Anna.
 * @date 2024/4/4 15:37
 */
public class ScriptEngineDemo2 {
    public static void main(String[] args) throws Exception {
        // 创建ScriptEngineManager实例
        ScriptEngineManager manager = new ScriptEngineManager();
        // 获取所有可用的脚本引擎工厂
        for (ScriptEngineFactory factory : manager.getEngineFactories()) {
            System.out.println("ScriptEngineFactory Info:");
            System.out.println("  Script Engine Name: " + factory.getEngineName());
            System.out.println("  Script Engine Version: " + factory.getEngineVersion());
            System.out.println("  Script Engine Language: " + factory.getLanguageName());
            System.out.println("  Script Engine Language Version: " + factory.getLanguageVersion());
            System.out.println("  Names: " + factory.getNames());
            System.out.println("  Mime Types: " + factory.getMimeTypes());
            System.out.println("  Extensions: " + factory.getExtensions());
        }
    }
}
