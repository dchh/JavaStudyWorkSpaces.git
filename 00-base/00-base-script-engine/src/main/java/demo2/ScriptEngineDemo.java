package demo2;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.util.List;
import java.util.function.Consumer;

/**
 * 脚本引擎使用测试案例
 *
 * @author Anna.
 * @date 2024/4/4 15:53
 */
public class ScriptEngineDemo {
    public static void main(String[] args) throws Exception {
        // 创建脚本引擎管理对象
        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        // 获取js脚本引擎对象
        ScriptEngine jsEngine = scriptEngineManager.getEngineByName("js");

        // 通过Jjava方法定义变量，存储到引擎上下文中
        jsEngine.put("name", "张三");
        // 通过执行脚本定义
        jsEngine.eval("var age = 30;");

        // 输出上下文中变量的值
        System.out.printf("输出两种方式定义变量，引擎上限文中的值：name=%s,age=%s%n", jsEngine.get("name"), jsEngine.get("age"));

        // 通过脚本定义函数
        jsEngine.eval("function add(a,b){return a + b;}");

        // 执行函数
        Invocable jsInvocable = (Invocable) jsEngine;
        Object addRtn = jsInvocable.invokeFunction("add", 30, 20);
        System.out.printf("输出调用脚本定义函数结果值：%s%n", addRtn);

        // 脚本引擎中导入java包
        String jsCode = "var list = Java.type('java.util.Arrays').asList(\"张三\",\"李四\");";
        jsEngine.eval(jsCode);

        // 获取js中通过java类中方法定义的变量，并输出
        List<String> list = (List<String>) jsEngine.get("list");
        for (String item : list) {
            System.out.println(item);
        }
    }

    /**
     * 定义打印的方法
     *
     * @param str
     * @param consumer
     * @return void
     * @author Anna.
     * @date 2024/4/4 16:38
     */
    public static void print(String str, Consumer<String> consumer) {
        consumer.accept(str);
    }
}
