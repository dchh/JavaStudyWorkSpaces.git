package demo2;

import demo2.annotation.Id;
import demo2.annotation.Table;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * 反射模拟处理注解新
 *
 * @author Anna.
 * @date 2024/4/4 21:35
 */
public class AnnotationDemo {
    public static void main(String[] args) throws Exception {
        // 反射获取UserDo
        String path = AnnotationDemo.class.getClassLoader().getResource("").getPath();
//        System.out.println(path);
        Class<?> clazz = Class.forName("demo2.UserDo");

        // 初始化实例
        UserDo userDo = (UserDo) clazz.getDeclaredConstructor().newInstance();

        // 定义SQL 字段
        StringBuffer sb = new StringBuffer();
        sb.append("CREATE TABLE ");

        // 获取表注解
        Table table = clazz.getAnnotation(Table.class);
        sb.append(table.value()).append(" {");
        // 获取所有属性
        Field[] fields = clazz.getDeclaredFields();
        for (int i = 1; i <= fields.length; i++) {
            Field field = fields[i - 1];
            // 获取所有注解
            Annotation[] annotations = field.getAnnotations();
            String fieldStr = "";
            String idStr = "";
            for (Annotation a : annotations) {
                if (a instanceof demo2.annotation.Field) {
                    demo2.annotation.Field a1 = (demo2.annotation.Field) a;
                    fieldStr = a1.columnName() + " " + a1.type() + "(" + a1.length() + ")";
                } else if (a instanceof Id) {
                    idStr = " PRIMARY";
                }


            }
            sb.append(fieldStr).append(idStr);
            if (i < fields.length) {
                sb.append(",");
            }
        }
        sb.append("}");

        System.out.printf("输出SQL:%s%n ", sb.toString());

    }
}
