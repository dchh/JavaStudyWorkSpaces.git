package demo2.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 字段
 *
 * @author Anna.
 * @date 2024/4/4 21:42
 */
@Target(ElementType.FIELD)  // 设置作用范围
@Retention(RetentionPolicy.RUNTIME) // 设置生命周期
public @interface Field {
    // 字段名称
    String columnName();
    // 类型
    String type();
    // 长度
    int length();
}
