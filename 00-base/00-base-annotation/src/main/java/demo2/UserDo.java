package demo2;

import demo2.annotation.Field;
import demo2.annotation.Id;
import demo2.annotation.Table;

/**
 * |实体字段|表字段|备注|
 * |:---|:---|:---|
 * |id| int(10) | 主键 |
 * |name| varchar2(30) |-|
 *
 * @author Anna.
 * @date 2024/4/4 21:36
 */
@Table("USER_TABLE")
public class UserDo {

    @Id
    @Field(columnName = "id", type = "int", length = 10)
    private Integer id;

    @Field(columnName = "name", type = "varchar2", length = 30)
    private String name;

    public UserDo() {
    }

    public UserDo(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "UserDo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
