package demo2;

import demo1.UserDo;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * 测试反射
 * 1 通过反射创建UserDo对象，
 * 2 调用默认构造器创建对象
 * 3 调用公有方法设置name
 * 4 调用公有方法获取私有字段name属性值
 * 5 调用私有方法sayHello
 *
 * @author Anna.
 * @date 2024/4/3 23:19
 */
public class ReflexDemo {
    public static void main(String[] args) throws Exception {
        // 通过Class.forName()获取Class
        Class<?> clazz = Class.forName("demo1.UserDo");
        // 获取构造方法
        Constructor<?> declaredConstructor = clazz.getDeclaredConstructor(String.class, Integer.class, String.class);
        // 调用newInstance()初始化UserDo
        UserDo userDo = (UserDo) declaredConstructor.newInstance("张三", 20, "男");
        System.out.printf("输出初始化后的UserDo对象：%s%n", userDo);

        // 调用调用公有方法设置name
        Method setNameMethod = clazz.getDeclaredMethod("setName", String.class);
        // 方法调用前需要先初始化 否则会抛出 Exception in thread "main" java.lang.IllegalArgumentException: object is not an instance of declaring class
        // 从Java 9开始，newInstance()方法已被弃用，应使用getDeclaredConstructor().newInstance()
        UserDo userDo1 = (UserDo) clazz.getDeclaredConstructor().newInstance();
        // 执行共育setNameMethod方法
        setNameMethod.invoke(userDo1, "李四");

        //// 调用公有方法获取私有字段name属性值
        Method getNameMethod = clazz.getMethod("getName");
        System.out.printf("通过反射调用getName:%s%n", getNameMethod.invoke(userDo1));

        // 因为已经拿到UserDo的实例也可以通过实例调用getName
        System.out.printf("通过实例调用getName:%s%n", userDo1.getName());

        // 通过反射调用私有方法sayHello
        Method sayHelloMethod = clazz.getDeclaredMethod("sayHello", String.class);
        // 调用私有方法需要设置为可访问 否则会抛出异常 Exception in thread "main" java.lang.IllegalAccessException: class demo2.ReflexDemo cannot access a member of class demo1.UserDo with modifiers "private
        sayHelloMethod.setAccessible(true);
        sayHelloMethod.invoke(userDo1, "通过反射调用私有方法sayHello说了你好啊");

    }
}
