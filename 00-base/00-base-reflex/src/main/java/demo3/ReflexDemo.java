package demo3;

import demo1.UserDo;

import java.lang.reflect.Method;

/**
 * 比较反射与对象实例调用方法对比
 *
 * @author Anna.
 * @date 2024/4/3 23:54
 */
public class ReflexDemo {
    public static void main(String[] args) throws Exception {
        test1();
        test2();
        test3();
    }

    /**
     * 通过UserDo调用方法
     *
     * @author Anna.
     * @date 2024/4/3 23:54
     */
    public static void test1(){
        UserDo userDo = new UserDo();
        long start = System.currentTimeMillis();
        for (long i = 0; i < 10000000000L; i++) {
            userDo.getName();
        }
        long end = System.currentTimeMillis();
        System.out.printf("通过UserDo调用getName()方法耗时(ms)：%s%n", end - start);
    }

    /**
     * 通过反射调用getName方法，不设置跳过安全检查
     *
     * @author Anna.
     * @date 2024/4/3 23:57
     */
    public static void test2() throws Exception {
        // 获取Class
        Class<?> clazz = Class.forName("demo1.UserDo");
        // 初始化对象
        UserDo userDo = (UserDo) clazz.getDeclaredConstructor().newInstance();
        // 获取setName()方法
        Method getNameNameMethod = clazz.getMethod("getName");
        long start = System.currentTimeMillis();
        for (long i = 0; i < 10000000000L; i++) {
            getNameNameMethod.invoke(userDo);
        }
        long end = System.currentTimeMillis();
        System.out.printf("通过反射调用getName方法，不设置跳过安全检查(ms)：%s%n", end - start);
    }

    /**
     * 通过反射调用getName方法，设置跳过安全检查
     *
     * @author Anna.
     * @date 2024/4/3 23:57
     */
    public static void test3() throws Exception {
        // 获取Class
        Class<?> clazz = Class.forName("demo1.UserDo");
        // 初始化对象
        UserDo userDo = (UserDo) clazz.getDeclaredConstructor().newInstance();
        // 获取setName()方法
        Method getNameNameMethod = clazz.getMethod("getName");
        // 设置跳过安全检查
        getNameNameMethod.setAccessible(true);
        long start = System.currentTimeMillis();
        for (long i = 0; i < 10000000000L; i++) {
            getNameNameMethod.invoke(userDo);
        }
        long end = System.currentTimeMillis();
        System.out.printf("通过反射调用getName方法，设置跳过安全检查(ms)：%s%n", end - start);
    }
}
