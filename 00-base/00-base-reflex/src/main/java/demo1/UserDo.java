package demo1;

/**
 * 定义UserDo用于测试反射
 *
 * @author Anna.
 * @date 2024/4/3 23:06
 */
public class UserDo {
    private String name;
    private Integer age;
    public String sex;

    public UserDo() {
    }

    public UserDo(String name, Integer age, String sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * 定义一个私有方法
     *
     * @param content
     * @return void
     * @author Anna.
     * @date 2024/4/3 23:09
     */
    private void sayHello(String content){System.out.printf("%s说%s%n",this.name,content);}

    @Override
    public String toString() {
        return "UserDo{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                '}';
    }
}
