package demo1;

/**
 * 测试反射
 * 获取Class的三种方式
 *
 * @author Anna.
 * @date 2024/4/3 23:10
 */
public class ReflexDemo1 {
    public static void main(String[] args) throws ClassNotFoundException {
        // 通过.class获取
        Class<UserDo> userDoClass1 = UserDo.class;
        System.out.println(userDoClass1);   // class demo1.UserDo

        // 通过UserDo实例调用getClass()方法获取
        Class<? extends UserDo> userDoClass2 = new UserDo().getClass();
        System.out.println(userDoClass2 == userDoClass1); // true

        // 通过Class.forName()获取
        Class<UserDo> userDoClass3 = (Class<UserDo>) Class.forName("demo1.UserDo");
        System.out.println(userDoClass3 == userDoClass2);   // true
    }
}
