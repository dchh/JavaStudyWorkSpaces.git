package demo4;

import demo1.UserDo;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * 反射操作泛型
 *
 * @author Anna.
 * @date 2024/4/4 0:48
 */
public class ReflexDemo {
    public void test01(Map<String, UserDo> map, List<UserDo> userDoList) {
        System.out.println("调用test01");
    }

    public Map<String, UserDo> test02() {
        System.out.println("调用test02");
        return null;
    }

    public static void main(String[] args) throws Exception {
        // 获取制定法方法参数泛型信息
        Method test01Method = ReflexDemo.class.getMethod("test01", Map.class, List.class);
        // 获取方法的形式参数类型
        Type[] genericParameterTypes = test01Method.getGenericParameterTypes();
        for (Type genericParameter : genericParameterTypes) {
            // 判断形式参数类型是否为参数化的类型
            if (genericParameter instanceof ParameterizedType) {
                // 获取实际的类型参数
                Type[] actualTypeArguments = ((ParameterizedType) genericParameter).getActualTypeArguments();
                for (Type actualTypeArgument : actualTypeArguments) {
                    System.out.printf("参数，泛型类型：%s%n", actualTypeArgument);
                }
            }
        }

        // 获取制定法方法返回值泛型信息
        Method test02Method = ReflexDemo.class.getMethod("test02");
        // 获取方法的形式参数类型
        Type genericReturnType = test02Method.getGenericReturnType();
        // 判断返回值 类型是否为参数化的类型
        if (genericReturnType instanceof ParameterizedType) {
            // 获取实际的类型参数
            Type[] actualTypeArguments = ((ParameterizedType) genericReturnType).getActualTypeArguments();
            for (Type actualTypeArgument : actualTypeArguments) {
                System.out.printf("返回值，泛型类型：%s%n", actualTypeArgument);
            }
        }

    }


}
