package com.demo.demo1;

import javassist.*;

import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * 是用javassist创建一个UserDo,使用构造方法设置值，打印结果
 *
 * @author Anna.
 * @date 2024/4/4 17:32
 */
public class JavassistDemo {
    public static void main(String[] args) throws Exception {
        // 创建ClassPool对象
        ClassPool classPool = ClassPool.getDefault();
        // 创建一个新的类
        CtClass ctClass = classPool.makeClass("com.demo.entity.UserDo");

        // 创建属性
        CtField fieldName = CtField.make("private String name;", ctClass);
        CtField fieldAge = CtField.make("private Integer age;", ctClass);

        // 将属性添加到类中
        ctClass.addField(fieldName);
        ctClass.addField(fieldAge);

        // 创建get方法
        CtMethod getName = CtMethod.make("public String getName(){return this.name;}", ctClass);
        CtMethod getAge = CtMethod.make("public Integer getAge(){return this.age;}", ctClass);

        // 将方法添加到类中
        ctClass.addMethod(getName);
        ctClass.addMethod(getAge);

        // 创建构造器
        CtConstructor constructor1 = CtNewConstructor.make("public UserDo(){}", ctClass);
        CtConstructor constructor2 = CtNewConstructor.make("public UserDo(String name,Integer age){this.name = name; this.age = age;}", ctClass);

        // 添加构造器
        ctClass.addConstructor(constructor1);
        ctClass.addConstructor(constructor2);

        // 写入对应文件
        String path = JavassistDemo.class.getResource("/").getPath();
        ctClass.writeFile(path);

        // 使用反射获取对象并初始化，调用getName方法
        URLClassLoader urlClassLoader = URLClassLoader.newInstance(new URL[]{new URL("file:/" + path)});
        Class<?> clazz = urlClassLoader.loadClass("com.demo.entity.UserDo");
        Object o1 = clazz.getDeclaredConstructor(String.class, Integer.class).newInstance("张三", 20);

        // 调用getName方法
        Method getName1 = clazz.getMethod("getName");
        System.out.println(getName1.invoke(o1));

    }
}
