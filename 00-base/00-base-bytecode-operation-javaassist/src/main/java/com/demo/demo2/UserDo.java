package com.demo.demo2;

/**
 * 测试UserDo
 *
 * @author Anna.
 * @date 2024/4/4 19:58
 */
public class UserDo {

    private String name;

    public String getName() {
        System.out.println("调用了getName方法");
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "UserDo{" +
                "name='" + name + '\'' +
                '}';
    }
}
