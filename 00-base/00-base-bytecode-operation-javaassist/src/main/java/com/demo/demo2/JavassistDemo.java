package com.demo.demo2;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

import java.lang.reflect.Method;

/**
 * 1 通过Javassist 获取UserDo的class
 * 2 在setName之前添加打印信息
 * 3 修改getName第14行输出内容System.out.println("调用了getName方法"); 为System.out.println("通过avassist 修改了调用了getName方法");
 *
 * @author Anna.
 * @date 2024/4/4 19:58
 */
public class JavassistDemo {
    public static void main(String[] args) throws Exception {
        // 创建ClassPool对象
        ClassPool classPool = ClassPool.getDefault();
        // 获取UserDo
        CtClass ctClass = classPool.getCtClass("com.demo.demo2.UserDo");

        // 在setName之前添加打印信息
        // 获取调用setName方法
        CtMethod setName = ctClass.getDeclaredMethod("setName", new CtClass[]{classPool.get("java.lang.String")});
        // 设置方法调用前打印信息 $0 表示this,$1表示第一个参数
        setName.insertBefore("System.out.println(\"javassiist在setName前设置打印：\" + $1);");

        // 获取调用getName方法
        CtMethod getName = ctClass.getDeclaredMethod("getName");

        // 在getName第14行插入输出内容为System.out.println("通过avassist 修改了调用了getName方法");
        getName.insertAt(14, "System.out.println(\"通过avassist 修改了调用了getName方法\");");
        getName.insertAfter("System.out.println(\"javassiist在getName执行结束后return前设置打印\" );");

        // 通过反射调用新生成的方法
        Class<?> clazz = ctClass.toClass();
        // 初始化
        Object o = clazz.getDeclaredConstructor().newInstance();
        // 调用setName
        Method setName1 = clazz.getMethod("setName", String.class);
        setName1.invoke(o, "张三");

        // 调用getName
        Method getName1 = clazz.getMethod("getName");
        System.out.println(getName1.invoke(o));


    }
}
