# try-with-resources
## 为什么要介绍这个了
看看一下以下代码：
```
public static void fileCopyByTryWithResources(File src, File des) throws IOException {
    try (FileInputStream fis = new FileInputStream(src); FileOutputStream fos = new FileOutputStream(des);) {
        byte[] buffer = new byte[1024];
        int len = -1;
        while ((len = fis.read(buffer)) != -1) {
            fos.write(buffer, 0, len);
        }
    }
}
```
在不了解try-with-resources的情况下，有没有人会认为资源没有进行关闭了？那么看看原来try-cache-finally复制文件的写法是怎样的

## try-cache-finally复制文件写法
```java
package demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopyUtils {

    /**
     * try-cache-finally复制文件写法
     *
     * @param src
     * @param des
     * @return void
     * @author Anna.
     * @date 2024/4/5 18:34
     */
    public static void fileCopy(File src, File des) throws IOException {
        FileInputStream fis = null;
        FileOutputStream fos = null;

        try {
            fis = new FileInputStream(src);
            fos = new FileOutputStream(des);
            byte[] buffer = new byte[1024];
            int len = -1;
            while ((len = fis.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
            }
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

```

## try-with-resources复制文件写法
```java
package demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopyUtils {
    /**
     * try-with-resources复制文件写法
     *
     * @param src
     * @param des
     * @return void
     * @author Anna.
     * @date 2024/4/5 18:35
     */
    public static void fileCopyByTryWithResources(File src, File des) throws IOException {
        try (FileInputStream fis = new FileInputStream(src); FileOutputStream fos = new FileOutputStream(des);) {
            byte[] buffer = new byte[1024];
            int len = -1;
            while ((len = fis.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
            }
        }
    }
}

```

## 所以什么是try-with-resources
try-with-resources是Java 7引入的一个新特性，它提供了一种简化资源管理的机制。<br/>
该特性主要用于自动关闭实现了AutoCloseable或Closeable接口的资源，如文件流、数据库连接等，从而避免了资源泄漏的问题。<br/>
在try-with-resources语句中，你可以声明一个或多个资源，这些资源在try代码块执行完毕后会被自动关闭。<br/>
这是通过Java虚拟机的异常处理机制和字节码的异常表来实现的，通过自动调用资源的close()方法来确保资源被正确地关闭。<br/>
使用方式：资源被声明在try关键字后面的圆括号中，多个资源之间用分号分隔。当try块执行完毕后，无论是否发生异常，这些资源都会被自动关闭。

### 应用好处：

+ 自动关闭资源：try-with-resources语句会在退出作用域时自动关闭打开的资源，包括打开的文件、网络连接等。这可以避免因忘记关闭资源而造成的内存泄露等问题。 
+ 减少代码量：传统的try-catch-finally块需要额外的代码来确保资源被正确关闭。而在try-with-resources中，资源可以直接嵌入到try语句中，减少了代码量，提高了代码的可读性和可维护性。 
+ 更好的代码可读性：使用try-with-resources，开发者不再需要编写繁琐的finally块来关闭资源，代码更加清晰易懂。同时，在出现异常时，异常信息也更加明确，有助于减少调试时间。 
+ 处理多个资源：在try-with-resources中，你可以同时声明并初始化多个资源，使得处理多个资源关闭操作变得更加简洁和清晰。

### 与try{}的区别：

+ 异常处理与资源管理：基本的try{}块主要用于捕获和处理异常，而try-with-resources不仅可以捕获异常，还能自动管理资源，确保资源在使用后被正确关闭。 
+ 结构差异：try{}通常与catch和finally块结合使用，其中finally块用于执行清理操作，包括关闭资源。而try-with-resources将资源声明在try语句的括号中，并自动处理资源的关闭，无需显式编写finally块。 
+ 使用限制：<font color="red">try-with-resources要求资源必须实现AutoCloseable或Closeable接口</font>。这意味着不是所有的对象都可以使用try-with-resources进行自动资源管理。而基本的try{}块则没有这样的限制，可以用于任何需要异常处理的代码。 
+ 代码简洁性：由于try-with-resources能够自动处理资源关闭，使得代码更加简洁和易读。相比之下，使用传统的try-catch-finally结构处理资源关闭可能会使代码变得繁琐和难以维护。

![img.png](images/img.png)

## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)
