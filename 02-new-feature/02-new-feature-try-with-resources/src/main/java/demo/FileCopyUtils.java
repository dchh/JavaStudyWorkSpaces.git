package demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 显示try-with-resources区别
 *
 * @author Anna.
 * @date 2024/4/5 18:29
 */
public class FileCopyUtils {

    /**
     * try-cache-finally复制文件写法
     *
     * @param src
     * @param des
     * @return void
     * @author Anna.
     * @date 2024/4/5 18:34
     */
    public static void fileCopy(File src, File des) throws IOException {
        FileInputStream fis = null;
        FileOutputStream fos = null;

        try {
            fis = new FileInputStream(src);
            fos = new FileOutputStream(des);
            byte[] buffer = new byte[1024];
            int len = -1;
            while ((len = fis.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
            }
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * try-with-resources复制文件写法
     *
     * @param src
     * @param des
     * @return void
     * @author Anna.
     * @date 2024/4/5 18:35
     */
    public static void fileCopyByTryWithResources(File src, File des) throws IOException {
        try (FileInputStream fis = new FileInputStream(src); FileOutputStream fos = new FileOutputStream(des);) {
            byte[] buffer = new byte[1024];
            int len = -1;
            while ((len = fis.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
            }
        }
    }
}
