package demo04;

/**
 * 方法引用
 *
 * @author Anna.
 * @date 2024/4/1 11:07
 */
public class LambdaDemo04 {

    public static void main(String[] args) {
        // 使用Lambda表达式实现
        System.out.println("使用Lambda表达式实现");
        pase(s -> Integer.parseInt(s));

        // 使用方法引用实现
        System.out.println("使用方法引用实现");
        pase(Integer::parseInt);
    }

    public static void pase(Conversion c) {
        System.out.println(c.pase("123"));
    }
}
