package demo04;

/**
 * 定义一个接口将字符串数字转换成int类型
 *
 * @author Anna.
 * @date 2024/4/1 11:06
 */
public interface Conversion {
    int pase(String s);
}
