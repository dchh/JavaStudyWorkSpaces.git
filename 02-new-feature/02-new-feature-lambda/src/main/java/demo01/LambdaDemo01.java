package demo01;

import java.util.Arrays;

/**
 * Demo01:
 * 定义一只接口（Eatable）,里面定义一个抽象方法：void eat();表示吃这一个动作。
 * 然后以实现类，匿名内部类，Lambda表达式分别实现一下小明吃饭了这一动作。
 *
 * @author Anna.
 * @date 2024/3/31 22:36
 */
public class LambdaDemo01 {
    public static void main(String[] args) {
        // 通过实现类实现
        Eatable eat = new EatImpl();
        eat.eat();

        // 匿名内部类方式实现
        eat(new Eatable() {
            @Override
            public void eat() {
                System.out.println("通过匿名内部类方式实现-小明吃东西了");
            }
        });

        // Lambda表达式实现
        eat(() -> {
            System.out.println("通过Lambda表达式实现-小明吃东西了");
        });
    }

    /**
     * 定义一个调用Eatable接口eat()的方法
     *
     * @param eatable
     * @return void
     * @author Anna.
     * @date 2024/3/31 22:50
     */
    private static void eat(Eatable eatable) {
        eatable.eat();
    }
}
