package demo01;

/**
 * 定义吃接口
 *
 * @author Anna.
 * @date 2024/3/31 22:35
 */
public interface Eatable {
    /**
     * 吃
     *
     * @param
     * @author Anna.
     * @date 2024/3/31 22:35
     */
    void eat();
}
