package demo01;

/**
 * 吃实现类
 *
 * @author Anna.
 * @date 2024/3/31 22:37
 */
public class EatImpl implements Eatable{
    @Override
    public void eat() {
        System.out.println("通过实现类-小明吃东西了");
    }
}
