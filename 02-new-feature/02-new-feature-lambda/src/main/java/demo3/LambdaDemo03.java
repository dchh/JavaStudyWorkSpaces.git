package demo3;

/**
 * Demo03:
 * Lambda表达式方法引用
 *
 * @author Anna.
 * @date 2024/3/31 22:36
 */
public class LambdaDemo03 {
    public static void main(String[] args) {
        System.out.println("Lambda方法");
        sum(((x, y) -> x + y));

        System.out.println("静态方法引用");
        sum(Integer::sum);

        System.out.println("实例方法引用");
        Sumable sumable = new SumImpl();
        sum(sumable::sum);
        System.out.println("如果该接口有且仅有一个抽象方法，还可以进一步省略::内容");
        sum(sumable);

        System.out.println("引用类中的非静态方法");
        SumUtils sumUtils = new SumUtils();
        sum(sumUtils::sum);

        System.out.println("引用类的构造方法");
        init(SumEntity::new);
    }

    /**
     * 定义一个调用Sumable接口sum()的方法
     *
     * @param sumable
     * @return void
     * @author Anna.
     * @date 2024/3/31 23:11
     */
    private static void sum(Sumable sumable) {
        System.out.println(sumable.sum(20, 30));
    }

    /**
     * 定义一个调用EntityDo接口init()的方法
     *
     * @param entityDo
     * @return void
     * @author Anna.
     * @date 2024/3/31 23:11
     */
    private static void init(IEntityDo entityDo) {
        System.out.println(entityDo.init(20, 30));
    }
}
