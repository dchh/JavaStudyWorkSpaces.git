package demo3;

public interface Sumable {
    int sum(int x, int y);
}
