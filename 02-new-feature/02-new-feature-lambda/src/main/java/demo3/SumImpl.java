package demo3;

public class SumImpl implements Sumable {

    @Override
    public int sum(int x, int y) {
        return x + y;
    }
}
