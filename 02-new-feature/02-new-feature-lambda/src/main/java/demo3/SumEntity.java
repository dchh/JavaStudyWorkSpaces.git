package demo3;

public class SumEntity {
    private int x;
    private int y;

    public SumEntity(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "SumEntity{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
