package demo05;

/**
 * Lambda作用域
 *
 * @author Anna.
 * @date 2024/4/1 11:07
 */
public class LambdaDemo501 {

    public static void main(String[] args) {
        final String count = "123";
        int num = 0;
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "num:" + num);
            System.out.println(Thread.currentThread().getName() + "count:" + count);
        }, "Lambda表达式").start();
    }
}
