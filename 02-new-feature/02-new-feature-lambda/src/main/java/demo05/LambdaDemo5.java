package demo05;

/**
 * Lambda作用域
 *
 * @author Anna.
 * @date 2024/4/1 11:07
 */
public class LambdaDemo5 {

    public static void main(String[] args) {
        final String count = "123";
        int num = 0;
        new Thread(() -> {
//            num = 100;  // 编译报错
//            count = "232";    // 编译报错
        }, "Lambda表达式").start();
    }
}
