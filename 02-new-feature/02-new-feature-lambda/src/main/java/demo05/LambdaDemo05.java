package demo05;


import java.util.function.Supplier;

/**
 * Lambda作用域
 *
 * @author Anna.
 * @date 2024/4/1 11:07
 */
public class LambdaDemo05 {

    public static void main(String[] args) {
        LambdaDemo05 lambdaDemo05 = new LambdaDemo05();
        lambdaDemo05.scopeTest01();
    }

    public void scopeTest01() {
        System.out.println("方法体中使用this：" + this);
        pase(() -> {
            System.out.println("Lambda中使用this：" + this);
            return null;
        });
    }

    public static <T> T pase(Supplier<T> supplier) {
        return supplier.get();
    }
}
