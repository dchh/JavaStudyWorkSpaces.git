package demo02;

import demo01.Eatable;

/**
 * Demo02:
 * Lambda表达式省略模式
 *
 * @author Anna.
 * @date 2024/3/31 22:36
 */
public class LambdaDemo02 {
    public static void main(String[] args) {
        // 正常调用
        System.out.println("=========正常调用=========");
        eat(() -> {
            System.out.println("小明吃东西了");
        });
        sum((int x, int y) -> {
            return x + y;
        });
        sleep((String name) -> {
            System.out.println(name + "睡觉了");
        });

        // 省略模式
        System.out.println("=========省略模式=========");
        System.out.println("=========参数类型可以省略=========");
        sleep((name) -> {
            System.out.println(name + "睡觉了");
        });
        // 参数类型可以省略，但是多个参数的情况下，不能只省略一个
        System.out.println("=========参数类型可以省略，但是多个参数的情况下，不能只省略一个=========");
        sum((x, y) -> {
            return x + y;
        });

        // 如果参数有且仅有一个，那么小括号可以省略
        System.out.println("=========如果参数有且仅有一个，那么小括号可以省略=========");
        sleep(name -> {
            System.out.println(name + "睡觉了");
        });

        // 如果代码块的语句只有一条，大括号和分号可以省略
        System.out.println("=========如果代码块的语句只有一条，大括号和分号可以省略=========");
        sleep(name -> System.out.println(name + "睡觉了"));

        // 如果代码块的语句只有一条，且有返回值 return 也可以省略
        System.out.println("=========如果代码块的语句只有一条，且有返回值 return 也可以省略=========");
        sum((x, y) -> x + y);
    }

    /**
     * 定义一个调用Eatable接口eat()的方法
     *
     * @param eatable
     * @return void
     * @author Anna.
     * @date 2024/3/31 23:11
     */
    private static void eat(Eatable eatable) {
        eatable.eat();
    }

    /**
     * 定义一个调用Sumable接口sum()的方法
     *
     * @param sumable
     * @return void
     * @author Anna.
     * @date 2024/3/31 23:11
     */
    private static void sum(Sumable sumable) {
        System.out.println(sumable.sum(20, 30));
    }

    /**
     * 定义一个调用Sleepable接口sleep()的方法
     *
     * @param sleepable
     * @return void
     * @author Anna.
     * @date 2024/3/31 23:11
     */
    private static void sleep(Sleepable sleepable) {
        sleepable.sleep("小明");
    }
}
