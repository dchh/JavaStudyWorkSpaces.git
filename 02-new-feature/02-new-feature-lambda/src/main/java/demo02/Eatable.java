package demo02;

/**
 * 吃的能力
 *
 * @author Anna.
 * @date 2024/3/31 22:35
 */
public interface Eatable {
    void eat();
}
