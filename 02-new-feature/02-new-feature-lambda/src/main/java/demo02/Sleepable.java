package demo02;

/**
 * 睡觉的能力
 *
 * @author Anna.
 * @date 2024/3/31 22:35
 */
public interface Sleepable {
    void sleep(String name);
}
