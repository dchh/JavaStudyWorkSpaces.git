package demo02;

/**
 * 求和
 *
 * @author Anna.
 * @date 2024/3/31 23:06
 */
public interface Sumable {
    int sum(int x, int y);
}
