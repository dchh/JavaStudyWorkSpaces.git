# 接口新特性

## 接口组成更新概述
接口的组成
+ 常量 
```
  public static final String ZERO = "0";
```
+ 抽象方法 
```
  public abstract void dance();
```
+ 默认方法（JAVA8新增） 
```
  public default void dance(){}
```
+ 静态方法（JAVA8新增） 
```
  public static void dance(){}
```
+ 私有方法（JAVA9新增）
```
  private void dance(){}
  private static void dance(){}
```
> <font color="RED">注意事项：</font><br/>
> JAVA8之后在接口中新增了默认方法、静态方法、私有方法(JAVA9)。<br/>
> 接口中允许有方法体的只有以上四种。其中<font color="RED">public void</font> dance(){} / <font color="RED">protected void</font> dance(){} 是不允许的


## 接口中默认方法
<b>格式：</b>
```
public default 返回值类型 方法名(参数列表){} 
```
<b>示例：</b>
```
public default void eat(){}
```
> <font color="RED">注意事项：</font><br/>
> 默认方法不是抽象方法，所以不强制被重写，但可以被重写，重写的时候去掉default关键字<br/>
> public可以省略，default不能省略

<b>案例：</b>

    Eatable.java
```java
package demo1;

public interface Eatable {
    default void eat(){
        System.out.println("Eatable中eat方法被调用");
    }
}
```
    EatImpl.java
```java
package demo1;

public class EatImpl implements Eatable {
}
```
    InterfaceDemo1.java
```java
package demo1;

public class InterfaceDemo1 {
    public static void main(String[] args) {
        Eatable eatable = new EatImpl();
        eatable.eat();
    }
}
```
> 执行结果

![img.png](images/img.png)


## 接口中静态方法
<b>格式：</b>
```
public static 返回值类型 方法名(参数列表){} 
```
<b>示例：</b>
```
public static void eat(){}
```
> <font color="RED">注意事项：</font><br/>
> 静态方法只能通过接口名称调用，不能通过实现类名或对象名调用<br/>
> public可以省略，static不能省略

<b>案例：</b>

    Eatable.java
```java
package demo2;

public interface Eatable {
    static void eat(){
        System.out.println("Eatable中静态的eat方法被调用");
    }
}
```
    InterfaceDemo2.java
```java
package demo2;

public class InterfaceDemo2 {
    public static void main(String[] args) {
        Eatable.eat();
    }
}
```
> 执行结果

![img_1.png](images/img_1.png)



## 接口中私有方法
<b>格式1：</b>
```
private 返回值类型 方法名(参数列表){} 
```
<b>格式2：</b>
```
private static 返回值类型 方法名(参数列表){} 
```
<b>示例：</b>
```
private void eat(){}
private static void eat2(){}
```
> <font color="RED">注意事项：</font><br/>
> 默认方法可以调用私有的静态方法和非静态方法<br/>
> 静态方法只能调用私有的静态方法

<b>案例：</b>

    Eatable.java
```java
package demo3;

public interface Eatable {
    default void eat(){
        System.out.println("Eatable中的default方法被调用");
        eat1();
        eat2();
    }
    private void eat1(){System.out.println("Eatable中的private方法被调用");}
    private static void eat2(){System.out.println("Eatable中的静态private方法被调用");}
}
```
    EatableImpl.java
```java
package demo3;

public class EatableImpl implements Eatable{
}
```
    InterfaceDemo3.java
```java
package demo3;

public class InterfaceDemo3 {
    public static void main(String[] args) {
        Eatable eatable = new EatableImpl();
        eatable.eat();
    }
}
```
> 执行结果

![img_2.png](images/img_2.png)

## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)