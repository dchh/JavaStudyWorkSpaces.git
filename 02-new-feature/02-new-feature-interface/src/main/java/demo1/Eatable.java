package demo1;

/**
 * 接口
 *
 * @author Anna.
 * @date 2024/4/1 9:41
 */
public interface Eatable {
    default void eat(){
        System.out.println("Eatable中eat方法被调用");
    }
}
