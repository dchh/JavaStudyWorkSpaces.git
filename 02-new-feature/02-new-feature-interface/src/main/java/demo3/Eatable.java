package demo3;

public interface Eatable {
    default void eat(){
        System.out.println("Eatable中的default方法被调用");
        eat1();
        eat2();
    }

    private void eat1(){System.out.println("Eatable中的private方法被调用");}

    private static void eat2(){System.out.println("Eatable中的静态private方法被调用");}
}
