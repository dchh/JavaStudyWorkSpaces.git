package demo2;

/**
 * 接口
 *
 * @author Anna.
 * @date 2024/4/1 9:41
 */
public interface Eatable {
    static void eat(){
        System.out.println("Eatable中静态的eat方法被调用");
    }
}
