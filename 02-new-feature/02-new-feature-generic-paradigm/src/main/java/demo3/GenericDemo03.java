package main.java.demo3;

import java.util.ArrayList;
import java.util.List;

public class GenericDemo03 {
    public static void main(String[] args) {
        // 测试通配符上限
//        List<? extends Number> list1 = new ArrayList<Object>(); // 编译报错
        List<? extends Number> list2 = new ArrayList<Number>();
        List<? extends Number> list3 = new ArrayList<Integer>();

        // 测试通配符下限
        List<? super Number> list4 = new ArrayList<Object>();
        List<? super Number> list5 = new ArrayList<Number>();
//        List<? super Number> list6 = new ArrayList<Integer>();  // 编译报错
    }
}
