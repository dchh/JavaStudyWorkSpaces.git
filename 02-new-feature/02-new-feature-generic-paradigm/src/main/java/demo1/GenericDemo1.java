package main.java.demo1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * ArrayList 使用泛型与不使用泛型比较
 *
 * @author Anna.
 * @date 2024/4/1 21:34
 */
public class GenericDemo1 {
    public static void main(String[] args) {
        List<String> listStr = new ArrayList<String>();
        listStr.add("123");
//        listStr.add(123);  // 编译时会报错提示不允许设置数据类型与设置泛型类型不一致
        Iterator<String> stringIterator = listStr.iterator();
        while (stringIterator.hasNext()){
            String str = stringIterator.next(); // 避免强制类型转换问题 next()拿到的数据就是设置的泛型数据
            System.out.println(str);
        }

        List list = new ArrayList();
        list.add("123");
        list.add(123);

        Iterator iterator = list.iterator();
        while (iterator.hasNext()){
            Integer b = (Integer) iterator.next();  // 这里会报数据转换异常
            System.out.println(b);
        }
    }
}
