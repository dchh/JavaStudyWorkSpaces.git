package main.java.demo4;

import java.util.List;

public class Collections {
    // 把 src 的每个元素复制到 dest 中:
    public static <T> void copy(List<? super T> dest, List<? extends T> src) {
        for (int i = 0; i < src.size(); i++) {
            // 获取 src 集合中的元素，并赋值给变量 t，其数据类型为 T
            T t = src.get(i);
            // 将变量 t 添加进 dest 集合中
            dest.add(t);// 添加元素进入 dest 集合中
        }
    }

    // 把 dest 的每个元素复制到 src 中:
//    public static <T> void copy(List<? super T> dest, List<? extends T> src) {
//        for (int i = 0; i < src.size(); i++) {
//            // 获取 dest 集合中的元素，并赋值给变量 t，其数据类型为 T
//            T t = dest.get(i);
//            // 将变量 t 添加进 src 集合中
//            src.add(t);// 添加元素进入 src 集合中
//        }
//    }
}
