package main.java.demo2;

/**
 * 定义泛型接口类型实现
 *
 * @author Anna.
 * @date 2024/4/1 22:03
 */
public class ShowImpl<T> implements Show<T> {
    @Override
    public void show(T t) {
        System.out.println("泛型接口t = " + t);
    }
}
