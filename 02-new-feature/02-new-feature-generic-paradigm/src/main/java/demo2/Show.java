package main.java.demo2;

/**
 * 定义泛型接口
 *
 * @author Anna.
 * @date 2024/4/1 22:03
 */
public interface Show<T> {
    void show(T t);
}
