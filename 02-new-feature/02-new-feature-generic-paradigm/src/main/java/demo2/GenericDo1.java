package main.java.demo2;

public class GenericDo1 {

    /**
     * 定义泛型方法
     *
     * @param t
     * @return void
     * @author Anna.
     * @date 2024/4/1 22:11
     */
    public <T> void show(T t){
        System.out.println("泛型方法t = " + t);
    }
}
