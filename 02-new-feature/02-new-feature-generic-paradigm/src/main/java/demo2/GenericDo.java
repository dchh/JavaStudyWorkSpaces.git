package main.java.demo2;

/**
 * 定义泛型类
 *
 * @author Anna.
 * @date 2024/4/1 22:05
 */
public class GenericDo<T> {

    public void show(T t){
        System.out.println("泛型类t = " + t);
    }
}
