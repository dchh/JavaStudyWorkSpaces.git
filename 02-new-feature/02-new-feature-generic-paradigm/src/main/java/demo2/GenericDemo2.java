package main.java.demo2;

public class GenericDemo2 {
    public static void main(String[] args) {
        // 调用泛型类
        GenericDo<String> genericDo = new GenericDo<String>();
        genericDo.show("123");
//        genericDo.show(false); // 编译会报错
        GenericDo<Boolean> genericDo1 = new GenericDo<Boolean>();
        genericDo1.show(false);

        // 调用泛型接口
        Show<String> show1 = new ShowImpl<String>();
        show1.show("123");
//        show1.show(false); // 编译会报错
        Show<Boolean> show2 = new ShowImpl<Boolean>();
        show2.show(false);

        // 调用泛型方法
        GenericDo1 genericDo3 = new GenericDo1();
        genericDo3.show(false);
        genericDo3.show("123");
    }
}
