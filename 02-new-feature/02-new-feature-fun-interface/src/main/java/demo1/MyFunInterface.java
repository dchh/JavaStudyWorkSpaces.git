package demo1;

@FunctionalInterface
public interface MyFunInterface {
    public abstract void show();

    default void showInit(){}
}
