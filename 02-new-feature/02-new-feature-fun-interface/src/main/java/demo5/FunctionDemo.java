package demo5;

import java.util.function.Function;

/**
 * 使用Function函数式接口:
 *      1 将一个字符串数字，转换成int类型，输出在控制台
 *      2 将一个字符串数字，转换成int类型，然后使用第二个Lambda表达式完成，加20,最后输出在控制台
 * @author Anna.
 * @date 2024/4/3 14:59
 */
public class FunctionDemo {
    public static void main(String[] args) {
        // 将一个字符串数字，转换成int类型，输出在控制台
        print("213", Integer::parseInt);

        // 将一个字符串数字，转换成int类型，然后使用第二个Lambda表达式完成，加20,最后输出在控制台
        printAdd("100",Integer::parseInt,s -> s + 20);
    }

    public static void print(String str, Function<String,Integer> func){
        System.out.println(func.apply(str));
    }

    public static void printAdd(String str, Function<String,Integer> func1,Function<Integer,Integer> func2){
        System.out.println(func1.andThen(func2).apply(str));
    }
}
