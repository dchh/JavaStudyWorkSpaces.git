package demo4;

import java.util.function.Predicate;

/**
 * 使用函数式接口Predicate,完成以下校验
 * 1 判断一个字符串长度是否大于5
 * 2 判断一个字符串长度小于5且已S开头
 * 3 判断一个字符串小于等于5
 *
 * @author Anna.
 * @date 2024/4/3 14:10
 */
public class PredicateDemo {
    public static void main(String[] args) {
        // 判断一个字符串长度是否大于5
        test01("12321", s -> s != null && s.length() > 5);
        // 判断一个字符串长度小于5且已S开头
        test02(null, s -> s != null && s.length() > 5, s -> s.startsWith("S"));
        // 判断一个字符串小于等于5
        test03("S2321", s -> s != null && s.length() > 5);
    }

    public static void test01(String str, Predicate<String> predicate) {
        System.out.println(predicate.test(str));
    }

    public static void test02(String str, Predicate<String> pdc1, Predicate<String> pdc2) {
        System.out.println(pdc1.and(pdc2).test(str));
    }

    public static void test03(String str, Predicate<String> pdc1) {
        System.out.println(pdc1.negate().test(str));
    }
}
