package demo2;

import java.util.function.Supplier;

/**
 *  使用Supplier<T>定义一个泛型方法，等到一个同类型的返回数据
 *
 * @author Anna.
 * @date 2024/4/1 23:57
 */
public class SupplierDemo {
    public static void main(String[] args) {
        // 匿名内部类方法实现
        String str = null;
        str = get(new Supplier<String>() {
            @Override
            public String get() {
                return "Hello".toUpperCase();
            }
        });
        System.out.printf("匿名内部类方法实现:%s%n",str);

        // Lambda表达式实现
        str = get(() -> {return "Hello".toUpperCase();});
        System.out.printf("Lambda表达式实现:%s%n",str);

        // Lambda表达式简写实现
        str = get(() -> "Hello".toUpperCase());
        System.out.printf("Lambda表达式简写实现:%s%n",str);

        // 方法引用实现
        str = get("Hello"::toUpperCase);
        System.out.printf("方法引用实现:%s%n",str);
    }

    /**
     * 定义一个泛型方法，等到一个同类型的返回数据
     *
     * @param supplier
     * @return T
     * @author Anna.
     * @date 2024/4/2 0:00
     */
    public static <T> T get(Supplier<T> supplier){
        return supplier.get();
    }
}
