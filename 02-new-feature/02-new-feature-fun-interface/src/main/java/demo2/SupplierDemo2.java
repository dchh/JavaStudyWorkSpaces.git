package demo2;

import java.util.function.Supplier;

/**
 * 使用Supplier<T>定义一个泛型方法，获取数组中的最大值
 *
 * @author Anna.
 * @date 2024/4/1 23:57
 */
public class SupplierDemo2 {
    public static void main(String[] args) {
        // 定义一个数组
        int[] arr = {123, 12, 123, 233, 1231};
        Integer max = get(() -> {
            int rtn = 0;
            for (int item : arr) {
                if (item > rtn) {
                    rtn = item;
                }
            }
            return rtn;
        });

        System.out.println("max = " + max);

        // 抽取方法
        max = get(() -> getInteger(arr));

        System.out.println("max = " + max);
    }

    /**
     * 定义一个泛型方法，等到一个同类型的返回数据
     *
     * @param supplier
     * @return T
     * @author Anna.
     * @date 2024/4/2 0:00
     */
    public static <T> T get(Supplier<T> supplier) {
        return supplier.get();
    }

    /**
     * 提取成一个方法
     *
     * @param arr
     * @return java.lang.Integer
     * @author Anna.
     * @date 2024/4/2 9:22
     */
    private static Integer getInteger(int[] arr) {
        int rtn = 0;
        if (arr != null && arr.length > 0) {
            for (int item : arr) {
                if (item > rtn) {
                    rtn = item;
                }
            }
        }
        return rtn;
    }

}
