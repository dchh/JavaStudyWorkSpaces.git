package demo2;

import java.util.function.Supplier;

/**
 * 理解输出Supplier函数式接口
 * 以返回一个字符串“123”为例
 *
 * @author Anna.
 * @date 2024/4/3 12:37
 */
public class SupplierDemo3 {
    public static void main(String[] args) {
        // 通过实现Supplier接口调用get()方法实现
        Supplier<String> stringSupplier = new Supplier<String>() {
            @Override
            public String get() {
                return "123";
            }
        };
        System.out.printf("实现Supplier接口的Supplier：%s%n", stringSupplier);
        System.out.printf("通过实现Supplier接口调用get()方法实现：%s%n", stringSupplier.get());

        // 通过函数式接口传入Lambda表达式的方式实现
        String str = getStringBySupplier(() -> "123");
        System.out.printf("通过函数式接口传入Lambda表达式的方式实现：%s%n", str);
    }

    /**
     * 定义一个返回类型是String的Supplier接口函数调用方法
     *
     * @author Anna.
     * @date 2024/4/3 12:39
     */
    public static String getStringBySupplier(Supplier<String> supplier) {
        System.out.printf("定义方法中的Supplier：%s%n", supplier);
        return supplier.get();
    }
}
