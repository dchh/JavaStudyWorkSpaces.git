package demo3;

import java.util.function.Consumer;

/**
 * 定义一个Config对象，通过函数式接口，完成数据的初始化
 *
 * @author Anna.
 * @date 2024/4/3 12:08
 */
public class ConsumerConfig {

    private String ip;

    private Integer port;

    /**
     * 初始化对象
     *
     * @param consumer
     * @return void
     * @author Anna.
     * @date 2024/4/3 12:25
     */
    public void init(Consumer<ConsumerConfig> consumer) {
        consumer.accept(this);
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "ConsumerConfig{" +
                "ip='" + ip + '\'' +
                ", port=" + port +
                '}';
    }

    public static void main(String[] args) {
        ConsumerConfig config = new ConsumerConfig();
        System.out.println("未初始化之前：" + config);
        config.init((consumer) -> {
            consumer.setIp("127.0.0.1");
            consumer.setPort(8080);
        });
        System.out.println("初始化之后：" + config);
    }
}
