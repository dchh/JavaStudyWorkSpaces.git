package demo3;

import java.util.function.Consumer;

/**
 * String[] strArray = {"张三,数学,30","李四,语文,40","王五,体育,100"};
 * 字符串数组中包含多条信息，请按照格式，“姓名:XXX,科目：XXX,分数：XXX”的格式将信息打印出来
 * 要求：
 * 把打印姓名的动作作为第一个Consumer接口的Lambda示例
 * 把打印科目的动作作为第二个Consumer接口的Lambda示例
 * 把打印分数的动作作为第三个Consumer接口的Lambda示例
 * 将三个Consumer接口按照顺序组合到一起使用
 *
 * @author Anna.
 * @date 2024/4/3 12:08
 */
public class ConsumerDemo {

    public static void main(String[] args) {
        String[] strArray = {"张三,数学,30", "李四,语文,40", "王五,体育,100"};
        print(strArray,
                s -> System.out.print("姓名:" + s.split(",")[0] + ","),   // 第一个Consumer接口的Lambda
                s -> System.out.print("科目:" + s.split(",")[1] + ","),   // 第二个Consumer接口的Lambda
                s -> System.out.println("分数:" + s.split(",")[2]));     // 第三个Consumer接口的Lambda
    }

    public static void print(String[] arr, Consumer<String> com1, Consumer<String> com2, Consumer<String> com3) {
        for (String str : arr) {
            // 链式调用 等价于
            //  com1.accept(str);
            //  com2.accept(str);
            //  com3.accept(str);
            com1.andThen(com2).andThen(com3).accept(str);

        }
    }
}
