# Stream流
## 什么是Stream流
Stream流是Java 8中的一个新特性，它提供了一种处理集合和数组的方式。Stream流可以让我们以一种更加简洁、高效、可读性更强的方式来处理数据。<br/>
Stream流可以用于过滤、映射、排序、聚合等操作，它可以让我们避免使用循环和条件语句来处理数据，从而让代码更加简洁易懂。<br/>
Stream流的操作可以分为中间操作和终止操作两种类型，中间操作返回的是一个新的Stream流，终止操作返回的是一个非Stream类型的结果。<br/>
<font color="red">Stream流的处理是惰性求值的，只有在执行终止操作时才会开始处理数据。</font><br/>
流（Stream）是一种基于支持一次性处理数据的数据源的元素序列,流只能使用一次。<br/>
流的设计初衷是为了支持函数式编程，它的目的是将数据处理和数据存储分离开来，使得数据处理更加灵活和高效。因此，流的元素只是在流中传递的临时数据，它们并不是永久存储在内存中的数据。<br/>
当流的元素被消费后，它们就会被释放，不能再次使用.。如果需要对同一个数据集进行多次不同的操作，可以使用流的中间操作方法来构建多个流管道，每个流管道都可以对流进行不同的操作，并返回一个新的流。这样就可以对同一个数据集进行多次操作，而不需要重新获取数据集。

## 流的分类
### 顺序流
顺序流是一种单线程的流，它按照数据流的顺序依次处理每个元素，每个元素的处理都必须等待上一个元素的处理完成才能开始
### 并行流
并行流是一种多线程的流，它可以将数据分成多个部分并行处理，每个部分都可以在不同的线程中处理，从而提高处理效率。<br/>
使用顺序流可以保证数据处理的顺序和一致性，适用于处理数据量较小的情况。而使用并行流可以提高数据处理的速度，适用于处理数据量较大、处理时间较长的情况。<br/>
但是并行流也有一些缺点，比如线程之间的通信和同步会带来额外的开销，而且并行流可能会影响数据的顺序和一致性。因此<font color="red">在使用并行流时需要注意线程安全和数据一致性等问题</font>。
### 区别
顺序流和并行流的区别在于它们的处理方式不同，顺序流是单线程的，而并行流是多线程的。使用的方法也有一些区别，例如：<br/>
    
+ 获取顺序流：可以使用集合类的stream()方法、Arrays类的stream()方法、Stream类的of()方法、Stream类的iterate()方法、Stream类的generate()方法、Files类的lines()方法等来获取顺序流。<br/>
+ 获取并行流：可以使用集合类的parallelStream()方法、Stream类的of()方法的parallel()方法、Stream类的iterate()方法的parallel()方法、Stream类的generate()方法的parallel()方法等来获取并行流。<br/>

除此之外，顺序流和并行流的使用方法基本相同，例如可以使用map()、filter()、reduce()等方法对流进行操作。但需要注意的是，在使用并行流时需要考虑线程安全和数据一致性等问题。

## Stream流的使用
按照的处理过程可以分为
1. 生成流<br/>
    通过数据源（集合、数组等）生成流，例如：list.stream()
2. 中间操作<br/>
    一个流后面可以跟随零个或多个中间操作，其目的主要是打开流，做出某种程度的数据过滤/映射，然后返回一个新的流交给下一个操作使用.例如：filter()
3. 终结操作<br/>
    一个流只能有一个终结操作，当这个操作执行后，流就被使用完了，无法在继续操作。所以着必定是流的最后一个操作。例如：forEach()

### 生成流的方式
1. 通过集合获取流
```java
package demo1;

import java.util.*;
import java.util.stream.Stream;

/**
 * 生成流的方式：
 * 通过集合获取流：可以使用集合类中的stream()方法或parallelStream()方法来获取流
 *
 * @author Anna.
 * @date 2024/4/3 16:18
 */
public class StreamDemo01 {
    public static void main(String[] args) {
        System.out.println("=======List========");
        System.out.println("=======顺序流========");
        List<String> list = Arrays.asList("林青霞", "张曼玉", "王祖贤", "柳岩", "张敏", "张无忌");
        // 通过集合中stream()获取顺序流
        Stream<String> stream = list.stream();
        // 循环输出
        stream.forEach(System.out::println);

        System.out.println("=======并行流========");
        // 通过集合中stream()获取并行流
        Stream<String> parallelStream = list.parallelStream();
        parallelStream.forEach(s -> System.out.printf("%s 输出了：%s%n", Thread.currentThread().getName(), s));

        System.out.println("=======Set========");
        Set<String> set = new HashSet<String>(list);
        // 通过stream()获取顺序流
        Stream<String> streamSet = set.stream();
        // 循环输出
        streamSet.forEach(System.out::println);

        // 并行流演示（后续单独说）

        System.out.println("=======Map========");
        // Map中没有提供直接转换成Stream流的相应的方法，但是可以通过间接的方式获取
        Map<String, String> map = new HashMap<String, String>();
        map.put("name", "林青霞");
        map.put("age", "30");
        // 获取Map中keySet对应的Stream流
        Stream<String> stream1 = map.keySet().stream();
        stream1.forEach(System.out::println);

        // 获取Map中values对应的Stream流
        Stream<String> stream2 = map.values().stream();
        stream2.forEach(System.out::println);

        // 获取Map中entrySet对应的Stream流
        Stream<Map.Entry<String, String>> stream3 = map.entrySet().stream();
        stream3.forEach(s -> {
            System.out.printf("key=%s,value=%s%n", s.getKey(), s.getValue());
        });
    }
}
```
2. 通过Arrays数组工具类中stream()方法获取流
```java
package demo1;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * 生成流的方式：
 * 通过Arrays数组工具类中stream()方法获取流
 *  Arrays  没有提供并行流的原因是因为数组在内存中是连续存储的，不能像集合那样方便地分割成多个部分进行并行处理。  如果要对数组进行并行处理，需要将其转换为流或使用并发编程技术手动分割和处理数组的不同部分。因此，并行流更多地是针对集合类数据结构而设计的。
 * @author Anna.
 * @date 2024/4/3 16:18
 */
public class StreamDemo02 {
    public static void main(String[] args) {
        String[] arr = {"林青霞", "张曼玉"};
        Stream<String> stream = Arrays.stream(arr);
        // 循环输出
        stream.forEach(System.out::println);
    }
}
```
> <font color="red"><b>注意：</b></font><br/>
Arrays  没有提供并行流的原因是因为数组在内存中是连续存储的，不能像集合那样方便地分割成多个部分进行并行处理。  <br/>
> 如果要对数组进行并行处理，需要将其转换为流或使用并发编程技术手动分割和处理数组的不同部分。因此，并行流更多地是针对集合类数据结构而设计的。
3. 通过 Stream.of()方法获取流
```java
package demo1;

import java.util.stream.Stream;

/**
 * 生成流的方式：
 * 通过 Stream.of()方法获取流
 *
 * @author Anna.
 * @date 2024/4/3 16:18
 */
public class StreamDemo03 {
    public static void main(String[] args) {
        String[] arr = {"林青霞", "张曼玉"};
        Stream<String> stream = Stream.of(arr);
        // 循环输出
        stream.forEach(System.out::println);
    }
}
```
4. 通过Stream.iterate()方法获取流
```java
package demo1;

import java.util.stream.Stream;

/**
 * 生成流的方式：
 * 通过Stream.iterate()方法获取流：可以使用 Stream 类中的iterate()方法来获取流
 *
 * @author Anna.
 * @date 2024/4/3 16:18
 */
public class StreamDemo04 {
    public static void main(String[] args) {
        Stream<Integer> stream = Stream.iterate(1, (x) -> x + 100).limit(3);
        stream.forEach(System.out::println);
    }
}
```
> 执行结果

![img_1.png](images/img_1.png)

5. 通过Stream.generate()方法获取流
```java
package demo1;

import java.util.stream.Stream;

/**
 * 生成流的方式：
 * 通过Stream.generate()方法获取流：可以使用 Stream 类中的generate()方法来获取流
 *
 * @author Anna.
 * @date 2024/4/3 16:18
 */
public class StreamDemo05 {
    public static void main(String[] args) {
        Stream<String> stream = Stream.generate(() -> new String("a123")).limit(3);
        stream.forEach(System.out::println);
    }
}
```
> 执行结果

![img_2.png](images/img_2.png)

6. 通过Files.lines()方法获取流
> GenStream.txt
```
张三,30
李四,20
王五,40
```

```java
package demo1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * 生成流的方式：
 * 通过Files.lines()方法获取流：可以使用Files类中的lines()方法来获取流。
 *
 * @author Anna.
 * @date 2024/4/3 16:18
 */
public class StreamDemo06 {
    public static void main(String[] args) throws IOException {
        new StreamDemo06().genStreamByFile();
    }

    public void genStreamByFile() throws IOException {
        Stream<String> lines = Files.lines(Paths.get("D:/WordkSpaces/JavaWorkSpaces/IdeaWorkSpaces/StudyWorkSpaces/JavaStudyWorkSpaces/02-new-feature-stream/src/main/resources/GenStream.txt"));
        lines.forEach(System.out::println);
    }
}
```
> 执行结果

![img_3.png](images/img_3.png)

### 中间操作
常用中间操作如下：

|分类|方法名|作用|
|:---|:---|:---|
|<b>筛选与切片</b>|||
|-|filter(Predicate<? super T> predicate)|保留满足给定条件的元素|
|-|distinct()|返回由该流的不同元素（根据Object.equals(Object)）组成的流|
|-|limit(long maxSize)|返回一个新流，其元素是该流的前n个元素|
|-|skip(long n)|跳过前n个元素|
|<b>映射</b>|||
|-|map(Function<? super T,? extends R> mapper)|将每个元素应用到一个函数上，并返回结果的新流|
|-|flatMap(Function<? super T,? extends Stream<? extends R>> mapper)|将每个元素应用到一个函数上，该函数产生一个流，然后将所有产生的流连接成一个流|
|<b>排序</b>|||
|-|sorted()|返回由该流的元素组成的流，根据元素的自然顺序排序(即按照字母顺序)|
|-|sorted(Comparator<? super T> comparator)|返回由该流的元素组成的流，根据提供的Comparator进行排序|
|<b>有状态操作</b>|||
|-|distinct()|虽然不是典型的有状态操作，但在某些实现中，它可能需要跟踪遇到的元素以提供不同的结果|
|-|sorted()|这是一个有状态操作，因为它需要维护一个内部状态来跟踪排序顺序|

> <font color="red"><b>注意：</b></font><br/>
> 不是所有的中间操作都是有状态的。有状态的操作是那些需要访问多个输入元素才能产生输出元素的操作，比如sorted()，它需要在整个流上操作以产生排序后的结果。<br/>
> 此外，还有一些组合器操作，如peek(Consumer<? super T> action)，它主要用于调试目的，允许在流的处理过程中执行某些操作，但并不改变流的内容。


### 终结操作
常用终结操作如下：

|方法名|作用|
|:---|:---|
|forEach(Consumer<? super T> action)|对流中的每个元素执行给定的操作。例如，可以用于打印流中的每个元素或执行其他副作用。|
|collect(Collector<? super T,A,R> collector)|将流中的元素累积到一个汇总结果中，如List、Set或自定义类型。|
|reduce(T identity, BinaryOperator<T> accumulator)|将流中的元素使用提供的累积器函数组合起来，以产生单个输出值。|
|reduce(BinaryOperator<T> accumulator)|这是上一个reduce方法的简化版本，它假定流不为空，并且没有提供初始值。|
|anyMatch(Predicate<? super T> predicate)|检查流中是否至少有一个元素满足给定的条件。|
|allMatch(Predicate<? super T> predicate)|检查流中的所有元素是否都满足给定的条件。|
|noneMatch(Predicate<? super T> predicate)|检查流中是否没有任何元素满足给定的条件。|
|findAny()|返回流中的某个元素。此操作对于并行流可能返回流中的任何元素。|
|findFirst()|返回流中的第一个元素。对于有序流，这将是第一个元素；对于无序流，这将是遇到的某个元素。|
|count()|返回流中的元素数量。|
|max(Comparator<? super T> comparator)|返回流中的最大元素，根据提供的比较器进行排序。|
|min(Comparator<? super T> comparator)|返回流中的最小元素，根据提供的比较器进行排序。|
|toArray()|将流中的元素收集到一个新数组中。|

> <font color="red"><b>注意：</b></font><br/>
> 一旦执行了终结操作，流就会被消费掉，并且不能再次使用。如果你需要再次处理相同的数据，你需要重新创建流。同时，终结操作会触发流的中间操作链的执行，从而生成最终的结果。

### 案例
如图使用Stream对一个集合List,生成流后，首先筛选以张开头的姓名，然后筛选长度为3的姓名，最后输出结果

![img.png](images/img.png)

```java
package demo2;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * 使用Stream对一个集合List,生成流后，首先筛选以张开头的姓名，然后筛选长度为3的姓名，最后输出结果
 *   List<String> list = Arrays.asList("林青霞", "张曼玉", "王祖贤", "柳岩", "张敏", "张无忌");
 *
 * @author Anna.
 * @date 2024/4/3 20:40
 */
public class StreamDemo {
    public static void main(String[] args) {
        // 创建集合
        List<String> list = Arrays.asList("林青霞", "张曼玉", "王祖贤", "柳岩", "张敏", "张无忌");
        // 获取流
        Stream<String> stream = list.stream();
        // 执行操作，输出结果
        stream.filter(s -> s.startsWith("张"))   // 筛选以张开头的姓名
                .filter(s -> s.length() == 3)   // 筛选长度为3的姓名
                .forEach(System.out::println);  // 输出结果
        ;
    }
}
```
> 执行结果

![img_4.png](images/img_4.png)

## Stream的特点
1. stream不存储数据，而是按照特定的规则对数据进行计算，一般会输出结果；  
2. stream对于值传递的集合不改变数据源，如果是对象则 引用传递则会改变数据源
3. stream具有延迟执行特性，<font color="red">只有调用终端操作时，中间操作才会执行</font>。 
4. stream不可复用，对一个已经进行过终端操作的流再次调用，会抛出异常。

<b>案例</b>
```java
package demo3;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * 验证
 * 1 stream对于值传递的集合不改变数据源，如果是对象则 引用传递则会改变数据源
 * 2 只有调用终端操作时，中间操作才会执行
 * 3 stream不可复用
 *
 * @author Anna.
 * @date 2024/4/3 20:47
 */
public class StreamDemo {

    /**
     * 定义一个内部内
     */
    static class UserDo {
        private String name;
        private Integer age;

        public UserDo(String name, Integer age) {
            this.name = name;
            this.age = age;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "UserDo{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }

    public static void main(String[] args) {
        System.out.println("===============验证stream对于值传递的集合不改变数据源================");
        // 创建一个List集合
        List<Integer> list = Arrays.asList(1, 2, 3);
        // 获取流修改数据
        list.stream().map(s -> 20).forEach((s) -> System.out.printf("输出Straem处理后结果：%s%n", s));

        // 输出源数据
        list.forEach((s) -> {
            System.out.printf("输出源数据：%s%n", s);
        });

        System.out.println("===============验证stream对于对象引用传递则会改变数据源================");
        // 创建一个List集合
        List<UserDo> userDos = Arrays.asList(new UserDo("张三", 20), new UserDo("李四", 20));
        // 获取流
        Stream<UserDo> stream = userDos.stream();
        // 通过流将集合中所有所有UserDo属性age设置为30，并输出结果
        stream.map(s -> {    // 属性age设置为30
            s.setAge(30);
            return s;
        }).forEach((s) -> { // 输出结果
            System.out.printf("输出Straem处理后结果：%s%n", s);
        });

        // 输出源数据
        userDos.forEach((s) -> {
            System.out.printf("输出源数据：%s%n", s);
        });

        // 验证stream终结后是否可以复用
        // 报错 提示 Exception in thread "main" java.lang.IllegalStateException: stream has already been operated upon or closed
//        stream.map(s ->{    // 属性age设置为30
//            s.setAge(30);
//            return s;
//        }).forEach((s) ->{System.out.printf("输出Straem处理后结果：%s%n");});    // 输出结果

        System.out.println("===============验证只有调用终端操作时，中间操作才会执行================");
        // 验证只有调用终端操作时，中间操作才会执行
        // 重新获取流
        userDos.stream().peek(s -> System.out.println("===执行了===="));
    }
}
```
> 执行结果

![img_5.png](images/img_5.png)

## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)
