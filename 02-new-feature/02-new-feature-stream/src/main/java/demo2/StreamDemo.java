package demo2;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * 使用Stream对一个集合List,生成流后，首先筛选以张开头的姓名，然后筛选长度为3的姓名，最后输出结果
 *   List<String> list = Arrays.asList("林青霞", "张曼玉", "王祖贤", "柳岩", "张敏", "张无忌");
 *
 * @author Anna.
 * @date 2024/4/3 20:40
 */
public class StreamDemo {
    public static void main(String[] args) {
        // 创建集合
        List<String> list = Arrays.asList("林青霞", "张曼玉", "王祖贤", "柳岩", "张敏", "张无忌");
        // 获取流
        Stream<String> stream = list.stream();
        // 执行操作，输出结果
        stream.filter(s -> s.startsWith("张"))   // 筛选以张开头的姓名
                .filter(s -> s.length() == 3)   // 筛选长度为3的姓名
                .forEach(System.out::println);  // 输出结果
        ;
    }
}
