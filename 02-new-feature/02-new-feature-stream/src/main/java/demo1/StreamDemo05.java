package demo1;

import java.util.stream.Stream;

/**
 * 生成流的方式：
 * 通过Stream.generate()方法获取流：可以使用 Stream 类中的generate()方法来获取流
 *
 * @author Anna.
 * @date 2024/4/3 16:18
 */
public class StreamDemo05 {
    public static void main(String[] args) {
        Stream<String> stream = Stream.generate(() -> new String("a123")).limit(3);
        stream.forEach(System.out::println);
    }
}
