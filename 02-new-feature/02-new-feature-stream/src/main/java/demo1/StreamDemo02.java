package demo1;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * 生成流的方式：
 * 通过Arrays数组工具类中stream()方法获取流
 *  Arrays  没有提供并行流的原因是因为数组在内存中是连续存储的，不能像集合那样方便地分割成多个部分进行并行处理。  如果要对数组进行并行处理，需要将其转换为流或使用并发编程技术手动分割和处理数组的不同部分。因此，并行流更多地是针对集合类数据结构而设计的。
 * @author Anna.
 * @date 2024/4/3 16:18
 */
public class StreamDemo02 {
    public static void main(String[] args) {
        String[] arr = {"林青霞", "张曼玉"};
        Stream<String> stream = Arrays.stream(arr);
        // 循环输出
        stream.forEach(System.out::println);
    }
}
