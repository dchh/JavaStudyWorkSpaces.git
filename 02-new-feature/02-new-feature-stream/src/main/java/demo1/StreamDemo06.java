package demo1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * 生成流的方式：
 * 通过Files.lines()方法获取流：可以使用Files类中的lines()方法来获取流。
 *
 * @author Anna.
 * @date 2024/4/3 16:18
 */
public class StreamDemo06 {
    public static void main(String[] args) throws IOException {
        new StreamDemo06().genStreamByFile();
    }

    public void genStreamByFile() throws IOException {
        Stream<String> lines = Files.lines(Paths.get("D:/WordkSpaces/JavaWorkSpaces/IdeaWorkSpaces/StudyWorkSpaces/JavaStudyWorkSpaces/02-new-feature-stream/src/main/resources/GenStream.txt"));
        lines.forEach(System.out::println);
    }
}
