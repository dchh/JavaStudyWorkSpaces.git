package demo1;

import java.util.stream.Stream;

/**
 * 生成流的方式：
 * 通过 Stream.of()方法获取流
 *
 * @author Anna.
 * @date 2024/4/3 16:18
 */
public class StreamDemo03 {
    public static void main(String[] args) {
        String[] arr = {"林青霞", "张曼玉"};
        Stream<String> stream = Stream.of(arr);
        // 循环输出
        stream.forEach(System.out::println);
    }
}
