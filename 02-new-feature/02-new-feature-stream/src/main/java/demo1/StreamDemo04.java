package demo1;

import java.util.stream.Stream;

/**
 * 生成流的方式：
 * 通过Stream.iterate()方法获取流：可以使用 Stream 类中的iterate()方法来获取流
 *
 * @author Anna.
 * @date 2024/4/3 16:18
 */
public class StreamDemo04 {
    public static void main(String[] args) {
        Stream<Integer> stream = Stream.iterate(1, (x) -> x + 100).limit(3);
        stream.forEach(System.out::println);
    }
}
