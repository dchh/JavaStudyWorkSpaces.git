package demo1;

import java.util.*;
import java.util.stream.Stream;

/**
 * 生成流的方式：
 * 通过集合获取流：可以使用集合类中的stream()方法或parallelStream()方法来获取流
 *
 * @author Anna.
 * @date 2024/4/3 16:18
 */
public class StreamDemo01 {
    public static void main(String[] args) {
        System.out.println("=======List========");
        System.out.println("=======顺序流========");
        List<String> list = Arrays.asList("林青霞", "张曼玉", "王祖贤", "柳岩", "张敏", "张无忌");
        // 通过集合中stream()获取顺序流
        Stream<String> stream = list.stream();
        // 循环输出
        stream.forEach(System.out::println);

        System.out.println("=======并行流========");
        // 通过集合中stream()获取并行流
        Stream<String> parallelStream = list.parallelStream();
        parallelStream.forEach(s -> System.out.printf("%s 输出了：%s%n", Thread.currentThread().getName(), s));

        System.out.println("=======Set========");
        Set<String> set = new HashSet<String>(list);
        // 通过stream()获取顺序流
        Stream<String> streamSet = set.stream();
        // 循环输出
        streamSet.forEach(System.out::println);

        // 并行流演示（后续单独说）

        System.out.println("=======Map========");
        // Map中没有提供直接转换成Stream流的相应的方法，但是可以通过间接的方式获取
        Map<String, String> map = new HashMap<String, String>();
        map.put("name", "林青霞");
        map.put("age", "30");
        // 获取Map中keySet对应的Stream流
        Stream<String> stream1 = map.keySet().stream();
        stream1.forEach(System.out::println);

        // 获取Map中values对应的Stream流
        Stream<String> stream2 = map.values().stream();
        stream2.forEach(System.out::println);

        // 获取Map中entrySet对应的Stream流
        Stream<Map.Entry<String, String>> stream3 = map.entrySet().stream();
        stream3.forEach(s -> {
            System.out.printf("key=%s,value=%s%n", s.getKey(), s.getValue());
        });
    }
}
