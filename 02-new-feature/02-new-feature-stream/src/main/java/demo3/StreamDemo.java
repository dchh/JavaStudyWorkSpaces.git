package demo3;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * 验证
 * 1 stream对于值传递的集合不改变数据源，如果是对象则 引用传递则会改变数据源
 * 2 只有调用终端操作时，中间操作才会执行
 * 3 stream不可复用
 *
 * @author Anna.
 * @date 2024/4/3 20:47
 */
public class StreamDemo {

    /**
     * 定义一个内部内
     */
    static class UserDo {
        private String name;
        private Integer age;

        public UserDo(String name, Integer age) {
            this.name = name;
            this.age = age;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "UserDo{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }

    public static void main(String[] args) {
        System.out.println("===============验证stream对于值传递的集合不改变数据源================");
        // 创建一个List集合
        List<Integer> list = Arrays.asList(1, 2, 3);
        // 获取流修改数据
        list.stream().map(s -> 20).forEach((s) -> System.out.printf("输出Straem处理后结果：%s%n", s));

        // 输出源数据
        list.forEach((s) -> {
            System.out.printf("输出源数据：%s%n", s);
        });

        System.out.println("===============验证stream对于对象引用传递则会改变数据源================");
        // 创建一个List集合
        List<UserDo> userDos = Arrays.asList(new UserDo("张三", 20), new UserDo("李四", 20));
        // 获取流
        Stream<UserDo> stream = userDos.stream();
        // 通过流将集合中所有所有UserDo属性age设置为30，并输出结果
        stream.map(s -> {    // 属性age设置为30
            s.setAge(30);
            return s;
        }).forEach((s) -> { // 输出结果
            System.out.printf("输出Straem处理后结果：%s%n", s);
        });

        // 输出源数据
        userDos.forEach((s) -> {
            System.out.printf("输出源数据：%s%n", s);
        });

        // 验证stream终结后是否可以复用
        // 报错 提示 Exception in thread "main" java.lang.IllegalStateException: stream has already been operated upon or closed
//        stream.map(s ->{    // 属性age设置为30
//            s.setAge(30);
//            return s;
//        }).forEach((s) ->{System.out.printf("输出Straem处理后结果：%s%n");});    // 输出结果

        System.out.println("===============验证只有调用终端操作时，中间操作才会执行================");
        // 验证只有调用终端操作时，中间操作才会执行
        // 重新获取流
        userDos.stream().peek(s -> System.out.println("===执行了===="));
    }
}
