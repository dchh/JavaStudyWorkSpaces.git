package com.demo.demo2;

import javax.servlet.*;
import java.io.IOException;

/**
 * 获取配置信息DEMO
 *
 * @author Anna.
 * @date 2025/2/26 12:44
 */
public class GetInitInfoDemo implements Servlet {
    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        System.out.println("init获取初始化参数name = " + servletConfig.getInitParameter("name"));
    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {

    }

    @Override
    public String getServletInfo() {
        return "";
    }

    @Override
    public void destroy() {

    }
}
