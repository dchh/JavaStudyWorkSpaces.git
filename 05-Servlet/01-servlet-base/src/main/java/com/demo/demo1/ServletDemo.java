package com.demo.demo1;

import javax.servlet.*;
import java.io.IOException;

/**
 * Servlet
 *
 * @author Anna.
 * @date 2025/2/25 20:06
 */
public class ServletDemo implements Servlet {
    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        System.out.println("执行 init（）方法");
    }

    @Override
    public ServletConfig getServletConfig() {
        System.out.println("执行 getServletConfig（）方法");
        return null;
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        System.out.println("执行 service（）方法");
    }

    @Override
    public String getServletInfo() {
        System.out.println("执行 getServletInfo（）方法");
        return "";
    }

    @Override
    public void destroy() {
        System.out.println("执行 destroy（）方法");
    }
}
