package com.demo.demo3;

import javax.servlet.*;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.util.Enumeration;

/**
 * 使用Servlet的版本3.0以上通过注解完成配置
 *
 * @author Anna.
 * @date 2025/2/26 12:59
 */
@WebServlet(name = "demo4",urlPatterns = {"/demo3/123"},
        initParams = {
            @WebInitParam(name = "name",value = "张三"),
            @WebInitParam(name = "age",value = "22")
        })
public class ServletDmo2 implements Servlet {
    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        Enumeration<String> initParameterNames = servletConfig.getInitParameterNames();
        while (initParameterNames.hasMoreElements()) {
            String key = initParameterNames.nextElement();
            System.out.println(key +" = " + servletConfig.getInitParameter(key));
        }
    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        System.out.println("访问demo4-servlet完成");
    }

    @Override
    public String getServletInfo() {
        return "";
    }

    @Override
    public void destroy() {

    }
}
