# Servlet基础

## 什么是Servlet

Servlet是Server Applet的简称，称为小服务程序或服务连接器。是Java编写的服务器端程序，主要功能在于交互式地浏览和生成数据，生成动态Web内容。

## 快速入门

1. 创建Java EE项目

![img.png](images/img.png)

2. 定义一个类实现Servlet接口,实现接口的方法

demo1/ServletDemo.java
```java
package com.demo.demo1;

import javax.servlet.*;
import java.io.IOException;

public class ServletDemo implements Servlet {
    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        System.out.println("执行 init（）方法");
    }

    @Override
    public ServletConfig getServletConfig() {
        System.out.println("执行 getServletConfig（）方法");
        return null;
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        System.out.println("执行 service（）方法");
    }

    @Override
    public String getServletInfo() {
        System.out.println("执行 getServletInfo（）方法");
        return "";
    }

    @Override
    public void destroy() {
        System.out.println("执行 destroy（）方法");
    }
}

```

4. web.xml文件中配置Servlet
   web.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
    <!-- 配置Servlet -->
    <servlet>
        <servlet-name>servletDemo</servlet-name>
        <servlet-class>com.demo.demo1.ServletDemo</servlet-class>
    </servlet>
    <!-- 配置映射 -->
    <servlet-mapping>
        <servlet-name>servletDemo</servlet-name>
        <url-pattern>/demo1</url-pattern>
    </servlet-mapping>
</web-app>
```
5. 访问地址，查看执行结果

**访问地址：**

![img_1.png](images/img_1.png)

**执行结果：**

![img_2.png](images/img_2.png)

## 执行原理
1. 当服务器接受到客户端浏览器的请求后，会解析请求URL路径，获取访问的Servlet的资源路径
2. 查找web.xml文件，是否有对应的<url-pattern>标签体内容。
3. 如果有，则在找到对应的<servlet-class>全类名
4. tomcat会将字节码文件加载进内存，并且创建其对象
5. 调用其方法

![img_3.png](images/img_3.png)

## Servlet的生命周期

| 生命周期阶段 | 描述 | 触发条件 | 涉及的方法 |
| --- | --- | --- | --- |
| 加载（Loading） | Servlet类被加载到JVM中。这通常发生在Servlet容器启动时，或者第一次接收到对该Servlet的请求时。 | Servlet容器启动或首次请求 | 无需开发者干预 |
| 实例化（Instantiation） | Servlet容器创建Servlet的一个实例。每个Servlet在容器中只有一个实例，但会处理多个请求。 | Servlet容器根据配置或首次请求 | 无需开发者干预，但会调用构造器（通常不建议在构造器中编写业务逻辑） |
| 初始化（Initialization） | Servlet实例被初始化。在这个阶段，Servlet容器会调用Servlet的`init(ServletConfig config)`方法。这个方法只会在Servlet的生命周期中被调用一次。 | Servlet实例创建后立即 | `init(ServletConfig config)` |
| 请求处理（Request Handling） | Servlet容器接收客户端请求，并将其分派给相应的Servlet实例。Servlet实例通过调用`service(HttpServletRequest req, HttpServletResponse res)`方法（或其子类重写的`doGet`、`doPost`等方法）来处理请求。这个方法会被多次调用，每次有请求到达时都会执行。 | 客户端发送HTTP请求到Servlet容器 | `service(HttpServletRequest req, HttpServletResponse res)`（或其子类重写的方法） |
| 销毁（Destruction） | 当Servlet容器关闭或卸载Web应用时，它会调用Servlet的`destroy()`方法来销毁Servlet实例。这个方法在Servlet的生命周期中只会被调用一次，用于进行资源清理工作。 | Servlet容器关闭或Web应用卸载 | `destroy()` |

> 请注意，`service(HttpServletRequest req, HttpServletResponse res)`方法是Servlet的核心方法，用于处理所有类型的HTTP请求。而`doGet`、`doPost`等方法是`HttpServlet`类中提供的，用于处理特定类型的HTTP请求（如GET和POST）。开发者通常会在`HttpServlet`的子类中重写这些方法，而不是直接重写`service`方法。

> 此外，Servlet的初始化参数和上下文参数可以在`init(ServletConfig config)`方法中通过`ServletConfig`对象获取，而Web应用的上下文信息则可以通过`ServletContext`对象获取，这个对象在`init`方法的参数`ServletConfig`的`getServletContext()`方法中返回。

## Servlet定义的接口介绍
Servlet接口定义了Servlet的接口，这些接口构成了Servlet的生命周期，并允许Servlet处理客户端请求。

### 1. `init(ServletConfig config)`

**作用**：
`init`方法在Servlet实例化后被调用，用于初始化Servlet。此方法只会在Servlet的生命周期中被调用一次。它通常用于执行一次性设置，如加载配置数据、启动辅助线程等。

**参数**：
- `ServletConfig config`：包含Servlet的启动配置和初始化参数的对象。

**案例**：
```java
public void init(ServletConfig config) throws ServletException {
    // 读取初始化参数
    String paramValue = config.getInitParameter("paramName");
    // 执行其他初始化操作
}
```
在这个案例中，`init`方法从`ServletConfig`对象中读取了一个名为`paramName`的初始化参数。

### 2. `service(ServletRequest req, ServletResponse res)`

**作用**：
`service`方法是处理客户端请求的核心方法。每当有请求到达时，Servlet容器都会调用此方法。它根据请求的类型（如GET、POST）调用相应的`doGet`、`doPost`等方法。

**参数**：
- `ServletRequest req`：包含有关服务请求的信息的对象。
- `ServletResponse res`：用于向客户端返回信息的对象。

**案例**：
通常不直接重写`service`方法，而是在`HttpServlet`的子类中重写`doGet`、`doPost`等方法。但以下是一个简化的示例，说明`service`方法如何工作：
```java
public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
    if (req instanceof HttpServletRequest && res instanceof HttpServletResponse) {
        HttpServletRequest httpReq = (HttpServletRequest) req;
        HttpServletResponse httpRes = (HttpServletResponse) res;
        // 根据请求方法调用相应的方法
        if ("GET".equalsIgnoreCase(httpReq.getMethod())) {
            doGet(httpReq, httpRes);
        } else if ("POST".equalsIgnoreCase(httpReq.getMethod())) {
            doPost(httpReq, httpRes);
        }
        // ... 其他HTTP方法处理
    }
}
```
注意：在实际开发中，通常不需要重写`service`方法，因为`HttpServlet`类已经提供了对此方法的实现。

### 3. `destroy()`

**作用**：
`destroy`方法在Servlet实例被销毁之前调用，用于进行资源清理工作。此方法在Servlet的生命周期中只会被调用一次。它通常用于关闭数据库连接、释放内存资源等。

**案例**：
```java
public void destroy() {
    // 执行资源清理操作
    // 如关闭数据库连接、释放内存资源等
}
```
在这个案例中，`destroy`方法执行了一些资源清理操作，以确保Servlet在销毁时不会留下任何未释放的资源。

### 4. `getServletConfig()`

**作用**：
`getServletConfig`方法返回包含此Servlet的任何初始化参数和启动配置的`ServletConfig`对象。这个方法允许Servlet在运行时访问其配置信息。

**案例**：
```java
public ServletConfig getServletConfig() {
    // 通常不需要重写此方法，因为GenericServlet已经提供了实现
    // 直接调用super.getServletConfig()即可获取ServletConfig对象
    return super.getServletConfig();
}
```
在实际开发中，通常不需要重写此方法，因为`GenericServlet`类已经提供了对此方法的实现。Servlet可以通过调用`getServletConfig()`方法来获取其`ServletConfig`对象，并从中读取初始化参数。

### 5. `getServletInfo()`

**作用**：
`getServletInfo`方法返回一个包含有关Servlet的信息（如作者、版本和版权）的字符串。这个方法通常用于在管理工具中显示Servlet的信息。

## `servlet`标签及其子标签的作用

| 标签/子标签 | 作用 | 用法 | 注意事项 |
| --- | --- | --- | --- |
| `<servlet>` | 用于注册Servlet，告诉`web.xml`这是一个Servlet文件 | `<servlet>`标签中包含`<servlet-name>`和`<servlet-class>`两个主要的子元素 | - |
| `<servlet-name>` | 用于设置Servlet的注册名称，该名称可以为自定义的名称 | `<servlet-name>ServletDemo</servlet-name>` | 名称应与`<servlet-mapping>`中的`<servlet-name>`保持一致，不能使用`default`、`jsp`等保留字 |
| `<servlet-class>` | 用于指定Servlet对象的完整位置，包含Servlet对象的包名与类名 | `<servlet-class>com.servlet.ServletDemo</servlet-class>` | 必须提供Servlet类的全限定名（包名+类名） |
| `<load-on-startup>` | 可选标签，用于设置Servlet的创建时机，数字范围[0,10]，值越小创建的时机越早 | `<load-on-startup>1</load-on-startup>` | 若未指定，Servlet将在第一次请求时创建 |
| `<init-param>` | 可选标签，用于配置Servlet的初始化参数 | `<init-param><param-name>encoding</param-name><param-value>utf-8</param-value></init-param>` | 可配置多个`<init-param>`，用于传递初始化信息给Servlet |
| `<servlet-mapping>` | 用于映射一个已注册的Servlet的对外访问路径 | 与`<servlet>`标签成对出现，包含`<servlet-name>`和`<url-pattern>`两个子元素 | - |
| `<servlet-name>`（在`<servlet-mapping>`中） | 用于指定Servlet的注册名称，与`<servlet>`中的`<servlet-name>`相对应 | `<servlet-name>ServletDemo</servlet-name>` | 必须与`<servlet>`中的`<servlet-name>`一致 |
| `<url-pattern>` | 用于设置Servlet的对外访问路径 | `<url-pattern>/ServletDemo</url-pattern>` | 必须以`/`（斜杠）开头，可以使用`*`通配符，但格式有限制 |

**注意事项总结**：

1. `<servlet>`和`<servlet-mapping>`标签必须成对出现，且`<servlet-name>`在两个标签中必须保持一致。
2. `<servlet-class>`中必须提供Servlet类的全限定名。
3. `<url-pattern>`中设置的访问路径必须以`/`开头，且应遵循Servlet规范中的URL映射规则。
4. 在配置多个Servlet时，应确保每个Servlet的`<servlet-name>`是唯一的，以避免冲突。
5. 若使用了`<load-on-startup>`标签，应合理设置其值，以控制Servlet的创建时机，避免影响应用性能。
6. 在配置初始化参数时，应确保`<param-name>`的唯一性，并正确设置`<param-value>`。

## Servlet版本介绍

| Servlet 版本 | 所属Java EE/Jakarta EE规范 | 新增内容/特性                                                                            | 支持的JDK版本 |
|--------------|-----------------------------|--------------------------------------------------------------------------------------------|---------------|
| 2.5          | Java EE 5                   | 基本的Servlet API，用于开发Java Web应用程序，支持请求处理、会话管理等。                   | JDK 1.5及以上 |
| 3.0          | Java EE 6                   | 异步处理支持，注解支持（简化`web.xml`），可插性支持，简化了文件上传（`@MultipartConfig`）。 | JDK 1.6及以上 |
| 3.1          | Java EE 7                   | 改进了对WebSocket的支持等小幅增强。                                                       | 通常与Java EE 7规范保持一致 |
| 4.0          | Java EE 8                   | 支持HTTP/2，增强了安全性，改进了对异步处理的支持。                                       | JDK 1.8及以上 |
| Jakarta Servlet 4.0.x及后续 | Jakarta EE（原Java EE） | 继续提供对Java Web应用程序的支持，与Jakarta EE规范保持一致，可能包含更多的功能和安全性更新。 | 通常与Jakarta EE规范保持一致 |

**注意事项**：

- 表格中的“支持的JDK版本”是指该Servlet版本通常推荐或要求的最低JDK版本。实际项目中，可能由于其他依赖或要求而需要使用更高版本的JDK。
- 随着Jakarta EE（原Java EE）的发展，Servlet也更名为Jakarta Servlet。
- 在选择Servlet版本时，请务必考虑项目的具体需求、目标JDK版本以及与其他Java EE/Jakarta EE组件的兼容性。

![img_4.png](images/img_4.png)

## 案例-init方法中获取ServletConfig配置
GetInitInfoDemo.java
```java
package com.demo.demo2;

import javax.servlet.*;
import java.io.IOException;

public class GetInitInfoDemo implements Servlet {
    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        System.out.println("init获取初始化参数name = " + servletConfig.getInitParameter("name"));
    }
    // ...
}

```
配置文件：
web.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
    <!-- 配置Servlet -->
    <servlet>
        <servlet-name>getInitInfoDemo</servlet-name>
        <servlet-class>com.demo.demo2.GetInitInfoDemo</servlet-class>
        <init-param>
            <param-name>name</param-name>
            <param-value>张三</param-value>
        </init-param>
    </servlet>
    <!-- 配置映射 -->
    <servlet-mapping>
        <servlet-name>getInitInfoDemo</servlet-name>
        <url-pattern>/demo2</url-pattern>
    </servlet-mapping>
</web-app>
```

**执行结果：**

![img_5.png](images/img_5.png)

## Servlet注解及其属性的详细介绍

以下表格详细介绍了Servlet中HandlesTypes、HttpConstraint、HttpMethodConstraint、MultipartConfig、ServletSecurity、WebFilter、WebInitParam、WebListener、WebServlet注解及其属性的作用、可选值（及可选值的作用）、默认值及注意事项：

| 注解 | 作用与描述 | 属性 | 作用 | 可选值（及可选值的作用） | 默认值 | 注意事项 |
| --- | --- | --- | --- | --- | --- | --- |
| **HandlesTypes** | 声明ServletContainerInitializer可以处理哪些类型的类。 | `value` | 指定要处理的类类型，用于初始化程序识别和处理特定的Servlet类。 | 类类型的数组 | 无 | 必须在ServletContainerInitializer上使用。 |
| **HttpConstraint** | 表示适用于所有HTTP方法的安全约束。 | `rolesAllowed` | 指定允许访问的角色。 | 字符串数组，每个字符串表示一个角色名。 | 无 | 必须在ServletSecurity注解中使用。 |
| | | `transportGuarantee` | 指定数据保护要求，如SSL/TLS。 | `NONE`（不要求），`CONFIDENTIAL`（要求加密连接），`INTEGRAL`（要求加密连接和数据完整性） | `NONE` |  |
| | | `emptyRoleSemantic` | 指定当rolesAllowed为空时的默认授权语义。 | `PERMIT`（允许访问），`DENY`（拒绝访问） | `PERMIT` |  |
| **HttpMethodConstraint** | 表示特定HTTP方法的安全约束。 | `value` | 指定HTTP方法，如GET、POST等。 | `GET`、`POST`、`PUT`、`DELETE`、`HEAD`、`OPTIONS`、`TRACE` | 无 | 必须在ServletSecurity注解中使用。 |
| | | `rolesAllowed` | 指定允许访问的角色。 | 字符串数组，每个字符串表示一个角色名。 | 无 |  |
| | | `transportGuarantee` | 指定数据保护要求，如SSL/TLS。 | `NONE`（不要求），`CONFIDENTIAL`（要求加密连接），`INTEGRAL`（要求加密连接和数据完整性） | `NONE` |  |
| | | `emptyRoleSemantic` | 指定当rolesAllowed为空时的默认授权语义。 | `PERMIT`（允许访问），`DENY`（拒绝访问） | `PERMIT` |  |
| **MultipartConfig** | 标识Servlet支持multipart/form-data类型的请求，用于文件上传。 | `location` | 指定临时文件存储位置。 | 字符串，表示文件路径。 | 系统默认位置 |  |
| | | `maxFileSize` | 指定上传文件的最大大小。 | 长整型，表示文件大小。 | 无 | 超出此大小的文件将被拒绝。 |
| | | `maxRequestSize` | 指定请求的最大大小。 | 长整型，表示请求大小。 | 无 | 超出此大小的请求将被拒绝。 |
| | | `fileSizeThreshold` | 指定文件大小阈值，超过该值时将文件写入磁盘。 | 长整型，表示文件大小。 | 0 | 在内存中的文件超过此阈值时将写入磁盘。 |
| **ServletSecurity** | 指定Servlet容器对HTTP消息实施的安全约束。 | `value` | 指定适用于所有HTTP方法的安全约束。 | HttpConstraint注解 | 无 |  |
| | | `httpMethodConstraints` | 指定特定HTTP方法的安全约束。 | HttpMethodConstraint注解的数组 | 无 |  |
| **WebFilter** | 声明Servlet筛选器，用于过滤请求和响应。 | `filterName` | 指定过滤器的名称。 | 字符串 | 无 |  |
| | | `urlPatterns` | 指定过滤器应用的URL模式。 | 字符串数组 | 无 | 过滤器将应用于匹配这些模式的请求。 |
| | | `servletNames` | 指定过滤器应用的Servlet名称。 | 字符串数组 | 无 | 过滤器将应用于具有这些名称的Servlet。 |
| | | `dispatcherTypes` | 指定过滤器的转发模式。 | `ASYNC`（异步请求）、`ERROR`（错误请求）、`FORWARD`（转发请求）、`INCLUDE`（包含请求）、`REQUEST`（普通请求） | `REQUEST` |  |
| | | `initParams` | 指定过滤器的初始化参数。 | WebInitParam注解的数组 | 无 |  |
| | | `asyncSupported` | 指定是否支持异步处理。 | `true`（支持）、`false`（不支持） | `false` |  |
| | | `description` | 指定过滤器的描述。 | 字符串 | 无 |  |
| | | `displayName` | 指定过滤器的显示名称。 | 字符串 | 无 |  |
| | | `largeIcon` | 指定过滤器的大图标。 | 字符串（图标路径） | 无 |  |
| | | `smallIcon` | 指定过滤器的小图标。 | 字符串（图标路径） | 无 |  |
| **WebInitParam** | 指定Servlet或Filter的初始化参数。 | `name` | 指定参数名称。 | 字符串 | 无 |  |
| | | `value` | 指定参数值。 | 字符串 | 无 |  |
| | | `description` | 指定参数的描述。 | 字符串 | 无 |  |
| **WebListener** | 声明WebListener，用于监听Web事件。 | `value` | 指定监听器的描述。 | 字符串 | 无 |  |
| **WebServlet** | 声明Servlet，用于处理HTTP请求。 | `name` | 指定Servlet的名称。 | 字符串 | 空字符串 |  |
| | | `value` | 指定URL映射路径。 | 字符串 | 无 | 与`urlPatterns`互斥，通常使用`urlPatterns`。 |
| | | `urlPatterns` | 指定URL映射路径。 | 字符串数组 | 无 | Servlet将处理匹配这些模式的请求。 |
| | | `loadOnStartup` | 指定Servlet在服务器启动时是否加载，并指定加载顺序。 | 正整数 | 无 | 数值越小，加载优先级越高。 |
| | | `initParams` | 指定初始化参数。 | WebInitParam注解的数组 | 无 |  |
| | | `asyncSupported` | 指定是否支持异步处理。 | `true`（支持）、`false`（不支持） | `false` |  |
| | | `description` | 指定Servlet的描述。 | 字符串 | 无 |  |
| | | `displayName` | 指定Servlet的显示名称。 | 字符串 | 无 |  |
| | | `largeIcon` | 指定Servlet的大图标。 | 字符串（图标路径） | 无 |  |
| | | `smallIcon` | 指定Servlet的小图标。 | 字符串（图标路径） | 无 |  |

**注意事项**：

1. **HandlesTypes**：

   * 必须在ServletContainerInitializer类上使用。

2. **HttpConstraint** 和 **HttpMethodConstraint**：

   * 必须在ServletSecurity注解中使用。
   * `emptyRoleSemantic`属性在`rolesAllowed`为空时生效，用于决定默认授权语义。

3. **MultipartConfig**：

   * 用于文件上传功能，配置不当可能导致文件上传失败。

4. **ServletSecurity**：

   * 可以在Servlet类上使用，以强加安全约束。
   * `value`和`httpMethodConstraints`属性可以单独或组合使用，以提供灵活的安全策略。

5. **WebFilter**：

   * 必须在实现Filter接口的类上使用。
   * `urlPatterns`和`servletNames`属性至少应包含一个，用于指定过滤器的作用范围。

6. **WebInitParam**：

   * 必须在@WebServlet或@WebFilter注解中使用，用于传递初始化参数。

7. **WebListener**：

   * 必须在实现至少一个Servlet监听器接口的类上使用。

8. **WebServlet**：

   * `value`和`urlPatterns`属性不能共存，通常使用`urlPatterns`来指定请求路径。

## Servlet路径定义规则

| 路径定义规则 | 描述 | 示例 | 匹配情况 |
| --- | --- | --- | --- |
| /xxx | 路径匹配，精确匹配请求的URI | /test | 仅当请求URI为/test时匹配 |
| /xxx/xxx | 多层路径，目录结构，精确匹配具有层次结构的请求URI | /user/profile | 仅当请求URI为/user/profile时匹配 |
| *.do | 扩展名匹配，基于文件扩展名匹配请求的URI | login.do, register.do | 任何以.do结尾的请求URI都会匹配 |

> 注意：在配置路径是 通配符 * 优先级最低。当A中配置了`demo3/*`,B中配置了`demo/123`,在访问`demo/123`路径时，优先选择执行B中方法。如果存在指定相同的路径则启动会报错。

## 使用Servlet的版本3.0以上通过注解完成配置

demo3/ServletDmo.java

```java
package com.demo.demo3;

import javax.servlet.*;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.util.Enumeration;

@WebServlet(name = "demo3", urlPatterns = {"/demo3", "/demo3/*"},
        initParams = {
                @WebInitParam(name = "name", value = "张三"),
                @WebInitParam(name = "age", value = "22")
        })
public class ServletDmo implements Servlet {
   @Override
   public void init(ServletConfig servletConfig) throws ServletException {
      Enumeration<String> initParameterNames = servletConfig.getInitParameterNames();
      while (initParameterNames.hasMoreElements()) {
         String key = initParameterNames.nextElement();
         System.out.println(key +" = " + servletConfig.getInitParameter(key));
      }
   }

   @Override
   public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
      System.out.println("访问demo3-servlet完成");
   }
   // ..
}

```

**执行结果：**

![img_6.png](images/img_6.png)


## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)