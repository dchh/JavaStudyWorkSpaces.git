# Servlet会话技术

## 什么是会话技术
会话（Session）是指用户打开一个浏览器，访问一个网站，只要不关闭该浏览器，无论用户点击多少个超链接、访问多少资源，直到用户关闭浏览器，整个过程称为一次会话。

### 作用
在一次会话的范围内的多次请求间，共享数据。

> 1. cookie一般用于存出少量的不太敏感的数据
> 2. 在不登录的情况下，完成服务器对客户端的身份识别

### 实现方式
Servlet会话技术主要分为两类：客户端会话技术和服务器会话技术。
1. 客户端会话技术：Cookie
2. 服务器端会话技术：Session

## 什么是Cookie
Cookie是由W3C组织提出的一种机制，用于在客户端（通常是浏览器）存储少量信息。服务器通过HTTP响应头中的Set-Cookie字段将Cookie发送给客户端，客户端浏览器则将这些Cookie保存在本地。当浏览器再次访问相同的服务器时，它会在HTTP请求头中包含这些Cookie，从而使服务器能够识别出用户的身份和状态。

### 实现原理
基于响应头set-cookie和请求头cookie实现。

![img.png](images/img.png)

### 主要特点：
1. 存储在客户端浏览器中，因此访问速度快。
2. 有大小限制（通常为4KB），且数量也有限制。
3. 可以设置有效期，过期后自动删除。
4. 具有不可跨域名性，即不同域名之间的Cookie无法互相访问。

#### 各浏览器对Cookie数量限制

| 浏览器类型 | 单个域名下Cookie数量限制 | 浏览器允许的Cookie总数量限制 | 单个Cookie大小限制 |
| --- | --- | --- | --- |
| Internet Explorer (IE) 6及更早 | 20 | 无明确限制 | 4KB |
| Internet Explorer (IE) 7及更新 | 50 | 无明确限制 | 4KB |
| Firefox | 50 | 300（总数量） | 4KB |
| Opera | 30 | 无明确限制 | 4KB |
| Chrome | 无明确限制（受HTTP头大小限制） | 无明确限制 | 4KB |
| Safari | 无明确限制（受HTTP头大小限制） | 无明确限制 | 4KB |

> 注意：
> 1. 大部分浏览器对单个Cookie的大小也有限制，通常为4KB（4096字节）。这里的4KB包括Cookie的名称、值以及等号等字符。需要注意的是，多字节字符在计算时通常算作两个字节。如果Cookie的大小超过了这个限制，浏览器会忽略该Cookie，并且不会将其设置。
> 2. 当Cookie数量超过浏览器的限制时，不同的浏览器会采取不同的处理机制。例如，Firefox可能会随机清理过多的Cookie，而Opera则可能使用LRU（Least Recently Used，最近最少使用）机制来删除最近最少使用的Cookie。

### Servlet中Cookie常用API

| 方法名称 | 所属类 | 参数类型及描述 | 功能描述 |
| --- | --- | --- | --- |
| Cookie(String name, String value) | Cookie | name: Cookie的名称（String类型）<br>value: Cookie的值（String类型） | 构造一个具有指定名称和值的Cookie对象 |
| setMaxAge(int expiry) | Cookie | expiry: Cookie的最大年龄（有效期），单位为秒（int类型） | 设置Cookie的最大年龄（有效期）。正整数表示持久化，0表示删除，负数表示仅保存在浏览器缓存中 |
| setPath(String uri) | Cookie | uri: Cookie的路径属性（String类型） | 设置Cookie的路径属性，决定哪些路径下的资源可以访问到该Cookie |
| setDomain(String pattern) | Cookie | pattern: Cookie的域属性（String类型） | 设置Cookie的域属性，决定哪些域名下的资源可以访问到该Cookie |
| setSecure(boolean flag) | Cookie | flag: Cookie的安全属性（boolean类型） | 设置Cookie的安全属性。如果为true，则仅通过安全协议（如HTTPS）发送Cookie |
| setComment(String purpose) | Cookie | purpose: Cookie的注释属性（String类型） | 设置Cookie的注释属性，可能在浏览器呈现给用户时显示。并非所有浏览器都支持 |
| setVersion(int v) | Cookie | v: Cookie遵循的协议版本（int类型） | 设置Cookie遵循的协议版本。默认为Netscape的Cookie规范（版本0） |
| addCookie(Cookie cookie) | HttpServletResponse | cookie: 要添加的Cookie对象（Cookie类型） | 将一个Cookie对象添加到HTTP响应头中，以便浏览器保存 |
| getCookies() | HttpServletRequest | 无参数 | 从HTTP请求头中检索出所有的Cookie，并返回一个Cookie数组 |

下面是对表格中每个方法的详细介绍，包括其功能描述、作用以及注意事项：

#### 1. **Cookie(String name, String value)**

- **所属类**：`Cookie`
- **参数类型及描述**：
    - `name`：`String`类型，Cookie的名称。
    - `value`：`String`类型，Cookie的值。
- **功能描述**：
    - 构造一个具有指定名称和值的Cookie对象。
    - 名称和值都不能为`null`，且必须遵循特定的字符规范（如不能包含空格、分号、逗号等特殊字符）。
- **注意事项**：
    - Cookie的名称和值应该是经过URL编码的，以避免特殊字符导致的问题。
    - 名称和值的长度限制可能因浏览器而异。

#### 2. **setMaxAge(int expiry)**

- **所属类**：`Cookie`
- **参数类型及描述**：
    - `expiry`：`int`类型，Cookie的最大年龄（有效期），单位为秒。
- **功能描述**：
    - 设置Cookie的最大年龄（有效期）。
    - 正整数表示Cookie将在浏览器中持久化存储，直到过期时间到达。
    - `0`表示删除Cookie，即立即过期。
    - 负数表示Cookie仅保存在浏览器会话中，关闭浏览器后即删除。
- **注意事项**：
    - 设置过期时间时，应考虑到用户体验和数据安全性。
    - 不同浏览器对Cookie过期时间的处理可能略有不同。

#### 3. **setPath(String uri)**

- **所属类**：`Cookie`
- **参数类型及描述**：
    - `uri`：`String`类型，Cookie的路径属性。
- **功能描述**：
    - 设置Cookie的路径属性，决定哪些路径下的资源可以访问到该Cookie。
    - 路径是相对于服务器根目录的。
    - 根路径`"/"`表示服务器上所有路径都可以访问该Cookie。
- **注意事项**：
    - 路径设置应与应用的结构相匹配，以避免不必要的Cookie传输。
    - 多次设置路径，只有最后一次生效。

#### 4. **setDomain(String pattern)**

- **所属类**：`Cookie`
- **参数类型及描述**：
    - `pattern`：`String`类型，Cookie的域属性。
- **功能描述**：
    - 设置Cookie的域属性，决定哪些域名下的资源可以访问到该Cookie。
    - 通常用于在子域名之间共享Cookie。
    - 域名必须以点号开头，如`.example.com`。
- **注意事项**：
    - 不能设置跨域Cookie（即不能设置不属于当前域或其子域的域名）。
    - 某些浏览器对域名的设置有更严格的限制。

#### 5. **setSecure(boolean flag)**

- **所属类**：`Cookie`
- **参数类型及描述**：
    - `flag`：`boolean`类型，Cookie的安全属性。
- **功能描述**：
    - 设置Cookie的安全属性。
    - 如果为`true`，则仅通过安全协议（如HTTPS）发送Cookie。
    - 如果为`false`，则无论通过HTTP还是HTTPS都可以发送Cookie。
- **注意事项**：
    - 在处理敏感信息时，应设置`secure`属性为`true`。
    - 即使设置了`secure`属性，也应确保整个应用都使用HTTPS，以避免中间人攻击。

#### 6. **setComment(String purpose)**

- **所属类**：`Cookie`
- **参数类型及描述**：
    - `purpose`：`String`类型，Cookie的注释属性。
- **功能描述**：
    - 设置Cookie的注释属性，可能在浏览器呈现给用户时显示。
    - 用于说明Cookie的用途或来源。
- **注意事项**：
    - 并非所有浏览器都支持显示Cookie的注释属性。
    - 注释应简洁明了，避免使用敏感信息。

#### 7. **setVersion(int v)**

- **所属类**：`Cookie`
- **参数类型及描述**：
    - `v`：`int`类型，Cookie遵循的协议版本。
- **功能描述**：
    - 设置Cookie遵循的协议版本。
    - 默认为Netscape的Cookie规范（版本0）。
    - 版本1引入了`setPath`和`setDomain`等更细粒度的控制。
- **注意事项**：
    - 通常情况下，不需要显式设置Cookie的版本。
    - 不同版本的Cookie规范在浏览器中的支持程度可能不同。

#### 8. **addCookie(Cookie cookie)**

- **所属类**：`HttpServletResponse`
- **参数类型及描述**：
    - `cookie`：`Cookie`类型，要添加的Cookie对象。
- **功能描述**：
    - 将一个Cookie对象添加到HTTP响应头中，以便浏览器保存。
- **注意事项**：
    - 应在发送响应之前添加Cookie。
    - 可以添加多个Cookie，但每个Cookie的名称在响应中必须是唯一的。

#### 9. **getCookies()**

- **所属类**：`HttpServletRequest`
- **参数类型及描述**：
    - 无参数。
- **功能描述**：
    - 从HTTP请求头中检索出所有的Cookie，并返回一个Cookie数组。
- **注意事项**：
    - 返回的Cookie数组可能为空，表示请求中没有包含任何Cookie。
    - 应对返回的Cookie数组进行遍历和处理，以提取所需的信息。

### Cookie常用场景

| 场景类别 | 具体应用 | Cookie作用 | 注意事项 |
| --- | --- | --- | --- |
| **用户认证与会话管理** | 登录状态管理 | 存储用户的登录状态信息，如用户名、会话ID或加密令牌 | 确保Cookie的安全性，避免敏感信息泄露；使用HTTPS协议传输Cookie |
|  | 会话保持 | 在无状态应用中传递会话信息，保持请求间的状态连续性 | 设置合理的Cookie过期时间，避免会话长时间未关闭导致的安全风险 |
| **个性化设置与服务** | 用户偏好设置 | 存储用户的个性化偏好，如语言、主题、字体大小等 | 尊重用户隐私，提供选项让用户能够修改或删除Cookie中的偏好设置 |
|  | 内容推荐 | 识别用户身份和兴趣，提供个性化的内容推荐 | 避免过度追踪用户行为，确保推荐内容的合法性和合规性 |
| **跟踪与分析** | 用户行为分析 | 跟踪用户的浏览行为，分析用户行为模式 | 匿名化处理用户数据，确保用户隐私不被泄露；遵守相关法律法规 |
|  | 广告追踪 | 跟踪用户广告点击和浏览行为，实现精准广告投放 | 提供用户广告偏好设置选项，允许用户选择是否接受广告追踪 |
| **电商应用** | 购物车功能 | 存储用户的购物车信息，保持购物车状态 | 确保购物车信息的安全性，避免被篡改或泄露；提供用户查看和修改购物车内容的选项 |
| **安全防御** | CSRF防御 | 在Cookie中设置令牌，防止跨站请求伪造攻击 | 确保令牌的安全性和随机性，避免被预测或重复使用 |
| **其他场景** | 第三方服务集成 | 跟踪用户与第三方服务的交互行为或存储登录状态 | 明确告知用户第三方服务对Cookie的使用情况，并获得用户同意 |
|  | A/B测试 | 向不同用户展示不同版本或内容，跟踪行为以确定有效版本 | 确保测试内容的合法性和合规性，避免对用户造成不良影响 |

### 案例-基础测试
访问`/test1`，如果是第一次访问，则提示：您好，欢迎您首次访问。 如果不是第一次访问，则提示：欢迎回来，您上次访问时间为:显示时间字符串.

[CookieDemo1.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2Fservletcookiesession%2Fcom%2Fdemo%2FCookieDemo1.java)
```java
package com.demo.servletcookiesession.com.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/test1")
public class CookieDemo1 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置编码格式
        req.setCharacterEncoding("UTF-8");
        // 获取cookie
        String cookie = req.getHeader("Cookie");
        // 设置Cookie
        resp.setHeader("Set-Cookie", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));

        // 设置响应编码格式
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        // 判断cookie是否为空
        if (cookie == null || "".equals(cookie)) {
            // 输出Hello
            out.printf("您好，欢迎您首次访问");
        } else {
            // 输出 上次访问时间
            out.printf("欢迎回来，您上次访问时间为:%s\n", cookie);
        }
    }
}
```

**执行结果：**

第一次访问：

![img_1.png](images/img_1.png)

第二次访问：

![img_2.png](images/img_2.png)

### 测试Cookie持久化

访问`/test2`，如果是第一次访问，则提示：您好，欢迎您首次访问。 如果不是第一次访问，则提示：欢迎回来，您上次访问时间为:显示时间字符串.5秒后最后访问时间失效。

[CookieDemo2.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2Fservletcookiesession%2Fcom%2Fdemo%2FCookieDemo2.java)
```java
package com.demo.servletcookiesession.com.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/test2")
public class CookieDemo2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置编码格式
        req.setCharacterEncoding("UTF-8");
        // 创建Cookie
        Cookie cookie1 = new Cookie("lasttime", URLEncoder.encode(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())));
        // 设置5S自动失效
        cookie1.setMaxAge(5);
        // 设置Cookie
        resp.addCookie(cookie1);
        // 创建Cookie 使用URL编码处理cookie中不能直接存储中文数据
        Cookie cookie2 = new Cookie("name", URLEncoder.encode("张三"));
        // 设置Cookie
        resp.addCookie(cookie2);

        // 获取cookie
        Cookie[] cookies = req.getCookies();
        // 设置响应编码格式
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        // 判断cookie是否为空
        if (cookies == null || cookies.length == 0) {
            // 输出Hello
            out.printf("您好，欢迎您首次访问");
        } else {
            // 输出 上次访问时间
            if (cookies.length == 2) {
                out.printf("欢迎回来%s=%s，您上次访问时间为:%s\n", cookies[0].getName(), URLDecoder.decode(cookies[0].getValue()), URLDecoder.decode(cookies[1].getValue()));
            } else if (cookies.length == 1) {
                out.printf("欢迎回来%s=%s，您上次访问时间为:%s\n", cookies[0].getName(), URLDecoder.decode(cookies[0].getValue()), "");
            } else {
                out.printf("欢迎回来%s=%s，您上次访问时间为:%s\n", "", "", "");
            }
        }
    }
}
```

**执行结果：**

第一次访问：

![img_4.png](images/img_4.png)

5秒内地二次访问：

![img_5.png](images/img_5.png)

5秒后地二次访问：

![img_6.png](images/img_6.png)

>  注意：根据 HTTP 协议规范，Cookie 的值中只能包含特定的字符集。通常，这些字符包括字母（a-z, A-Z）、数字（0-9）、以及一些特定的符号（比如 -、_、.、!、~、*、'、(、) 等）。<br/>
> 空格，中文字符等不在此列中，因此就会报如下错误。<br/>
> 在tomcat 8 之前 cookie中不能直接存储中文数据。在tomcat 8 之后，cookie支持中文数据。特殊字符还是不支持，因此建议使用URL编码存储，URL解码解析<br/>
> 解决方法如下：<br/>
> 1. URL 编码：如果 Cookie 值需要包含空格或其他特殊字符，你可以考虑对这些值进行 URL 编码。在 Java 中，可以使用 URLEncoder.encode(String s, String enc) 方法来进行编码。注意，接收方（通常是服务器）需要相应地解码这些值。 
> 2. 使用 Base64 编码：另一种处理包含特殊字符的数据的方法是使用 Base64 编码。这种方法将二进制数据转换为只包含 A-Z, a-z, 0-9, +, / 和 = 的字符串。Java 提供了 java.util.Base64 类来处理这种编码。


![img_3.png](images/img_3.png)

### 删除cookie
访问`/test2`，如果是第一次访问，则提示：您好，欢迎您首次访问。 如果不是第一次访问，则提示：欢迎回来，您上次访问时间为:显示时间字符串.
访问`/test3`删除所有cookie，然后在访问，`/test2`

[CookieDemo3.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2Fservletcookiesession%2Fcom%2Fdemo%2FCookieDemo3.java)
```java
package com.demo.servletcookiesession.com.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/test3")
public class CookieDemo3 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置编码格式
        req.setCharacterEncoding("UTF-8");
        // 获取cookie
        Cookie[] cookies = req.getCookies();
        // 删除所有Cookie
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                cookie.setMaxAge(0);
                resp.addCookie(cookie);
            }
        }
        // 设置响应编码格式
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        // 删除所有Cookie
        out.println("删除所有Cookie");
    }
}
```

**执行结果：**

![img_7.png](images/img_7.png)

>  持久化存储：`setMaxAge(int seconds)`
> 1. 正数：将Cookie数据写到硬盘的文件中。持久化存储。并指定cookie存活时间，时间到后，cookie文件自动失效
> 2. 负数：默认值,表示仅保存在浏览器缓存中
> 3. 零：删除cookie信息

### 同一个Tomcat对个应用下Cookie共享问题
将应用部署在同一个服务器不同Tomcat中，同时访问`/test4`查看cookie共享情况。

[CookieDemo4.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2Fservletcookiesession%2Fcom%2Fdemo%2FCookieDemo4.java)
```java
package com.demo.servletcookiesession.com.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/test4")
public class CookieDemo4 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置编码格式
        req.setCharacterEncoding("UTF-8");
        // 设置Cookie
        resp.addCookie(new Cookie("lasttime", URLEncoder.encode(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()))));
        // 设置Cookie 使用URL编码处理cookie中不能直接存储中文数据
        resp.addCookie(new Cookie("name", URLEncoder.encode("张三")));

        // 获取cookie
        Cookie[] cookies = req.getCookies();
        // 设置响应编码格式
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        // 输出 上次访问时间
        if (cookies != null && cookies.length == 2) {
            out.printf("欢迎回来%s=%s，您上次访问时间为:%s\n", cookies[0].getName(), URLDecoder.decode(cookies[0].getValue()), URLDecoder.decode(cookies[1].getValue()));
        } else if (cookies != null && cookies.length == 1) {
            out.printf("欢迎回来%s=%s，您上次访问时间为:%s\n", cookies[0].getName(), URLDecoder.decode(cookies[0].getValue()), "");
        } else {
            out.printf("您好，欢迎您首次访问");
        }
    }
}
```

访问demo:

![img_8.png](images/img_8.png)

访问demo1：

![img_9.png](images/img_9.png)

> 结论： 根据结果可以看出，同一个服务器不同Tomcat，不同应用中，默认情况下cookie不能共享。

### 同一个Tomcat对个应用下Cookie共享问题，设置cookie的路径属性
将应用部署在同一个服务器不同Tomcat中，同时访问`/test5`查看cookie共享情况。

访问`http://192.168.245.132:8080/demo/test5`：

![img_10.png](images/img_10.png)

访问`http://192.168.245.132:8080/demo1/test5`：

![img_11.png](images/img_11.png)

> 同一服务器，相同tomcat，不同应用Cookie设置了获取范围后可以共享Cookie。

访问`http://192.168.245.132:8080/demo/test5`：

![img_10.png](images/img_10.png)

访问`http://192.168.245.132:9090/demo1/test5`：

![img_12.png](images/img_12.png)

> 同一服务器，不同tomcat，不同应用Cookie设置了获取范围后可以共享Cookie。

访问`http://192.168.245.132:8080/demo/test5`：

![img_13.png](images/img_13.png)

访问`http://192.168.245.132:9090/demo/test5`：

![img_14.png](images/img_14.png)

> 同一服务器，不同tomcat，相同应用Cookie不用设置获取范围后也可以共享Cookie。

访问`http://192.168.245.132:8080/demo/test5`：

![img_15.png](images/img_15.png)

访问`http://192.168.245.132:8080/demo/test5`：

![img_16.png](images/img_16.png)

> 不同服务器，相同应用Cookie设置获取范围后，也无法共享Cookie。

### 不同服务器下Cookie共享问题，设置cookie的域属性
设置cookie的域属性，将应用部署在不服务器不同Tomcat中，同时访问`/test6`查看cookie共享情况。

修改本地hosts文件,路径：`C:\Windows\System32\drivers\etc`
hosts
```
127.0.0.1 localhost
192.168.245.132 demo.test.com
192.168.245.128 demo1.test.com
```

[CookieDemo6.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2Fservletcookiesession%2Fcom%2Fdemo%2FCookieDemo6.java)
```java
package com.demo.servletcookiesession.com.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/test6")
public class CookieDemo6 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置编码格式
        req.setCharacterEncoding("UTF-8");
        // 设置Cookie
        resp.addCookie(new Cookie("lasttime", URLEncoder.encode(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()))));
        // 使用URL编码处理cookie中不能直接存储中文数据
        Cookie cookie = new Cookie("name", URLEncoder.encode("张三"));
        // 设置Cookie的域属性
        cookie.setDomain("test.com");
        // 设置Cookie
        resp.addCookie(cookie);

        // 获取cookie
        Cookie[] cookies = req.getCookies();
        // 设置响应编码格式
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String nameStr = "您好", timeStr = "欢迎您首次访问";
        if (cookies != null) {
            for (Cookie c : cookies) {
                if ("name".equals(c.getName())) {
                    nameStr = String.format("欢迎回来%s=%s", c.getName(), URLDecoder.decode(c.getValue(), "UTF-8"));
                } else if ("lasttime".equals(c.getName())) {
                    timeStr = String.format("您上次访问时间为:%s", URLDecoder.decode(c.getValue(), "UTF-8"));
                }
            }
        }
        // 输出 上次访问时间
        out.printf("%s，%s", nameStr, timeStr);
    }
}
```

访问`http://demo.test.com:8080/demo/test6`：

![img_17.png](images/img_17.png)

访问`http://demo1.test.com:8080/demo/test6`：

![img_18.png](images/img_18.png)

> 不同服务器，设置域属性后，相同应用可以共享Cookie。

访问`http://demo1.test.com:8080/demo1/test6`：

![img_19.png](images/img_19.png)

> 不同服务器，设置域属性后，不要同应用，不可以共享Cookie。

### 不同服务器下Cookie共享问题，设置cookie的域属性及路径属性
设置cookie的域属性，将应用部署在不服务器不同Tomcat中，同时访问`/test7`查看cookie共享情况。

[CookieDemo7.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2Fservletcookiesession%2Fcom%2Fdemo%2FCookieDemo7.java)
```java
package com.demo.servletcookiesession.com.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

@WebServlet("/test7")
public class CookieDemo7 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置编码格式
        req.setCharacterEncoding("UTF-8");
        // 设置Cookie
        resp.addCookie(new Cookie("lasttime", URLEncoder.encode(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()))));
        // 使用URL编码处理cookie中不能直接存储中文数据
        Cookie cookie = new Cookie("name", URLEncoder.encode("张三"));
        // 设置Cookie的域属性
        cookie.setDomain("test.com");
        // 服务器上所有路径都可以访问该Cookie
        cookie.setPath("/");
        // 设置Cookie
        resp.addCookie(cookie);

        // 获取cookie
        Cookie[] cookies = req.getCookies();
        // 设置响应编码格式
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String nameStr = "您好", timeStr = "欢迎您首次访问";
        if (cookies != null) {
            for (Cookie c : cookies) {
                if ("name".equals(c.getName())) {
                    nameStr = String.format("欢迎回来%s=%s", c.getName(), URLDecoder.decode(c.getValue(), "UTF-8"));
                } else if ("lasttime".equals(c.getName())) {
                    timeStr = String.format("您上次访问时间为:%s", URLDecoder.decode(c.getValue(), "UTF-8"));
                }
            }
        }
        // 输出 上次访问时间
        out.printf("%s，%s", nameStr, timeStr);
    }
}
```

访问`http://demo.test.com:8080/demo/test7`：

![img_20.png](images/img_20.png)

访问`http://demo1.test.com:8080/demo1/test7`：

![img_21.png](images/img_21.png)

> 设置cookie的域属性及路径属性后，同一顶级域名下的子域名都可以相互共享Cookie。

## 什么是Session

Session是一个服务端存储的对象，主要用来存储所有访问过该服务端的客户端的用户信息（也可以存储其他信息），从而实现保持用户会话状态的功能。

### 作用

主要用于在服务器端存储和管理客户端会话信息，从而保持跨多个请求和响应的用户状态。

### 实现原理

1. 创建Session：当浏览器第一次发送请求到服务器时，服务器会生成一个哈希表（或类似结构）和一个唯一的Session ID来标识这个哈希表。然后，服务器会通过响应头Set-Cookie将这个Session ID返回给浏览器，浏览器将这个Session ID存储在一个名为JESSIONID的Cookie中。 
2. 维持会话：此后，每当浏览器发送请求到服务器时，都会携带这个包含Session ID的Cookie。服务器通过提取请求中的Session ID，可以找到对应的哈希表，从而获取到该用户的会话信息。 
3. 销毁Session：Session可以在多种情况下被销毁，如用户关闭浏览器、会话过期（服务器设置的超时时间到了）、手动调用Session的invalidate()方法等。

![img_22.png](images/img_22.png)

### 主要特点

1. **服务器端存储**

   * Session是服务器端存储的一个对象，用于存储访问过该服务端的客户端的用户信息（也可以存储其他信息），从而实现保持用户会话状态。这意味着用户数据是保存在服务器上的，相对于保存在客户端的Cookie，这种方式提供了更高的安全性。 

3. **依赖Cookie实现会话跟踪**

   * 虽然Session数据存储在服务器上，但Session的实现是依赖于Cookie的。当客户端首次访问服务器并调用getSession()方法时，服务器会创建一个Session对象，并生成一个唯一的SessionId（通常是JSESSIONID）。这个SessionId通过Set-Cookie的形式发送给客户端，并保存在客户端的Cookie中。
   * 当客户端再次访问服务器时，会在请求头中携带这个包含SessionId的Cookie。服务器通过解析这个Cookie中的SessionId，可以找到对应的Session对象，从而实现会话跟踪。

3. **会话唯一性**

   * 浏览器和服务器之间的一次对话只有一个Session对象。这意味着，同一个用户在同一个浏览器会话中访问服务器时，会共享同一个Session对象。如果换一个浏览器或者关闭当前浏览器并重新打开，那么将会创建一个新的Session对象。

4. **内存存储与超时管理**

   * Session对象通常存储在服务器的内存中。这增加了服务器的存储压力，特别是在高流量的网站上。因此，需要合理控制Session的数量和大小。
   * 为了防止内存溢出，服务器会设置Session的超时时间。在超时时间内没有活跃的Session会被从内存中删除。Tomcat服务器中Session的默认超时时间为30分钟，但这个时间可以通过配置进行修改。

5. **安全性与隐私保护**

   * 由于Session中存储了用户的信息，因此需要采取措施防止用户信息被非法获取和使用。例如，可以对Session中的数据进行加密处理，或者使用安全的Cookie来存储Session的id值。
   * 在用户注销或退出系统时，应该及时销毁Session，以避免用户的敏感信息被非法访问。同时，对于长时间无操作的Session，也应该自动销毁以释放服务器资源。

### Session的生命周期

Session的生命周期从用户第一次访问服务器并创建Session开始，到用户关闭浏览器、会话过期或手动销毁Session结束。
在这个过程中，用户可以发起多次请求，每次请求都会携带相同的Session ID，从而保持会话状态。

### Servlet中Session常用API

| 方法名称 | 所属类 | 参数类型及描述 | 功能描述 |
| --- | --- | --- | --- |
| getSession | HttpServletRequest | 无或boolean create | 获取当前请求的Session对象。如果Session不存在且create为true，则创建一个新的Session。 |
| setAttribute | HttpSession | String name, Object value | 将value对象以name名称绑定到会话中。 |
| getAttribute | HttpSession | String name | 取得name的属性值，如果属性不存在则返回null。 |
| removeAttribute | HttpSession | String name | 从会话中删除name属性，如果不存在则不执行，也不会抛出错误。 |
| getAttributeNames | HttpSession | 无 | 返回和会话有关的枚举值。 |
| invalidate | HttpSession | 无 | 使会话失效，同时删除属性对象。 |
| isNew | HttpSession | 无 | 用于检测当前客户是否为新的会话。 |
| getCreationTime | HttpSession | 无 | 返回会话创建时间。 |
| getLastAccessedTime | HttpSession | 无 | 返回在会话时间内web容器接收到客户最后发出的请求的时间。 |
| getMaxInactiveInterval | HttpSession | 无 | 返回在会话期间内客户请求的最长时间（秒）。 |
| setMaxInactiveInterval | HttpSession | int seconds | 设置在会话期间内客户请求的最长时间（秒）。 |
| getServletContext | HttpSession | 无 | 返回当前会话的上下文环境，ServletContext对象可以使Servlet与web容器进行通信。 |
| getId | HttpSession | 无 | 返回会话期间的识别号，即Session ID。 |

#### 1. getSession

* **所属类**：`HttpServletRequest`
* **参数类型及描述**：无参数或`boolean create`
  + 无参数时，如果请求中不包含有效的会话ID，则返回`null`。
  + `create`为`true`时，如果请求中不包含有效的会话ID，则创建一个新的会话。
* **功能描述**：获取当前请求的Session对象。
* **注意事项**：
  + 在使用无参数的`getSession`方法时，如果请求中没有有效的会话ID，并且开发者期望获取一个会话对象，那么应该使用带有`create`参数的版本，并将`create`设置为`true`。
  + 创建过多的会话可能会导致服务器内存消耗过大，因此应该合理控制会话的数量和生命周期。

#### 2. setAttribute

* **所属类**：`HttpSession`
* **参数类型及描述**：`String name`（属性名），`Object value`（属性值）
* **功能描述**：将指定的对象绑定到会话中，使用指定的名称。
* **注意事项**：
  + 确保属性的名称是唯一的，以避免覆盖之前设置的同名属性。
  + 存储在会话中的对象应该是可序列化的，因为会话对象可能会被持久化到磁盘上。

#### 3. getAttribute

* **所属类**：`HttpSession`
* **参数类型及描述**：`String name`（属性名）
* **功能描述**：返回与指定名称绑定的会话对象的值。
* **注意事项**：
  + 如果指定的属性名不存在，则返回`null`。
  + 在使用返回的对象之前，应该检查它是否为`null`，以避免空指针异常。

#### 4. removeAttribute

* **所属类**：`HttpSession`
* **参数类型及描述**：`String name`（属性名）
* **功能描述**：从会话中移除指定的属性。
* **注意事项**：
  + 如果指定的属性名不存在，则不执行任何操作，也不会抛出异常。
  + 移除属性后，与该属性关联的对象将不再占用会话内存。

#### 5. getAttributeNames

* **所属类**：`HttpSession`
* **参数类型及描述**：无参数
* **功能描述**：返回会话中所有属性的名称的枚举。
* **注意事项**：
  + 可以使用返回的枚举来遍历会话中的所有属性。
  + 在遍历过程中，不要修改会话（例如添加或删除属性），因为这可能会导致枚举行为不一致。

#### 6. invalidate

* **所属类**：`HttpSession`
* **参数类型及描述**：无参数
* **功能描述**：使会话失效，并删除会话中的所有属性。
* **注意事项**：
  + 调用此方法后，会话对象将不再有效。
  + 在用户注销或退出系统时，应该调用此方法以销毁会话，避免敏感信息被非法访问。

#### 7. isNew

* **所属类**：`HttpSession`
* **参数类型及描述**：无参数
* **功能描述**：检查会话是否为新的。
* **注意事项**：
  + 如果会话是新的（即用户刚刚创建了一个会话），则返回`true`。
  + 此方法通常用于在用户首次访问网站时执行特定操作（例如显示欢迎消息）。

#### 8. getCreationTime

* **所属类**：`HttpSession`
* **参数类型及描述**：无参数
* **功能描述**：返回会话的创建时间。
* **注意事项**：
  + 返回的时间是自1970年1月1日00:00:00 GMT以来的毫秒数。
  + 可以使用`java.util.Date`类将毫秒数转换为可读的日期和时间格式。

#### 9. getLastAccessedTime

* **所属类**：`HttpSession`
* **参数类型及描述**：无参数
* **功能描述**：返回会话的最后访问时间。
* **注意事项**：
  + 返回的时间也是自1970年1月1日00:00:00 GMT以来的毫秒数。
  + 每当用户发送请求到服务器时，会话的最后访问时间都会更新。

#### 10. getMaxInactiveInterval

* **所属类**：`HttpSession`
* **参数类型及描述**：无参数
* **功能描述**：返回会话的最大不活动时间间隔（以秒为单位）。
* **注意事项**：
  + 如果会话在指定的时间内没有活动（即没有接收到任何请求），则会话将失效。
  + 可以使用`setMaxInactiveInterval`方法来设置此值。

#### 11. setMaxInactiveInterval

* **所属类**：`HttpSession`
* **参数类型及描述**：`int seconds`（最大不活动时间间隔，以秒为单位）
* **功能描述**：设置会话的最大不活动时间间隔。
* **注意事项**：
  + 如果指定的时间为0或负数，则会话将永远不会失效。
  + 应该根据应用的需求合理设置此值，以避免过多的会话占用服务器内存。

#### 12. getServletContext

* **所属类**：`HttpSession`
* **参数类型及描述**：无参数
* **功能描述**：返回当前会话的上下文环境。
* **注意事项**：
  + `ServletContext`对象是一个全局对象，它允许Servlet与Web容器进行通信。
  + 可以使用`ServletContext`对象来访问Web应用的初始化参数、资源文件等。

#### 13. getId

* **所属类**：`HttpSession`
* **参数类型及描述**：无参数
* **功能描述**：返回会话的唯一标识符。
* **注意事项**：
  + 会话ID通常用于在客户端和服务器之间传递会话信息。
  + 会话ID通常存储在客户端的Cookie中，但也可以通过URL重写等方式传递。

在Java Servlet中，会话（Session）的默认失效时间通常是由Servlet容器（如Tomcat、Jetty等）配置的。你可以通过几种方式来设置会话的默认失效时间（即会话超时时间）。

### 设置Session失效时间

#### 方法一：在 `web.xml` 中配置

在的Web应用的 `web.xml` 文件中配置会话超时时间。`web.xml` 文件通常位于 `WEB-INF` 目录下。

```xml
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee
         http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
         version="3.1">

    <!-- 其他配置 -->

    <session-config>
        <session-timeout>30</session-timeout> <!-- 单位为分钟 -->
    </session-config>

    <!-- 其他配置 -->

</web-app>
```

> 如果禁用会话超时，可以将值设置为 `0` 或一个负值。

#### 方法二：在代码中动态设置

可以在代码中为每个会话动态设置超时时间。这通常在创建或获取会话对象后执行。

```java
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class MyServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        // 设置会话超时时间为30分钟（1800秒）
        session.setMaxInactiveInterval(1800);

        // 其他处理逻辑
    }
}
```

#### 方法三：在Servlet容器配置中设置

大多数Servlet容器允许你通过配置文件或管理界面来设置默认的会话超时时间。例如，在Tomcat中，你可以在 `conf/web.xml` 文件中设置默认会话超时时间：

```xml
<Context>
    <!-- 其他配置 -->
    <SessionCookieName>JSESSIONID</SessionCookieName>
    <SessionTimeout>30</SessionTimeout> <!-- 单位为分钟 -->
    <!-- 其他配置 -->
</Context>
```

#### 注意事项

1. **单位**：无论是通过 `web.xml` 还是代码设置，会话超时时间的单位都是分钟。如果通过代码设置，时间单位是秒。
2. **优先级**：如果在 `web.xml` 和代码中同时设置了会话超时时间，代码中的设置会覆盖 `web.xml` 中的设置。
3. **容器配置**：容器级别的配置（如Tomcat的 `conf/web.xml`）通常作为默认值，可以被Web应用自己的 `web.xml` 或代码中的设置覆盖。

### 案例

[SessionDemo.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2Fservletcookiesession%2Fcom%2Fdemo%2FSessionDemo.java)
```java
package com.demo.servletcookiesession.com.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/test10")
public class SessionDemo extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 获取Session
        HttpSession session = req.getSession();
        // 设置Session
        session.setAttribute("name", "张三");
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        out.println("设置Session");
    }
}

```

[SessionDemo1.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2Fservletcookiesession%2Fcom%2Fdemo%2FSessionDemo1.java)
```java
package com.demo.servletcookiesession.com.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

@WebServlet("/test11")
public class SessionDemo1 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 获取Session
        HttpSession session = req.getSession();
        // 获取Session存储的值
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        Enumeration<String> attributeNames = session.getAttributeNames();
        out.println("获取Session存储的值：<br/>");
        while (attributeNames.hasMoreElements()) {
            String attributeName = attributeNames.nextElement();
            out.printf("name=%s,value=%s<br/>", attributeName, session.getAttribute(attributeName));
        }
    }
}
```

访问`http://localhost:8080/demo/test10`:

![img_23.png](images/img_23.png)

访问`http://localhost:8080/demo/test11`:

![img_24.png](images/img_24.png)

## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)