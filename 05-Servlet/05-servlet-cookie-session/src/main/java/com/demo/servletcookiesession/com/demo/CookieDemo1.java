package com.demo.servletcookiesession.com.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Cookie测试1
 *
 * @author Anna.
 * @date 2025/3/10 22:26
 */
@WebServlet("/test1")
public class CookieDemo1 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置编码格式
        req.setCharacterEncoding("UTF-8");
        // 获取cookie
        String cookie = req.getHeader("Cookie");
        // 设置Cookie
        resp.setHeader("Set-Cookie", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));

        // 设置响应编码格式
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        // 判断cookie是否为空
        if (cookie == null || "".equals(cookie)) {
            // 输出Hello
            out.printf("您好，欢迎您首次访问");
        } else {
            // 输出 上次访问时间
            out.printf("欢迎回来，您上次访问时间为:%s\n", cookie);
        }
    }
}
