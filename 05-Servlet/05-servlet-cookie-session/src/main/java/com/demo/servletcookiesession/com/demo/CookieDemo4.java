package com.demo.servletcookiesession.com.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Cookie测试
 *
 * @author Anna.
 * @date 2025/3/10 22:26
 */
@WebServlet("/test4")
public class CookieDemo4 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置编码格式
        req.setCharacterEncoding("UTF-8");
        // 设置Cookie
        resp.addCookie(new Cookie("lasttime", URLEncoder.encode(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()))));
        // 设置Cookie 使用URL编码处理cookie中不能直接存储中文数据
        resp.addCookie(new Cookie("name", URLEncoder.encode("张三")));

        // 获取cookie
        Cookie[] cookies = req.getCookies();
        // 设置响应编码格式
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        // 输出 上次访问时间
        if (cookies != null && cookies.length == 2) {
            out.printf("欢迎回来%s=%s，您上次访问时间为:%s\n", cookies[0].getName(), URLDecoder.decode(cookies[0].getValue()), URLDecoder.decode(cookies[1].getValue()));
        } else if (cookies != null && cookies.length == 1) {
            out.printf("欢迎回来%s=%s，您上次访问时间为:%s\n", cookies[0].getName(), URLDecoder.decode(cookies[0].getValue()), "");
        } else {
            out.printf("您好，欢迎您首次访问");
        }
    }
}
