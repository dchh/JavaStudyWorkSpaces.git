package com.demo.servletcookiesession.com.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Cookie测试
 *
 * @author Anna.
 * @date 2025/3/10 22:26
 */
@WebServlet("/test6")
public class CookieDemo6 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置编码格式
        req.setCharacterEncoding("UTF-8");
        // 设置Cookie
        resp.addCookie(new Cookie("lasttime", URLEncoder.encode(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()))));
        // 使用URL编码处理cookie中不能直接存储中文数据
        Cookie cookie = new Cookie("name", URLEncoder.encode("张三"));
        // 设置Cookie的域属性
        cookie.setDomain("test.com");
        // 设置Cookie
        resp.addCookie(cookie);

        // 获取cookie
        Cookie[] cookies = req.getCookies();
        // 设置响应编码格式
        resp.setContentType("text/html;charset=UTF-8");
        PrintWriter out = resp.getWriter();
        String nameStr = "您好", timeStr = "欢迎您首次访问";
        if (cookies != null) {
            for (Cookie c : cookies) {
                if ("name".equals(c.getName())) {
                    nameStr = String.format("欢迎回来%s=%s", c.getName(), URLDecoder.decode(c.getValue(), "UTF-8"));
                } else if ("lasttime".equals(c.getName())) {
                    timeStr = String.format("您上次访问时间为:%s", URLDecoder.decode(c.getValue(), "UTF-8"));
                }
            }
        }
        // 输出 上次访问时间
        out.printf("%s，%s", nameStr, timeStr);
    }
}
