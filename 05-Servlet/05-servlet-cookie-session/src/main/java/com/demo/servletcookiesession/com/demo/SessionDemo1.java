package com.demo.servletcookiesession.com.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

/**
 * Session测试
 *
 * @author Anna.
 * @date 2025/3/12 15:25
 */
@WebServlet("/test11")
public class SessionDemo1 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 获取Session
        HttpSession session = req.getSession();
        // 获取Session存储的值
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        Enumeration<String> attributeNames = session.getAttributeNames();
        out.println("获取Session存储的值：<br/>");
        while (attributeNames.hasMoreElements()) {
            String attributeName = attributeNames.nextElement();
            out.printf("name=%s,value=%s<br/>", attributeName, session.getAttribute(attributeName));
        }
    }
}
