package com.demo;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 获取资源路径
 *
 * @author Anna.
 * @date 2025/3/5 10:31
 */
@WebServlet("/test3")
public class ServletContextDemo3 extends GenericServlet {

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        // 通过Request获取
        ServletContext servletContext = servletRequest.getServletContext();
        // 设置响应编码格式
        servletResponse.setContentType("text/html;charset=utf-8");
        // 获取输出对象
        PrintWriter writer = servletResponse.getWriter();
        // 输出
        writer.println("获取web目录下资源访问：" + servletContext.getRealPath("/a.txt"));
        writer.println("<br/> 获取WEB-INF目录下资源访问: " + servletContext.getRealPath("/WEB-INF/b.txt"));
        writer.println("<br/> 获取resources目录下资源访问: " + servletContext.getRealPath("/WEB-INF/classes/c.txt"));
    }
}
