package com.demo;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 使用ServletContext存取数据
 *
 * @author Anna.
 * @date 2025/3/5 10:31
 */
@WebServlet("/test5")
public class ServletContextDemo5 extends GenericServlet {

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        // 通过Request获取
        ServletContext servletContext = servletRequest.getServletContext();

        // 设置响应编码格式
        servletResponse.setContentType("text/html;charset=utf-8");
        // 获取输出对象
        PrintWriter writer = servletResponse.getWriter();
        // 输出
        writer.printf("获取ServletContext存储name：%s%n", servletContext.getAttribute("name"));
    }
}
