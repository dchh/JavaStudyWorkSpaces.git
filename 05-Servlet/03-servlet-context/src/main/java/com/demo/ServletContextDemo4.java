package com.demo;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 使用ServletContext存取数据
 *
 * @author Anna.
 * @date 2025/3/5 10:31
 */
@WebServlet("/test4")
public class ServletContextDemo4 extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 通过Request获取
        ServletContext servletContext = req.getServletContext();
        // 存储数据
        servletContext.setAttribute("name","张三");
        // 重定向/test5
        resp.sendRedirect(servletContext.getContextPath() + "/test5");
    }
}
