package com.demo;

import javax.servlet.*;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 获取web.xml中全局配置参数
 *
 * @author Anna.
 * @date 2025/3/5 10:31
 */
public class ServletContextDemo2 extends GenericServlet {

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        // 通过Request获取
        ServletContext servletContext = servletRequest.getServletContext();
        // 设置响应编码格式
        servletResponse.setContentType("text/html;charset=utf-8");
        // 获取输出对象
        PrintWriter writer = servletResponse.getWriter();
        // 输出
        writer.println("通过Servlet中getInitParameter的获取配置参数：<hr/> ");
        writer.println("name: " + this.getInitParameter("name"));
        writer.println("<br/> ");
        writer.println("account: " + this.getInitParameter("account"));
        writer.println("<br/><br/>通过ServletContext中getInitParameter的获取配置参数：<hr/> ");
        writer.println("name: " + servletContext.getInitParameter("name"));
        writer.println("<br/> ");
        writer.println("account: " + servletContext.getInitParameter("account"));
    }
}
