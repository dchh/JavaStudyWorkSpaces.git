package com.demo;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

/**
 * 生成随机验证码
 *
 * @author Anna.
 * @date 2025/3/5 10:31
 */
@WebServlet("/test7")
public class ServletContextDemo7 extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 定义图片长宽
        int width = 100, height = 100;
        // 创建一对象，在内存中图片(验证码图片对象)
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        // 美化图片
        Graphics graphics = bufferedImage.getGraphics();
        // 设置画笔颜色
        graphics.setColor(Color.BLACK);
        // 填充大小
        graphics.fillRect(0, 0, width, height);

        // 设置画笔颜色
        graphics.setColor(Color.LIGHT_GRAY);
        // 绘制边框
        graphics.drawRect(0, 0, width - 1, height - 1);

        // 设置画笔颜色
        graphics.setColor(Color.WHITE);
        graphics.setFont(new Font("Times New Roman", Font.BOLD, 30));
        // 生成随机数
        String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghigklmnopqrstuvwxyz0123456789";
        Random ran = new Random();
        for (int i = 1; i <= 4; i++) {
            int index = ran.nextInt(str.length());
            // 获取字符
            char ch = str.charAt(index);// 随机字符
            // 绘制验证码
            graphics.drawString(ch + "", width / 6 * i, height / 2);
        }

        // 设置画笔颜色
        graphics.setColor(Color.GREEN);
        //随机生成坐标点
        for (int i = 0; i < 10; i++) {
            int x1 = ran.nextInt(width);
            int x2 = ran.nextInt(width);

            int y1 = ran.nextInt(height);
            int y2 = ran.nextInt(height);
            // 绘制干扰线
            graphics.drawLine(x1, y1, x2, y2);
        }

        // 输出图片
        ImageIO.write(bufferedImage, "jpg", resp.getOutputStream());
    }
}
