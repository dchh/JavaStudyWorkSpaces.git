package com.demo;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * ServletContext的获取方式
 *
 * @author Anna.
 * @date 2025/3/5 10:31
 */
@WebServlet("/test1")
public class ServletContextDemo1 extends GenericServlet {

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        // 通过Request获取
        ServletContext servletContext = servletRequest.getServletContext();
        // 通过Servlet的getServletContext方法
        ServletContext servletContext1 = this.getServletContext();
        // 通过ServletConfig对象
        ServletContext servletContext2 = this.getServletConfig().getServletContext();

        // 设置响应编码格式
        servletResponse.setContentType("text/html;charset=utf-8");
        // 获取输出对象
        PrintWriter writer = servletResponse.getWriter();
        // 输出并判断是否是同一对象
        writer.println("通过Request获取: " + servletContext);
        writer.println("<br/> ");
        writer.println("通过Servlet的getServletContext方法: " + servletContext1);
        writer.println("<br/> ");
        writer.println("通过ServletConfig对象: " + servletContext2);
        writer.println("<br/> ");
        // 判断是否是同一对象
        writer.println("判断是否是同一对象： " + (servletContext == servletContext2));
    }
}
