# ServletContext介绍

## 什么是ServletContext
ServletContext是Servlet中的一个核心接口，它呈现了web应用的Servlet视图，为web应用提供了一个全局的上下文环境。

### ServletContext作用与特点

1. **全局上下文对象**：ServletContext是web应用的全局上下文对象，它封装了web应用的信息，并允许在web应用的所有组件（如Servlet、JSP等）之间共享数据。
2. **唯一性**：在一个web应用中，ServletContext对象是唯一的。这意味着无论你在web应用的哪个位置获取ServletContext对象，得到的都是同一个实例。
3. **生命周期**：ServletContext对象在服务器启动时创建，并在服务器关闭时销毁。它的生命周期与web应用的生命周期紧密相关。

### ServletContext常用方法列表

| 序号 | 使用类型 | 方法名称 | 作用 | 注意事项                                                          |
| --- | --- | --- | --- |---------------------------------------------------------------|
| 1 | 获取全局配置参数 | `getInitParameter(String name)` | 获取在web.xml中配置的全局参数值 | 确保web.xml中通过 <context-param> 标签来定义全局初始化参数,已正确配置对应的参数名和值       |
| 2 | 获取资源路径 | `getRealPath(String path)` | 获取资源在服务器上的绝对路径 | 路径应根据Web应用的根目录来确定。确保资源在Web应用的可访问范围内。                          |
| 3 | 获取资源输入流 | `getResourceAsStream(String path)` | 根据相对路径直接获取资源的输入流 | 同上                                                            |
| 4 | 存取数据 | `setAttribute(String name, Object object)`, `getAttribute(String name)`, `removeAttribute(String name)` | 在ServletContext中设置、获取或移除属性及其值 | 用于在Web应用程序范围内共享数据。设置的属性可以被Web应用中的所有Servlet访问。避免使用保留的关键字作为属性名。 |
| 5 | 获取MIME类型 | `getMimeType(String file)` | 根据文件扩展名获取对应的MIME类型 | 传入的参数应为文件的扩展名（如.jpg、.html等）。返回的MIME类型用于指示浏览器如何处理文件。           |

> 注意事项：

1. **确保web.xml配置正确**：在使用ServletContext获取全局配置参数时，需要确保web.xml文件中的`<context-param>`节点配置正确。
2. **资源路径问题**：在使用`getRealPath`方法获取资源路径时，需要注意路径的正确性和资源的存在性。
3. **线程安全问题**：虽然ServletContext对象是线程安全的，但在多线程环境下访问共享数据时仍需谨慎，以避免出现数据不一致的问题。

#### 什么是MIME类型
MIME类型（Multipurpose Internet Mail Extensions类型）是用于描述消息内容类型的标准，也被称为媒体类型或内容类型。
它主要用来表示文档、文件或字节流的性质和格式，以便浏览器或邮件客户端能够正确处理。以下是关于MIME类型的详细介绍：

1. **定义与作用**：
  - MIME类型是一种文本标签，用于指示数据的格式和编码方式。
  - 它在网络通信中起着关键作用，帮助服务器和客户端识别传输的数据格式，并确保数据能在客户端和服务器之间正确地传输和处理。

2. **组成结构**：
  - MIME类型通常由两个字符串组成，中间用“/”分隔，例如`text/html`或`image/jpeg`。
  - 第一个字符串称为“类型”，表示数据的大类，如`text`、`image`、`audio`、`video`、`application`等。
  - 第二个字符串称为“子类型”，提供更具体的数据格式信息，如`plain`、`html`、`jpeg`、`png`等。

3. **常见MIME类型**：
  - **文本类型**：如`text/plain`（纯文本）、`text/html`（HTML文档）、`text/css`（CSS样式表）等。
  - **图像类型**：如`image/jpeg`（JPEG图像）、`image/png`（PNG图像）、`image/gif`（GIF图像）等。
  - **音频类型**：如`audio/mpeg`（MP3音频）、`audio/wav`（WAV音频）等。
  - **视频类型**：如`video/mp4`（MP4视频）、`video/x-msvideo`（AVI视频）等。
  - **应用类型**：如`application/json`（JSON数据）、`application/pdf`（PDF文件）等。

4. **使用场景**：
  - 在HTTP协议中，MIME类型通常用于指定响应或请求的内容类型，确保浏览器能够正确解析和显示数据。
  - 在电子邮件中，MIME类型用于指定邮件附件的格式，以便邮件客户端能够正确处理。

5. **重要性**：
  - 正确设置MIME类型对于网页的正常显示和文件的正确处理至关重要。
  - 错误的MIME类型可能导致文件被错误处理或无法显示。

## 案例

### ServletContext获取方式

ServletContext可以通过以下几种方式获取：

1. **通过Servlet的getServletContext方法**：在每个Servlet中，可以直接调用getServletContext()方法来获取ServletContext对象。例如：`ServletContext context = getServletContext();`。
2. **通过ServletConfig对象**：首先获取ServletConfig对象，然后调用其getServletContext()方法。例如：`ServletConfig servletConfig = getServletConfig(); ServletContext context = servletConfig.getServletContext();`。
3. **通过HttpServletRequest对象**：可以从HttpServletRequest对象中获取Session，再通过Session获取ServletContext。例如：`HttpSession session = request.getSession(); ServletContext context = session.getServletContext();`。或者直接通过HttpServletRequest对象的getServletContext()方法获取，例如：`ServletContext context = request.getServletContext();`。
4. **通过ServletContextListener**：ServletContextListener是一个接口，可以监听Web应用程序的启动和关闭事件。在Web应用程序启动时，可以获取到ServletContext对象。这通常用于在Web应用程序启动时执行一些初始化操作。

> `getServletConfig()`由Servlet接口定义，由其子类实现。`getServletContext()`方法，通常由Servlet接口子类定义并实现。

#### 测试代码

[ServletContextDemo1.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2FServletContextDemo1.java)
```java
package com.demo;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/test1")
public class ServletContextDemo1 extends GenericServlet {

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        // 通过Request获取
        ServletContext servletContext = servletRequest.getServletContext();
        // 通过Servlet的getServletContext方法
        ServletContext servletContext1 = this.getServletContext();
        // 通过ServletConfig对象
        ServletContext servletContext2 = this.getServletConfig().getServletContext();

        // 设置响应编码格式
        servletResponse.setContentType("text/html;charset=utf-8");
        // 获取输出对象
        PrintWriter writer = servletResponse.getWriter();
        // 输出并判断是否是同一对象
        writer.println("通过Request获取: " + servletContext);
        writer.println("<br/> ");
        writer.println("通过Servlet的getServletContext方法: " + servletContext1);
        writer.println("<br/> ");
        writer.println("通过ServletConfig对象: " + servletContext2);
        writer.println("<br/> ");
        // 判断是否是同一对象
        writer.println("判断是否是同一对象： " + (servletContext == servletContext2));
    }
}
```

执行结果：

![img.png](images/img.png)

### ServletContext获取全局配置参数
ServletContext 的 `getInitParameter(String name)` 方法用于获取在 web.xml 文件中通过 <context-param> 标签定义的上下文初始化参数。

[ServletContextDemo2.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2FServletContextDemo2.java)
```java
package com.demo;

import javax.servlet.*;
import java.io.IOException;
import java.io.PrintWriter;

public class ServletContextDemo2 extends GenericServlet {

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        // 通过Request获取
        ServletContext servletContext = servletRequest.getServletContext();
        // 设置响应编码格式
        servletResponse.setContentType("text/html;charset=utf-8");
        // 获取输出对象
        PrintWriter writer = servletResponse.getWriter();
        // 输出
        writer.println("通过Servlet中getInitParameter的获取配置参数：<hr/> ");
        writer.println("name: " + this.getInitParameter("name"));
        writer.println("<br/> ");
        writer.println("account: " + this.getInitParameter("account"));
        writer.println("<br/><br/>通过ServletContext中getInitParameter的获取配置参数：<hr/> ");
        writer.println("name: " + servletContext.getInitParameter("name"));
        writer.println("<br/> ");
        writer.println("account: " + servletContext.getInitParameter("account"));
    }
}
```

[web.xml](src%2Fmain%2Fwebapp%2FWEB-INF%2Fweb.xml)
```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
    <servlet>
        <servlet-name>test2</servlet-name>
        <servlet-class>com.demo.ServletContextDemo2</servlet-class>
        <init-param>
            <param-name>name</param-name>
            <param-value>张三</param-value>
        </init-param>
        <init-param>
            <param-name>account</param-name>
            <param-value>zhangsan123</param-value>
        </init-param>
    </servlet>
    <servlet-mapping>
        <servlet-name>test2</servlet-name>
        <url-pattern>/test2</url-pattern>
    </servlet-mapping>

    <context-param>
        <param-name>name</param-name>
        <param-value>李四</param-value>
    </context-param>
    <context-param>
        <param-name>account</param-name>
        <param-value>lisi123</param-value>
    </context-param>
</web-app>
```

> 需要注意的是Servlet同样提供一个 `getInitParameter(String name)` 方法，但其获取的是 <servlet>标签下<init-param>子标签定义的参数。

执行结果：

![img_1.png](images/img_1.png)

### 获取资源路径
[ServletContextDemo3.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2FServletContextDemo3.java)
```java
package com.demo;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/test3")
public class ServletContextDemo3 extends GenericServlet {

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        // 通过Request获取
        ServletContext servletContext = servletRequest.getServletContext();
        // 设置响应编码格式
        servletResponse.setContentType("text/html;charset=utf-8");
        // 获取输出对象
        PrintWriter writer = servletResponse.getWriter();
        // 输出
        writer.println("获取web目录下资源访问：" + servletContext.getRealPath("/a.txt"));
        writer.println("<br/> 获取WEB-INF目录下资源访问: " + servletContext.getRealPath("/WEB-INF/b.txt"));
        writer.println("<br/> 获取resources目录下资源访问: " + servletContext.getRealPath("/WEB-INF/classes/c.txt"));
    }
}
```

执行结果：

![img_2.png](images/img_2.png)

### 使用ServletContext存取数据

[ServletContextDemo4.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2FServletContextDemo4.java)
```java
package com.demo;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/test4")
public class ServletContextDemo4 extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 通过Request获取
        ServletContext servletContext = req.getServletContext();
        // 存储数据
        servletContext.setAttribute("name","张三");
        // 重定向/test5
        resp.sendRedirect(servletContext.getContextPath() + "/test5");
    }
}
```

[ServletContextDemo5.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2FServletContextDemo5.java)
```java
package com.demo;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/test5")
public class ServletContextDemo5 extends GenericServlet {

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        // 通过Request获取
        ServletContext servletContext = servletRequest.getServletContext();

        // 设置响应编码格式
        servletResponse.setContentType("text/html;charset=utf-8");
        // 获取输出对象
        PrintWriter writer = servletResponse.getWriter();
        // 输出
        writer.printf("获取ServletContext存储name：%s%n", servletContext.getAttribute("name"));
    }
}
```

执行结果：

![img_3.png](images/img_3.png)

### 使用ServletContext获取MIME类型
[ServletContextDemo6.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2FServletContextDemo6.java)
```java
package com.demo;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/test6")
public class ServletContextDemo6 extends GenericServlet {

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        // 通过Request获取
        ServletContext servletContext = servletRequest.getServletContext();

        // 设置响应编码格式
        servletResponse.setContentType("text/html;charset=utf-8");
        // 获取输出对象
        PrintWriter writer = servletResponse.getWriter();
        // 输出
        writer.printf("获取MIME类型：%s%n", servletContext.getMimeType("a.jpg"));
    }
}
```

执行结果：

![img_4.png](images/img_4.png)

### 案例-生成随机验证码

[ServletContextDemo7.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2FServletContextDemo7.java)
```java
package com.demo;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

@WebServlet("/test7")
public class ServletContextDemo7 extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 定义图片长宽
        int width = 100, height = 100;
        // 创建一对象，在内存中图片(验证码图片对象)
        BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        // 美化图片
        Graphics graphics = bufferedImage.getGraphics();
        // 设置画笔颜色
        graphics.setColor(Color.BLACK);
        // 填充大小
        graphics.fillRect(0, 0, width, height);

        // 设置画笔颜色
        graphics.setColor(Color.LIGHT_GRAY);
        // 绘制边框
        graphics.drawRect(0, 0, width - 1, height - 1);

        // 设置画笔颜色
        graphics.setColor(Color.WHITE);
        graphics.setFont(new Font("Times New Roman", Font.BOLD, 30));
        // 生成随机数
        String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghigklmnopqrstuvwxyz0123456789";
        Random ran = new Random();
        for (int i = 1; i <= 4; i++) {
            int index = ran.nextInt(str.length());
            // 获取字符
            char ch = str.charAt(index);// 随机字符
            // 绘制验证码
            graphics.drawString(ch + "", width / 6 * i, height / 2);
        }

        // 设置画笔颜色
        graphics.setColor(Color.GREEN);
        //随机生成坐标点
        for (int i = 0; i < 10; i++) {
            int x1 = ran.nextInt(width);
            int x2 = ran.nextInt(width);

            int y1 = ran.nextInt(height);
            int y2 = ran.nextInt(height);
            // 绘制干扰线
            graphics.drawLine(x1, y1, x2, y2);
        }

        // 输出图片
        ImageIO.write(bufferedImage, "jpg", resp.getOutputStream());
    }
}
```

[index.jsp](src%2Fmain%2Fwebapp%2Findex.jsp)
```jsp
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
  <title>随机验证码</title>
</head>
<body>
<h1><%= "Hello World!" %></h1>
点击刷新图片
<br/>
<%
  String contextPath = request.getContextPath();
%>

<img id="checkCode" src="<%=contextPath%>/test7" alt="验证码" />
<script>
  window.onload = function(){
    //1.获取图片对象
    var img = document.getElementById("checkCode");
    //2.绑定单击事件
    img.onclick = function(){
      //加时间戳
      var date = new Date().getTime();
      img.src = "<%=contextPath%>/test7?"+date;
    }
  }
</script>
</body>
</html>
```

执行结果：

![img_5.png](images/img_5.png)

## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)