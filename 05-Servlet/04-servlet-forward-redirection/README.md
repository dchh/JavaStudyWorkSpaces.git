# Servlet转发与重定向
在Servlet中，重定向和转发是两种常用的页面跳转方式。

##  什么是转发
在Web开发中，页面跳转是一个常见的需求。在Servlet中，重定向和转发是两种常用的页面跳转方式。以下是对Servlet转发与重定向的详细介绍：
1.**定义**：Servlet转发是一种服务器端行为，通过转发可以在服务器内部将请求从一个资源转发给另一个资源。<font color="red">是指将一个请求从当前的Servlet或JSP页面转发到同一个Web应用中的另一个Servlet、JSP页面或资源的过程</font>。
2. **工作原理**：当容器接收到请求后，Servlet会先对请求进行一些预处理，然后将请求传递给其他Web资源（如另一个Servlet、JSP页面或静态资源），由这些资源来完成包括生成响应在内的后续工作。
3. **实现方式**：通过`RequestDispatcher`接口的`forward()`方法实现。首先，使用`request.getRequestDispatcher(String path)`方法获取`RequestDispatcher`对象，其中`path`参数指定目标资源的路径（可以是绝对路径或相对路径）。然后，调用`forward(request, response)`方法将请求转发给指定的资源。
4. **特点**：

    * 转发过程中，请求在服务器内部传递，客户端并不知道发生了转发。
    * 转发只有一次请求响应过程。
    * 转发后，浏览器的地址栏不会发生变化。
    * 转发只能将请求转发到当前Web应用内的资源，无法跳转到当前Web应用之外的URL。
    * 在转发过程中，可以将数据保存到`request`域对象中，以便在目标资源中访问这些数据。

### 案例
测试访问`/test2`转发到`test1`,并输出`/test2`中设置请求参数。

[ForwardDemo2.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2FForwardDemo2.java)
```java
package com.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/test2")
public class ForwardDemo2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        req.setAttribute("message","hello 张三！");
        // 设置转发
        req.getRequestDispatcher("/test1").forward(req, resp);
    }
}
```

[ForwardDemo.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2FForwardDemo.java)
```java
package com.demo;

import com.sun.org.apache.regexp.internal.RE;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/test1")
public class ForwardDemo extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置编码格式
        req.setCharacterEncoding("UTF-8");
        // 设置编码格式
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        // 输出request请求参数
        out.println("message:" + req.getParameter("message"));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
```

执行结果：

![img.png](images/img.png)

> 注意：servlet只能转发当前Web应用中的另一个Servlet、JSP页面或资源。如果转发到其他地址则会返回404。且浏览器地址不变，只发送一次请求。

例如：
```
 // 设置转发
req.getRequestDispatcher("www.baidu.com").forward(req, resp);
```
访问结果如下：

![img_1.png](images/img_1.png)

## 什么是重定向

1. **定义**：Servlet重定向是一种客户端行为，通过重定向可以将请求转发给其他资源（如另一个Servlet、JSP页面或静态资源）。
2. **工作原理**：当执行重定向时，客户端会收到一个包含新URL的响应，然后重新发送一个新的请求到新的URL。
3. **实现方式**：通过`HttpServletResponse`对象的`sendRedirect()`方法实现。该方法会生成一个302响应码和`Location`响应头，通知客户端去重新访问`Location`响应头中指定的URL。
4. **特点**：

    * 重定向涉及两次请求响应过程。
    * 重定向后，浏览器的地址栏会显示新的URL。
    * 重定向可以跳转到任意的URL，包括当前Web应用之外的URL。
    * 在重定向过程中，无法将数据保存到`request`域对象中（因为已经是一个新的请求了）。

### 案例
测试访问`/test3`转发到`test1`,并输出`/test3`中设置请求参数。

[RedirectDemo.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2FRedirectDemo.java)
```java
package com.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/test3")
public class RedirectDemo extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        req.setAttribute("message","hello 张三！");
        // 设置重定向 需要设置虚拟目录
       resp.sendRedirect(req.getContextPath() + "/test1");
    }
}

```

执行结果：

![img_2.png](images/img_2.png)

> 重定向是一种客户端行为，客服端会发生两次请求，且浏览器地址有变成转发后的地址。在重定向过程中，无法将数据保存到`request`域对象中（因为已经是一个新的请求了）。
> 重定向时，需要设置虚拟目录，指定访问的Web资源。当然也可以写外部资源，例如：`www.baidu.com`

```
resp.sendRedirect("www.baidu.com");
```

## Servlet转发与重定向的区别

1. **行为主体**：转发是服务器端行为，重定向是客户端行为。
2. **请求响应次数**：转发只有一次请求响应过程，重定向涉及两次请求响应过程。
3. **地址栏变化**：转发后地址栏不变，重定向后地址栏变为新的URL。
4. **跳转范围**：转发只能在当前Web应用内跳转，重定向可以跳转到任意URL。
5. **数据共享**：在转发过程中，数据可以保存在`request`域对象中并在目标资源中访问；在重定向过程中，由于已经是一个新的请求，所以无法共享之前的数据。


## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)