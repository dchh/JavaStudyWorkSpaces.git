package com.demo;

import com.sun.org.apache.regexp.internal.RE;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 测试转发
 *
 * @author Anna.
 * @date 2025/3/7 16:44
 */
@WebServlet("/test1")
public class ForwardDemo extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置编码格式
        req.setCharacterEncoding("UTF-8");
        // 设置编码格式
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        // 输出request请求参数
        out.println("message:" + req.getAttribute("message"));
    }
}
