package com.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 测试重定向
 *
 * @author Anna.
 * @date 2025/3/7 16:44
 */
@WebServlet("/test3")
public class RedirectDemo extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        req.setAttribute("message","hello 张三！");
        // 设置重定向 需要设置虚拟目录
        resp.sendRedirect(req.getContextPath() + "/test1");
    }
}
