<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>JSTL语法测试</title>
</head>
<body>
    <c:redirect url="hello.jsp">
        <c:param name="name">张三</c:param>
        <c:param name="age">23</c:param>
    </c:redirect>
</body>
</html>
