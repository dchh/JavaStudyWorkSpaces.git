<%@ page contentType="text/html;charset=UTF-8" session="false" language="java" %>
<%@ page import="com.demo.entity.UserInfoEntity" %>
<html>
<head>
    <title>JSP语法测试</title>
</head>
<body>
    <%
        UserInfoEntity userInfo = new UserInfoEntity();
        userInfo.setUserId(1123);
        userInfo.setUserName("张三");
        session.setAttribute("user", userInfo);
    %>
    <h1>用户个人资料</h1>
    <p>用户ID: ${user.userId}</p>
    <p>用户名: ${user.userName}</p>
</body>
</html>
