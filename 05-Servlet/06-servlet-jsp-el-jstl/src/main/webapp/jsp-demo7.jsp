<%@ page contentType="text/html;charset=UTF-8" session="false" language="java" %>
<%-- 指定失败时的处理路径 --%>
<%@ page errorPage="500.jsp" %>
<html>
<head>
    <title>JSP语法测试</title>
</head>
<body>
    <%
        // 设置运行时异常
        int i = 1 / 0;
    %>
</body>
</html>
