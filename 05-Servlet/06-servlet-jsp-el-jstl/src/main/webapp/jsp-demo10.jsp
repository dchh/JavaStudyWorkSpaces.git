<%@ page import="com.demo.entity.UserInfoEntity" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSP语法测试</title>
</head>
<body>
    <%
        pageContext.setAttribute("name", "pageContext-name");
        request.setAttribute("name", "request-name");
        session.setAttribute("name", "session-name");
        application.setAttribute("name", "application-name");

        request.setAttribute("age", "request-age");
        session.setAttribute("age", "session-age");
        application.setAttribute("age", "application-age");

        session.setAttribute("id", "session-id");
        application.setAttribute("id", "application-id");

        application.setAttribute("account", "application-account");
    %>
    <b>指定域名获取:\${pageScope.name}</b>：${pageScope.name}<br/>
    <b>指定域名获取:\${requestScope.name}</b>：${requestScope.name}<br/>
    <b>指定域名获取:\${sessionScope.name}</b>：${sessionScope.name}<br/>
    <b>指定域名获取:\${applicationScope.name}</b>：${applicationScope.name}<br/><br/>

    <h3>测试作用域的范围：pageContext、request、session、application</h3>
    <b>\${键名}方式获取:\${name}</b>：${name}<br/>
    <b>\${键名}方式获取:\${age}</b>：${age}<br/>
    <b>\${键名}方式获取:\${id}</b>：${id}<br/>
    <b>\${键名}方式获取:\${account}</b>：${account}<br/><br/>

    <%
        UserInfoEntity userInfoEntity = new UserInfoEntity();
        userInfoEntity.setUserName("张三");
        request.setAttribute("user", userInfoEntity);
    %>
    <b>获取对象user中name属性:\${requestScope.user.userName}</b>：${requestScope.user.userName}<br/><br/>

    <%
        List<UserInfoEntity> list = new ArrayList<>();
        list.add(userInfoEntity);
        request.setAttribute("list", list);
    %>
    <b>获取List对象user中name属性:\${requestScope.list[0].userName}</b>：${requestScope.list[0].userName}<br/><br/>

    <%
        Map<String,UserInfoEntity> map = new HashMap<>();
        map.put("user",userInfoEntity);
        request.setAttribute("userMap", map);
    %>
    <b>获取Map对象user中name属性:\${requestScope.userMap.user.userName}</b>：${requestScope.userMap.user.userName}<br/>
    <b>获取Map对象user中name属性:\${requestScope.userMap.user[userName]}</b>：${requestScope.userMap.user[userName]}<br/>
</body>
</html>
