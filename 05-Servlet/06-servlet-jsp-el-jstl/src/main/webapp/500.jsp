<%@ page import="java.io.PrintWriter" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%-- 指定页面是失败页--%>
<%@ page isErrorPage="true" %>
<html>
<head>
    <title>500错误页面</title>
</head>
<body>

<pre>


服务器繁忙。

失败信息：<%= exception.getMessage()%>

<pre>
堆栈信息：<% out.write(exception.getLocalizedMessage());%>
</pre>
<%
PrintWriter printWriter = response.getWriter();
exception.printStackTrace(printWriter);
%>
</pre>
</body>
</html>
