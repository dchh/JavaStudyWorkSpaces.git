<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSP语法测试</title>
</head>
<body>

    <%!
        // 声明一个类成员变量
        private int counter = 0;

        // 声明一个类方法
        public int incrementCounter() {
            return ++counter;
        }
    %>
    <jsp:declaration>
        // 声明一个类成员变量
        private String name;
    </jsp:declaration>
</body>
</html>
