<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="myCore" %>
<html>
<head>
    <title>JSTL语法测试</title>
</head>
<body>
    <myCore:redirect url="hello.jsp">
        <myCore:param name="name">张三</myCore:param>
        <myCore:param name="age">23</myCore:param>
    </myCore:redirect>
</body>
</html>
