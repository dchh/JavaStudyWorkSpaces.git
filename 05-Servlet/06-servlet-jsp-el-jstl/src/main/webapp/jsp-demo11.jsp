<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSP语法测试</title>
</head>
<body>
    <b>获取动态获取虚拟目录\${pageContext.request.contextPath}</b>：${pageContext.request.contextPath}<br/><br/>

    <b>获取name参数\${param.name}</b>：${param.name}<br/><br/>

    <% response.addCookie(new Cookie("name","lisi")); %>
    <b>获取访问请求头user-agent \${header["user-agent"]}</b>：${header["user-agent"]}<br/><br/>

    <b>获取访问cookie \${cookie.name.value}</b>：${cookie.name.value}<br/><br/>
</body>
</html>
