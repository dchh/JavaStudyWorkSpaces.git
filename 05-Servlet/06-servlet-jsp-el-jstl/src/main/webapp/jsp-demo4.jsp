<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSP语法测试</title>
</head>
<body>
    <% // 代码片段
        String name = "张三";
        request.setAttribute("name",name);
    %>

        HTML注释<hr/>
<!--    Hello <%= name%> -->
        JSP注释<hr/>
    <%--    Hello <%= name%>--%>
</body>
</html>
