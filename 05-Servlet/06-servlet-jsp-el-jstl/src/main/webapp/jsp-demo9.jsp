<%@ page import="com.demo.entity.UserInfoEntity" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSP语法测试</title>
</head>
<body>

<table>
    <tr>
        <td><b>算数运算符</b></td>
    </tr>

    <tr>
        <td>1 + 3: ${1 + 3}</td>
        <td>1 - 3: ${1 - 3}</td>
    </tr>
    <tr>
        <td>1 + 3: ${1 + 3}</td>
        <td>1 - 3: ${1 - 3}</td>
    </tr>
    <tr>
        <td>1 * 3: ${1 * 3}</td>
        <td>1 / 3: ${1 / 3}</td>
    </tr>
    <tr>
        <td>1 % 3: ${1 % 3}</td>
        <td>1 mod 3: ${1 mod 3}</td>
    </tr>

    <tr>
        <td><b>关系运算符</b></td>
    </tr>
    <%
        session.setAttribute("A", "123");
        session.setAttribute("B", "123");
    %>

    <tr>
        <td>A == B: ${A == B}</td>
        <td>A eq B: ${A eq B}</td>
    </tr>
    <tr>
        <td>A != B: ${A != B}</td>
        <td>A ne B: ${A ne B}</td>
    </tr>

    <tr>
        <td><b>逻辑运算符</b></td>
    </tr>
    <%
        session.setAttribute("A", true);
        session.setAttribute("B", false);
    %>

    <tr>
        <td>A && B: ${A && B}</td>
        <td>A and B: ${A and B}</td>
    </tr>
    <tr>
        <td>A || B: ${A || B}</td>
        <td>A or B: ${A or B}</td>
    </tr>

    <tr>
        <td><b>逻辑运算符</b></td>
    </tr>
    <%
        UserInfoEntity userInfoEntity = new UserInfoEntity();
        userInfoEntity.setUserName("zhangsan");
        session.setAttribute("user", userInfoEntity);
    %>

    <tr>
        <td>\${empty user}: ${empty user}</td>
        <td>\${empty user.userId}: ${empty user.userId}</td>
    </tr>
    <tr>
        <td>\${empty user.userName}: ${empty user.userName}</td>
        <td></td>
    </tr>

    <tr>
        <td><b>条件运算符</b></td>
    </tr>
    <%
        int age = 18;
        session.setAttribute("age", age);
    %>

    <tr>
        <td>\${A ? B : C}: ${age >= 18 ? "成年了" : "未成年"}</td>
    </tr>

    <tr>
        <td><b>方括号运算符（[]）</b></td>
    </tr>
    <%
        List<String> data = Arrays.asList("1", "2", "3");
        session.setAttribute("data", data);
    %>

    <tr>
        <td>data[0]: ${data[0]}</td>
        <td>data[1]: ${data[1]}</td>
    </tr>
    <tr>
        <td>data[2]: ${data[2]}</td>
        <td></td>
    </tr>
</table>
</body>
</html>
