package com.demo.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户实体类
 *
 * @author Anna.
 * @date 2025/3/17 9:03
 */
@Data
public class UserInfoEntity implements Serializable {

    /** id */
    private int userId;

    /** 用户名称 */
    private String userName;
}
