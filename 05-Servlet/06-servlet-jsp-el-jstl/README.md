# JSP技术及EL表达式及JSTL表达式介绍

## 什么是JSP
JSP（JavaServer Pages）是一种用于创建动态Web内容的技术，它允许将Java代码嵌入到HTML页面中。
主要用于生成动态网页，其内容可以根据服务器端的逻辑动态生成。JSP页面最终会由Servlet容器（如Tomcat）编译成Servlet。

### JSP的工作原理

1. 客户端请求：用户通过浏览器向服务器发送请求，请求访问某个JSP页面。 
2. JSP页面转换：服务器上的JSP容器（如Tomcat）接收到请求后，会将JSP页面转换为Servlet源代码，然后编译成Servlet类文件。这个过程通常只在第一次请求时发生，之后服务器会缓存编译后的Servlet类文件。 
3. Servlet执行：服务器调用编译后的Servlet类文件的service()方法或doGet()/doPost()方法来处理请求。在Servlet的执行过程中，可以根据需要调用Java EE的其他组件（如EJB、JDBC等）来处理业务逻辑。 
4. 生成响应：Servlet处理完请求后，会生成一个响应，并将响应发送回客户端。响应通常是一个HTML页面，但也可以是其他类型的内容（如JSON、XML等）。

![img.png](images/img.png)

> 注意：JSP容器（如Tomcat）只在第一次请求时发生时，JSP页面转换为Servlet源代码，然后编译成Servlet类文件。编译后的文件通常缓存在工作目录下。如下所示：

![img_1.png](images/img_1.png)

> JSP页面编译后的Servlet文件

![img_2.png](images/img_2.png)

### JSP中的语法

JSP（JavaServer Pages）是一种用于创建动态Web页面的技术，它允许开发者在HTML中嵌入Java代码来生成动态内容。

| **JSP语法分类** | **语法说明** | **示例** |
| --- | --- | --- |
| **JSP脚本程序** | 嵌入Java代码 | `<% 代码片段 %>` 或 `<jsp:scriptlet> 代码片段 </jsp:scriptlet>` |
| **JSP声明** | 声明变量、方法 | `<%! declaration; [declaration; ]+... %>` 或 `<jsp:declaration> 代码片段 </jsp:declaration>` |
| **JSP表达式** | 输出值到客户端 | `<%= 表达式 %>` |
| **JSP注释** | 注释代码 | `<%-- 注释 --%>`（服务器端注释），`<!-- 注释 -->`（HTML注释） |
| **JSP指令** | 设置页面属性 | `<%@ directive attribute="value" %>`（如`<%@ page %>`, `<%@ include %>`, `<%@ taglib %>`） |
| **JSP动作标签** | 控制servlet引擎 | `<jsp:action_name attribute="value" />`（如`<jsp:include>`, `<jsp:useBean>`, `<jsp:setProperty>`, `<jsp:getProperty>`, `<jsp:forward>`） |
| **JSP隐含对象** | 自动定义的变量 | `request`, `response`, `out`, `session`, `application`, `config`, `pageContext`, `page`, `Exception` |

#### JSP脚本程序

脚本程序的语法格式有两种：
1. `<% 代码片段 %>`：这是最常用的语法格式，用于在JSP页面中嵌入Java代码。
2. `<jsp:scriptlet> 代码片段 </jsp:scriptlet>`：这是XML格式的语法，与第一种语法等价。(不推荐使用)

[jsp-demo.jsp](src%2Fmain%2Fwebapp%2Fjsp-demo.jsp)
```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSP语法测试</title>
</head>
<body>
    <% // 代码片段
        String name = "张三";
    %>

    <jsp:scriptlet>
        // XML格式的语法
        int age = 10;
    </jsp:scriptlet>
</body>
</html>
```

#### JSP声明
JSP声明用于声明一个或多个变量、方法，供后面的Java代码使用。声明的语法格式有两种：
1. `<%! declaration; [declaration; ]+... %>`：这是最常用的语法格式，用于声明变量和方法。
2. `<jsp:declaration> 代码片段 </jsp:declaration>`：这是XML格式的语法，与第一种语法等价。(不推荐使用)

> 声明的变量和方法在整个JSP页面中有效，为全局变量和方法。

[jsp-demo2.jsp](src%2Fmain%2Fwebapp%2Fjsp-demo2.jsp)
```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSP语法测试</title>
</head>
<body>

    <%!
        // 声明一个类成员变量
        private int counter = 0;

        // 声明一个类方法
        public int incrementCounter() {
            return ++counter;
        }
    %>
    <jsp:declaration>
        // 声明一个类成员变量
        private String name;
    </jsp:declaration>
</body>
</html>
```

#### JSP表达式
JSP表达式用于输出一个值到客户端。表达式的值会被转化成String，然后插入到表达式出现的地方。表达式的语法格式为：<%= 表达式 %>。

例如，<%= new java.util.Date().toLocaleString() %>会输出当前的日期和时间。

[jsp-demo3.jsp](src%2Fmain%2Fwebapp%2Fjsp-demo3.jsp)
```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSP语法测试</title>
</head>
<body>
    <% // 代码片段
        String name = "张三";
        request.setAttribute("name",name);
    %>

    Hello <%= name%>
</body>
</html>
```

执行结果：

![img_3.png](images/img_3.png)

前端输源码：

![img_4.png](images/img_4.png)

#### JSP注释
JSP注释主要有两种：
方式1：`<%-- 注释 --%>` ,JSP注释主要有两个作用：为代码作注释以及将某段代码注释掉。JSP注释的语法格式为：<%-- 注释 --%>。这种注释内容不会被发送至浏览器甚至不会被编译。
方式2：`<!-- 注释 -->`,HTML注释的语法格式为：<!-- 注释 -->。这种注释可以通过浏览器查看网页源代码时看见。

[jsp-demo4.jsp](src%2Fmain%2Fwebapp%2Fjsp-demo4.jsp)
```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSP语法测试</title>
</head>
<body>
    <% // 代码片段
        String name = "张三";
        request.setAttribute("name",name);
    %>

        HTML注释<hr/>
<!--    Hello <%= name%> -->
        JSP注释<hr/>
    <%--    Hello <%= name%>--%>
</body>
</html>
```

执行结果：

![img_5.png](images/img_5.png)

前端源代码：

![img_6.png](images/img_6.png)

> 需要注意的是，HTML注释的语法格式为：<!-- 注释 -->。这种注释可以通过浏览器查看网页源代码时看见。

#### JSP指令
JSP指令用来设置与整个JSP页面相关的属性。指令的语法格式为：`<%@ 指令名称 属性名1=属性值1 属性名2=属性值2 ... %>`。

其中，有三种常用的指令标签：
1. `<%@ page ... %>`：定义页面的依赖属性，比如脚本语言、error页面、缓存需求等。
2. `<%@ include ... %>`：包含其他文件。
3. `<%@ taglib ... %>`：引入标签库的定义，可以是自定义标签。

以下是一个关于JSP指令及其属性的详细表格：

| 指令名称 | 属性名称 | 作用 | 可选值 | 默认值 | 注意事项及实现 |
| --- | --- | --- | --- | --- | --- |
| page | language | 设置JSP页面使用的脚本语言 | java（目前只支持Java） | java | - |
| page | extends | 指定JSP页面转换为Servlet后继承的类 | 任何有效的Java类名 | 由JSP容器指定 | 不常用，可能影响服务器性能优化 |
| page | import | 导入Java包或类 | 包名.类名 或 包名.*（表示导入整个包） | java.lang.*, javax.servlet.*, javax.servlet.jsp.*, javax.servlet.http.* | 可多次使用以导入多个类或包 |
| page | session | 指定页面是否使用HTTP session会话对象 | true, false | true | 若为false，则不能使用session对象 |
| page | buffer | 设置JSP页面输出缓冲区的大小 | none, 8kb, 或其他指定大小（KB为单位） | 8kb | 单位只能是KB，建议使用8的倍数 |
| page | autoFlush | 设置缓冲区满时是否自动刷新 | true, false | true | 若为false，缓冲区满时可能抛出异常 |
| page | contentType | 设置发送到客户端的MIME类型和字符编码 | MIME类型; charset=字符编码 | text/html; charset=ISO-8859-1 | 例如：text/html; charset=utf-8 |
| page | pageEncoding | 设置JSP页面的字符编码 | 有效的字符集名称 | ISO-8859-1 | 例如：UTF-8 |
| page | info | 设置JSP页面的相关信息 | 任意字符串 | 无 | 可通过Servlet.getServletInfo()方法获取 |
| page | errorPage | 指定处理当前页面异常的另一个JSP页面的相对路径 | 相对URL | 无 | 指定的JSP错误处理页面需设置isErrorPage属性为true |
| page | isErrorPage | 指定当前页面是否为错误处理页面 | true, false | false | 若为true，则当前页面可以处理异常 |
| page | isThreadSafe | 指定页面是否为线程安全 | true, false | false | - |
| page | isELIgnored | 指定页面是否忽略EL表达式的使用 | true, false | false | - |
| include | file | 指定要包含的另一个JSP页面或文件的路径 | 相对路径 | 无 | 静态包含，被包含文件的内容会原样包含到当前JSP页面中 |
| taglib | uri | 指定标签库文件的存放位置 | 标签库URI | 无 | - |
| taglib | prefix | 指定标签库的前缀 | 任意非保留前缀 | 无 | 前缀不能命名为jsp、jspx、java等保留字 |

**注意事项及实现细节**：

1. **page指令**：

    * page指令对整个JSP页面有效，包括静态包含的文件，但不适用于动态包含的文件（如使用`<jsp:include>`包含的文件）。
    * 在一个JSP页面中可以使用多个page指令，但除了import属性外，其他属性只能出现一次。

2. **include指令**：

    * include指令用于静态包含另一个JSP页面或文件。被包含文件的内容会原样包含到当前JSP页面中，且在JSP文件转换时期就进行包含。

3. **taglib指令**：

    * taglib指令用于引入标签库，并指定标签库的前缀。通过该指令，可以在JSP页面中使用自定义的标签。

4. **属性值的设置**：

    * 对于可选值，应根据实际需求进行设置。例如，contentType属性应根据页面输出的内容类型进行设置，以确保浏览器能够正确解释和显示页面内容。
    * 默认值通常是JSP引擎的默认行为，但在某些情况下，可能需要显式设置属性值以满足特定需求。

5. **实现细节**：

    * JSP指令在JSP文件被编译成

##### 案例

###### JSP导入Java包或类

[jsp-demo5.jsp](src%2Fmain%2Fwebapp%2Fjsp-demo5.jsp)
```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.demo.entity.UserInfoEntity" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Arrays" %>
<html>
<head>
    <title>JSP语法测试</title>
</head>
<body>
    <%
        UserInfoEntity userInfo = new UserInfoEntity();
        userInfo.setUserId(1123);
        userInfo.setUserName("张三");
        List<String> list = Arrays.asList("1","2");
    %>
</body>
</html>
```

> 可多次使用以导入多个类或包。语法：`<%@ page import="包名.类名 或 包名.*" %>` .其中，包名.*（表示导入整个包）。

###### JSP指定页面是否使用HTTP中session会话对象

[jsp-demo6.jsp](src%2Fmain%2Fwebapp%2Fjsp-demo6.jsp)
```jsp
<%@ page contentType="text/html;charset=UTF-8" session="true" language="java" %>
<%@ page import="com.demo.entity.UserInfoEntity" %>
<html>
<head>
    <title>JSP语法测试</title>
</head>
<body>
    <%
        UserInfoEntity userInfo = new UserInfoEntity();
        userInfo.setUserId(1123);
        userInfo.setUserName("张三");
        session.setAttribute("user", userInfo);
    %>
    <h1>用户个人资料</h1>
    <p>用户ID: ${user.userId}</p>
    <p>用户名: ${user.userName}</p>
</body>
</html>
```

执行结果：

![img_7.png](images/img_7.png)

> 默认情况下，jsp内置session会话对象为true,如果我们指定`<%@ page  session="false"%>`后，则不能直接使用session对象。此时编译器会报错，如下：

![img_8.png](images/img_8.png)

> 同时访问会抛出异常。

![img_9.png](images/img_9.png)

###### 指定处理当前页面异常的另一个JSP页面的相对路径
当服务器发生异常时，则直接输出异常的堆栈信息。生产上则十分不友好，因此我们可以根据响应码指定失败后的处理页面。

[500.jsp](src%2Fmain%2Fwebapp%2F500.jsp)
```jsp
<%@ page import="java.io.PrintWriter" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%-- 指定页面是失败页--%>
<%@ page isErrorPage="true" %>
<html>
<head>
    <title>500错误页面</title>
</head>
<body>

<pre>
<%
    PrintWriter printWriter = response.getWriter();
    exception.printStackTrace(printWriter);
%>

服务器繁忙。

失败信息：<%= exception.getMessage()%>
</pre>
</body>
</html>
```

[jsp-demo7.jsp](src%2Fmain%2Fwebapp%2Fjsp-demo7.jsp)
```jsp
<%@ page contentType="text/html;charset=UTF-8" session="false" language="java" %>
<%-- 指定失败时的处理路径 --%>
<%@ page errorPage="500.jsp" %>
<html>
<head>
    <title>JSP语法测试</title>
</head>
<body>
    <%
        // 设置运行时异常
        int i = 1 / 0;
    %>
</body>
</html>
```

访问结果：

![img_10.png](images/img_10.png)

> 当页面出现异常时，会根据响应码，转发到对应处理的失败页面中进行处理。当然如果要输出错误的堆栈信息，JSP页面是不能直接输出堆栈信息的，需要设置`<%@ page isErrorPage="true" %>`指定页面是失败页面即可。

> 注意： `response.getWriter()和out.write()的区别`

| 项目 | `response.getWriter()` | `out.write()` |
| --- | --- | --- |
| **类型** | 返回`PrintWriter`对象 | JSP页面中隐式的`JspWriter`对象 |
| **获取方式** | 通过`HttpServletResponse`接口的`getWriter()`方法获取 | 作为JSP页面的隐式对象直接可用 |
| **编码处理** | 可以明确设置字符编码（通过`response.setCharacterEncoding()`或`response.setContentType()`） | 编码通常继承自JSP页面的页面指令设置 |
| **使用场景** | 适合在Servlet或复杂的JSP页面中，需要直接控制响应输出时使用 | 适合在标准的JSP页面开发中，利用JSP的标签库和EL表达式进行输出 |
| **缓冲机制** | 没有特定的缓冲机制，但可以通过设置响应的缓冲来管理 | 通常与JSP页面的内容缓冲机制集成，有助于在处理大型输出时提高性能 |
| **示例代码** | `PrintWriter out = response.getWriter();out.println("Hello, World!");` | `<%   out.println("Hello, World!");%>` |

>  * 在tomcat服务器真正给客户端做出响应之前，会先找response缓冲区数据，再找out缓冲区数据。
>  * response.getWriter()数据输出永远在out.write()之前

###### 包含其他文件

[common.jsp](src%2Fmain%2Fwebapp%2Fcommon.jsp)
```jsp
<%@ page contentType="text/html;charset=UTF-8" session="false" language="java" %>
<h1>这个是一个通用页面</h1>

```

[jsp-demo8.jsp](src%2Fmain%2Fwebapp%2Fjsp-demo8.jsp)
```jsp
<%@ page contentType="text/html;charset=UTF-8" session="false" language="java" %>
<html>
<head>
<title>JSP语法测试</title>
</head>
<body>
<h1>11111</h1>
<%@ include file="common.jsp"%>
<h1>22222</h1>
<%@ include file="common.jsp"%>
<h1>33333</h1>
<%@ include file="common.jsp"%>
</body>
</html>
```

![img_11.png](images/img_11.png)

> 需要注意的是include导入页面，在哪里导入则在哪里使用。

### JSP隐含对象
以下是以表格形式展示的JSP支持的九个内置对象的名称、作用、说明、简单示例及注意事项：

| 内置对象名称 | 作用 | 说明 | 简单示例 | 注意事项                       |
| --- | --- | --- | --- |----------------------------|
| request | 用于获取客户端的请求信息 | 可以通过该对象获取到用户提交的数据，以及浏览器的其它信息 | String userName = request.getParameter("username"); | -                          |
| response | 用于向客户端发送响应 | 可以用于设置响应头信息、设置响应的MIME类型等 | response.setContentType("text/html"); | -                          |
| pageContext | 提供了对其他八大隐含对象的统一访问 | 它包含了其他的八大对象，通过它可以访问其他八大对象 | String encoding = pageContext.findPage("page.encoding"); | 线程安全、只读、不可序列化、容器管理         |
| session | 用于跟踪用户的会话信息 | 可以在多个页面之间共享数据，以及标记用户的会话状态 | session.setAttribute("username", userName); | -                          |
| application | 表示服务器启动后就创建的一个对象 | 它在整个Web应用运行期间都存在，可以用来存储整个Web应用共享的数据 | application.setAttribute("userList", userList); | -                          |
| out | 用于向客户端输出内容 | 它是JSP页面向客户端发送响应的出口，可以用于输出HTML、XML等数据 | out.println("Hello, World!"); | 通常与<%= ... %>标记一起使用 |
| config | 用于获取Servlet的初始化参数 | 可以用于获取配置在web.xml中的参数值 | String dataBaseURL = config.getInitParameter("dataBaseURL"); | -                          |
| page | 代表当前JSP页面的实例 | 可以用于在页面中跳转或执行Java代码 | pageContext.setAttribute("message", "Hello from page object!"); | -                          |
| exception | 用于处理JSP页面中的异常 | 当JSP页面抛出异常时，可以通过该对象来获取异常信息（注意：该对象只在错误页面中可用） | try { // some code } catch (Exception e) { exception.printStackTrace(); } | 在正常JSP页面中使用会编译错误           |

**注意事项总结**：

1. JSP内置对象是线程安全的，可以在多线程环境中安全地使用。
2. JSP内置对象通常是只读的，不能被修改。
3. JSP内置对象是不可序列化的，不能被序列化和反序列化。
4. JSP内置对象是容器管理的，由JSP容器创建和销毁，开发者无需关心其生命周期。

## 什么是EL表达式
EL表达式（Expression Language）是JSP 2.0规范中加入的内容，它主要用于在Java Web应用中访问和操作数据，使得JSP页面能够摆脱Java代码块和JSP表达式，实现代码的简化。
**语法**：`${表达式}`
**注意**：
   1. jsp默认支持el表达式的。如果要忽略el表达式
   2. 设置jsp中page指令中：isELIgnored="true" 忽略当前jsp页面中所有的el表达式
   3. `\${表达式}` ：忽略当前这个el表达式

### 运算符的使用
运算符使得EL表达式在JSP页面中能够执行各种基本运算和判断操作，从而简化了页面开发。

| 运算符 | 使用方式 | 作用 | 简单案例 |
| --- | --- | --- | --- |
| **算术运算符** | `${A + B}, ${A - B}, ${A * B}, ${A / B}, ${A % B} 或 ${A mod B}` | 进行加、减、乘、除和求余运算 | `${5 + 3}` 结果为 8，`${10 % 3}` 结果为 1 |
| **关系运算符** | `${A == B} 或 ${A eq B}, ${A != B} 或 ${A ne B}, ${A < B} 或 ${A lt B}, ${A > B} 或 ${A gt B}, ${A <= B} 或 ${A le B}, ${A >= B} 或 ${A ge B}` | 比较两个表达式的大小或是否相等 | `${5 == 3}` 结果为 false，`${5 > 3}` 结果为 true |
| **逻辑运算符** | `${A && B} 或 ${A and B}, ${A \|\| B} 或 ${A or B}, ${!A} 或 ${A not B}`（注意：not是与单一布尔值使用的） | 进行逻辑与、逻辑或和逻辑非运算 | `${true && false}` 结果为 false，`${true \|\| false}` 结果为 true |
| **Empty运算符** | `${empty A}` | 判断EL表达式中的对象或变量是否为空或null | `${empty param.name}` 如果请求参数中没有name，则结果为true |
| **条件运算符** | `${A ? B : C}` | 根据条件A的真假，返回B或C的值 | `${param.age > 18 ? "Adult" : "Minor"}` 根据年龄判断是否为成年人 |
| **点运算符（.）** | `${A.B}` | 访问JavaBean的属性或Map类型的值 | `${user.name}` 访问user对象的name属性 |
| **方括号运算符（[]）** | `${A[B]}` | 访问数组或列表的元素，或包含特殊符号的属性名 | `${booklist[0].price}` 访问booklist列表的第一个元素的price属性 |
| **()运算符** | `${(A + B) * C}` | 改变运算优先级 | `${(2 + 3) * 2}` 结果为10，而`${2 + 3 * 2}` 结果为8 |

**注意**：

* 在使用EL表达式时，需要注意数据类型转换。EL会自动进行数据类型转换，但某些情况下可能会引发异常，如使用`+`运算符连接两个不能转换为数值型的字符串。
* EL表达式中的变量和属性名区分大小写。
* EL表达式主要用于简化JSP页面中的代码，提高可读性和可维护性。

[jsp-demo9.jsp](src%2Fmain%2Fwebapp%2Fjsp-demo9.jsp)
```jsp
<%@ page import="com.demo.entity.UserInfoEntity" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSP语法测试</title>
</head>
<body>

<table>
    <tr>
        <td><b>算数运算符</b></td>
    </tr>

    <tr>
        <td>1 + 3: ${1 + 3}</td>
        <td>1 - 3: ${1 - 3}</td>
    </tr>
    <tr>
        <td>1 + 3: ${1 + 3}</td>
        <td>1 - 3: ${1 - 3}</td>
    </tr>
    <tr>
        <td>1 * 3: ${1 * 3}</td>
        <td>1 / 3: ${1 / 3}</td>
    </tr>
    <tr>
        <td>1 % 3: ${1 % 3}</td>
        <td>1 mod 3: ${1 mod 3}</td>
    </tr>

    <tr>
        <td><b>关系运算符</b></td>
    </tr>
    <%
        session.setAttribute("A", "123");
        session.setAttribute("B", "123");
    %>

    <tr>
        <td>A == B: ${A == B}</td>
        <td>A eq B: ${A eq B}</td>
    </tr>
    <tr>
        <td>A != B: ${A != B}</td>
        <td>A ne B: ${A ne B}</td>
    </tr>

    <tr>
        <td><b>逻辑运算符</b></td>
    </tr>
    <%
        session.setAttribute("A", true);
        session.setAttribute("B", false);
    %>

    <tr>
        <td>A && B: ${A && B}</td>
        <td>A and B: ${A and B}</td>
    </tr>
    <tr>
        <td>A || B: ${A || B}</td>
        <td>A or B: ${A or B}</td>
    </tr>

    <tr>
        <td><b>逻辑运算符</b></td>
    </tr>
    <%
        UserInfoEntity userInfoEntity = new UserInfoEntity();
        userInfoEntity.setUserName("zhangsan");
        session.setAttribute("user", userInfoEntity);
    %>

    <tr>
        <td>\${empty user}: ${empty user}</td>
        <td>\${empty user.userId}: ${empty user.userId}</td>
    </tr>
    <tr>
        <td>\${empty user.userName}: ${empty user.userName}</td>
        <td></td>
    </tr>

    <tr>
        <td><b>条件运算符</b></td>
    </tr>
    <%
        int age = 18;
        session.setAttribute("age", age);
    %>

    <tr>
        <td>\${A ? B : C}: ${age >= 18 ? "成年了" : "未成年"}</td>
    </tr>

    <tr>
        <td><b>方括号运算符（[]）</b></td>
    </tr>
    <%
        List<String> data = Arrays.asList("1", "2", "3");
        session.setAttribute("data", data);
    %>

    <tr>
        <td>data[0]: ${data[0]}</td>
        <td>data[1]: ${data[1]}</td>
    </tr>
    <tr>
        <td>data[2]: ${data[2]}</td>
        <td></td>
    </tr>
</table>
</body>
</html>
```

执行结果：

![img_12.png](images/img_12.png)

### EL表达式中的域对象
EL表达式中的域对象是指用于存储和访问数据的作用域对象。以下是JSP中EL表达式的域对象的详细介绍.

| 域对象 | 别名 | 作用范围 | 访问方式 | 示例 |
| --- | --- | --- | --- | --- |
| PageContext | pageContext / pageScope | 当前JSP页面 | 用于在当前页面中存储和访问数据 | `${pageScope.username}` |
| HttpServletRequest | request / requestScope | 请求范围 | 用于在一次请求中存储和访问数据 | `${requestScope.paramValue}` |
| HttpSession | session / sessionScope | 会话范围 | 用于在一次会话中存储和访问数据 | `${sessionScope.sessionData}` |
| ServletContext | application / applicationScope | 全局范围 | 用于在整个Web应用中存储和访问数据 | `${applicationScope.globalData}` |

#### 说明：

1. **PageContext**：pageContext对象是一个特殊的对象，它提供了对JSP页面中其他内置对象的访问，包括request、response、session、application等。pageScope别名用于访问存储在PageContext中的数据。
2. **HttpServletRequest**：request对象代表了一次客户端请求，requestScope别名用于访问存储在HttpServletRequest中的数据。这些数据在一次请求中是有效的。
3. **HttpSession**：session对象代表了一次用户会话，sessionScope别名用于访问存储在HttpSession中的数据。这些数据在一次会话中是有效的，直到会话结束或数据被显式删除。
4. **ServletContext**：application对象代表了整个Web应用，applicationScope别名用于访问存储在ServletContext中的数据。这些数据在整个Web应用的生命周期内都是有效的。

#### 访问方式：

* 使用EL表达式的语法`${}`来访问域对象中的数据。
* 在`${}`中，可以使用域对象的别名加上点（.）或方括号（[]）来访问数据。例如，`${requestScope.paramValue}`或`${requestScope['paramValue']}`。

#### 示例：

* 假设在request对象中存储了一个名为"username"的数据，可以使用`${requestScope.username}`来访问这个数据。
* 假设在session对象中存储了一个名为"sessionData"的数据，可以使用`${sessionScope.sessionData}`来访问这个数据。

#### 注意事项：

* EL表达式会自动进行类型转换，例如将字符串转换为整数或浮点数进行算术运算。
* 在使用EL表达式时，需要注意作用域对象的优先级。如果没有指定作用域对象别名，EL表达式会按照pageContext、request、session、application的顺序依次查找数据。

### 获取值
el表达式只能从域对象中获取值
1. 语法：`${域名称.键名}`：从指定域中获取指定键的值
2. `${键名}`：表示依次从最小的域中查找是否有该键对应的值，直到找到为止,EL表达式会按照pageContext、request、session、application的顺序依次查找数据。
3. 获取对象、List集合、Map集合的值
   1. 对象：`${域名称.键名.属性名}`,本质上会去调用对象的getter方法
   2. List集合：`${域名称.键名[索引]}`
   3. Map集合：
     + `${域名称.键名.key名称}`
     + `${域名称.键名["key名称"]}`

[jsp-demo10.jsp](src%2Fmain%2Fwebapp%2Fjsp-demo10.jsp)
```jsp
<%@ page import="com.demo.entity.UserInfoEntity" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSP语法测试</title>
</head>
<body>
    <%
        pageContext.setAttribute("name", "pageContext-name");
        request.setAttribute("name", "request-name");
        session.setAttribute("name", "session-name");
        application.setAttribute("name", "application-name");

        request.setAttribute("age", "request-age");
        session.setAttribute("age", "session-age");
        application.setAttribute("age", "application-age");

        session.setAttribute("id", "session-id");
        application.setAttribute("id", "application-id");

        application.setAttribute("account", "application-account");
    %>
    <b>指定域名获取:\${pageScope.name}</b>：${pageScope.name}<br/>
    <b>指定域名获取:\${requestScope.name}</b>：${requestScope.name}<br/>
    <b>指定域名获取:\${sessionScope.name}</b>：${sessionScope.name}<br/>
    <b>指定域名获取:\${applicationScope.name}</b>：${applicationScope.name}<br/><br/>

    <h3>测试作用域的范围：pageContext、request、session、application</h3>
    <b>\${键名}方式获取:\${name}</b>：${name}<br/>
    <b>\${键名}方式获取:\${age}</b>：${age}<br/>
    <b>\${键名}方式获取:\${id}</b>：${id}<br/>
    <b>\${键名}方式获取:\${account}</b>：${account}<br/><br/>

    <%
        UserInfoEntity userInfoEntity = new UserInfoEntity();
        userInfoEntity.setUserName("张三");
        request.setAttribute("user", userInfoEntity);
    %>
    <b>获取对象user中name属性:\${requestScope.user.userName}</b>：${requestScope.user.userName}<br/><br/>

    <%
        List<UserInfoEntity> list = new ArrayList<>();
        list.add(userInfoEntity);
        request.setAttribute("list", list);
    %>
    <b>获取List对象user中name属性:\${requestScope.list[0].userName}</b>：${requestScope.list[0].userName}<br/><br/>

    <%
        Map<String,UserInfoEntity> map = new HashMap<>();
        map.put("user",userInfoEntity);
        request.setAttribute("userMap", map);
    %>
    <b>获取Map对象user中name属性:\${requestScope.userMap.user.userName}</b>：${requestScope.userMap.user.userName}<br/>
    <b>获取Map对象user中name属性:\${requestScope.userMap.user[userName]}</b>：${requestScope.userMap.user[userName]}<br/>
</body>
</html>
```

执行结果：

![img_13.png](images/img_13.png)

### 隐式对象

EL表达式使用一组隐式对象来简化常见任务。以下是JSP中EL表达式的隐式对象的详细介绍：

| 隐式对象 | 描述 | 示例 |
| :--: | :--: | :--: |
| `pageContext` | 提供对JSP页面上下文对象的访问。允许访问各种命名空间和页面范围内的属性。 | `${pageContext.request.contextPath}` 获取应用程序的上下文路径 |
| `pageScope` | 允许访问页面范围内的属性。 | `${pageScope.someAttribute}` 访问名为`someAttribute`的页面范围属性 |
| `requestScope` | 允许访问请求范围内的属性。 | `${requestScope.param1}` 访问请求参数`param1`的值 |
| `sessionScope` | 允许访问会话范围内的属性。 | `${sessionScope.user}` 访问会话范围内的`user`属性 |
| `applicationScope` | 允许访问应用程序范围内的属性。 | `${applicationScope.appConfig}` 访问应用程序范围内的`appConfig`属性 |
| `param` | 允许访问请求参数。 | `${param.username}` 访问请求参数`username`的值 |
| `paramValues` | 允许访问请求参数值数组（适用于多值参数）。 | `${paramValues.hobbies[0]}` 访问请求参数`hobbies`的第一个值 |
| `header` | 允许访问请求头。 | `${header.host}` 访问请求头`Host`的值 |
| `headerValues` | 允许访问请求头值数组（适用于多值头）。 | `${headerValues.Accept[0]}` 访问请求头`Accept`的第一个值 |
| `cookie` | 允许访问请求中的cookie。 | `${cookie.sessionId.value}` 访问名为`sessionId`的cookie的值 |
| `initParam` | 允许访问Servlet上下文初始化参数。 | `${initParam.defaultTheme}` 访问名为`defaultTheme`的上下文初始化参数 |
| `scope` | 一个复合对象，允许在指定的范围内查找属性。 | `${scope.someScope.someAttribute}` 在`someScope`范围内查找名为`someAttribute`的属性（`someScope`可以是`page`、`request`、`session`或`application`） |

[jsp-demo11.jsp](src%2Fmain%2Fwebapp%2Fjsp-demo11.jsp)
```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSP语法测试</title>
</head>
<body>
    <b>获取动态获取虚拟目录\${pageContext.request.contextPath}</b>：${pageContext.request.contextPath}<br/><br/>

    <b>获取name参数\${param.name}</b>：${param.name}<br/><br/>

    <% response.addCookie(new Cookie("name","lisi")); %>
    <b>获取访问请求头user-agent \${header["user-agent"]}</b>：${header["user-agent"]}<br/><br/>

    <b>获取访问cookie \${cookie.name.value}</b>：${cookie.name.value}<br/><br/>
</body>
</html>
```

执行结果：

![img_14.png](images/img_14.png)

## JSTL表达式
JavaServer Pages Standard Tag Library (JSTL) 是一组用于在JSP页面中简化常见编程任务的自定义标签库。JSTL 提供了用于处理XML数据、数据库访问、国际化、格式化输出、SQL查询、条件语句和循环等功能的标签。JSTL 标签库主要包括以下五个核心标签库：

1. Core Tags：核心标签库，用于控制流（如条件语句和循环）以及国际化。 
2. SQL Tags：SQL标签库，用于简化数据库操作。 
3. XML Tags：XML标签库，用于处理XML数据。 
4. JSTL Functions：JSTL函数库，提供了一些常用的字符串操作函数。 
5. FMT Tags：格式化标签库，用于格式化输出（如日期、时间、数字等）。

以下是关于JSTL核心标签库中一些常用表达式的详细介绍：

| 标签库 | 标签 | 描述 | 示例 | 注意事项 |
| --- | --- | --- | --- | --- |
| **Core Tags** | `<c:set>` | 设置变量的值 | `<c:set var="myVar" value="Hello, JSTL!" />` | 确保变量名在作用域中是唯一的。 |
|  | `<c:remove>` | 移除变量 | `<c:remove var="myVar" />` | 移除后，变量将不再可用。 |
|  | `<c:out>` | 输出变量的值 | `<c:out value="${myVar}" escapeXml="true" />` | 使用`escapeXml="true"`来防止XSS攻击。 |
|  | `<c:if>` | 条件判断 | `<c:if test="${someCondition}">True</c:if>` | 确保条件表达式是有效的EL表达式。 |
|  | `<c:choose>`, `<c:when>`, `<c:otherwise>` | 多路条件判断 | 见上文 | 类似于Java中的`switch-case`语句。 |
|  | `<c:forEach>` | 循环遍历集合 | `<c:forEach var="item" items="${myList}">${item}</c:forEach>` | 可以遍历数组、List等集合。 |
|  | `<c:forTokens>` | 遍历由分隔符分隔的字符串 | `<c:forTokens var="token" items="apple,banana,cherry" delims=",">${token}</c:forTokens>` | 使用`delims`属性指定分隔符。 |
|  | `<c:redirect>` | 重定向到另一个URL | `<c:redirect url="newPage.jsp" />` | 重定向后，当前页面的内容将不会被发送。 |
|  | `<c:url>` | 构建URL | `<c:url var="myUrl" value="somePage.jsp"><c:param name="param1" value="value1" /></c:url>` | 与`<c:param>`一起使用添加查询参数。 |
|  | `<c:param>` | 在URL中添加查询参数 | `<c:param name="param1" value="value1" />` | 通常与`<c:url>`或`<c:redirect>`一起使用。 |
| **SQL Tags** | - | 简化数据库操作 | - | 在现代应用中，建议使用JPA、Hibernate等持久层框架。 |
| **XML Tags** | - | 处理XML数据 | - | 需要适当的XML解析器和数据源。 |
| **JSTL Functions** | - | 字符串操作函数 | `<c:out value="${fn:length(myString)}" />` | 需要导入`http://java.sun.com/jsp/jstl/functions`标签库。 |
| **FMT Tags** | `<fmt:formatDate>` | 格式化日期 | `<fmt:formatDate value="${now}" pattern="yyyy-MM-dd" />` | 需要导入`http://java.sun.com/jsp/jstl/fmt`标签库。 |
|  | 其他格式化标签 | 格式化时间、数字等 | - | 根据需要选择合适的格式化标签。 |

**注意事项**：

1. **变量作用域**：在使用`<c:set>`设置变量时，要确保变量名在作用域中是唯一的，以避免冲突。
2. **安全性**：在使用`<c:out>`输出变量值时，建议使用`escapeXml="true"`来防止XSS攻击。
3. **条件表达式**：在`<c:if>`和`<c:when>`等条件判断标签中，确保条件表达式是有效的EL表达式。
4. **重定向**：使用`<c:redirect>`进行重定向时，当前页面的内容将不会被发送，而是直接跳转到指定的URL。
5. **XML处理**：在使用XML标签库时，需要确保有适当的XML解析器和数据源。
6. **持久层框架**：在现代应用中，建议使用JPA、Hibernate等持久层框架来处理数据库操作，而不是直接使用JSTL SQL标签库。
7. **标签库导入**：在使用JSTL标签库之前，需要在JSP页面的顶部声明相应的标签库。

#### 添加MAVEN依赖
如需使用JSTL，则在pom.xml添加如下依赖
```xml
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>jstl</artifactId>
    <version>1.2</version>
</dependency>
```

#### 重定向
[jsp-demo12.jsp](src%2Fmain%2Fwebapp%2Fjsp-demo12.jsp)
```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>JSTL语法测试</title>
</head>
<body>
    <c:redirect url="hello.jsp">
        <c:param name="name">张三</c:param>
        <c:param name="age">23</c:param>
    </c:redirect>
</body>
</html>
```


#### Core Tags
在使用JSTL标签库之前，需要在JSP页面的顶部声明相应的标签库。例如，要使用核心标签库：

```jsp
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
```

> 注意，前缀prefix="c"的值可以任意指定（通常情况下指定为c），如果指定为其他则该页面的标签使用前缀就要指定的标签。

[jsp-demo13.jsp](src%2Fmain%2Fwebapp%2Fjsp-demo13.jsp)
```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="myCore" %>
<html>
<head>
    <title>JSTL语法测试</title>
</head>
<body>
    <myCore:redirect url="hello.jsp">
        <myCore:param name="name">张三</myCore:param>
        <myCore:param name="age">23</myCore:param>
    </myCore:redirect>
</body>
</html>
```

##### `<c:set>`
用于设置变量的值。
```jsp
<c:set var="myVar" value="Hello, JSTL!" />
```

##### `<c:remove>`
用于移除变量。
```jsp
<c:remove var="myVar" />
```

##### `<c:out>`
用于输出变量的值，并可以选择是否进行HTML转义。
```jsp
<c:out value="${myVar}" escapeXml="true" />
```

##### `<c:if>`
用于条件判断。
```jsp
<c:if test="${someCondition}">
    This is true.
</c:if>
```

##### `<c:choose>`, `<c:when>`, `<c:otherwise>`
用于多路条件判断。
```jsp
<c:choose>
    <c:when test="${condition1}">
        Condition 1 is true.
    </c:when>
    <c:when test="${condition2}">
        Condition 2 is true.
    </c:when>
    <c:otherwise>
        None of the conditions are true.
    </c:otherwise>
</c:choose>
```

##### `<c:forEach>`
用于循环遍历集合（如数组、List等）。
```jsp
<c:forEach var="item" items="${myList}">
    ${item}
</c:forEach>
```

##### `<c:forTokens>`
用于遍历由特定分隔符分隔的字符串。
```jsp
<c:forTokens var="token" items="apple,banana,cherry" delims=",">
    ${token}
</c:forTokens>
```

##### `<c:redirect>`
用于重定向到另一个URL。
```jsp
<c:redirect url="newPage.jsp" />
```

##### `<c:url>`
用于构建URL，并可选择添加查询参数。
```jsp
<c:url var="myUrl" value="somePage.jsp">
    <c:param name="param1" value="value1" />
</c:url>
<a href="${myUrl}">Click here</a>
```

##### `<c:param>`
用于在URL中添加查询参数，通常与`<c:url>`或`<c:redirect>`一起使用。
```jsp
<c:param name="param1" value="value1" />
```

#### JSTL Functions

JSTL函数库提供了一些实用的字符串操作函数，如字符串长度、子字符串截取等。

```jsp
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:out value="${fn:length(myString)}" />
```

#### FMT Tags

JSTL格式化标签库用于格式化日期、时间、数字等。

```jsp
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:formatDate value="${now}" pattern="yyyy-MM-dd" />
```

## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)