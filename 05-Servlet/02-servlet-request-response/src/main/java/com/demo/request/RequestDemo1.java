package com.demo.request;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;

/**
 * 请求测试
 *
 * @author Anna.
 * @date 2025/2/27 10:48
 */
@WebServlet("/req1")
public class RequestDemo1 extends GenericServlet {
    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        System.out.println("继承GenericServlet实现service()方法调用");
    }
}
