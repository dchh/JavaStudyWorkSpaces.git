package com.demo.request;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * 测试下载文件
 *
 * @author Anna.
 * @date 2025/2/27 10:48
 */
@WebServlet("/resp")
public class ResponseDemo extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // 加载本地路径文件
        ServletContext servletContext = req.getServletContext();
        // 获取资源路径
        String realPath = servletContext.getRealPath("/img.png");
        File file = new File(realPath);
        if (!file.exists()) {
            // 获取输出流
            try (ServletOutputStream outputStream = resp.getOutputStream();) {
                // 设置响应头
                resp.setContentType("text/html;charset=UTF-8");
                outputStream.write("文件不存在".getBytes(StandardCharsets.UTF_8));
            }
            return;
        }
        // 获取输出流
        try (ServletOutputStream outputStream = resp.getOutputStream(); InputStream inputStream = new FileInputStream(file);) {
            // 获取MIME类型
            String mimeType = getServletContext().getMimeType(realPath);
            if (mimeType == null) {
                // 设置默认MIME类型
                mimeType = "application/octet-stream";
            }

            // 设置响应内容类型
            resp.setContentType(mimeType);
            resp.setContentLength((int) file.length());

            // 设置Content-Disposition头，指定下载的文件名
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"", URLEncoder.encode("图片_", "UTF-8") + file.getName());
            resp.setHeader(headerKey, headerValue);
            byte[] buffer = new byte[4096];
            int bytesRead = -1;

            // 将文件内容写入响应输出流
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
        }

    }
}
