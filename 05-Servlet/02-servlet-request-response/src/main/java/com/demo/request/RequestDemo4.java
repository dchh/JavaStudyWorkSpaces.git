package com.demo.request;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Enumeration;

/**
 * 测试Request及Resopnse的对象
 *
 * @author Anna.
 * @date 2025/2/27 10:48
 */
@WebServlet("/req4")
public class RequestDemo4 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置请求编码格式
        req.setCharacterEncoding("UTF-8");
        // 获取请求行 GET https://127.0.0.1:8080/demo2/req4?name=123&age=23 HTTP/1.1
        System.out.println("=======请求行======");
        // 获取请求方式 GET
        System.out.println("获取请求方式: " + req.getMethod());
        // 获取请求路径URL http://127.0.0.1:8080/demo/req4
        System.out.println("获取请求路径URL: " + req.getRequestURL());
        // 获取请求路径URI /demo/req4
        System.out.println("获取请求路径URI: " + req.getRequestURI());
        // 获取虚拟目录 /demo
        System.out.println("获取虚拟目录: " + req.getContextPath());
        // 获取Servlet路径 /req4
        System.out.println("获取Servlet路径: " + req.getServletPath());
        // 获取获取GET方式请求参数 name=%E5%BC%A0%E4%B8%89&age=23
        System.out.println("获取获取GET方式请求参数: " + req.getQueryString());
        // 获取协议及版本 HTTP/1.1
        System.out.println("获取协议及版本: " + req.getProtocol());
        // 获取客户端地址 127.0.0.1
        System.out.println("获取客户端地址: " + req.getRemoteAddr());
        // 获取客户端主机名 127.0.0.1
        System.out.println("获取客户端主机名: " + req.getRemoteHost());

        System.out.println("=======请求头======");
        // 获取所有请求头
        Enumeration<String> headerNames = req.getHeaderNames();
        System.out.println("获取所有请求头: " );
        while (headerNames.hasMoreElements()) {
            System.out.print(headerNames.nextElement() + " ");
        }
        System.out.println("");
        // 根据名称获取请求头数据
        System.out.println("根据名称获取请求头数据-user-agent: " + req.getHeader("user-agent"));

        System.out.println("=======请求体======");
        // 获取请求体
        // 获取所有请求参数KEY
        Enumeration<String> parameterNames = req.getParameterNames();
        System.out.println("获取所有请求参数KEY-通过key获取参数值: " );
        while (parameterNames.hasMoreElements()) {
            String key = parameterNames.nextElement();
            System.out.println(key + ": " + req.getParameter(key));
        }
        // 根据请求参数名称获取请求参数数据
        System.out.println("根据请求参数名称获取请求参数数据-name: " + req.getParameter("name"));

        System.out.println("===通过IO流获取请求体===");
        String reqData = "";
        String line = "";
        BufferedReader reader = req.getReader();
        while ((line = reader.readLine()) != null) {
            reqData += line;
        }
        System.out.println("获取请求体: " + reqData);
    }
}
