# Servlet请求与响应

## Servlet的继承体系

Servlet的继承体系主要由以下几个类组成：

+ **Servlet接口**

    定义：Servlet接口是所有Servlet类的根接口。它定义了Servlet必须实现的方法，包括init()、service()、destroy()等。

    特点：Servlet接口是一个抽象接口，开发者通常不会直接实现它，而是<font color="red">继承其实现类或子类</font>。

+ **GenericServlet抽象类**

  定义：GenericServlet抽象类实现了Servlet接口，并为Servlet提供了一些通用的功能。

  特点：GenericServlet类实现了Servlet接口中的init()、destroy()等方法，但保留了service()方法作为抽象方法，供其子类去实现。此外，GenericServlet类还实现了ServletConfig接口，提供了对Servlet配置信息的访问。

  **测试案例：**

  [RequestDemo1.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2Frequest%2FRequestDemo1.java)
```java
package com.demo.request;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import java.io.IOException;

@WebServlet("/req1")
public class RequestDemo1 extends GenericServlet {
    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        System.out.println("继承GenericServlet实现service()方法调用");
    }
}

```
  **执行结果：**

  ![img.png](images/img.png)

+ **HttpServlet类**

  定义：HttpServlet类继承自GenericServlet类，专门用于处理HTTP请求和响应。

  特点：HttpServlet类实现了Servlet接口中的service()方法，该方法根据请求的类型（如GET、POST等）调用相应的doXXX()方法（如doGet()、doPost()等）来处理请求。此外，HttpServlet类还提供了对HTTP请求和响应的封装，使得开发者可以更方便地处理HTTP请求和响应。

  **测试案例：**

  [RequestDemo2.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2Frequest%2FRequestDemo2.java)
```java
package com.demo.request;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/req2")
public class RequestDemo2 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("继承HttpServlet类-重写doGet()/doPost()方法调用");
    }
}

```
  **执行结果：**

  ![img_1.png](images/img_1.png)

## HTTP协议

### 什么是HTTP协议
HTTP（HyperText Transfer Protocol，超文本传输协议）是一种用于分布式、协作式和超媒体信息系统的应用层协议。它是Web浏览器和Web服务器之间通信的基础协议，是互联网上应用最为广泛的一种网络协议。

### HTTP协议请求方法介绍

| 方法名称 | 作用 | 注意事项 |
| --- | --- | --- |
| **GET** | 请求指定的页面信息，并返回实体主体。通常用于请求数据，而不是提交数据。 | 1. **幂等性**：多次执行相同的GET请求应该总是返回相同的结果。 |
| | | 2. **缓存性**：响应可以被缓存。 |
| | | 3. **可收藏性**：GET请求的结果可以被收藏为书签。 |
| | | 4. **安全性**：GET请求可以被缓存、存储和无限次重放，因此不应该用于传输敏感数据或执行具有副作用的操作。 |
| | | 5. **参数传递**：请求的数据会附加在URL之后（即查询字符串）。 |
| **POST** | 向指定资源提交数据进行处理请求（例如提交表单或者上传文件）。数据被包含在请求体中。POST请求可能会导致新的资源的建立和/或已有资源的修改。 | 1. **非幂等性**：多次执行相同的POST请求可能会导致不同的结果（例如，每次都会创建一个新的资源）。 |
| | | 2. **安全性**：POST请求不会被浏览器缓存，也不会保留在浏览器历史记录中，因此相对GET请求更安全，但仍需注意数据加密和验证。 |
| | | 3. **请求体**：数据包含在请求体中，可以传输大量数据。 |
| **PUT** | 从客户端向服务器传送的数据取代指定的文档的内容。PUT请求通常用于更新资源。 | 1. **幂等性**：多次执行相同的PUT请求应该总是返回相同的结果（即资源状态）。 |
| | | 2. **安全性**：与POST请求类似，PUT请求也需要注意数据加密和验证。 |
| **DELETE** | 请求服务器删除指定的页面。DELETE请求通常用于删除资源。 | 1. **幂等性**：多次执行相同的DELETE请求应该总是返回相同的结果（即资源被删除）。 |
| | | 2. **安全性**：执行删除操作需要谨慎，确保不会误删重要数据。 |
| **HEAD** | 与GET方法一样，只返回响应头信息，不返回实际数据。通常用于获取报头信息，以查看资源的修改日期、内容类型等。 | 1. **无响应体**：HEAD请求不会返回响应体，仅返回响应头。 |
| **OPTIONS** | 允许客户端查看服务器的性能。可以用来查询服务器支持的HTTP请求方法。会返回服务器支持的通信选项。 | 1. **预检请求**：在进行跨域请求之前，浏览器可能会先发送一个OPTIONS请求，以检查服务器是否允许跨域访问。 |
| **TRACE** | 回显服务器收到的请求，主要用于测试或诊断。客户端发起一个TRACE请求时，服务器会返回之前发送的请求信息，有助于查看请求是否在中间环节被篡改。 | 1. **安全性**：TRACE请求可能会暴露敏感信息，因此服务器通常不允许使用TRACE方法。 |
| **CONNECT** | HTTP/1.1协议中预留给能够将连接改为管道方式的代理服务器。通常用于SSL加密服务器的链接（经由非加密的HTTP代理）。 | 1. **代理服务器**：主要用于代理服务器，将连接改为管道方式，以便进行加密通信。 |
| **PATCH** | 部分更新资源，对资源进行局部修改。 | 1. **幂等性**：多次执行相同的PATCH请求应该总是返回相同的结果（即资源状态被局部更新）。 |
| **LINK** | 建立与资源的连接。 | 1. **较少使用**：在HTTP协议中较少使用此方法。 |
| **UNLINK** | 断开连接关系。 | 1. **较少使用**：在HTTP协议中较少使用此方法。 |

### HTTP协议版本介绍

| 版本 | 发行时间 | 特点 | 注意事项 |
| --- | --- | --- | --- |
| **HTTP/0.9** | 1989年 | 1. 仅支持GET方法。 | 1. 这是一个非常早期的版本，现代应用中已不使用。 |
| | | 2. 不支持头部信息。 | 2. 功能非常有限，仅用于简单的请求和响应。 |
| | | 3. 只支持纯文本响应。 | 3. 安全性、扩展性等方面存在严重缺陷。 |
| **HTTP/1.0** | 1996年 | 1. 引入了头部信息。 | 1. 每次连接只能发送一个请求，效率较低。 |
| | | 2. 支持更多的文件类型。 | 2. 需要客户端和服务端进行3次握手建立连接，但连接成功后只能发送一次请求。 |
| | | 3. 增加了状态码。 | 3. 可以通过非标准的connection字段暂时解决多次请求效率低的问题，但行为不一致。 |
| | | 4. 引入了Content-Type字段。 |  |
| **HTTP/1.1** | 1997年1月 | 1. 默认TCP连接不关闭，可以被多个请求复用。 | 1. 同一个TCP连接里面，所有的数据通信是按次序进行的，可能导致“队头堵塞”问题。 |
| | | 2. 引入了管道机制，允许客户端在同一个TCP连接中同时发送多个请求。 | 2. 需要注意连接管理和资源释放。 |
| | | 3. 新增了PUT、PATCH、OPTIONS、DELETE、TRACE等请求方法。 | 3. 增加了Host字段，支持虚拟主机。 |
| | | 4. 引入了Content-Length字段和“分块传输编码”方式。 | 4. 需要处理持久连接和连接超时等问题。 |
| | | 5. 支持持久连接（Connection: keep-alive）。 |  |
| **HTTP/2** | 2015年5月 | 1. 采用二进制协议，提高了传输效率。 | 1. 需要客户端和服务器都支持HTTP/2才能发挥最大性能。 |
| | | 2. 支持多路复用，允许在单个连接上并发处理多个请求和响应。 | 2. 头部信息压缩可能带来安全和隐私问题。 |
| | | 3. 头部信息压缩，减少了传输开销。 | 3. 需要处理流控制、优先级等问题。 |
| | | 4. 引入了服务器推送功能。 | 4. 某些情况下可能增加服务器的负载。 |
| | | 5. 支持更大的请求和响应头。 |  |
| **HTTP/3** | 2022年6月 | 1. 使用QUIC协议替代了TCP协议，提高了传输的可靠性和效率。 | 1. 需要客户端和服务器都支持HTTP/3才能使用。 |
| | | 2. 支持快速重传和拥塞控制机制。 | 2. QUIC协议本身还在不断发展和完善中。 |
| | | 3. 减少了连接建立的延迟和开销。 | 3. 安全性、扩展性等方面需要持续关注。 |
| | | 4. 提供了更好的移动网络和弱网络环境下的性能。 | 4. 可能需要更新网络设备和软件以支持QUIC协议。 |

## 请求消息数据格式（Request）
HTTP协议的请求消息数据格式是客户端向服务器发送请求时所使用的数据结构。
一个完整的HTTP请求消息通常由以下几个部分组成：
请求行（Request Line）、请求头（Request Headers）、空行和请求体（Request Body）。

HTTP协议的请求消息数据格式是客户端向服务器发送请求时所使用的数据结构。一个完整的HTTP请求消息通常由以下几个部分组成：请求行（Request Line）、请求头（Request Headers）、空行和请求体（Request Body）。以下是对这些部分的详细介绍：

### 1. 请求行（Request Line）

请求行是HTTP请求消息的第一行，用于描述客户端的请求方式、请求的资源路径以及使用的HTTP协议版本。请求行由以下三个部分组成，用空格分隔：

+ 请求方法（Request Method）：指定客户端希望对服务器执行的操作。常用的请求方法包括GET、POST、PUT、DELETE等。每种方法都有其特定的用途和语义，例如GET用于请求资源，POST用于提交数据。
+ 请求URL（Request-URI）：指定客户端希望访问的资源的路径。URL通常包括主机名、端口号（如果非默认）、路径和查询字符串。
+ HTTP协议版本（HTTP-Version）：指定客户端使用的HTTP协议版本，例如HTTP/1.1或HTTP/2。不同的协议版本可能支持不同的功能和性能优化。

**示例：**

```
请求方式 请求Url 请求协议/版本
GET https://www.baidu.com HTTP/2
```

### 2. 请求头（Request Headers）

请求头包含了一系列的键值对，用于描述客户端的请求信息、客户端的浏览器信息、缓存控制等。每个请求头字段由名称和值组成，用冒号（:）分隔。请求头字段之间使用回车和换行符（CRLF）分隔。

- **常见的请求头字段**：

| 请求头字段 | 描述 | 示例 | 注意事项 |
| --- | --- | --- | --- |
| **Host** | 指定请求的主机名和端口号（如果端口号不是默认端口号）。 | `Host: www.example.com:8080` | 1. 必需字段，用于服务器区分请求的资源。 |
| | | | 2. 如果端口号是默认端口（如HTTP的80端口），可以省略。 |
| **User-Agent** | 标识发出请求的客户端软件信息，包括浏览器类型、版本、操作系统等。 | `User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3` | 1. 有助于服务器识别客户端类型和版本，进行兼容性处理。 |
| | | | 2. 客户端可以伪造User-Agent字段，因此不能完全依赖它进行安全决策。 |
| **Accept** | 指定客户端能够接受的响应内容类型，如HTML、JSON、XML等。 | `Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8` | 1. `q`参数表示优先级，`q`值越大优先级越高。 |
| | | | 2. 如果没有指定`Accept`字段，服务器通常会返回HTML格式的响应。 |
| **Accept-Language** | 指定客户端首选的语言环境，服务器可以根据这个字段返回不同语言版本的资源。 | `Accept-Language: zh-CN,zh;q=0.9,en;q=0.8` | 1. `q`参数表示优先级，`q`值越大优先级越高。 |
| | | | 2. 客户端可以指定多种语言，服务器应优先返回客户端首选语言的资源。 |
| **Accept-Encoding** | 指定客户端能够接受的响应内容编码格式，如gzip、deflate等。 | `Accept-Encoding: gzip, deflate, br` | 1. 服务器可以使用这些编码格式压缩响应内容，减少传输数据量。 |
| | | | 2. 客户端应支持这些编码格式，以正确解压缩响应内容。 |
| **Connection** | 指定客户端希望与服务器保持连接的方式。 | `Connection: keep-alive` | 1. `keep-alive`表示保持连接，以便在同一个TCP连接上发送和接收多个请求和响应。 |
| | | `Connection: close` | 2. `close`表示关闭连接，每个请求和响应都使用一个新的TCP连接。 |
| | | | 3. HTTP/1.1默认使用`keep-alive`，HTTP/1.0默认使用`close`。 |
| **Cookie** | 包含客户端发送给服务器的cookie信息，用于会话管理、用户跟踪等。 | `Cookie: sessionid=abc123; user=john` | 1. cookie是服务器在客户端存储的小段数据，用于跟踪用户会话。 |
| | | | 2. 客户端在后续请求中会自动发送这些cookie，服务器可以根据cookie识别用户。 |
| **Authorization** | 包含客户端的身份验证信息，如用户名和密码。 | `Authorization: Basic dXNlcjpwYXNzd29yZA==` | 1. `Basic`表示基本身份验证，用户名和密码使用Base64编码。 |
| | | | 2. 客户端应确保身份验证信息的安全性，避免明文传输。 |
| **Content-Type** | 指定请求体的内容类型，如application/json、application/x-www-form-urlencoded等。 | `Content-Type: application/json` | 1. 对于POST和PUT请求，`Content-Type`字段是必需的，用于告诉服务器请求体的格式。 |
| | | | 2. 服务器根据`Content-Type`字段解析请求体内容。 |
| **Content-Length** | 指定请求体的字节长度。 | `Content-Length: 123` | 1. 对于需要发送请求体的请求（如POST和PUT请求），`Content-Length`字段是必需的。 |
| | | | 2. 服务器根据`Content-Length`字段读取请求体内容。 |

> **字段名称不区分大小写**：HTTP请求头字段名称不区分大小写，如`Host`和`host`是等价的。
> **字段值**：字段值通常是可选的，但某些字段（如`Host`、`Content-Length`等）对于某些请求类型是必需的。
> **多个字段值**：某些字段（如`Accept`、`Accept-Language`等）可以指定多个值，使用逗号分隔，并通过`q`参数指定优先级。
> **安全性**：对于包含敏感信息的字段（如`Authorization`），应确保信息的安全性，避免明文传输。
> **兼容性**：不同的服务器和客户端可能对某些字段的支持程度不同，开发时应考虑兼容性。

- **请求头的作用**：

  - 提供客户端的浏览器信息，帮助服务器进行兼容性处理。
  - 指定客户端能够接收的响应内容类型，帮助服务器进行内容协商。
  - 控制缓存行为，提高响应速度。

### 3. 空行

请求头和请求体之间有一个空行，用于分隔它们。这个空行仅包含回车和换行符（CRLF），没有其他字符。空行是必需的，用于通知服务器请求头已经结束，接下来的部分是请求体（如果存在的话）。

### 4. 请求体（Request Body）

请求体用于包含客户端发送给服务器的实际数据，例如表单数据、文件内容等。请求体是否存在取决于请求方法和请求头中的设置。

- **GET请求**：通常没有请求体，请求参数通过URL的查询字符串传递。

- **POST请求**：通常包含请求体，请求参数通过请求体传递。请求体的格式由请求头中的Content-Type字段指定，常见的格式包括：

  - **application/x-www-form-urlencoded**：表单数据被编码为键值对，类似于URL的查询字符串。
  - **multipart/form-data**：用于上传文件，支持包含多个部分（如文本字段和文件字段）。
  - **application/json**：以JSON格式发送数据，常用于RESTful API中。

- **请求体的作用**：

  - 用于传递客户端发送给服务器的实际数据。
  - 支持多种数据格式，满足不同应用场景的需求。

> **字符编码**：请求行、请求头和请求体中的文本数据通常使用UTF-8字符编码。<br/>
> **请求方法的选择**：不同的请求方法具有不同的语义和用途，应根据实际需求选择合适的请求方法。<br/>
> **请求头和请求体的长度限制**：虽然HTTP协议本身没有明确规定请求头和请求体的长度限制，但实际应用中可能会受到服务器和客户端的限制。<br/>
> **安全性**：在发送敏感信息（如密码、个人数据等）时，应使用HTTPS协议进行加密传输，确保数据的安全性。<br/>

### 完整案例
```
// 请求行
POST /login.html HTTP/2
// 请求头
Host: localhost
User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2
Accept-Encoding: gzip, deflate
Referer: http://localhost/login.html
Connection: keep-alive
Upgrade-Insecure-Requests: 1
// 请求空行

// 请求体
username=zhangsan
```

## 响应消息数据格式
HTTP响应消息数据格式是服务器在接收到客户端的请求后，返回给客户端的数据结构。一个完整的HTTP响应消息通常由以下几个部分组成：状态行、响应头、空行和响应体。以下是对这些部分的详细介绍：

### 一、状态行（Status Line）

状态行是HTTP响应消息的第一行，用于描述服务器的响应状态。它由以下三个部分组成，用空格分隔：

1. **HTTP协议版本**：指示服务器使用的HTTP协议版本，例如HTTP/1.1或HTTP/2。
2. **状态码（Status Code）**：一个三位数字的代码，用于描述服务器的响应状态。状态码分为五类，每一类代表不同的响应类别：

  * **1xx（信息性状态码）**：表示请求已接收，继续处理。例如，100表示客户端应继续发送请求。
  * **2xx（成功状态码）**：表示请求已成功被服务器接收、理解、接受。例如，200表示请求成功，204表示请求成功但没有返回任何内容。
  * **3xx（重定向状态码）**：表示需要客户端采取进一步操作才能完成请求。例如，301表示请求的资源已永久移动到新位置，302表示请求的资源临时移动到新位置。
  * **4xx（客户端错误状态码）**：表示请求存在语法错误或请求无法实现。例如，400表示客户端请求有语法错误，404表示请求的资源不存在。
  * **5xx（服务器错误状态码）**：表示服务器未能实现合法的请求。例如，500表示服务器内部错误，503表示服务器当前无法处理请求。

**常见状态码：**

| **分类** | **状态码** | **描述** | **示例** |
| --- | --- | --- | --- |
| **信息响应（1xx）** | 100 Continue | 客户端应继续发送请求。服务器已收到请求的初始部分，正在等待其余部分。 | 客户端发送一个包含大量数据的请求时，服务器返回100状态码，要求客户端继续发送剩余数据。 |
|  | 101 Switching Protocols | 服务器根据客户端的请求切换协议。例如，将HTTP协议升级到WebSocket。 | 客户端请求协议升级时，服务器返回101状态码，并在响应头中指定新的协议。 |
|  | 102 Processing (WebDAV) | 服务器正在处理请求，但尚未准备好响应。此状态码用于WebDAV协议。 | 客户端向支持WebDAV的服务器发送请求时，服务器可能返回102状态码。 |
| **成功响应（2xx）** | 200 OK | 请求成功。服务器返回了请求的资源或结果。 | 客户端请求一个网页时，服务器返回200状态码和网页内容。 |
|  | 201 Created | 请求成功，并创建了新的资源。服务器在响应头中提供新资源的URL。 | 客户端通过POST请求创建新资源时，服务器返回201状态码和新资源的URL。 |
|  | 202 Accepted | 请求已接受，但尚未处理。服务器可能在后续处理请求。 | 客户端提交一个需要长时间处理的请求时，服务器返回202状态码。 |
|  | 204 No Content | 请求成功，但响应中没有返回内容。通常用于DELETE请求。 | 客户端删除一个资源时，服务器返回204状态码，表示删除成功但没有返回删除的资源。 |
|  | 206 Partial Content | 服务器成功处理了部分GET请求。客户端可以使用Range请求头指定需要返回的部分内容。 | 客户端请求一个大文件的特定部分时，服务器返回206状态码和指定部分的内容。 |
| **重定向（3xx）** | 301 Moved Permanently | 请求的资源已永久移动到新位置。客户端应使用新的URL进行后续请求。 | 客户端请求的资源已永久移动到新URL时，服务器返回301状态码和新URL。 |
|  | 302 Found | 请求的资源暂时移动到新位置。客户端应使用新的URL进行本次请求，但不应更新书签或链接。 | 客户端请求的资源暂时移动到新URL时，服务器返回302状态码和新URL。 |
|  | 303 See Other | 请求的资源可在另一个URL找到。客户端应使用GET方法请求该URL。 | 客户端提交POST请求后，服务器返回303状态码，要求客户端使用GET方法请求另一个URL以获取结果。 |
|  | 304 Not Modified | 资源未修改。客户端可以使用缓存的版本。 | 客户端发送带条件的GET请求时，如果资源未修改，服务器返回304状态码。 |
| **客户端错误（4xx）** | 400 Bad Request | 请求有语法错误或无法被服务器理解。客户端应检查请求的格式和内容。 | 客户端发送格式错误的请求时，服务器返回400状态码。 |
|  | 401 Unauthorized | 请求需要身份验证。服务器返回WWW-Authenticate头部字段，要求客户端提供有效的凭证。 | 客户端请求受保护的资源时，服务器返回401状态码，要求客户端进行身份验证。 |
|  | 403 Forbidden | 服务器拒绝请求。客户端没有权限访问请求的资源。 | 客户端请求没有权限访问的资源时，服务器返回403状态码。 |
|  | 404 Not Found | 请求的资源不存在。服务器无法找到匹配的URL。 | 客户端请求的资源不存在时，服务器返回404状态码。 |
|  | 405 Method Not Allowed | 服务器不支持请求的方法。例如，服务器不支持DELETE方法。 | 客户端使用服务器不支持的方法请求资源时，服务器返回405状态码。 |
| **服务器错误（5xx）** | 500 Internal Server Error | 服务器在处理请求时遇到内部错误。服务器无法完成请求。 | 服务器内部发生错误时，返回500状态码。 |
|  | 501 Not Implemented | 服务器不支持请求的功能或方法。 | 客户端请求服务器不支持的功能或方法时，服务器返回501状态码。 |
|  | 502 Bad Gateway | 服务器作为网关或代理时，从上游服务器收到无效的响应。 | 服务器作为网关或代理时，上游服务器发生错误，导致服务器返回502状态码。 |
|  | 503 Service Unavailable | 服务器暂时无法处理请求。通常由于服务器过载或维护。 | 服务器过载或维护时，返回503状态码，并在响应头中提供重试时间。 |

以上表格涵盖了HTTP状态码的主要分类、取值、描述及示例。状态码是服务器响应请求时返回的重要信息，用于描述请求的处理结果和状态。通过了解状态码的含义和用法，可以更好地理解和调试HTTP通信中的问题。

3. **状态描述（Reason Phrase）**：对状态码的简短文字描述，帮助人类理解状态码的含义。例如，对于状态码200，状态描述可以是"OK"。

**示例:**
```
HTTP协议版本 状态码 状态描述
HTTP/2 200 OK
```

### 二、响应头（Response Headers）

响应头包含了一系列的键值对，用于描述服务器的响应信息、资源的元数据、缓存控制等。每个响应头字段由名称和值组成，用冒号（:）分隔。响应头字段之间使用回车和换行符（CRLF）分隔。

**常见的响应头字段及示例**：

| 响应头字段 | 描述 | 示例 |
| --- | --- | --- |
| **Content-Type** | 指定响应体的媒体类型（MIME类型）。 | `Content-Type: text/html; charset=UTF-8` |
| **Content-Length** | 指定响应体的字节长度。 | `Content-Length: 12345` |
| **Content-Encoding** | 指定响应体使用的编码格式，如gzip、deflate等。 | `Content-Encoding: gzip` |
| **Date** | 指定响应消息生成的日期和时间。 | `Date: Wed, 17 Oct 2012 09:45:38 GMT` |
| **Server** | 指定服务器的软件信息。 | `Server: Apache/2.4.41` |
| **Last-Modified** | 指定资源的最后修改时间。 | `Last-Modified: Wed, 17 Oct 2012 09:35:13 GMT` |
| **Cache-Control** | 指定响应的缓存控制策略。 | `Cache-Control: max-age=3600` |
| **Set-Cookie** | 设置cookie，用于会话管理、用户跟踪等。 | `Set-Cookie: sessionid=abc123; HttpOnly; Secure` |
| **Location** | 在重定向响应中，指定请求的资源的新位置。 | `Location: http://www.example.com/new-resource` |
| **Content-Disposition** | 指定响应体的处理方式，如以附件形式下载。 | `Content-Disposition: attachment; filename="example.pdf"` |

### 三、空行

响应头和响应体之间有一个空行，用于分隔它们。这个空行仅包含回车和换行符（CRLF），没有其他字符。空行是必需的，用于通知客户端响应头已经结束，接下来的部分是响应体（如果存在的话）。

### 四、响应体（Response Body）

响应体是服务器返回给客户端的实际数据，例如HTML页面、图片、JSON数据等。响应体的格式和内容完全取决于服务器端的处理逻辑和响应头中的设置。

### 注意事项

1. **字符编码**：响应头中的文本数据（如状态描述、响应头字段的值）通常使用ASCII码，而响应体中的数据则可能使用不同的字符编码，这由`Content-Type`头中的`charset`参数指定。
2. **状态码的选择**：服务器应根据实际情况选择合适的状态码，以准确描述响应状态。状态码的选择对客户端的行为有直接影响，例如，404状态码会导致大多数浏览器显示“页面未找到”的错误信息。
3. **缓存控制**：通过响应头中的`Cache-Control`、`Expires`等字段，服务器可以控制响应内容在客户端的缓存行为，以提高响应速度和减少服务器负载。
4. **安全性**：对于包含敏感信息的响应（如用户数据、密码等），应确保信息的安全性，避免明文传输。可以使用HTTPS协议进行加密传输。

### 完整示例
```
// 状态行
HTTP/1.1 200 OK
// 响应头
Content-Type: text/html;charset=UTF-8
Content-Length: 101
Date: Wed, 06 Jun 2018 07:08:42 GMT
// 响应空行

// 响应体
<html>
  <head>
    <title>$Title$</title>
  </head>
  <body>
  hello , response
  </body>
</html>
```

## Servlet中Request及Response
Servlet中Request对象是来获取请求消息，Response对象是来设置响应消息。且Request和Response对象是由服务器创建。

ServletRequest
|--HttpServletRequest
|--|--org.apache.catalina.connector.RequestFacade（Tomcat 作为服务器）

```java
package com.demo.request;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/req3")
public class RequestDemo3 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("req = " + req + ", resp = " + resp);
    }
}
```

![img_2.png](images/img_2.png)

**使用Jboss访问返回如下：**

![img_3.png](images/img_3.png)

> 可以看出Request和Response对象是由服务器创建的。且服务器不同创建的对象不同。但是他们都实现了HttpServletRequest接口。

## Request对象常用方法介绍

| 方法名称 | 作用 | 示例                                                                                                                                                                                                                                                                                                                          | 注意事项 |
| --- | --- |-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------| --- |
| `getRequestURL()` | 返回客户端发出请求时的完整URL。 | ```String url = request.getRequestURL();System.out.println(url); // 输出类似：http://localhost:8080/myapp/myServlet```                                                                                                                                                                                      | 该方法返回的是一个包含协议、服务器名称、端口号、服务器路径的URL，但不包含查询字符串参数。 |
| `getRequestURI()` | 返回请求行中的资源名部分，即请求URI。 | ```String uri = request.getRequestURI();System.out.println(uri); // 输出类似：/myapp/myServlet```                                                                                                                                                                                                                | 该方法返回的是请求资源的绝对路径，不包含协议、服务器名称、端口号等。 |
| `getQueryString()` | 返回请求行中的参数部分，即查询字符串。 | ```String queryString = request.getQueryString();System.out.println(queryString); // 输出类似：param1=value1&param2=value2```                                                                                                                                                                                    | 该方法只能获取GET请求的请求参数，因为GET请求的参数在URL后面。 |
| `getRemoteAddr()` | 返回发出请求的客户机的IP地址。 | ```String ip = request.getRemoteAddr();System.out.println(ip); // 输出类似：127.0.0.1```                                                                                                                                                                                                                         | 该方法返回的是客户机的IP地址，可以用于记录访问日志、进行访问控制等。 |
| `getRemoteHost()` | 返回发出请求的客户机的完整主机名。 | ```String host = request.getRemoteHost();System.out.println(host); // 输出类似：localhost```                                                                                                                                                                                                                     | 如果对应的IP地址在DNS中注册，则返回主机名；否则返回IP地址。 |
| `getHeader(String name)` | 根据请求头的名称获取对应的值。 | ```String userAgent = request.getHeader("User-Agent");System.out.println(userAgent); // 输出类似：Mozilla/5.0 ...```                                                                                                                                                                                             | 头名称是不区分大小写的。 |
| `getHeaders(String name)` | 获取所有请求头中指定的某个头信息的全部内容。 | ```Enumeration<String> headers = request.getHeaders("Accept-Encoding");while (headers.hasMoreElements()) {    String header = headers.nextElement();    System.out.println(header); // 输出类似：gzip, deflate}```                                                                                   | 适用于请求头可能有多个值的情况。 |
| `getHeaderNames()` | 获取所有请求头的名称。 | ```Enumeration<String> headerNames = request.getHeaderNames();while (headerNames.hasMoreElements()) {    String headerName = headerNames.nextElement();    String headerValue = request.getHeader(headerName);    System.out.println(headerName + ": " + headerValue);}```                  | 可以用于遍历所有请求头。 |
| `getParameter(String name)` | 根据参数名获取请求参数的值。 | ```String username = request.getParameter("username");System.out.println(username); // 输出类似：张三```                                                                                                                                                                                                           | 该方法用于获取GET或POST请求中的单个参数值。 |
| `getParameterValues(String name)` | 根据参数名获取请求参数的所有值（适用于多选情况）。 | ```String[] hobbies = request.getParameterValues("hobbies");for (String hobby : hobbies) {    System.out.println(hobby); // 输出类似：游泳, 跑步}```                                                                                                                                                         | 适用于复选框等可能返回多个值的表单元素。 |
| `getParameterNames()` | 获取所有请求参数的名称。 | ```Enumeration<String> paramNames = request.getParameterNames();while (paramNames.hasMoreElements()) {    String paramName = paramNames.nextElement();    String paramValue = request.getParameter(paramName);    System.out.println(paramName + ": " + paramValue);}```                    | 可以用于遍历所有请求参数。 |
| `getParameterMap()` | 获取所有请求参数，封装为Map集合。 | ```Map<String, String[]> paramMap = request.getParameterMap();for (Map.Entry<String, String[]> entry : paramMap.entrySet()) {    String paramName = entry.getKey();    String[] paramValues = entry.getValue();    System.out.println(paramName + ": " + Arrays.toString(paramValues));}``` | 适用于需要处理多个参数的情况。 |
| `setCharacterEncoding(String enc)` | 设置请求字符编码，解决中文乱码问题。 | ```request.setCharacterEncoding("UTF-8");String name = request.getParameter("name");System.out.println(name); // 输出正确的中文字符```                                                                                                                                                                           | 在处理POST请求时，需要设置字符编码以避免中文乱码。 |
| `getRequestDispatcher(String path)` | 获取请求转发器对象，用于请求转发。 | ```RequestDispatcher dispatcher = request.getRequestDispatcher("/nextServlet");dispatcher.forward(request, response);```                                                                                                                                                                                    | 请求转发是服务器内部的操作，客户端地址栏的URL不会改变。 |
| `setAttribute(String name, Object o)` | 在Request域中存储数据，用于请求转发时的数据共享。 | ```request.setAttribute("username", "张三");RequestDispatcher dispatcher = request.getRequestDispatcher("/nextServlet");dispatcher.forward(request, response);```                                                                                                                                         | 存储的数据在当前请求范围内有效。 |
| `getAttribute(String name)` | 从Request域中获取数据。 | ```String username = (String) request.getAttribute("username");System.out.println(username); // 输出：张三```                                                                                                                                                                                                    | 用于获取在Request域中存储的数据。 |
| `removeAttribute(String name)` | 从Request域中移除数据。 | ```request.removeAttribute("username");```                                                                                                                                                                                                                                                                      | 移除指定名称的数据。 |

### 案例- 获取请求消息数据
[RequestDemo4.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2Frequest%2FRequestDemo4.java)
```java
package com.demo.request;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Enumeration;

@WebServlet("/req4")
public class RequestDemo4 extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    doPost(req, resp);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    // 获取请求行 GET https://127.0.0.1:8080/demo2/req4?name=123&age=23 HTTP/1.1
    System.out.println("=======请求行======");
    // 获取请求方式 GET
    System.out.println("获取请求方式: " + req.getMethod());
    // 获取请求路径URL http://127.0.0.1:8080/demo/req4
    System.out.println("获取请求路径URL: " + req.getRequestURL());
    // 获取请求路径URI /demo/req4
    System.out.println("获取请求路径URI: " + req.getRequestURI());
    // 获取虚拟目录 /demo
    System.out.println("获取虚拟目录: " + req.getContextPath());
    // 获取Servlet路径 /req4
    System.out.println("获取Servlet路径: " + req.getServletPath());
    // 获取获取GET方式请求参数 name=%E5%BC%A0%E4%B8%89&age=23
    System.out.println("获取获取GET方式请求参数: " + req.getQueryString());
    // 获取协议及版本 HTTP/1.1
    System.out.println("获取协议及版本: " + req.getProtocol());
    // 获取客户端地址 127.0.0.1
    System.out.println("获取客户端地址: " + req.getRemoteAddr());
    // 获取客户端主机名 127.0.0.1
    System.out.println("获取客户端主机名: " + req.getRemoteHost());

    System.out.println("=======请求头======");
    // 获取所有请求头
    Enumeration<String> headerNames = req.getHeaderNames();
    System.out.println("获取所有请求头: " );
    while (headerNames.hasMoreElements()) {
      System.out.print(headerNames.nextElement() + " ");
    }
    System.out.println("");
    // 根据名称获取请求头数据
    System.out.println("根据名称获取请求头数据-user-agent: " + req.getHeader("user-agent"));

    System.out.println("=======请求体======");
    // 获取请求体
    // 获取所有请求参数KEY
    Enumeration<String> parameterNames = req.getParameterNames();
    System.out.println("获取所有请求参数KEY-通过key获取参数值: " );
    while (parameterNames.hasMoreElements()) {
      String key = parameterNames.nextElement();
      System.out.println(key + ": " + req.getParameter(key));
    }
    // 根据请求参数名称获取请求参数数据
    System.out.println("根据请求参数名称获取请求参数数据-name: " + req.getParameter("name"));

    System.out.println("===通过IO流获取请求体===");
    String reqData = "";
    String line = "";
    BufferedReader reader = req.getReader();
    while ((line = reader.readLine()) != null) {
      reqData += line;
    }
    System.out.println("获取请求体: " + reqData);
  }
}

```

**执行结果：**

![img_4.png](images/img_4.png)

## Response对象常用方法介绍

| 方法名 | 作用 | 可选参数 | 可选参数含义 | 默认值 |
| --- | --- | --- | --- | --- |
| `sendRedirect(String location)` | 将客户端重定向到指定的URL。通常用于在提交表单后，将用户引导到另一个页面。 | `location`（String类型） | 要重定向到的URL地址。 | 无默认值，必须提供。 |
| `setContentType(String type)` | 设置响应的内容类型（MIME类型）。告诉客户端如何解析响应体中的数据。 | `type`（String类型） | 响应的内容类型，例如"text/html"、"application/json"等。 | 默认值为"text/html"，表示HTML文档。 |
| `setCharacterEncoding(String charset)` | 设置响应的字符编码。确保客户端正确解析响应中的字符。 | `charset`（String类型） | 字符编码名称，例如"UTF-8"、"ISO-8859-1"等。 | 默认编码取决于服务器和容器的配置，但通常建议显式设置。 |
| `addHeader(String name, String value)` | 添加一个响应头。如果响应头已经存在，则不会替换它，而是添加一个新的头字段。 | `name`（String类型） | 响应头的名称。 | 无默认值，必须提供。 |
|  |  | `value`（String类型） | 响应头的值。 | 无默认值，必须提供。 |
| `setIntHeader(String name, int value)` | 添加一个带有整数值的响应头。 | `name`（String类型） | 响应头的名称。 | 无默认值，必须提供。 |
|  |  | `value`（int类型） | 响应头的整数值。 | 无默认值，必须提供。 |
| `setDateHeader(String name, long date)` | 添加一个带有日期值的响应头。日期值以自1970年1月1日00:00:00 GMT以来的毫秒数表示。 | `name`（String类型） | 响应头的名称。 | 无默认值，必须提供。 |
|  |  | `date`（long类型） | 响应头的日期值。 | 无默认值，必须提供。 |
| `setBufferSize(int size)` | 设置响应缓冲区的大小。如果缓冲区溢出，数据将被发送到客户端。 | `size`（int类型） | 缓冲区的大小（以字节为单位）。 | 默认值取决于服务器和容器的配置。 |
| `flushBuffer()` | 将缓冲区中的数据发送到客户端，但不关闭响应流。 | 无 | - | - |
| `getBufferSize()` | 获取当前响应缓冲区的实际大小（以字节为单位）。如果未设置缓冲区大小，则返回0。 | 无 | - | 取决于服务器和容器的配置。 |
| `isCommitted()` | 检查响应是否已经被提交到客户端。一旦响应被提交，就不能再对其进行修改。 | 无 | - | - |
| `reset()` | 清除缓冲区中的数据，并重置响应状态，以便可以重新发送响应。 | 无 | - | - |
| `setContentLength(int len)` | 设置响应的内容长度（以字节为单位）。这有助于客户端在接收响应时了解数据的总量。 | `len`（int类型） | 响应内容的长度。 | 如果未设置，则内容长度由响应数据本身决定。 |

### HTTP响应头介绍

| 响应头名称 | 可选值 | 作用 | 示例 | 结果 |
| --- | --- | --- | --- | --- |
| `Content-Type` | `text/html`, `application/json`, `application/pdf`, `image/png`, `application/octet-stream` 等 | 指定响应体的媒体类型 | `Content-Type: application/json` | 浏览器根据此类型解析响应体内容，如JSON数据将被正确解析为对象或数组。 |
| `Content-Length` | 数字（字节数） | 指定响应体的长度（字节） | `Content-Length: 1234` | 浏览器知道需要接收多少字节的数据来完成整个响应。 |
| `Set-Cookie` | `name=value; Expires=Wed, 09 Jun 2021 10:18:14 GMT; HttpOnly` 等 | 设置一个或多个cookie | `Set-Cookie: sessionId=abc123; Path=/; HttpOnly` | 浏览器存储cookie，并在后续请求中发送回服务器。 |
| `Cache-Control` | `no-cache`, `no-store`, `max-age=3600`, `public`, `private` 等 | 控制缓存机制 | `Cache-Control: no-cache, no-store, must-revalidate` | 浏览器不会缓存响应，每次请求都会直接发送到服务器。 |
| `Expires` | HTTP日期格式（如`Wed, 21 Oct 2015 07:28:00 GMT`） | 指定资源过期的时间 | `Expires: Wed, 21 Oct 2015 07:28:00 GMT` | 浏览器在指定时间之前不会从缓存中加载资源，而是会发送请求到服务器。 |
| `ETag` | 字符串（通常是资源的哈希值） | 标识资源的特定版本 | `ETag: "12345"` | 浏览器可以使用此标签来验证资源自上次请求后是否已更改。 |
| `Location` | URL | 重定向到新位置 | `Location: /new-page` | 浏览器会自动请求指定的新URL。 |
| `Last-Modified` | HTTP日期格式 | 指定资源最后修改的时间 | `Last-Modified: Wed, 21 Oct 2015 07:28:00 GMT` | 浏览器可以使用此信息来判断是否需要从服务器获取资源的更新版本。 |
| `Content-Disposition` | `inline`, `attachment; filename="example.pdf"` | 表示响应的内容该以何种形式展示 | `Content-Disposition: attachment; filename="example.pdf"` | 浏览器会提示用户下载文件，而不是直接在浏览器中打开。 |
| `Content-Encoding` | `gzip`, `deflate`, `br` 等 | 指定对响应体使用的编码类型 | `Content-Encoding: gzip` | 浏览器知道需要对响应体进行解压缩以获取原始内容。 |
| `Content-Language` | `en`, `zh-CN`, `fr` 等 | 指定响应体的自然语言 | `Content-Language: zh-CN` | 浏览器可以使用此信息来选择合适的语言显示内容（如果支持）。 |
| `Content-Range` | `bytes 200-1000/67589` 等 | 指定响应体的范围（用于部分内容请求） | `Content-Range: bytes 200-1000/67589` | 浏览器知道这是资源的哪个部分，并可以请求其他部分以完成整个资源的获取。 |

这个表格提供了每个响应头的基本信息和示例，以及设置这些响应头后可能产生的结果。在实际开发中，根据具体需求和场景选择合适的响应头进行设置。

### 案例-下载文件（直接下载）
欢迎页提供一下下载链接，点击下载则直接下载，而不是加载浏览。
[ResponseDemo.java](src%2Fmain%2Fjava%2Fcom%2Fdemo%2Frequest%2FResponseDemo.java)
```java
package com.demo.request;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@WebServlet("/resp")
public class ResponseDemo extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // 加载本地路径文件
        ServletContext servletContext = req.getServletContext();
        // 获取资源路径
        String realPath = servletContext.getRealPath("/img.png");
        File file = new File(realPath);
        if (!file.exists()) {
            // 获取输出流
            try (ServletOutputStream outputStream = resp.getOutputStream();) {
                // 设置响应头
                resp.setContentType("text/html;charset=UTF-8");
                outputStream.write("文件不存在".getBytes(StandardCharsets.UTF_8));
            }
            return;
        }
        // 获取输出流
        try (ServletOutputStream outputStream = resp.getOutputStream(); InputStream inputStream = new FileInputStream(file);) {
            // 获取MIME类型
            String mimeType = getServletContext().getMimeType(realPath);
            if (mimeType == null) {
                // 设置默认MIME类型
                mimeType = "application/octet-stream";
            }

            // 设置响应内容类型
            resp.setContentType(mimeType);
            resp.setContentLength((int) file.length());

            // 设置Content-Disposition头，指定下载的文件名
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"", URLEncoder.encode("图片_", "UTF-8") + file.getName());
            resp.setHeader(headerKey, headerValue);
            byte[] buffer = new byte[4096];
            int bytesRead = -1;

            // 将文件内容写入响应输出流
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
        }

    }
}
```

**执行结果：**

![img_5.png](images/img_5.png)

## gitee源码
> [git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git](https://gitee.com/dchh/JavaStudyWorkSpaces.git)