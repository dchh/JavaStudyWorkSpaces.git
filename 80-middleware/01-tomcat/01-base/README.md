# Tomcat介绍

## 什么是Tomcat

Tomcat起源于1999年，由Sun Microsystems捐赠给Apache软件基金会。一个功能强大、易于使用和轻量级的Java Web应用服务器。

## Tomcat的基础使用

### 下载
[下载地址：http://tomcat.apache.org/](http://tomcat.apache.org/)

**选择下载版本：**

![img.png](images/img.png)

**下载安装包：**
根据系统版本，选择对应的安装包就可以了
![img_1.png](images/img_1.png)

### 安装
解压压缩包及可

![img_2.png](images/img_2.png)

### 卸载
删除安装目录即可

### 启动
> 启动 ：bin/startup.bat ,双击运行该文件即可

![img_3.png](images/img_3.png)

> 访问：浏览器输入：http://localhost:8080 回车访问

![img_4.png](images/img_4.png)

### 常见问题

1. **黑窗口一闪而过**

    原因：没有正确配置JAVA_HOME环境变量

    解决方案：正确配置JAVA_HOME环境变量

2. **端口占用**

   + **修改自身端口号方式：**
      修改配置文件：config/server.xml文件

```
<Server port="8005" shutdown="SHUTDOWN">
    ...
     <Connector port="8080" protocol="HTTP/1.1"
       connectionTimeout="20000"
       redirectPort="8443" />
    ...
</Server>
```
修改8005,8080,8443不要重复即可

   + **前置删除进程：**
      - Linux系统：
         > 查询进程：ps ef|grep "8080"
         > 删除进程：kill -9 PID

      - Windows系统：
        > 查询进程：netstat -ano | findstr "端口号"
        > 删除进程：taskkill /F /PID "4632"  （4632 对应PID）





## 项目部署方式

### 资源分类
1. 静态资源：所有用户访问后，得到的结果都是一样的，称为静态资源.静态资源可以直接被浏览器解析
   * 如： html,css,JavaScript
2. 动态资源:每个用户访问相同资源后，得到的结果可能不一样。称为动态资源。动态资源被访问后，需要先转换为静态资源，在返回给浏览器
   * 如：servlet/jsp,php,asp....

### 项目分类

1. **静态项目**

    * **定义**：静态项目通常指的是那些只包含静态资源的项目。这些资源在项目开发完成后，其内容一般不会发生变化，除非手动进行修改。
    * **特性**：
        + 资源类型：主要包括HTML、CSS、JavaScript、图片、文本、音频、视频等。
        + 内容固定：页面内容在开发完成后是固定的，不会因为访问者的不同或访问时间的不同而发生变化。
        + 无需服务器处理：静态资源可以直接由浏览器加载和显示，无需服务器端的额外处理。
    * **目录结构**：
      通常只包含静态资源的文件夹和文件，如HTML、CSS、JavaScript文件夹和相应的文件。
2. **动态项目**

    * **定义**：动态项目则不仅包含静态资源，还包含需要服务器端程序生成或处理的动态资源。
    * **特性**：
        + 资源类型：除了静态资源外，还包括如Servlet、JSP等服务器端程序。
        + 内容可变：页面内容可以根据访问者的不同、访问时间的不同或数据库中的数据变化而动态生成。
        + 需要服务器处理：动态资源需要服务器端的程序进行处理，然后生成相应的HTML内容发送给客户端。
    * **目录结构**：
      除了包含静态资源的文件夹和文件外，通常还会包含如WEB-INF目录（包含web.xml配置文件、classes目录和lib目录等）以及服务器端程序的源代码或编译后的字节码文件。
      项目的根目录
      |-- WEB-INF目录：
          |-- web.xml：web项目的核心配置文件
          |-- classes目录：放置字节码文件的目录
          |-- lib目录：放置依赖的jar包
      |-- 静态资源



### 部署方式1: 直接将项目放到webapps目录下
静态项目：直接放在webapps目录下
动态资源：将项目打成一个war包，再将war包放置到webapps目录下。
>  war包会自动解压缩

**案例：**
部署一个简单的静态资源，目录结构如下：
**目录结构：**
    hello/index.html
**文件：**
    index.html
```html
 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hello</title>
</head>
<body>
    Hello World !!!
</body>
</html>
```

**部署：**

![img_5.png](images/img_5.png)

**访问结果：**

![img_6.png](images/img_6.png)

### Tomcat如何解析地址：`http://127.0.0.1:8080/hello/index.html`

1. **协议和主机名解析**：
    - `http`：使用的协议，表示这是一个HTTP请求。
    - `127.0.0.1`：请求的主机名，它是一个特殊的IP地址，代表本机（localhost）。
    - `8080`：Tomcat服务器监听的端口号，默认情况下，Tomcat在8080端口上运行。

2. **上下文路径（Context Path）解析**：
    - `/hello`：这部分是上下文路径。在Tomcat中，上下文路径对应于一个web应用。

3. **资源路径（Servlet Path + Path Info）解析**：
    - `index.html`：资源路径，它进一步细分为Servlet路径（在这个例子中可能不存在，因为通常HTML文件不是由Servlet直接服务的，除非有特殊的配置）和路径信息（Path Info）。在这个例子中，`index.html`可以被视为路径信息，因为Tomcat默认配置下会使用其内置的默认Servlet来处理对静态资源的请求。

4. **请求处理步骤**：
    - Tomcat会查找上下文路径为`/hello`的web应用。
    - 在该web应用中，Tomcat会尝试找到与请求匹配的资源。由于请求的是一个HTML文件，Tomcat会首先尝试在其内置的默认Servlet中查找该文件。
    - 如果在web应用的`WEB-INF`目录之外找到了`index.html`文件，Tomcat就会将该文件的内容返回给客户端。
    - 如果在`WEB-INF`目录之外没有找到该文件，但web应用配置了一个欢迎文件列表，并且`index.html`在该列表中，Tomcat会尝试找到并返回该文件。
    - 如果以上条件都不满足，Tomcat会返回404错误，表示未找到资源。

### 部署方式2： 配置conf/server.xml文件

**修改配置：**

修改配置在conf/server.xml文件,在<Host>标签体中配置如下：
config/server.xml
```
<Server port="8005" shutdown="SHUTDOWN">
    ...
     <Host name="localhost"  appBase="webapps"
            unpackWARs="true" autoDeploy="true">
        <Context docBase="../webapps/hello" path="/haha" />
     </Host>
    ...
</Server>
```

**访问结果：**

![img_7.png](images/img_7.png)

> 以下是`<Context>`标签及其属性的信息：

| 属性名称        | 含义                                                             | 示例                                                         | 默认值/说明                                 |
|-----------------|----------------------------------------------------------------|--------------------------------------------------------------|----------------------------------------|
| `path`          | 指定访问Web应用程序的URL路径                                   | `<Context path="/hello" ... />`                             | 无（在context XML片段文件中，默认为文件名不含`.xml`扩展名） |
| `docBase`       | 指定Web应用程序的根目录或文档根目录的路径                     | `<Context docBase="/opt/software/app" ... />`               | 无（需明确指定,可以使用相对路径和绝对路径）                 |
| `reloadable`    | 启用或禁用自动重新加载（热部署）功能                           | `<Context reloadable="true" ... />`                         | `false`                                |
| `cachingAllowed`| 启用或禁用Context的静态资源缓存                                 | `<Context cachingAllowed="true" ... />`                     | 根据Tomcat版本和配置可能有所不同                    |
| `cacheMaxSize`  | 静态资源缓存的最大值（以KB为单位）                             | `<Context cacheMaxSize="20480" ... />`                      | 根据Tomcat版本和配置可能有所不同                    |
| `cacheTTL`      | 缓存刷新之间的活跃时间间隔（以毫秒为单位）                     | `<Context cacheTTL="10000" ... />`                          | 根据Tomcat版本和配置可能有所不同                    |
| `cookies`       | 启用或禁用与该context交互的会话ID对应的cookies                 | `<Context cookies="true" ... />`（通常不直接配置）          | 根据Tomcat版本和配置可能有所不同                    |
| `crossContext`  | 指定`ServletContext.getContext(otherWebApp)`是否会成功或返回`null` | `<Context crossContext="true" ... />`                       | `false`（通常用于安全考虑）                      |
| `unpackWAR`     | 是否自动解包WAR文件                                             | `<Context unpackWAR="true" ... />`                          | 根据Tomcat版本和配置可能有所不同（某些版本默认解包）          |
| `useNaming`     | 启用或禁用为该应用程序创建JNDI网页范围                         | `<Context useNaming="true" ... />`（通常不直接配置）        | 根据Tomcat版本和配置可能有所不同                    |
| `workDir`       | Web应用程序的临时文件目录的路径名                             | `<Context workDir="/tmp/hello-app-workdir" ... />`          | Tomcat工作目录下的子目录（通常自动管理）                |

**注意事项**：
- 表格中的“示例”列提供了属性的配置示例，但实际应用中需要根据具体情况进行调整。
- “默认值/说明”列中的信息可能因Tomcat版本和配置的不同而有所变化，请参考具体版本的Tomcat文档以获取准确信息。
- 在实际配置中，并非所有属性都需要设置，通常只需根据需求配置必要的属性即可。
- 对于生产环境，建议谨慎配置`reloadable`属性，以避免性能下降。同时，静态资源缓存的配置也需要根据实际需求进行合理调整。



### 部署方式3：在`conf\Catalina\localhost`创建xml文件

在Tomcat的conf\Catalina\localhost目录下创建任意名称的XML文件， 这种方法允许你为特定的Web应用程序配置自定义的部署描述符，而不必修改全局的server.xml文件。
文件中只需要配置`Context`标签即可。

**配置文件：**

conf\Catalina\localhost/ccc.xml
```xml
<Context docBase="D:/temp/hello" path="/cc" reloadable="true">
    <!-- 你可以在这里添加其他配置，例如资源引用、环境变量等 -->
</Context>
```

**访问结果：**

![img_8.png](images/img_8.png)

> 注意： 在使用该方式进行部署时，不能将项目放在webapps目录下，且文件名不能与项目webapps目录下已存在目录一致，否则该配置文件将会被忽略。
> 当然这里`docBase`也可以使用相对路径，不过这里的相对路径，通常是相对于webapps目录的

