package com.utils.common.captcha.entity;

import lombok.Data;

import java.awt.image.BufferedImage;

/**
 * 图形验证码
 *
 * @author Anna.
 * @date 2025/3/12 23:26
 */
@Data
public class GraphicCaptcha extends Captcha {

    /**
     * 随机值
     */
    private String randomness = "";

    /**
     * 随机生成图片
     */
    private BufferedImage image;

}
