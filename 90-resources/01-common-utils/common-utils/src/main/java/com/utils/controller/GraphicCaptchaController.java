package com.utils.controller;

import com.utils.common.captcha.DefaultGraphicCaptchaDirector;
import com.utils.common.captcha.config.GraphicCaptchaConfig;
import com.utils.common.captcha.entity.GraphicCaptcha;
import com.utils.common.captcha.graphic.GraphicCaptchaBuider;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import java.io.IOException;

/**
 *  图形验证码测试
 *
 * @author Anna.
 * @date 2025/3/13 8:48
 */
@RestController
@RequestMapping("/graphic/captcha")
public class GraphicCaptchaController {

    @GetMapping("/base")
    public void getBaseGraphicCaptcha(HttpServletResponse response) throws IOException {
        // 输出图像
        response.setContentType("image/png");
        GraphicCaptcha captcha = (GraphicCaptcha) new DefaultGraphicCaptchaDirector(new GraphicCaptchaBuider()).createBaseGraphicCaptcha();;
        System.out.println("随机验证码 = " + captcha.getRandomness());
        ImageIO.write(captcha.getImage(), "PNG", response.getOutputStream());
    }

    @GetMapping("/image")
    public void getImageGraphicCaptcha(HttpServletResponse response) throws IOException {
        // 输出图像
        response.setContentType("image/png");
        GraphicCaptcha captcha = (GraphicCaptcha) new DefaultGraphicCaptchaDirector(new GraphicCaptchaBuider()).createImageGraphicCaptcha();;
        System.out.println("随机验证码 = " + captcha.getRandomness());
        ImageIO.write(captcha.getImage(), "PNG", response.getOutputStream());
    }

    @PostMapping("/test")
    public void getImageGraphicCaptcha(GraphicCaptchaConfig config,HttpServletResponse response) throws IOException {
        // 输出图像
        response.setContentType("image/png");
        GraphicCaptcha captcha = (GraphicCaptcha) new GraphicCaptchaBuider().createBaseBufferedImage(config)
                .setBackground(config)
                .setFont(config)
                .generateRandomNumbers(config)
                .drawInterfere(config)
                .buildCatcha();
        System.out.println("随机验证码 = " + captcha.getRandomness());
        ImageIO.write(captcha.getImage(), "PNG", response.getOutputStream());
    }

}
