package com.utils.common.captcha.graphic;

import com.utils.common.captcha.config.GraphicCaptchaConfig;
import com.utils.common.captcha.entity.Captcha;
import com.utils.common.captcha.entity.GraphicCaptcha;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * 图形验证码 建造者实现类
 *
 * @author Anna.
 * @date 2025/3/12 23:24
 */
public class GraphicCaptchaBuider implements IGraphicCaptchaBuider {

    /**
     * 定义建造者返回对象
     */
    private GraphicCaptcha captcha;

    /**
     * 图形处理工具
     */
    private Graphics graphics;

    /**
     * 随机值
     */
    private String randomness = "";


    public GraphicCaptchaBuider() {
        this.captcha = new GraphicCaptcha();
    }

    @Override
    public IGraphicCaptchaBuider createBaseBufferedImage(GraphicCaptchaConfig captchaConfig) {
        paramCheck(captchaConfig);
        this.captcha.setImage(new BufferedImage(captchaConfig.getImageWidth(), captchaConfig.getImageHeight(), captchaConfig.getImageType()));
        graphics = this.captcha.getImage().getGraphics();
        return this;
    }

    /**
     * 参数校验
     *
     * @param captchaConfig
     * @return void
     * @author Anna.
     * @date 2025/3/13 20:38
     */
    private void paramCheck(GraphicCaptchaConfig captchaConfig) {
        if (captchaConfig.getImageWidth() < 0) {
            throw new RuntimeException("图片宽度不能小于0");
        }
        if (captchaConfig.getImageHeight() < 0) {
            throw new RuntimeException("图片高度不能小于0");
        }
        if(captchaConfig.getBackgroundType() < 0 || captchaConfig.getBackgroundType() > 3){
            throw new RuntimeException("背景类型参数错误,取值[1,3]");
        }
        if(captchaConfig.getBorderWidth() < 0 ){
            throw new RuntimeException("边框宽度不能小于0");
        }
        if(captchaConfig.getContentRotateIntensity() < 0 || captchaConfig.getContentRotateIntensity() > 50){
            throw new RuntimeException("旋转强度参数错误,取值[0,50]");
        }
        if(captchaConfig.getRandomType() < 0 || captchaConfig.getRandomType() > 2){
            throw new RuntimeException("随机数类型参数错误,取值[0,2]");
        }
        if(captchaConfig.getRandomNumber() < 0){
            throw new RuntimeException("随机数个数参数错误,取值[0,无穷]");
        }
        if(captchaConfig.getInterfereType() < 1 || captchaConfig.getInterfereType() > 3){
            throw new RuntimeException("干扰类型参数错误,取值[1,3]");
        }
        if(captchaConfig.getInterfereLineIntensity() < 0){
            throw new RuntimeException("干扰线强度参数错误,取值[0,无穷]");
        }
        if(captchaConfig.getInterfereDropIntensity() < 0){
            throw new RuntimeException("干扰点强度参数错误,取值[0,无穷]");
        }
        if(captchaConfig.getInterfereDropDiameter() < 0){
            throw new RuntimeException("干扰点随机直径参数错误,取值[0,无穷]");
        }
    }

    @Override
    public IGraphicCaptchaBuider setBackground(GraphicCaptchaConfig captchaConfig) {
        switch (captchaConfig.getBackgroundType()) {
            case GraphicCaptchaConfig.BACKGROUND_TYPE_COLOR:
                // 设置背景颜色
                graphics.setColor(captchaConfig.getBackgroundColor());
                // 绘制一个矩形
                graphics.fillRect(0, 0, captchaConfig.getImageWidth(), captchaConfig.getImageHeight());
                break;
            case GraphicCaptchaConfig.BACKGROUND_TYPE_IMAGE:
                // 绘制一张图片
                graphics.drawImage(captchaConfig.getBackgroundImg(), captchaConfig.getBackgroundImgXoffset(), captchaConfig.getBackgroundImgYoffset(), captchaConfig.getBackgroundImgObserver());
                break;
            default:
                graphics.fillRect(0, 0, captchaConfig.getImageWidth(), captchaConfig.getImageHeight());
                break;
        }
        return this;
    }

    @Override
    public IGraphicCaptchaBuider setBorder(GraphicCaptchaConfig captchaConfig) {
        // 设置画笔颜色
        graphics.setColor(captchaConfig.getBorderColor());
        // 绘制边框  图片宽高 - 边框宽度
        graphics.drawRect(0, 0, captchaConfig.getImageWidth() - captchaConfig.getBorderWidth(), captchaConfig.getImageHeight() - captchaConfig.getBorderWidth());
        return this;
    }

    @Override
    public IGraphicCaptchaBuider setFont(GraphicCaptchaConfig captchaConfig) {
        // 设置字体大小
        graphics.setFont(captchaConfig.getFont());
        return this;
    }

    @Override
    public IGraphicCaptchaBuider generateRandomNumbers(GraphicCaptchaConfig captchaConfig) {
        // 获取随机数
        String randomStr = getRandomStr(captchaConfig.getRandomType());
        Random ran = new Random();
        if (captchaConfig.getRandomNumber() > 0 && captchaConfig.getContentRotateIntensity() == 0) {
            for (int i = 1; i <= captchaConfig.getRandomNumber(); i++) {
                int index = ran.nextInt(randomStr.length());
                // 获取随机字符
                char ch = randomStr.charAt(index);
                this.captcha.setRandomness(this.captcha.getRandomness() + ch);
                // 设置字体颜色
                graphics.setColor(getRandomColor());
                if(captchaConfig.getFontColor() != null) {
                    graphics.setColor(captchaConfig.getFontColor());
                }
                // 绘制验证码
                graphics.drawString(ch + "", captchaConfig.getImageWidth() / (captchaConfig.getRandomNumber() + 2) * i, (captchaConfig.getImageHeight() + captchaConfig.getFont().getSize() / 2) / 2);
            }
        } else if (captchaConfig.getRandomNumber() > 0 ) {
            // 转为Graphics2D
            Graphics2D g2d = (Graphics2D) graphics;
            // 保存原始的变换状态
            AffineTransform originalTransform = g2d.getTransform();
            for (int i = 1; i <= captchaConfig.getRandomNumber(); i++) {
                int index = ran.nextInt(randomStr.length());
                // 获取随机字符
                char ch = randomStr.charAt(index);
                this.captcha.setRandomness(this.captcha.getRandomness() + ch);
                // 生成一个随机的旋转角度
                Random random = new Random();
                double angle = -captchaConfig.getContentRotateIntensity() + (captchaConfig.getContentRotateIntensity() - (-captchaConfig.getContentRotateIntensity())) * random.nextDouble(); // 随机生成 -45 到 45 之间的角度
                double radians = Math.toRadians(angle);
                // 创建一个旋转变换
                AffineTransform rotateTransform = AffineTransform.getRotateInstance(radians);

                // 应用旋转变换
                g2d.setTransform(rotateTransform);

                // 设置字体颜色
                graphics.setColor(getRandomColor());
                if(captchaConfig.getFontColor() != null) {
                    graphics.setColor(captchaConfig.getFontColor());
                }

                // 绘制旋转的文本
                // 注意：旋转是围绕原点(0,0)进行的，因此需要调整文本位置
                g2d.drawString(ch + "", captchaConfig.getImageWidth() / (captchaConfig.getRandomNumber() + 2) * i, (captchaConfig.getImageHeight() + captchaConfig.getFont().getSize() / 2) / 2);
            }
            // 恢复原始的变换状态
            g2d.setTransform(originalTransform);
        }

        return this;
    }

    /**
     * 获取随机数文本
     *
     * @param randomType
     * @return java.lang.String
     * @author Anna.
     * @date 2025/3/13 8:37
     */
    private String getRandomStr(int randomType) {
        switch (randomType) {
            case GraphicCaptchaConfig.RANDOM_TYPE_NUMBER:
                return GraphicCaptchaConfig.RANDOM_TYPE_NUMBER_CONTENT;
            case GraphicCaptchaConfig.RANDOM_TYPE_LETTER:
                return GraphicCaptchaConfig.RANDOM_TYPE_LETTER_CONTENT;
            case GraphicCaptchaConfig.RANDOM_TYPE_NUMBER_AND_LETTER:
                return GraphicCaptchaConfig.RANDOM_TYPE_NUMBER_AND_LETTER_CONTENT;
            default:
                return GraphicCaptchaConfig.RANDOM_TYPE_NUMBER_AND_LETTER_CONTENT;
        }
    }

    /**
     * 随机颜色
     *
     * @param
     * @return java.awt.Color
     * @author Anna.
     * @date 2025/3/13 9:26
     */
    private Color getRandomColor() {
        // 创建一个随机数生成器
        Random random = new Random();
        return new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256));
    }

    @Override
    public IGraphicCaptchaBuider drawInterfere(GraphicCaptchaConfig captchaConfig) {
        // 判断是否设置干扰强度
        boolean mark = (GraphicCaptchaConfig.INTERFERE_TYPE_LINE == captchaConfig.getInterfereType() && captchaConfig.getInterfereLineIntensity() > 0)
                || (GraphicCaptchaConfig.INTERFERE_TYPE_DROP == captchaConfig.getInterfereType() && captchaConfig.getInterfereDropIntensity() > 0)
                || (GraphicCaptchaConfig.INTERFERE_TYPE_DROP_AND_LINE == captchaConfig.getInterfereType() && (captchaConfig.getInterfereDropIntensity() > 0 || captchaConfig.getInterfereLineIntensity() > 0));
        if(!mark) {
            return this;
        }

        // 备份当前画笔颜色
        Color color = graphics.getColor();


        switch (captchaConfig.getInterfereType()) {
            case GraphicCaptchaConfig.INTERFERE_TYPE_LINE:
                drawInterfereLine(captchaConfig);
                break;
            case GraphicCaptchaConfig.INTERFERE_TYPE_DROP:
                drawInterfereDrop(captchaConfig);
                break;
            case GraphicCaptchaConfig.INTERFERE_TYPE_DROP_AND_LINE:
                drawInterfereLine(captchaConfig);
                drawInterfereDrop(captchaConfig);
                break;
            default:
        }

        // 还原画笔颜色
        graphics.setColor(color);
        return this;
    }

    /**
     * 绘制干扰点
     *
     * @param captchaConfig
     * @return void
     * @author Anna.
     * @date 2025/3/13 10:30
     */
    private void drawInterfereDrop(GraphicCaptchaConfig captchaConfig) {
        Random ran = new Random();
        //随机生成坐标点
        for (int i = 0; i < captchaConfig.getInterfereDropIntensity(); i++) {
            // 随机点直径
            int diameter = ran.nextInt(captchaConfig.getInterfereDropDiameter());
            int x1 = ran.nextInt(captchaConfig.getImageWidth());
            int y1 = ran.nextInt(captchaConfig.getImageHeight());

            // 设置随机颜色
            graphics.setColor(getRandomColor());
            // 设置画笔颜色
            if(captchaConfig.getInterfereColor() != null) {
                graphics.setColor(Color.GREEN);
            }
            // 绘制干扰点
            graphics.fillOval(x1, y1, diameter, diameter);
        }
    }

    /**
     * 绘制干扰线
     *
     * @param captchaConfig
     * @return void
     * @author Anna.
     * @date 2025/3/13 10:27
     */
    private void drawInterfereLine(GraphicCaptchaConfig captchaConfig) {
        Random ran = new Random();
        //随机生成坐标点
        for (int i = 0; i < captchaConfig.getInterfereLineIntensity(); i++) {
            int x1 = ran.nextInt(captchaConfig.getImageWidth());
            int x2 = ran.nextInt(captchaConfig.getImageWidth());
            int y1 = ran.nextInt(captchaConfig.getImageHeight());
            int y2 = ran.nextInt(captchaConfig.getImageHeight());

            // 设置随机颜色
            graphics.setColor(getRandomColor());
            // 设置画笔颜色
            if(captchaConfig.getInterfereColor() != null) {
                graphics.setColor(Color.GREEN);
            }
            // 绘制干扰线
            graphics.drawLine(x1, y1, x2, y2);
        }
    }

    @Override
    public Captcha buildCatcha() {
        // 释放资源
        graphics.dispose();
        return captcha;
    }
}
