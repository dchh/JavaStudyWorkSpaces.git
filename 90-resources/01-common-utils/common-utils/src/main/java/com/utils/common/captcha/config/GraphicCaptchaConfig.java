package com.utils.common.captcha.config;

import lombok.Data;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

/**
 * 图形验证码配置类
 *
 * @author Anna.
 * @date 2025/3/12 23:12
 */
@Data
public class GraphicCaptchaConfig extends CaptchaConfig {
    /**
     * 背景类型  图片
     */
    public static final int BACKGROUND_TYPE_IMAGE = 0;

    /**
     * 背景类型  颜色
     */
    public static final int BACKGROUND_TYPE_COLOR = 1;

    /** 随机数类型 数字 */
    public static final int RANDOM_TYPE_NUMBER = 0;

    /** 随机数类型 字母 */
    public static final int RANDOM_TYPE_LETTER = 1;

    /** 随机数类型 数字和字母 */
    public static final int RANDOM_TYPE_NUMBER_AND_LETTER = 2;

    /** 随机数类型 数字 文本 */
    public static final String RANDOM_TYPE_NUMBER_CONTENT  = "0123456789";

    /** 随机数类型 字母 文本*/
    public static final String RANDOM_TYPE_LETTER_CONTENT = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghigklmnopqrstuvwxyz";

    /** 随机数类型 数字和字母 文本 */
    public static final String RANDOM_TYPE_NUMBER_AND_LETTER_CONTENT = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghigklmnopqrstuvwxyz0123456789";

    /** 干扰类型 随机线 */
    public static final int INTERFERE_TYPE_LINE = 1;

    /** 干扰类型 随机点 */
    public static final int INTERFERE_TYPE_DROP = 2;

    /** 干扰类型 随机线 和 随机点 */
    public static final int INTERFERE_TYPE_DROP_AND_LINE = 3;

    /** 图片宽度 */
    private int imageWidth = 200;

    /** 图片高度 */
    private int imageHeight = 100;

    /** 图片类型 */
    private int imageType = BufferedImage.TYPE_INT_RGB;

    /**
     * 背景类型
     */
    private int backgroundType = BACKGROUND_TYPE_COLOR;

    /** 背景颜色 */
    private Color backgroundColor = Color.WHITE;

    /** 背景图片 */
    private Image backgroundImg = new ImageIcon(this.getClass().getResource("/background.jpg")).getImage();

    /** 背景图片 X 轴 偏移量 */
    private int backgroundImgXoffset = 0;

    /** 背景图片 Y 轴 偏移量 */
    private int backgroundImgYoffset = 0;

    /** 图像加载监控*/
    private ImageObserver backgroundImgObserver;

    /** 边框宽度 */
    private int borderWidth = 0;

    /** 边框颜色 */
    private Color borderColor = Color.LIGHT_GRAY;

    /** 字体颜色 */
    private Color fontColor;

    /** 字体 */
    private Font font = new Font("Times New Roman", Font.BOLD, 50);

    /** 文本 旋转强度 */
    private int contentRotateIntensity = 10;

    /** 随机数类型 */
    private int randomType = RANDOM_TYPE_NUMBER_AND_LETTER;

    /** 随机数个数 */
    private int randomNumber = 4;

    /** 干扰类型 */
    private int interfereType = INTERFERE_TYPE_DROP_AND_LINE;

    /** 干扰线强度 */
    private int interfereLineIntensity = 10;

    /** 干扰点强度 */
    private int interfereDropIntensity = 50;

    /** 干扰点随机直径 */
    private int interfereDropDiameter = 10;

    /** 干扰颜色 */
    private Color interfereColor;
}
