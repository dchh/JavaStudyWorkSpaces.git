package com.utils.common.captcha.graphic;

import com.utils.common.captcha.entity.Captcha;
import com.utils.common.captcha.config.GraphicCaptchaConfig;

/**
 * 图形验证码 抽象建造者定义接口
 *
 * @author Anna.
 * @date 2025/3/12 23:09
 */
public interface IGraphicCaptchaBuider {

    /**
     * 创建验证码图片对象
     *
     * @param GraphicCaptchaConfig
     * @return com.utils.common.captcha.graphic.GraphicCaptchaBuider
     * @author Anna.
     * @date 2025/3/12 23:14
     */
    IGraphicCaptchaBuider createBaseBufferedImage(GraphicCaptchaConfig GraphicCaptchaConfig);

    /**
     * 设置背景
     *
     * @param GraphicCaptchaConfig
     * @return com.utils.common.captcha.graphic.GraphicCaptchaBuider
     * @author Anna.
     * @date 2025/3/12 23:16
     */
    IGraphicCaptchaBuider setBackground(GraphicCaptchaConfig GraphicCaptchaConfig);

    /**
     * 设置边框
     *
     * @param GraphicCaptchaConfig
     * @return com.utils.common.captcha.graphic.GraphicCaptchaBuider
     * @author Anna.
     * @date 2025/3/12 23:16
     */
    IGraphicCaptchaBuider setBorder(GraphicCaptchaConfig GraphicCaptchaConfig);

    /**
     * 设置字体
     *
     * @param GraphicCaptchaConfig
     * @return com.utils.common.captcha.graphic.GraphicCaptchaBuider
     * @author Anna.
     * @date 2025/3/12 23:17
     */
    IGraphicCaptchaBuider setFont(GraphicCaptchaConfig GraphicCaptchaConfig);

    /**
     * 生成随机数
     *
     * @param GraphicCaptchaConfig
     * @return com.utils.common.captcha.graphic.GraphicCaptchaBuider
     * @author Anna.
     * @date 2025/3/12 23:20
     */
    IGraphicCaptchaBuider generateRandomNumbers(GraphicCaptchaConfig GraphicCaptchaConfig);

    /**
     * 绘制干扰
     *
     * @param GraphicCaptchaConfig
     * @return com.utils.common.captcha.graphic.GraphicCaptchaBuider
     * @author Anna.
     * @date 2025/3/12 23:21
     */
    IGraphicCaptchaBuider drawInterfere(GraphicCaptchaConfig GraphicCaptchaConfig);

    /**
     * 生成图形验证码
     *
     * @param
     * @return com.utils.common.captcha.entity.Captcha
     * @author Anna.
     * @date 2025/3/12 23:11
     */
    Captcha buildCatcha();
}
