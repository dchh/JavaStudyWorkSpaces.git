package com.utils.common.captcha;

import com.utils.common.captcha.config.GraphicCaptchaConfig;
import com.utils.common.captcha.entity.GraphicCaptcha;
import com.utils.common.captcha.graphic.GraphicCaptchaBuider;
import com.utils.common.captcha.graphic.IGraphicCaptchaBuider;

import javax.swing.*;

/**
 * 默认图形验证码
 *
 * @author Anna.
 * @date 2025/3/12 23:01
 */
public class DefaultGraphicCaptchaDirector {
    // 定义建造者属性
    private IGraphicCaptchaBuider graphicCaptchaBuider;

    public DefaultGraphicCaptchaDirector(IGraphicCaptchaBuider graphicCaptchaBuider) {
        this.graphicCaptchaBuider = graphicCaptchaBuider;
    }

    /**
     * 生成颜色填充背景验证码
     *      长宽：200 * 100
     *      文本：字母 + 数字
     *      验证码长度：4
     *      背景：颜色填充
     *
     * @param
     * @return com.utils.common.captcha.entity.GraphicCaptcha
     * @author Anna.
     * @date 2025/3/13 11:38
     */
    public GraphicCaptcha createBaseGraphicCaptcha() {
        GraphicCaptchaConfig config = new GraphicCaptchaConfig();
        return (GraphicCaptcha) this.graphicCaptchaBuider.createBaseBufferedImage(config)
                .setBackground(config)
                .setFont(config)
                .generateRandomNumbers(config)
                .drawInterfere(config)
                .buildCatcha();
    }

    /**
     * 生成颜色填充背景验证码 -- 自定义验证码长度
     *      长宽：200 * 100
     *      文本：字母 + 数字
     *      验证码长度：自定义
     *      背景：颜色填充
     *
     * @param randomNumber
     * @return com.utils.common.captcha.entity.GraphicCaptcha
     * @author Anna.
     * @date 2025/3/13 11:53
     */
    public GraphicCaptcha createBaseGraphicCaptcha(int randomNumber) {
        GraphicCaptchaConfig config = new GraphicCaptchaConfig();
        config.setRandomNumber(randomNumber);
        return (GraphicCaptcha) this.graphicCaptchaBuider.createBaseBufferedImage(config)
                .setBackground(config)
                .setFont(config)
                .generateRandomNumbers(config)
                .drawInterfere(config)
                .buildCatcha();
    }

    /**
     * 生成图片填充背景验证码
     *      长宽：200 * 100
     *      文本：字母 + 数字
     *      验证码长度：4
     *      背景：图片填充
     *
     * @param
     * @return com.utils.common.captcha.entity.createImageGraphicCaptcha
     * @author Anna.
     * @date 2025/3/13 11:40
     */
    public GraphicCaptcha createImageGraphicCaptcha() {
        GraphicCaptchaConfig config = new GraphicCaptchaConfig();
        config.setBackgroundType(GraphicCaptchaConfig.BACKGROUND_TYPE_IMAGE);
        return (GraphicCaptcha) this.graphicCaptchaBuider.createBaseBufferedImage(config)
                .setBackground(config)
                .setFont(config)
                .generateRandomNumbers(config)
                .drawInterfere(config)
                .buildCatcha();
    }


    /**
     * 生成图片填充背景验证码 -- 自定义背景图片
     *      长宽：200 * 100
     *      文本：字母 + 数字
     *      验证码长度：4
     *      背景：图片填充
     *
     * @param backgroundImagePath
     * @return com.utils.common.captcha.entity.GraphicCaptcha
     * @author Anna.
     * @date 2025/3/13 11:51
     */
    public GraphicCaptcha createImageGraphicCaptcha(String backgroundImagePath) {
        GraphicCaptchaConfig config = new GraphicCaptchaConfig();
        config.setBackgroundType(GraphicCaptchaConfig.BACKGROUND_TYPE_IMAGE);
        config.setBackgroundImg(new ImageIcon(this.getClass().getResource(backgroundImagePath)).getImage());
        return (GraphicCaptcha) this.graphicCaptchaBuider.createBaseBufferedImage(config)
                .setBackground(config)
                .setFont(config)
                .generateRandomNumbers(config)
                .drawInterfere(config)
                .buildCatcha();
    }
}
