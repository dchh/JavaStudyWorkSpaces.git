# 工具类

## 验证码

### 封装路径
[captcha](src%2Fmain%2Fjava%2Fcom%2Futils%2Fcommon%2Fcaptcha)

### 源码说明
支持扩展及二次开发

参数说明：
[GraphicCaptchaConfig.java](src%2Fmain%2Fjava%2Fcom%2Futils%2Fcommon%2Fcaptcha%2Fconfig%2FGraphicCaptchaConfig.java)

| 参数 | 说明                          | 可选值                   | 默认值                                                                        |
| --- |-----------------------------|-----------------------|----------------------------------------------------------------------------|
| imageWidth | 图像的宽度                       | 整数                    | 200                                                                        |
| imageHeight | 图像的高度                       | 整数                    | 100                                                                        |
| backgroundType | 背景类型                        | 1 纯色 0 图片             | 1                                                                          |
| backgroundImgXoffset | 背景图片在X轴上的偏移量                | 整数                    | 0                                                                          |
| backgroundImgYoffset | 背景图片在Y轴上的偏移量                | 整数                    | 0                                                                          |
| contentRotateIntensity | 内容旋转的强度                     | 整数                    | 10                                                                         |
| randomNumber | 随机生成的个数                     | 整数                    | 4                                                                          |
| randomType | 随机类型的标识符                    | 0 纯数字 1 纯字母 2 数字和字母   | 2                                                                          |
| interfereType | 干扰类型的标识符                    | 1 随机线 2 随机点 3 随机线和随机点 | 3                                                                          |
| interfereLineIntensity | 干扰线的强度                      | 整数                    | 10                                                                         |
| interfereDropIntensity | 干扰点的强度                      | 整数                    | 50                                                                         |
| interfereDropDiameter | 干扰点的随机最大直径                  | 整数                    | 10                                                                         |
| backgroundColor | 背景颜色（如果backgroundType为1时有效） | Color                 | 颜色随机                                                                       |
| backgroundImg | 背景图片（如果backgroundType为0时有效） | Image                 | `new ImageIcon(this.getClass().getResource("/background.jpg")).getImage()` |
| backgroundImgObserver | 背景图片的观察者（可能用于监听图片加载状态）      | ImgObserver           | null                                                                       |
| borderWidth | 边框的宽度                       | 整数                    | 0                                                                          |
| borderColor | 边框的颜色                       | Color                 |   `Color.LIGHT_GRAY`                                      |
| fontColor | 字体颜色                        | Color                 | 颜色随机                                                                  |
| font | 字体样式（可能包括字体名称、大小等）          | Font                  |             `new Font("Times New Roman", Font.BOLD, 50);`                                                    |
| interfereColor | 干扰元素的颜色                     | Color                 | 颜色随机                                                                     |

#### 图形验证码

配置类：
[GraphicCaptchaConfig.java](src%2Fmain%2Fjava%2Fcom%2Futils%2Fcommon%2Fcaptcha%2Fconfig%2FGraphicCaptchaConfig.java)

实现类：
[GraphicCaptchaBuider.java](src%2Fmain%2Fjava%2Fcom%2Futils%2Fcommon%2Fcaptcha%2Fgraphic%2FGraphicCaptchaBuider.java)

返回实体：
[GraphicCaptcha.java](src%2Fmain%2Fjava%2Fcom%2Futils%2Fcommon%2Fcaptcha%2Fentity%2FGraphicCaptcha.java)

##### 测试案例
[GraphicCaptchaController.java](src%2Fmain%2Fjava%2Fcom%2Futils%2Fcontroller%2FGraphicCaptchaController.java)
```java
package com.utils.controller;

import com.utils.common.captcha.DefaultGraphicCaptchaDirector;
import com.utils.common.captcha.config.GraphicCaptchaConfig;
import com.utils.common.captcha.entity.GraphicCaptcha;
import com.utils.common.captcha.graphic.GraphicCaptchaBuider;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import java.io.IOException;

@RestController
@RequestMapping("/graphic/captcha")
public class GraphicCaptchaController {

    @GetMapping("/base")
    public void getBaseGraphicCaptcha(HttpServletResponse response) throws IOException {
        // 输出图像
        response.setContentType("image/png");
        GraphicCaptcha captcha = (GraphicCaptcha) new DefaultGraphicCaptchaDirector(new GraphicCaptchaBuider()).createBaseGraphicCaptcha();;
        System.out.println("随机验证码 = " + captcha.getRandomness());
        ImageIO.write(captcha.getImage(), "PNG", response.getOutputStream());
    }

    @GetMapping("/image")
    public void getImageGraphicCaptcha(HttpServletResponse response) throws IOException {
        // 输出图像
        response.setContentType("image/png");
        GraphicCaptcha captcha = (GraphicCaptcha) new DefaultGraphicCaptchaDirector(new GraphicCaptchaBuider()).createImageGraphicCaptcha();;
        System.out.println("随机验证码 = " + captcha.getRandomness());
        ImageIO.write(captcha.getImage(), "PNG", response.getOutputStream());
    }

    @PostMapping("/test")
    public void getImageGraphicCaptcha(GraphicCaptchaConfig config,HttpServletResponse response) throws IOException {
        // 输出图像
        response.setContentType("image/png");
        GraphicCaptcha captcha = (GraphicCaptcha) new GraphicCaptchaBuider().createBaseBufferedImage(config)
                .setBackground(config)
                .setFont(config)
                .generateRandomNumbers(config)
                .drawInterfere(config)
                .buildCatcha();
        System.out.println("随机验证码 = " + captcha.getRandomness());
        ImageIO.write(captcha.getImage(), "PNG", response.getOutputStream());
    }

}
```


访问`http://127.0.0.1:8080/graphic/captcha/base`：

![img.png](images/img.png)

访问`http://127.0.0.1:8080/graphic/captcha/image`：

![img_1.png](images/img_1.png)

postman访问`http://127.0.0.1:8080/graphic/captcha/test`：

![img_2.png](images/img_2.png)

参数入下：
```
imageWidth:300
imageHeight:150
backgroundType:1
backgroundImgXoffset:0
backgroundImgYoffset:0
contentRotateIntensity:10
randomNumber:6
randomType:2
interfereType:3
interfereLineIntensity:10
interfereDropIntensity:100
interfereDropDiameter:10
```


## gitee源码
> [git clone https://gitee.com/anna_spaces/JavaStudyWorkSpaces.git](https://gitee.com/anna_spaces/JavaStudyWorkSpaces.git)