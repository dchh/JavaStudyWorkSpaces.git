# 工具类

> 自定义工具类

| 日期       | DEMO                                    | 描述  |
|:---------|:------------------------------------------|:----|
| 20250313 | 01-common-utils/[common-utils](01-common-utils%2Fcommon-utils)/captcha | 验证码 |


## gitee源码
> [git clone https://gitee.com/anna_spaces/JavaStudyWorkSpaces.git](https://gitee.com/anna_spaces/JavaStudyWorkSpaces.git)