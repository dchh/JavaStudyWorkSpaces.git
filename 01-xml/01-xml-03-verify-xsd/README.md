# XSD
1. XML Schema是DTD的替代者 
2. 不仅可以定义XML文档的结构，还可以规范文档的内容 
3. XSD本身也是XML文件 
4. XSD采用XML文档来定义语义约束，比DTD要复杂一些，但是功能强大的多 
   + 支持丰富的数据类型
   + 允许开发者自定义数据类型
   + 可读性强
   + 可针对未来需求进行扩展

## TODO 后续补充

## gitee源码
> git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git