# XML
> 被设计出来用于数据的记录和传递，经常被作用为配置文件

## 什么是XML
+ 可扩展标记语言（Extensible Markup Language）,没有固定的标签，所有的标签都可以自定义。
+ 使用简单的标记来描述数据
+ 通常，xml被用于信息的记录和传递，因此经常被作用为配置文件

## XML示例
```xml
<?xml version="1.0" encoding="UTF-8"?><!-- xml声明：定义XML版本和编码格式 -->
<book><!-- 根元素-->
    <!-- 图书信息 -->
    <book id="bk101"><!-- id为属性-->
        <author>张三</author><!-- 元素由开始标签、元素内容、结束标签组成-->
        <title>Java教程</title>
    </book>
</book>
```

## XML的定义
### 语法
1. 声明信息``<?xml version="1.0" encoding="UTF-8"?>``
2. 根元素（有且仅有一个根元素） 
3. 标签大小写敏感 
4. 标签要成对，而且要正确嵌套 
5. 属性值要使用双引号 
6. 注释的写法如下：``<!-- 注释内容 -->``
7. 尽量使用元素来描述数据。避免使用属性描述数据 ``Eg:<note day="08" month="08" year="2008"></note>``
### XML元素命名规则
1. 名称可以含字母、数字以及其他的字符 
2. 名称不能以数字或者标点符号开始 
3. 名称不能以字符 “xml”（或者 XML、Xml）开始 
4. 名称不能包含空格 
5. 名称应当比较简短，比如：<book_title>，而不是：<the_title_of_the_book>
6. 避免 "-" 字符。如果您按照这样的方式进行命名："first-name"，一些软件会认为你需要提取第一个单词
7. 避免 "." 字符。如果您按照这样的方式进行命名："first.name"，一些软件会认为 "name" 是对象 "first" 的属性。 
8. 避免 ":" 字符。冒号会被转换为命名空间来使用

### 有效的XML文档
+ 首先必须是格式正确的
+ 使用DTD和XSD（XML Schema）定义语义约束

## gitee源码
> git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git