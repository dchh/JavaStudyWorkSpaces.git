# DTD
>DTD（文档类型定义）的作用是定义 XML 文档的合法构建模块。 它使用一系列的合法元素来定义文档结构。

## XML 构建模块
所有的 XML 文档（以及 HTML 文档）均由以下简单的构建模块构成：
+ 元素
  > 元素是 XML 以及 HTML 文档的主要构建模块。 <br/>
  > HTML 元素的例子是 "body" 和 "table"。XML 元素的例子是 "note" 和 "message" 。<br/>
  > 元素可包含文本、其他元素或者是空的。空的 HTML 元素的例子是 "hr"、"br" 以及 "img"。
+ 属性
  > 属性可提供有关元素的额外信息。 属性总是被置于某元素的开始标签中。<br/>
  > 属性总是以名称/值的形式成对出现的。下面的 "img" 元素拥有关于源文件的额外信息：<br/>
  > ````<img src="computer.gif" />````<br/>
  > 元素的名称是 "img"。属性的名称是 "src"。属性的值是 "computer.gif"。由于元素本身为空，它被一个 " /" 关闭。
+ 实体
  >实体是用来定义普通文本的变量。实体引用是对实体的引用。 <br/>
  > 大多数同学都了解这个 HTML 实体引用："&nbsp;"。这个“无折行空格”实体在 HTML 中被用于在某个文档中插入一个额外的空格。 <br/>
  > 当文档被 XML 解析器解析时，实体就会被展开。<br/>
  > EG: ```&lt;```	<
+ PCDATA
  > PCDATA 的意思是被解析的字符数据（parsed character data）。 <br/>
  > 可把字符数据想象为 XML 元素的开始标签与结束标签之间的文本。 <br/>
  > PCDATA 是会被解析器解析的文本。这些文本将被解析器检查实体以及标记。 <br/>
  > 文本中的标签会被当作标记来处理，而实体会被展开。 <br/>
  > 不过，被解析的字符数据不应当包含任何 &、< 或者 > 字符；需要使用 ```&amp;、&lt; 以及 &gt;``` 实体来分别替换它们。
+ CDATA
  > CDATA 的意思是字符数据（character data）。 <br/>
  > CDATA 是不会被解析器解析的文本。在这些文本中的标签不会被当作标记来对待，其中的实体也不会被展开。

## 语法
### 声明元素
#### 语法
```
<!ELEMENT 元素名称 元素内容>
```
#### 示例
> DTD 实例:```<!ELEMENT br EMPTY>```<br/>
> XML 实例:```<br/>```

|说明|语法|示例|描述|
|:---|:---:|:------:|:---|
|空元素|```<!ELEMENT 元素名称 EMPTY>```|```<!ELEMENT br EMPTY>```|空元素通过类别关键词EMPTY进行声明|
|只有 PCDATA 的元素|```<!ELEMENT 元素名称 (#PCDATA)>```|```<!ELEMENT book (#PCDATA)>```|只有 PCDATA 的元素通过圆括号中的 #PCDATA 进行声明|
|带有任何内容的元素|```<!ELEMENT 元素名称 ANY>```|```<!ELEMENT book ANY>```|通过类别关键词 ANY 声明的元素，可包含任何可解析数据的组合|
|带有子元素（序列）的元素|```<!ELEMENT 元素名称 (元素[,元素,元素...])>```|```<!ELEMENT book (id,name,author)>```|带有一个或多个子元素的元素通过圆括号中的子元素名进行声明|

> 包含指定的子元素以及文本内容的元素时，必须将 #PCDATA 放在最前面，必须用*号结尾,必须用“|”分隔。 <br/>
> 语法：<br/>
> ```<!ELEMENT element-name (#PCDATA | child1 | child2 | ...)*>```<br/>
>局限性： <br/>
>  只可约束子表记的类型，而不可约束其出现的次数及顺序 <br/>
>  约束条件中不能出现限制符号 <br/>
>  例如:（#PCDATA|子标记+|子标记*）是错误的<br/>

### 声明属性
#### 语法
```
<!ATTLIST 元素名称 属性名称 属性类型 默认值>
```
#### 示例
> DTD 实例:```<!ATTLIST booktype type (java|vue) "java">```<br/>
> XML 实例:```<booktype type="java"/>```

#### 属性类型的选项
|类型|描述|
|:---|:---|
|CDATA|值为字符数据 (character data)|
|ID|值为唯一的 id|
|IDREF|值为另外一个元素的 id|
|IDREFS|值为其他 id 的列表|
|NMTOKEN|值为合法的 XML 名称|
|NMTOKENS|值为合法的 XML 名称的列表|
|ENTITY|值是一个实体|
|ENTITIES|值是一个实体列表|
|NOTATION|此值是符号的名称|
|xml:|值是一个预定义的 XML 值|

#### 默认值参数的选项
|值|解释|
|:---|:---|
|#REQUIRED|属性值是必需的|
|#IMPLIED|属性不是必需的|
|#FIXED value|属性值是固定的|
|值|属性的默认值|
	
## 数量词
用于描述元素出现次数

|符号|描述|
|:---|:---|
|+|至少出现一次|
|?|出现0次或1次|
|*|出现任意次数|

### 示例
> 声明最少出现一次的元素```<!ELEMENT books (book+)>``

## 引用方式
### 内部的DOCTYPE声明
#### 语法
```
<!DOCTYPE 根元素 [元素声明]>
```
#### 示例
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!-- 内部的DOCTYPE声明
【语法】
  <!DOCTYPE 根元素 [元素声明]>
【示例】
    book:根节点
    ELEMENT 定义元素
-->
<!DOCTYPE book[
        <!ELEMENT book (id,name,author)>
        <!ELEMENT id (#PCDATA)>
        <!ELEMENT name (#PCDATA)>
        <!ELEMENT author (#PCDATA)>
        ]>
<book>
    <id>2</id>
    <name>《这是书名》</name>
    <author>张三</author>
</book>
```
### 外部文档声明
#### 语法
```
<!DOCTYPE 根元素 [元素声明]>
```
#### 示例
+ resources/book.dtd
```dtd
<!ELEMENT book (id,name,author)>
<!ELEMENT id (#PCDATA)>
<!ELEMENT name (#PCDATA)>
<!ELEMENT author (#PCDATA)>
```

+ resources/demo.xml
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE book SYSTEM "book.dtd">
<book>
    <id>1</id>
    <name>《这是书名》</name>
    <author>张三</author>
</book>
```

## 简要运用
```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE books[
        <!ELEMENT books (book)+>
        <!ELEMENT book (id,name,author)>
        <!ELEMENT id (#PCDATA)>
        <!ELEMENT name (#PCDATA)>
        <!ELEMENT author (#PCDATA)>
        <!ATTLIST author type (man|woman) "man">
        <!ATTLIST author age CDATA #REQUIRED>
        ]>
<books>
    <!-- book至少出现一次 -->
    <book>
        <!-- id -->
        <id>1</id>
        <!-- 书名 -->
        <name>《JAVA从入门到放弃》</name>
        <!-- 作者 
            属性type：可选（man|woman）默认值 “man”
            属性age：必填属性
             -->
        <author type="man" age="23">张三</author>
    </book>
    <book>
        <id>2</id>
        <name>《这是一本书》</name>
        <author type="man" age="32">李四</author>
    </book>
</books>
```

## gitee源码
> git clone https://gitee.com/dchh/JavaStudyWorkSpaces.git