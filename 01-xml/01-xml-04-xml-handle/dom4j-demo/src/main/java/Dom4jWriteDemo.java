import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Dom4j写XML文件
 *
 * @author Anna.
 * @date 2024/3/31 19:40
 */
public class Dom4jWriteDemo {

    public static void main(String[] args) {
        String path = System.getProperty("user.dir") + File.separator + "01-xml-04-xml-handle/dom4j-demo/src/main/resources";
        new Dom4jWriteDemo().write(path + File.separator + "books2.xml");
    }

    public void write(String path) {
        // 通过documentHelper生成一个Documen对象
        Document document = DocumentHelper.createDocument();
        // 添加并得到根元素
        Element books = document.addElement("books");
        // 为根元素添加子元素
        Element book = books.addElement("book");
        // 为book添加子元素
        Element id = book.addElement("id");
        Element name = book.addElement("name");
        Element author = book.addElement("author");
        // 为子元素添加文本
        id.addText("1");
        name.addText("《Java自学基础》");
        author.addText("张三");
        author.addAttribute("type", "man");
        author.addAttribute("age", "12");
        // 将DOC输出到XML文件 简单输出
//        Writer writer = null;
//        try {
//            writer = new FileWriter(new File(path));
//            document.write(writer);
//            // 关闭资源
//            writer.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (writer != null) {
//                try {
//                    writer.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
        // 美化格式输出
        OutputFormat format = OutputFormat.createPrettyPrint();
        XMLWriter xmlWriter = null;
        try {
            xmlWriter = new XMLWriter(new FileWriter(new File(path)), format);
            xmlWriter.write(document);
            // 关闭资源
            xmlWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (xmlWriter != null) {
                try {
                    xmlWriter.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
