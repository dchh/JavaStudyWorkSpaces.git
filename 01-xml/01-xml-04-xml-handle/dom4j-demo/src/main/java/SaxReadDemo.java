import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.Iterator;

/**
 * SAX读XML文件
 *
 * @author Anna.
 * @date 2024/3/31 19:40
 */
public class SaxReadDemo {

    public static void main(String[] args) {
        new SaxReadDemo().read();
    }

    public void read() {
        // 获取资源路径
        String path = this.getClass().getResource("books.xml").getPath();
        // 获取文件判断文件是否存在
        File file = new File(path);
        if (!file.exists() || !file.isFile()) {
            throw new RuntimeException("获取资源失败");
        }

        // 1 创建SAXReader对象，用于读取XML文件
        SAXReader saxReader = new SAXReader();
        // 读取XML文件，得到document对象
        try {
            Document document = saxReader.read(new File(path));
            // 获取根元素
            Element rootElement = document.getRootElement();
            System.out.println("根元素名称：" + rootElement.getName());
            // 获取根元素下所有子元素
            Iterator<?> iterator = rootElement.elementIterator();
            while (iterator.hasNext()) {
                // 取出元素
                Element element = (Element) iterator.next();
                System.out.println("子元素名称：" + element.getName());
                // 获取子元素
                Element id = element.element("id");
                Element name = element.element("name");
                Element author = element.element("author");

                System.out.printf("子元素的子元素值-id：%s -name: %s --author:%s[type=%s,age=%s]%n",
                        id.getStringValue(), name.getText(),
                        author.getText(),
                        // 获取author属性type
                        author.attribute("type").getValue(),
                        // 获取author属性age
                        author.attribute("age").getValue());
            }
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }
}
