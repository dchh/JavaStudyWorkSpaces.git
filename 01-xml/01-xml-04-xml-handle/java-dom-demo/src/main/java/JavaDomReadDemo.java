import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * JDK内置DOM读XML
 *
 * @author Anna.
 * @date 2024/3/31 17:42
 */
public class JavaDomReadDemo {

    public static void main(String[] args) throws Exception {
        new JavaDomReadDemo().read();
    }

    public void read() throws ParserConfigurationException, IOException, SAXException {
        // 获取资源路径
        String path = this.getClass().getResource("books.xml").getPath();
        // 获取文件判断文件是否存在
        File file = new File(path);
        if (!file.exists() || !file.isFile()) {
            throw new RuntimeException("获取资源失败");
        }

        // 从DocumentBuilderFactory获得DocumentBuilder。 DocumentBuilder包含用于从 XML 文档中获取 DOM 文档实例的 API。
        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        // parse()方法将 XML 文件解析为Document
        Document doc = documentBuilder.parse(file);

        System.out.printf("根节点: %s", doc.getDocumentElement().getNodeName());

        // 循环打印
        NodeList nList = doc.getElementsByTagName("book");

        for (int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);
            System.out.println("");
            System.out.printf("元素: %s", nNode.getNodeName());
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element elem = (Element) nNode;
                String id = elem.getElementsByTagName("id").item(0).getTextContent();
                String name = elem.getElementsByTagName("name").item(0).getTextContent();
                Element authorNode = (Element) elem.getElementsByTagName("author").item(0);
                String author = elem.getElementsByTagName("author").item(0).getTextContent();
                String type = authorNode.getAttribute("type");
                String age = authorNode.getAttribute("age");
                System.out.println("");
                System.out.printf("id: %s - name:%s - author:%s[type=%s,age=%s]", id, name, author, type, age);
            }
        }
    }

}
