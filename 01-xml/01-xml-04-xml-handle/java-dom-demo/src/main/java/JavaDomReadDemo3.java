import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.NodeIterator;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * JDK内置DOM读XML
 * 使用NodeIterator读取文本自定义NodeFilter
 * @author Anna.
 * @date 2024/3/31 17:42
 */
public class JavaDomReadDemo3 {

    public static void main(String[] args) throws Exception {
        new JavaDomReadDemo3().read();
    }

    public void read() throws ParserConfigurationException, IOException, SAXException {
        // 获取资源路径
        String path = this.getClass().getResource("books.xml").getPath();
        // 获取文件判断文件是否存在
        File file = new File(path);
        if (!file.exists() || !file.isFile()) {
            throw new RuntimeException("获取资源失败");
        }

        // 从DocumentBuilderFactory获得DocumentBuilder。 DocumentBuilder包含用于从 XML 文档中获取 DOM 文档实例的 API。
        DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        // parse()方法将 XML 文件解析为Document
        Document doc = documentBuilder.parse(file);

        DocumentTraversal trav = (DocumentTraversal) doc;

        NodeIterator it = trav.createNodeIterator(doc.getDocumentElement(), NodeFilter.SHOW_ELEMENT, new MyFilter(), true);

        for (Node node = it.nextNode(); node != null;
            node = it.nextNode()) {
            // 判断是否有属性
            if(node.hasAttributes()){
                String attrStr = getAttrStr(node.getAttributes());
                System.out.printf("元素名称：%s-元素值：%s-属性：%s %n", node.getNodeName(),node.getTextContent(),"".equalsIgnoreCase(attrStr) ? "null" : attrStr, node.getNodeType());
            }
            else {
                System.out.printf("元素名称：%s-元素值：%s%n", node.getNodeName(),node.getTextContent());
            }
        }
    }

    private String getAttrStr(NamedNodeMap attributes){
        StringBuffer sb = new StringBuffer();
        if(attributes != null && attributes.getLength() > 0){
            sb.append("[");
            for(int i = 0; i < attributes.getLength(); i++){
                Node item = attributes.item(i);
                sb.append(item.getNodeName()).append("=").append(item.getNodeValue()).append(" ");
            }
            sb.append("]");
        }
        return sb.toString();
    }

    /**
     * 自定义过滤器
     *  实现NodeFilter接口
     *  通过返回NodeFilter.FILTER_ACCEPT和NodeFilter.FILTER_REJECT来控制要使用的节点
     * @author Anna.
     * @date 2024/3/31 18:50
     */
    static class MyFilter implements NodeFilter {
        @Override
        public short acceptNode(Node thisNode) {
            if (thisNode.getNodeType() == Node.ELEMENT_NODE) {
                Element e = (Element) thisNode;
                String nodeName = e.getNodeName();
                if ("author".equals(nodeName)) {
                    return NodeFilter.FILTER_ACCEPT;
                }
            }
            return NodeFilter.FILTER_REJECT;
        }
    }
}
