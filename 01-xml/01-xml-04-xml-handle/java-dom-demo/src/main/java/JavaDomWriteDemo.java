import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * JDK内置DOM写XML
 *
 * @author Anna.
 * @date 2024/3/31 17:42
 */
public class JavaDomWriteDemo {

    public static void main(String[] args) throws Exception {
        new JavaDomWriteDemo().write();
    }

    public void write() throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();

        // 创建根节点
        Element root = doc.createElementNS("", "books");
        doc.appendChild(root);
        // 设置子元素
        Map<String, String> book1 = new HashMap<String, String>();
        book1.put("type", "man");
        book1.put("age", "34");
        root.appendChild(createBook(doc, "1", "《JAVA从入门到放弃》", "张三", book1));
        Map<String, String> book2 = new HashMap<String, String>();
        book2.put("type", "man");
        book2.put("age", "34");
        root.appendChild(createBook(doc, "2", "《这是一本书》", "李四", book2));

        // Java DOM 使用Transformer生成 XML 文件。 之所以称为转换器，是因为它也可以使用 XSLT 语言转换文档。
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transf = transformerFactory.newTransformer();

        // 设置文档的编码和缩进
        transf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transf.setOutputProperty(OutputKeys.INDENT, "yes");
        transf.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        // DOMSource保存 DOM 树
        DOMSource source = new DOMSource(doc);
        // 获取资源路径
        String path = System.getProperty("user.dir") + File.separator + "01-xml-04-xml-handle/java-dom-demo/src/main/resources";
        File myFile = new File(path + File.separator + "books2.xml");

        StreamResult console = new StreamResult(System.out);
        StreamResult file = new StreamResult(myFile);

        // 写入控制台和文件
        transf.transform(source, console);
        transf.transform(source, file);
    }

    /**
     * 创建Book
     * e
     *
     * @param doc
     * @param id
     * @param name
     * @param author
     * @return org.w3c.dom.Node
     * @author Anna.
     * @date 2024/3/31 19:03
     */
    private static Node createBook(Document doc, String id, String name, String author, Map<String, String> attrsMap) {
        Element book = doc.createElement("book");
        book.setAttribute("id", id);
        book.appendChild(createUserElement(doc, "id", id, null));
        book.appendChild(createUserElement(doc, "name", name, null));
        book.appendChild(createUserElement(doc, "author", author, attrsMap));
        return book;
    }

    /**
     * 创建子节点
     *
     * @param doc
     * @param name
     * @param value
     * @param attrsMap
     * @return org.w3c.dom.Node
     * @author Anna.
     * @date 2024/3/31 19:02
     */
    private static Node createUserElement(Document doc, String name, String value, Map<String, String> attrsMap) {
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));

        if (attrsMap != null && attrsMap.size() > 0) {
            for (Map.Entry entry : attrsMap.entrySet()) {
                node.setAttribute(entry.getKey().toString(), entry.getValue().toString());
            }
        }

        return node;
    }
}
